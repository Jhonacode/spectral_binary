#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import 	matplotlib   		as 	mpl 
#from   	Parameters 		import 	*
#from 	SPF 			import 	*
from   	plotparameters 		import 	*


M	=	np.loadtxt('./S05h07/mods1.dat',unpack=True) 
#N11     =	M.shape[1]
#N12     =	M.shape[0]

omega1_1=	M[0,:]
x1_1	=	M[1,:]
y1_1	=	M[2,:]

phase1_1	= omega1_1/x1_1

M	=	np.loadtxt('./S05h07/mods2.dat',unpack=True) 
omega1_2=	M[0,:]
x1_2	=	M[1,:]
y1_2	=	M[2,:]

phase1_2	= omega1_2/x1_2
################################################

M	=	np.loadtxt('./S075h07/mods1.dat',unpack=True) 
omega2_1=	M[0,:]
x2_1	=	M[1,:]
y2_1	=	M[2,:]

phase2_1	= omega2_1/x2_1

M	=	np.loadtxt('./S075h07/mods2.dat',unpack=True) 

omega2_2=	M[0,:]
x2_2	=	M[1,:]
y2_2	=	M[2,:]
phase2_2	= omega2_2/x2_2
################################################

M	=	np.loadtxt('../isotermico/h07/mods1.dat',unpack=True) 
omega3_1=	M[0,:]
x3_1	=	M[1,:]
y3_1	=	M[2,:]

phase3_1	= omega3_1/x3_1

M	=	np.loadtxt('../isotermico/h07/mods2.dat',unpack=True) 
omega3_2=	M[0,:]
x3_2	=	M[1,:]
y3_2	=	M[2,:]
phase3_2	= omega3_2/x3_2

######################################################
M	=	np.loadtxt('./S025h07/mods1.dat',unpack=True) 
#N11     =	M.shape[1]
#N12     =	M.shape[0]

omega4_1=	M[0,:]
x4_1    =	M[1,:]
y4_1	=	M[2,:]
phase4_1	= omega4_1/x4_1

M	=	np.loadtxt('./S025h07/mods2.dat',unpack=True) 
omega4_2=	M[0,:]
x4_2	=	M[1,:]
y4_2	=	M[2,:]
phase4_2	= omega4_2/x4_2



#Direct input 
#plt.rcParams['text.latex.preamble']=[r"\usepackage{elsarticle}"]
#Options
mpl.rcParams.update(params)

fig = plt.figure(1)


#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 

ax = plt.axes()

plt.xlabel(r'-$\mathrm{\alpha_r}$')
plt.ylabel(r'-$\mathrm{\alpha_i}$')

##############################         ########
plt.plot(x4_1,y4_1,color='slateblue',dashes=[1, 0],label ='S=0.25')
plt.plot(x4_2,y4_2,color='slateblue',dashes=[1, 0])
##############################         ########
plt.plot(x1_1,y1_1,color='g',dashes=[1, 1],label = 'S=0.5')
plt.plot(x1_2,y1_2,color='g',dashes=[1, 1])
##############################         ########
plt.plot(x2_1,y2_1,color='m',dashes=[3, 1],label ='S=0.75')
plt.plot(x2_2,y2_2,color='m',dashes=[3, 1])
##############################         ########
plt.plot(x3_1,y3_1,color='k',dashes=[2, 1],label ='S=1.0')
plt.plot(x3_2,y3_2,color='k',dashes=[2, 1])
############
plt.savefig('alphari.pdf', format='pdf', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 5, 0.01, 0.70])

plt.legend()

plt.figure(2)
plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{\alpha_i}$')

################################
##############################         ########
plt.plot(omega4_1,y4_1,color='slateblue',dashes=[1, 0],label ='S=0.25')
plt.plot(omega4_2,y4_2,color='slateblue',dashes=[1, 0])
#########omega####################         ########
plt.plot(omega1_1,y1_1,color='g',dashes=[1, 1],label = 'S=0.5')
plt.plot(omega1_2,y1_2,color='g',dashes=[1, 1])
#########omega####################         ########
plt.plot(omega2_1,y2_1,color='m',dashes=[3, 1],label ='S=0.75')
plt.plot(omega2_2,y2_2,color='m',dashes=[3, 1])
#########omega####################         ########
plt.plot(omega3_1,y3_1,color='k',dashes=[2, 1],label ='S=1.0')
plt.plot(omega3_2,y3_2,color='k',dashes=[2, 1])
############
plt.savefig('walphai.pdf', format='pdf', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 3, 0.01, 0.70])

plt.legend()

plt.figure(3)
plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'$\mathrm{C_p}$')

################################
plt.plot(omega4_1,phase4_1,color='slateblue',dashes=[1, 0],label ='S=0.25')
plt.plot(omega4_2,phase4_2,color='slateblue',dashes=[1, 0])
#########omega####phase###############         ########
plt.plot(omega1_1,phase1_1,color='g',dashes=[1, 1],label = 'S=0.5')
plt.plot(omega1_2,phase1_2,color='g',dashes=[1, 1])
#########omega####phase###############         ########
plt.plot(omega2_1,phase2_1,color='m',dashes=[3, 1],label ='S=0.75')
plt.plot(omega2_2,phase2_2,color='m',dashes=[3, 1])
#########omega####phase###############         ########
plt.plot(omega3_1,phase3_1,color='k',dashes=[2, 1],label ='S=1.0')
plt.plot(omega3_2,phase3_2,color='k',dashes=[2, 1])
############
plt.savefig('fase.pdf', format='pdf', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 3, 0.19, 0.70])

plt.legend()


#plt.plot(X[10],Y[10],'ko')
#plt.plot(X[11],Y[11],'ko')
#plt.plot(X,Y,'k.')
#SPF()
plt.show()


