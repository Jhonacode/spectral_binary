#! /usr/bin/python 
# Base flow to spectral methods 
# create by: jhonatan
# date: 11-05-2018
# Jets Parameters
# Paper: D Perrault-Joncas and S.A Maslowe,
#       "Linear Stability of compressible coaxial jet 
#        with continuos velocity and temperature profile "
#module for symbolic albegra 
import numpy as np
import math  as mt
#from math import exp, expm1
import matplotlib.pyplot as plt
#from sci to make fft
import scipy.fftpack
################################
# letura de aquivos para obter as transformadas
# number of frequency files 

def Baseflow(r):

    #number of spectral points
    N1      =   np.shape(r)

    N       =   N1[0]

    dr      =   r[1]-r[0]
    
    Wb      =   np.zeros(N)
    #mini    =   np.zeros(N-1)
    Tb      =   np.zeros(N)

    Rhob    =   np.zeros(N)

    u1      =   np.zeros(N)

    u2      =   np.zeros(N)

    u3      =   np.zeros(N)
    
    
    #Jets Parameters
    #h      = U_secondary/U_primary
    h       =   0.7 

    #M0=M1, sound velocity adimensionalization, Joncas is with the U1
    M0      =   0.6558#0.8
    #M2      =   h*M1#0.4 

    #T0=T_ref, sound velocity adimensionalization, Joncas is with the U1
    T_ref   =   300#0.8
    
    #Gamma  = R2/R1
    Gamma   =   2.0 
    
    #Radii Primary jet
    R1      =   1.0
    #Diameter Primary 
    D1      =   2.0*R1 
    #Momemtum thickness Primary 
    theta1  =   3.0/100.0*(D1+2.0/3.0*D1) 

    #Radii Secondary jet
    R2      =   R1*Gamma
    #Diameter Primary 
    D2      =   2.0*R2
    ##Momemtum thickness Secondary
    theta2  =   3.0/100.0*(D1+2.0/3.0*D2) 
    theta2  =   0.14 
    
    #print(R1,theta1,R2,theta2)
    

    #Parameters bases flow

    b1      =   R1/(4.0*theta1)
    b2      =   R2/(4.0*theta2)
    
    
    for j in range (0,N):
    
        #Primary Stream
        u1[j]   =   0.5*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1))) 
        #Secondary Stream
        u2[j]   =   0.5*(1.0+np.tanh(b2*(R2/r[j]-r[j]/R2))) 
    
    #du1dr   =   np.gradient(u1, dr)
    ####return the  the min value
    #v1      =   np.amin(du1dr)
    ###return the indeces of the min value
    #pt1     =   np.argmin(du1dr)
    #
    #print(r[pt1],u1[pt1])
    #
    #du2dr   =   np.gradient(u2, dr)
    #v2      =   np.amin(du2dr)
    ###return the indeces of the min value
    #pt2     =   np.argmin(du2dr)
    #print(r[pt2],u2[pt2])
    
    
    ###return the  the min value
    #v=np.amin(dWbdr[150:300])
    ###return the indeces of the min value
    #pt=np.argmin(dWbdr)
    
    #M1= 0.4
    #M2= 0.8
    
    for j in range (0,N):

    	#Wb[j]    = 0.50*(M1+M2+(M1-M2)*np.tanh(2.0*(r[j]-3.0)/0.4))
        Wb[j]    = ((1.0-h)*u1[j]+h*u2[j])*1.0/M0
        Tb[j]    =  1.0#T1b*(Wb[j]-u2b)/(u1b-u2b)+T2b*(u1b-Wb[j])/(u1b-u2b)+(gamma-1.0)/2.0*(u1b-Wb[j])*(Wb[j]-u2b)
        Rhob[j]  = 1.0/(Tb[j]) 
    

    dWbdr   =   np.gradient(Wb, dr)
    
    
    return Wb,Tb,Rhob
