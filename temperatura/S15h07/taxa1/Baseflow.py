#! /usr/bin/python 
# Base flow to spectral methods 
# create by: jhonatan
# date: 11-05-2018
# Jets Parameters
# Paper: D Perrault-Joncas and S.A Maslowe,
#       "Linear Stability of compressible coaxial jet 
#        with continuos velocity and temperature profile "
#module for symbolic albegra 
import numpy as np
import math  as mt
#from math import exp, expm1
import matplotlib.pyplot as plt
#from sci to make fft
import scipy.fftpack
from  Parameters import * 
################################
# letura de aquivos para obter as transformadas
# number of frequency files 

def Baseflow(r,NN):

    #number of spectral points
    #N1      =   np.shape(r)

    #N       =   N1[0]
    #print(N)

    dr      =   r[1]-r[0]
    
    Wb      =   np.zeros(NN+1)

    Tb      =   np.zeros(NN+1)

    Rhob    =   np.zeros(NN+1)

    u1      =   np.zeros(NN+1)

    u2      =   np.zeros(NN+1)

    u3      =   np.zeros(NN+1)

    
    #Diameter Primary 
    D1      =   2.0*R1 
    #Momemtum thickness Primary 
    theta1  =   3.0/100.0*(D1+2.0/3.0*D1) 

    #Radii Secondary jet
    R2      =   R1*Gamma
    #Diameter Primary 
    D2      =   2.0*R2
    ##Momemtum thickness Secondary

    theta2  =   3.0/100.0*(D1+2.0/3.0*D2) 
    #theta2  =   0.14 
    
    #print(R1,theta1,R2,theta2)
    

    #Parameters bases flow

    b1      =   R1/(4.0*theta1)
    b2      =   R2/(4.0*theta2)
    
    g	    =   open('./%s/baseflow.dat'%file1,'w+')
    
    for j in range (0,NN+1):
    
        #Primary Stream
        u1[j]   =   0.5*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1))) 
        #Secondary Stream
        u2[j]   =   0.5*(1.0+np.tanh(b2*(R2/r[j]-r[j]/R2))) 

    	#Wb[j]    = 0.50*(M1+M2+(M1-M2)*np.tanh(2.0*(r[j]-3.0)/0.4))
        Wb[j]    = ((1.0-h)*u1[j]+h*u2[j])*M0
        Tb[j]    =  0.5*(1-S_T)*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1)))+S_T
        Rhob[j]  = 1.0/(Tb[j]) 

	g.write("%f\t%f\t%f\t%f\n"%(r[j],Wb[j],Tb[j],Rhob[j])); 
    
    #dWbdr   =   np.gradient(Wb, dr)
    
    g.close()
    
    return Wb,Tb,Rhob

def Baseflow_geometric(r,Gamma,NN):

    #number of spectral points
    #N1      =   np.shape(r)

    #N       =   N1[0]
    #print(N)

    dr      =   r[1]-r[0]
    
    Wb      =   np.zeros(NN+1)

    Tb      =   np.zeros(NN+1)

    Rhob    =   np.zeros(NN+1)

    u1      =   np.zeros(NN+1)

    u2      =   np.zeros(NN+1)

    u3      =   np.zeros(NN+1)

    
    #Diameter Primary 
    D1      =   2.0*R1 
    #Momemtum thickness Primary 
    theta1  =   3.0/100.0*(D1+2.0/3.0*D1) 

    #Radii Secondary jet
    R2      =   R1*Gamma
    #Diameter Primary 
    D2      =   2.0*R2
    ##Momemtum thickness Secondary

    theta2  =   3.0/100.0*(D1+2.0/3.0*D2) 
    #theta2  =   0.14 
    
    #print(R1,theta1,R2,theta2)
    

    #Parameters bases flow

    b1      =   R1/(4.0*theta1)
    b2      =   R2/(4.0*theta2)
    
    #g	    =   open('./%s/baseflow.dat'%file1,'w+')
    
    for j in range (0,NN+1):
    
        #Primary Stream
        u1[j]   =   0.5*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1))) 
        #Secondary Stream
        u2[j]   =   0.5*(1.0+np.tanh(b2*(R2/r[j]-r[j]/R2))) 

        Wb[j]    = ((1.0-h)*u1[j]+h*u2[j])*M0
        Tb[j]    =  0.5*(1-S_T)*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1)))+S_T
        Rhob[j]  = 1.0/(Tb[j]) 

	#g.write("%f\t%f\t%f\t%f\n"%(r[j],Wb[j],Tb[j],Rhob[j])); 
    
    #dWbdr   =   np.gradient(Wb, dr)
    #
    ##for j in range (0,NN+1):

    ##    g.write("%f\t%f\t%f\t%f\t%f\n"%(r[j],Wb[j],dWbdr[j],Tb[j],Rhob[j])); 

    #g.close()
    
    return Wb,Tb,Rhob
