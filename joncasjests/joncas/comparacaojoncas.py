#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


################################################

#M	=	np.loadtxt('./joncashot/mods1.dat',unpack=True) 
#omega4_1=	M[0,:]
#x4_1	=	M[1,:]
#y4_1	=	M[2,:]
#phase4_1=	(omega4_1)/(x4_1)

#M	=	np.loadtxt('./joncashot/mods2.dat',unpack=True) 
#omega4_2=	M[0,:]
#x4_2	=	M[1,:]
#y4_2	=	M[2,:]
#phase4_2=	(omega4_2)/(x4_2)

M	=	np.loadtxt('./joncas/cold.dat',unpack=True) 
#omega5_1=	M[0,:]
x1_1	=	M[0,:]
y1_1	=	M[1,:]
#phase5_1=	(omega5_1)/(x5_1)
################################################

M	=	np.loadtxt('./joncas/joncashot.dat',unpack=True) 
#omega5_1=	M[0,:]
x2_1	=	M[0,:]
y2_1	=	M[1,:]
#phase5_1=	(omega5_1)/(x5_1)

#M	=	np.loadtxt('./joncas/m.dat',unpack=True) 
##omega5_2=	M[0,:]
#x5_2	=	M[0,:]
#y5_2	=	M[1,:]
##phase5_2=	(omega5_2)/(x5_2)

################################################
mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()

ax.legend()
ax.legend(frameon=False)

#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 


plt.xlabel(r' $\mathrm{\alpha_r}R$')
plt.ylabel(r'-$\mathrm{\alpha_i}R$')
#plt.xlabel(r'$\alpha_r$')
#plt.ylabel(r'$-\alpha_i$')

#########x####################         ########
plt.plot(x1_1,y1_1,'g',dashes=[1, 1],label = '$Hot$' )
#plt.plot(x4_2,y4_2,'g',dashes=[1, 1])
#########x####################         ########

plt.plot(x2_1,y2_1,'m',dashes=[3, 1],label = 'Cold' )
#plt.plot(x5_2,y5_2,'m',dashes=[3, 1])
#########x####################         ########

ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 5.0, 0.01, 0.70])

plt.legend()
ax.legend(frameon=False)
plt.savefig('alphari.pdf', format='pdf', dpi=1000)


##SPF()
plt.show()


