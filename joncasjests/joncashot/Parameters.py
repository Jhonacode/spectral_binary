#Defining the parameters using 
#in the execution of the progra

import numpy as np 
#def dadosin():

file1       =   'taxa' 
file2       =   'baseflow' 
	###1
N	    =   400

omegai      =   0.05
omegaf      =   1.50
domega	    = 	0.025
iomega	    =	np.int(round((omegaf-omegai)/domega))
print(iomega)
#iomega      =   128
#maximum size r
maxr    =   50
#Jets Parameters
#h      = U_secondary/U_primary
h       =   0.70
#M0=M1, sound velocity adimensionalization, Joncas is with the U1
M0      =   0.6558#0.8
#M2      =   h*M1#0.4 
#h      = U_infty/U_primary,r=0
S_T     =   1.00
#T0=T_ref, sound velocity adimensionalization, Joncas is with the U1
T_ref   =   300#0.8
T_r     =   1.0
T_0     =   S_T*T_r 
#Gamma  = R2/R1
Gamma   =   2.0 
#Radii Primary jet
R1      =   1.0
###
#theta2  =   0.14 
###
#numero de modos a encontrar if  >nm breakk 
nm	=   0
# Tolerancia para procura dos autovalores
tol	=   0.001
#Max_imaginary part 
min_imag=   0.0005
max_imag=   0.8
min_real=   0.0005
max_real=   5.2
#
#tol	=   0.005
