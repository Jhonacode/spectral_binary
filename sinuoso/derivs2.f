      subroutine derivs2(etaj,zy,zd)

      implicit none
      include 'par.f'

      integer jmax
      complex*16 alfa, zy(2), zd(2), g

      real*8 omega, beta, Ma, eta(jm), etaj
      real*8 u, up, T, Tp, rho, s1, s1p, R, Rp
      real*8 U1, U2, T1, T2, Rratio

      common /data/ alfa, beta, omega, Ma, eta, jmax
      common /base/ U1, U2, T1, T2, Rratio


c.................................. base flow ..........................
      call baseflow(etaj,u,up,T,Tp,rho,s1,s1p,R,Rp)


c.......................................................................
      g = (alfa*alfa + dcmplx(beta*beta)) / (rho*alfa*alfa) -
     ^    Ma*Ma * (alfa*u - omega)*(alfa*u - omega) / alfa/alfa

      zd(1) = - gamma * Ma*Ma*rho*im*(alfa*u-omega)*zy(2)

      zd(2) = im*alfa*alfa*zy(1)*g/(gamma*Ma*Ma*(alfa*u-omega)) +
     ^        alfa*zy(2)*up/(alfa*u-omega)

      return
      end
