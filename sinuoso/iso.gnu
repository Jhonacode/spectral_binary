reset
set zero 1e-12
set contour base
unset key
set isosamples 100
set hidden3d
set view map 
unset surface
set cntrparam bspline
set cntrparam levels discrete 0
set style data lines
set ylabel '{/Symbol a}_i'
set xlabel '{/Symbol a}_r'
splot 'fort.10' u 1:2:3 w l lw 2 lt 1, 'fort.10' u 1:2:4 w l lw 2 lt 3
