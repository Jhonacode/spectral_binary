      program ray

      implicit none
      integer imax, jmax, i1, i2, i3, j
      include 'par.f'

      complex*16 alfa, alfa0, zeig(jm), det

      real*8 beta, beta0, Ma, Ma0, etamax, eta(jm)
      real*8 etaj, u, up, T, Tp, rho, s1, s1p, R, Rp
      real*8 U1, U2, T1, T2, Rratio
      real*8 omega, omega0, omegaf
      real*8 alfar, alfai

      common /data/ alfa, beta, omega, Ma, eta, jmax
      common /base/ U1, U2, T1, T2, Rratio

      namelist /raydata/ alfa0, beta0, omega0, omegaf, etamax, 
     ^                   jmax, imax, Ma0, U1, U2, T1, T2, Rratio

c.......................................................................
c     usar jmax impar
c.......................................................................

c...................................read in eigenvalues and initial guess
      open(unit=1,file='ray.in',status='old')
         read(1,nml=raydata)
      close (unit=1)

      Ma    = Ma0
      alfa  = alfa0
      beta  = beta0
      omega = omega0

c     Rratio = 1.d0
c     Rratio = 0.06d0
c     Rratio = RO2/RH2
c     Rratio = RH2/RO2

      call grid(eta,etamax,jmax)


c.... salva o baseflow para plotagem 
c     do j=1,jmax
c     etaj = eta(j)
c     call baseflow(etaj,u,up,T,Tp,rho,s1,s1p,R,Rp)
c     write(1,*)etaj,u,up,rho,s1,s1p,R,Rp
c     end do
c     stop

      i3 = 0
c     do i3=1,8
c        Ma = Ma + 0.5d0


      do i1=1,imax

         alfar = 1.-(1.-0.001)/dfloat(imax-1)*dfloat(i1-1)

         do i2=1,imax

         alfai = .1-(.1-.001)/dfloat(imax-1)*dfloat(i2-1)
         alfa  = dcmplx(alfar,alfai)
         
c        omega = omega0 + (omegaf-omega0)*dfloat(i2-1)/dfloat(imax-1)
c        Ma    = Ma0 + (0.01d0-Ma0)*dfloat(i2-1)/dfloat(imax-1)
c        Rratio= 1.d0 + (16.d0-1.d0)*dfloat(i2-1)/dfloat(imax-1)

         call interp(det,zeig)

         write(10+i3,*)real(real(alfa)),real(imag(alfa)),
     ^                 real(det),real(imag(det)),
     ^                 real(cdabs(det))

c        write(*,*)real(omega), real(imag(alfa))
c        write(9,*)real(Ma), real(omega), 
c    ^             real(imag(alfa)), real(real(alfa))

         end do
         write(10+i3,*)
         write(*,*)real(omega), real(imag(alfa)), i1
      end do

c     end do

c.... recover the eigenvectors hat{p} and hat{v} .......................
c     call eigen(zeig)

      stop
      end


c***********************************************************************
      subroutine grid(eta,etamax,jmax)

      implicit none
      include 'par.f'
      integer  jmax, j

      real*8 eta(jm), deta, etamax

c.......................................................................
c
c     malha uniformemente espassada na direcao eta
c     jmax = numero de pontos de eta = - etamax até eta = etamax
c
c     -etamax <= dominio <= etamax
c
c     eta      = coordenada transversal
c
c.......................................................................
c
c
c  j=jmax       ____7__________________________ etamax   = + 10
c                     |                |
c                     |                |
c                   6 |                |
c                     |               /
c                   5 |              /
c                     |             /
c  j=(jmax+1)/2 ____4_|____________/__________  etamax/2  = 0
c                     |           /
c                     |          /
c                   3 |         /
c                     |        /
c                   2 |       |
c                     |       |
c  j=1          ____1_|_______|________________ eta0      = - 10
c
c
c.......................................................................

      deta = 2.d0*etamax/dble(jmax-1)

      do j=1,jmax
        eta(j) = - etamax + dble(j-1)*deta
      end do

      return
      end

