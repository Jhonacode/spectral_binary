      subroutine baseflow(etaj,u,up,T,Tp,rho,s1,s1p,R,Rp)

      implicit none
      include 'par.f'

      integer jmax
      complex*16 alfa

      real*8 omega, beta, Ma, eta(jm), etaj
      real*8 u, up, T, Tp, rho, s1, s1p, R, Rp
      real*8 U1, U2, T1, T2, Rratio, h, coflow
      real*8 ub1, ub2, theta1, theta2, R1, R2, b1, b2, ubp1, ubp2

      common /data/ alfa, beta, omega, Ma, eta, jmax
      common /base/ U1, U2, T1, T2, Rratio


c.................................. base flow ..........................

      R1 = 2.0d0
      R2 = 6.0d0

      theta1 = .075d0
      theta2 = .075d0

      b1 = R1/4.d0/theta1
      b2 = R2/4.d0/theta2

      ub1 = dtanh(b1*(R1/dabs(etaj)-dabs(etaj)/R1))
      ub2 = dtanh(b2*(R2/dabs(etaj)-dabs(etaj)/R2))

      ubp1 = 1.d0 - (dtanh(b1*(R1/dabs(etaj)-dabs(etaj)/R1)))**2
      ubp1 = ubp1*(b1*(-R1*etaj/dabs(etaj)**3-etaj/R1/dabs(etaj)))
      ubp2 = 1.d0 - (dtanh(b2*(R2/dabs(etaj)-abs(etaj)/R2)))**2
      ubp2 = ubp2*(b2*(-R2*etaj/dabs(etaj)**3-etaj/R2/dabs(etaj)))

      if(etaj.eq.0.d0)then
      ubp1 = 0.d0
      ubp2 = 0.d0
      end if

      ub1 = .5d0*(1.d0+ub1)
      ub2 = .5d0*(1.d0+ub2)

      ubp1 = .5d0*ubp1
      ubp2 = .5d0*ubp2

      s1  = ub1
      s1p = ubp1

      coflow = .0d0
      h      = .7d0

c.... (1-h) eh a amplitude de ub1 para somar a ub2 e dar 1
c.... (h-coflow) eh a amplitude de ub2 para somar com coflow e dar h
      u  = (1-coflow)*((1.d0-h)*ub1 + h*ub2) + coflow
      up = (1-coflow)*((1.d0-h)*ubp1 + h*ubp2)

      T   = 1.d0
      Tp  = 0.d0

      rho = 1.d0/(T*s1+(1.d0-s1)*T*Rratio)

      R  = s1  + Rratio*(1.d0-s1) 
      Rp = s1p - Rratio*s1p 

c.......................................................................

      return
      end
