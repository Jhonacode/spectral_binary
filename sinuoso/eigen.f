      subroutine eigen(zeig)

      implicit none
      integer jmax, j, jstart, jend, dir
      include 'par.f'

      complex*16 zystart(2), zp(jm), zv(jm), zeig(jm)
      complex*16 zu(jm), zrho(jm), zt(jm), zs1(jm)
      complex*16 alfa, alfau, dv

      real*8 omega, beta, Ma, norm, eta(jm), etaj
      real*8 u, up, T, Tp, rho, s1, s1p, R, Rp
      real*8 drho

      real*8 U1, U2, T1, T2, Rratio

      common /data/ alfa, beta, omega, Ma, eta, jmax
      common /base/ U1, U2, T1, T2, Rratio

c.......................................................................
c     zp = hat(p)
c     zv = hat(v)
c.......................................................................

c.... integrate downward ...............................................
      dir    = -1 
      jstart = (jmax+1)/2
      jend   = 2

      zystart(1) = zeig(jstart)*gamma*Ma*Ma/im/alfa
      zystart(2) = dcmplx(1.d0)

      call rkdumb2(zystart,zeig,zp,zv,jstart,jend,dir,eta)

c.... integrate upward .................................................
      dir    = 1 
      jstart = (jmax+1)/2
      jend   = jmax-1

      zystart(1) = zeig(jstart)*gamma*Ma*Ma/im/alfa
      zystart(2) = dcmplx(1.d0)

      call rkdumb2(zystart,zeig,zp,zv,jstart,jend,dir,eta)

c.......................................................................

c.... base flow ........................................................
      do j=2,jmax-1
         etaj = eta(j)

         call baseflow(etaj,u,up,T,Tp,rho,s1,s1p,R,Rp)

c.... eigenfunctions ...................................................
         alfau = alfa*u-dcmplx(omega)
         zu(j) = - alfa*zp(j)/gamma/dcmplx(Ma*Ma)/rho/alfau
     ^           - zv(j)/im/alfau*up

c        não tem o termo de beta, lembrar de por
         drho    = -1.d0/T/T*Tp
         dv      = (zv(j+1)-zv(j-1))/(eta(j+1)-eta(j-1))
         zrho(j) = - zv(j)/im/alfau*drho - rho*alfa*zu(j)/alfau -
     ^               rho/im/alfau*dv

         zt(j) = zp(j)/rho/R - zrho(j)*T/rho 
         ! ou 
         zt(j) = -(gamma-1.)*alfa*zu(j)/rho/R/alfau
     ^           -(gamma-1.)/rho/R/im/alfau*dv - zv(j)/im/alfau*Tp
         zs1(j) = - zv(j)*s1p/im/alfau
      end do

c.... plot eigenfunctions ..............................................
      norm = zabs(zu((jmax+1)/2))
      norm = 1.
      norm = maxval(zabs(zu))

      open(unit=21,file='sdu',status='unknown')
      open(unit=22,file='sdv',status='unknown')
      open(unit=23,file='sdp',status='unknown')
      open(unit=24,file='sdr',status='unknown')
      open(unit=25,file='sdt',status='unknown')
      open(unit=26,file='sdz',status='unknown')
      open(unit=27,file='sds',status='unknown')

      do j=1,jmax
         write(21,*)real(eta(j)),real(zu(j)/norm),
     ^              real(imag(zu(j))/norm)
         write(22,*)real(eta(j)),real(zv(j)/norm),
     ^              real(imag(zv(j))/norm)
         write(23,*)real(eta(j)),real(zp(j)/norm),
     ^              real(imag(zp(j))/norm)
         write(24,*)real(eta(j)),real(zrho(j)/norm),
     ^              real(imag(zrho(j))/norm)
         write(25,*)real(eta(j)),real(zt(j)/norm),
     ^              real(imag(zt(j))/norm)
         write(26,*)real(eta(j)),real(zeig(j)),
     ^              real(imag(zeig(j)))
         write(27,*)real(eta(j)),real(zs1(j)/norm),
     ^              real(imag(zs1(j))/norm)
      end do

      return
      end


c***********************************************************************
      subroutine rkdumb2(zystart,zeig,zp,zv,jstart,jend,dir,eta)

      implicit none
      integer j, jstart, jend, dir

      include 'par.f'

      complex*16 zystart(2), zy(2), zdydx(2), zyout(2)
      complex*16 zeig(jm), zp(jm), zv(jm)

      real*8 eta(jm), h

c.......................................................................
      zy(1) = zystart(1)
      zy(2) = zystart(2)

c.... zp = hat{p} and zv = hat{v} ......................................
      zp(jstart) = zystart(1)
      zv(jstart) = zystart(2)

      do j=jstart,jend,dir

         h = eta(j+dir)-eta(j)

         call rk42(dir,j,eta(j),zy,zdydx,h,zyout)

         zy(1)     = zyout(1)
         zp(j+dir) = zyout(1)

         zy(2)     = zyout(2)
         zv(j+dir) = zyout(2)

      end do

      return
      end 


c***********************************************************************
      subroutine rk42(dir,j,x,zy,zdydx,h,zyout)

      implicit none
      integer i, j, dir
      include 'par.f'

      complex*16 zy(2), zdydx(2), zyout(2) 
      complex*16 zyt(2), zdyt(2), zdym(2)

      real*8 h, hh, h6, x, xh

c.......................................................................
      hh = h*0.5d0
      h6 = h/6.d0

      call derivs2(x,zy,zdydx)
      do i=1,2
         zyt(i) = zy(i) + hh*zdydx(i)
      end do

c.......................................................................
      xh   = x + hh

      call derivs2(xh,zyt,zdyt)

      do i=1,2
         zyt(i) = zy(i) + dcmplx(hh)*zdyt(i)
      end do

c.......................................................................
      call derivs2(xh,zyt,zdym)

      do i=1,2
         zyt(i)  = zy(i)   + dcmplx(h)*zdym(i)
         zdym(i) = zdyt(i) + zdym(i)
      end do

c.......................................................................
      xh = x + h

      call derivs2(xh,zyt,zdyt) 

      do i=1,2
         zyout(i) = zy(i) + dcmplx(h6) *
     ^              (zdydx(i) + zdyt(i) + dcmplx(2.d0)*zdym(i))
      end do

      return
      end
