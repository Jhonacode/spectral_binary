      subroutine integ(zdet,zeig)

c.......................................................................
      implicit none
      integer jmax, j, jstart, jend, dir
      include 'par.f'

      complex*16 zystart(nm), zy(nm), zdet, zeig(jm)
      complex*16 f1, f2, alfa

      real*8 U1, U2, T1, T2, Rratio
      real*8 omega, beta, Ma, eta(jm)

      common /data/ alfa, beta, omega, Ma, eta, jmax
      common /base/ U1, U2, T1, T2, Rratio


c.... j = jmax ...........................eigenfunctions at eta = etamax
      call start(U2,T2,Rratio,zystart,-1)

c.... integrate inward to eta=0 ........................................
      dir    = -1 
      jstart = jmax
      jend   = (jmax+1)/2+1

      call rkdumb(zystart,zy,jstart,jend,dir,eta,zeig)
      f1 = zy(1)

c.... j = 1 ............................eigenfunctions at eta = - etamax
      call start(U2,T2,Rratio,zystart,1)

c.... integrate inward to eta=0 ........................................
      dir    = 1 
      jstart = 1
      jend   = (jmax+1)/2-1
      call rkdumb(zystart,zy,jstart,jend,dir,eta,zeig)

      f2 = zy(1)

c..............match upper and lower solution - evaluate the determinante
      zdet = f2 - f1

      return
      end


c***********************************************************************
      subroutine start(U,T,Rratio,zystart,iflag)

      implicit none
      include 'par.f'

      integer iflag, jmax

      complex*16 alfa, g, zystart(nm)

      real*8 omega, beta, Ma, eta(jm), U, T, Rratio

      common /data/ alfa, beta, omega, Ma, eta, jmax

c.... Groppenguiser equation ...........................................
      g = (alfa*alfa + beta*beta)*Rratio*T / (alfa*alfa) -
     ^    dcmplx(Ma*Ma) * (alfa*U-omega)**2 /(alfa*alfa)

      zystart(1) = iflag * (alfa*U-omega)/zsqrt(g*Rratio*T)

      return
      end


c***********************************************************************
      subroutine rkdumb(zystart,zy,jstart,jend,dir,eta,zeig)

      implicit none
      integer j, jstart, jend, dir

      include 'par.f'

      complex*16 zeig(jm), zystart(nm), zy(nm), zdydx(nm), zyout(nm)
      real*8     eta(jm), h
      
c.......................................................................
      zy(1)        = zystart(1)
      zeig(jstart) = zystart(1)

      do j=jstart,jend,dir

         h = eta(j+dir)-eta(j)

         call rk4(dir,j,eta(j),zy,zdydx,h,zyout)

         zy(1)       = zyout(1)
         zeig(j+dir) = zyout(1)

      end do

      return
      end 


c***********************************************************************
      subroutine rk4(dir,j,x,zy,zdydx,h,zyout)

      implicit none
      integer i, j, dir
      include 'par.f'

      complex*16 zy(nm), zdydx(nm), zyout(nm) 
      complex*16 zyt(nm), zdyt(nm), zdym(nm)
      real*8     h, hh, h6, x, xh


      hh = h * 0.5d0
      h6 = h / 6.d0

      call derivs(x,zy,zdydx)
      do i=1,nm
         zyt(i) = zy(i) + hh*zdydx(i)
      end do

      xh   = x + hh

      call derivs(xh,zyt,zdyt)
      do i=1,nm
         zyt(i) = zy(i) + dcmplx(hh)*zdyt(i)
      end do

      call derivs(xh,zyt,zdym) 
      do i=1,nm
         zyt(i)  = zy(i)   + dcmplx(h)*zdym(i)
         zdym(i) = zdyt(i) + zdym(i)
      end do

      xh = x + h

      call derivs(xh,zyt,zdyt) 
      do i=1,nm
         zyout(i) = zy(i) + dcmplx(h6) *
     ^              (zdydx(i) + zdyt(i) + dcmplx(2.d0)*zdym(i))
      end do

      return
      end
