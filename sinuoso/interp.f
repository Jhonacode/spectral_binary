      subroutine interp(det,zeig)

      implicit none
      integer jmax, j
      include 'par.f'

      complex*16 alfa, alfa2, zx1, zfl, zx2, zxl, zf, zrtsec
      complex*16 zswap, zdx, zdet1, zdet2, zeig(jm), det

      real*8 omega, beta, Ma, eta(jm)

      common /data/ alfa, beta, omega, Ma, eta, jmax

c...................................................................
      alfa2 = dcmplx(1.0000001d0) * alfa

      call integ(zdet1,zeig)
      det = zdet1
      return
      zx1 = alfa
      zfl = zdet1

      alfa  = alfa2
      call integ(zdet2,zeig)
      zx2 = alfa
      zf  = zdet2

      if (zabs(zfl).lt.zabs(zf)) then
         zrtsec = zx1
         zxl    = zx2
         zswap  = zfl
         zfl    = zf
         zf     = zswap
      else
         zxl    = zx1
         zrtsec = zx2
      endif

      do j = 1,18
         zdx    = (zxl-zrtsec)*zf/(zf-zfl)
         zxl    = zrtsec
         zfl    = zf
         zrtsec = zrtsec + zdx

         alfa  = zrtsec
         call integ(zf,zeig)

         if(zabs(zf).lt.1.d-6)then
           det = zabs(zf)
           return
         endif

      end do

      write(*,*)'too many iterations in interp'

      stop
      end
