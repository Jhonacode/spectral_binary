      subroutine derivs (etaj,zy,zd)

      implicit none
      include 'par.f'

      integer jmax
      complex*16 zy(nm), zd(nm), alfa, g

      real*8 omega, beta, Ma, eta(jm), etaj
      real*8 u, up, T, Tp, rho, s1, s1p, R, Rp
      real*8 U1, U2, T1, T2, Rratio

      common /data/ alfa, beta, omega, Ma, eta, jmax
      common /base/ U1, U2, T1, T2, Rratio


c.... base flow ........................................................
      call baseflow(etaj,u,up,T,Tp,rho,s1,s1p,R,Rp)


c.......................................................................
      g = (alfa*alfa + dcmplx(beta*beta)) / (rho*alfa*alfa) -
     ^    Ma*Ma * (alfa*u-omega)*(alfa*u-omega) / (alfa*alfa)

c     zd(1) = alfa*(alfa*u-omega)/R/T -
c    ^        zy(1)*(zy(1)*g + up)*alfa/(alfa*u-omega) 
      zd(1) = alfa*(alfa*u-omega)*rho -
     ^        zy(1)*(zy(1)*g + up)*alfa/(alfa*u-omega) 

      return
      end
