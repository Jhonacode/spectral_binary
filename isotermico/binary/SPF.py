# STANDARD PLOTTING FORMAT
#
# Format a plot as the adopted standard

import matplotlib.pyplot as plt

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Times']})
rc('text', usetex=True)

def SPF():
	F = plt.gcf()
	A = plt.gca()
	
#	F.set_size_inches(2.55906,2.55906)
	F.set_size_inches(5,5)
	
	L = plt.legend()
	if L!=None:
		L.get_frame().set_alpha(1.0)
		L.get_frame().set_edgecolor('w')
	
	A.tick_params(axis='y', which='minor',direction='in')
	A.tick_params(axis='x', which='minor',direction='in')
	A.tick_params(axis='both',direction='in')
	plt.tight_layout(pad=.15)
