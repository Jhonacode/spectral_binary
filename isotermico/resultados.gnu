reset
#set term jpeg enhanced large lw 2.0 size 1000,800
#set term jpeg enhanced large font times 20 lw 2.0 size 1000,800
set grid lw 0.3
#set xrange[0:6.1]
#set  yrange[0:0.1]
#set encoding utf8
#set xlabel '{ /Symbol w}'
set xlabel 'w'
set ylabel '-{/Symbol alpha}i'
#ƒ(αβγδεζ)"
#set xtics 0.1 
#set ytics 0.01 
#set nokey
set key right top
#set output 'taxa.jpeg'
set title  'Spatial Stability Compresible Mixing Layer'
plot  	\
'incompressible1h071966/taxa.dat' u 2:3 w p pt 6 t 'h=0.7', \
'incompressible1h071966/taxa.dat' u 4:5 w p pt 6 t 'h=0.7', \
'incompressible1h070106/taxa.dat' u 2:3 w p pt 7 t 'h=0.7', \
'incompressible1h070106/taxa.dat' u 4:5 w p pt 7 t 'h=0.7', \
#'incompressible1h071966/taxa.dat'u 2:3 w lp lt 5 t 'h=0.7', \
#'incompressible1h071966/taxa.dat'u 4:5 w lp lt 6 t 'h=0.7', \
#'incompressible3h07/taxaincompressible3h07.dat'  u 1:4 w )l  lt 6 t 'h=0.7', \ #'incompressible1h07/taxaincompressible1h07  u 1:($2) w p ps 2.0 pt 2 lt 2 t 'DNS', \ '' u 1:(-$2) w p   ps 0.5 pt 3 lt 3 t 'LNS'

