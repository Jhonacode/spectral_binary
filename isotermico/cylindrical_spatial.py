#matplotlib inline
#config InlineBackend.figure_format='svg'
from chebPy  import *
from mapping import *
from Baseflow import *
from Parameters import *
from numpy   import matrix,matmul,dot,diag,real,imag,argsort,zeros,exp,log,linspace,polyval,polyfit,where,tan,tanh,cosh,identity
from scipy.linalg  import eig
from scipy.special import airy
#from matplotlib   import figure,subplot,plot,title,grid,show,axis
from scipy.linalg  import block_diag
import numpy as np 
import matplotlib.pyplot as plt 
import numpy as np 
import os 

#import Parameters 

#GLOBAL VARIABLES

#global file1
#global N
#global maxr
#global h
#global M0
#global T_ref
#global Gamma 
#global R1
#global theta2
#global omegai, omegaf, iomega

# Creates a folder in the current directory called data
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)
# Example
#createFolder('./data/')
createFolder('./%s/'%file1)

domega      =   (omegaf-omegai)/iomega 

omega       =   zeros(iomega)
omega       =   np.arange(0,iomega,1)*domega+omegai

#omega[0]     = omega[0]


D,n1        =   cheb(N);


r           =    mappingtan(N,n1,maxr,D)

D2=dot(D,D)

#Base Flow
Wb,Tb,Rhob  =   Baseflow(r,N)


dWbdr      =   dot(D,Wb)
d2Wbdr     =   dot(D2,Wb)
dRhobdr    =   dot(D,Rhob)

plt.plot(Wb,r   ,'*r')
plt.plot(dWbdr,r,'*g')
plt.plot(d2Wbdr,r,'*b')
plt.show()


# coordinate selectors
#rr      =   np.arange(0,N,1)#0:N+1      
#uu      =   rr+N
#vv      =   uu+N
#pp      =   vv+N


Z   =   zeros((N+1,N+1))
I   =   identity(N+1) 

#Cartesian
#A   =   np.block([[diag(M),diag(Rho),Z,Z],[Z,diag(M),Z,diag(1./Rho)],[Z,Z,diag(M),Z],[Z,I,Z,diag(M)]])
#B   =   np.block([[Z,Z,diag(Rho),Z],[Z,Z,Z,Z],[Z,Z,Z,diag(1./Rho)],[Z,Z,I,Z]])
#C   =   np.block([[Z,Z,diag(Rhoy),Z],[Z,Z,diag(My),Z],[Z,Z,Z,Z],[Z,Z,Z,Z]])
#Cylindrical
Ae   =   np.block([[Z,diag(Rhob),Z,Z],[Z,Z,Z,diag(1.0/Rhob)],[Z,Z,Z,Z],[Z,I,Z,Z]])
Be   =   np.block([[diag(Wb),Z,diag(Rhob),Z],[Z,diag(Wb),Z,Z],[Z,Z,diag(Wb),diag(1.0/Rhob)],[Z,Z,I,diag(Wb)]])
Ce   =   np.block([[Z,diag(Rhob),Z,Z],[Z,Z,Z,Z],[Z,Z,Z,Z],[Z,I,Z,Z]])
De   =   np.block([[Z,diag(dRhobdr),Z,Z],[Z,Z,Z,Z],[Z,diag(dWbdr),Z,Z],[Z,Z,Z,Z]])
Ee   =   block_diag(I,I,I,I)


for i in range(0,N+1): 

    for j in range(0,N+1): 

        Ce[i,j]   = Ce[i,j]/r[j] 
	

#A=np.array([[1, 1],[2 , 2]])
#B=np.array([[0, 0],[3 , 3]])
##C=np.block([[Z,Mdiag],[I, I]])
#n2=np.shape(C) 
#print('C', n2)
#print(C[1,1]) 

DD      =   block_diag(D,D,D,D);
II      =   identity(4*(N+1))
Ov      =   zeros((1,(4*(N+1))))


#all for in python goint to n+1, to make n
for iomega in range(0,iomega): 

	print(omega[iomega])
	
	g       =   open('./%s/autospec%di%d.dat'%(file1,N,iomega),'w+')
	
	g.write("%f \n"%(omega[iomega])); 
	
	
	A0      =   np.multiply((1j*omega[iomega]),Ee)-matmul(Ae,DD)-Ce-De;
	B0      =   np.multiply(1j,Be)
	
	
 	#Posicoes	# DE PONTOS
	#[0,N]		=(final -  inicial +1)N+1 PONTOS
	#[N+1,2N+1]	=(final -  inicial +1)N+1 PONTOS
	#[2(N+1),3N+2]	=(final -  inicial +1)N+1 PONTOS
	#[3(N+1),4N+3]	=(final -  inicial +1)N+1 PONTOS
	#.....
	#[(N-1)(N+1),NN+N-1]=(final -  inicial +1)N+1 PONTOS

	# rho
	A0[0,:]        	 	= Ov
	B0[0,:]        	 	= Ov
	A0[0,0]        	 	= 1#A0[1,0]

	A0[N,:]        	 	= Ov 
	B0[N,:]        	 	= Ov 
	A0[N,N]        	 	= 1  
	
	#u
	A0[N+1,:]    		= Ov 
	B0[N+1,:]    		= Ov 
	A0[N+1,N+1]    		= 1#A0[N+1+1,0]  
	
	A0[2*N+1,:]     	= Ov
	A0[2*N+1,:]     	= Ov
	A0[2*N+1,2*N+1]  	= 1 

	#v
	A0[2*(N+1),:]          	= Ov
	B0[2*(N+1),:]          	= Ov
	A0[2*(N+1),2*(N+1)]     = 1.0#A0[2*(N+1)+1,0]
	
	A0[3*N+2,:]             = Ov
	B0[3*N+2,:]             = Ov
	A0[3*N+2,3*N+2]         = 1
	
	#p
	A0[3*(N+1),:]          	= Ov
	B0[3*(N+1),:]          	= Ov
	#nao posso colocar qualquer coisa na pressoa
	#interpolation in the pressure
	A0[3*(N+1),3*(N+1)]    	= A0[3*(N+1)+1,0]
	
	
	A0[4*N+3,:]             = Ov
	B0[4*N+3,:]             = Ov
	A0[4*N+3,4*N+3]         = 1
	#
	
	eigvals, eigvecs = eig(A0, B0) #
	
	S= list(filter(lambda x: np.abs(imag(x))<1 and 0.01< real(x) <5 , eigvals))
	#S= eigvals 
	
	Lamreal =   real(S)
	Lamimag =   imag(S)
	
	alphai  =   np.max(Lamimag)
	
	NN	=   np.shape(Lamreal)


	for kk in range(1,NN[0]):

        	g.write("%f %f\n"%(Lamreal[kk],Lamimag[kk])); 

        #Lreal[i][k]=Lamreal[i]
        #Limag[i][k]=Lamimag[i]

    #plt.figure(2)
	plt.plot(Lamreal,Lamimag,'.r')
    	plt.axis((0.0,6.0,00.0,1.0))
    	plt.grid()

g.close

plt.show()






