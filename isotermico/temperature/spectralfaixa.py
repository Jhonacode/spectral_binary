#matplotlib inline
#config InlineBackend.figure_format='svg'
from   chebPy        	import *
from   mapping       	import *
from   Baseflowfaixa    import *
from   Boundary      	import *
from   scipy.linalg  	import block_diag
from   Parametersfaixa  import *
import numpy 	         as np 
import matplotlib.pyplot as pl
import os 

def p_spectral_matrices(NN,maxr):

	# number of collocation nodes=N+1, because the cheb func
	D,n1	= cheb(NN)
	#D2	= dot(D,D)
	#D2[1:N,1:N]
	
	# Mappint to concentrate the point in 0. Withou using 0
	# r= Radial coordinate
	r   	= mappingtan(NN,n1,maxr,D)
	D2	= dot(D,D)
	#Base Flow

	Wb,Tb,Rhob  =   Baseflow(r,NN)


	
	
	dWbdr      =   dot(D,Wb)
	dRhobdr    =   dot(D,Rhob)
	
	# coordinate selectors
	#rr      =   np.arange(0,N,1)#0:N+1      
	#uu      =   rr+N
	#vv      =   uu+N
	#pp      =   vv+N
	
	
	## Especial Matrices

	Ov      =   np.zeros((1,(4*(NN+1))))
	Z   	=   np.zeros((NN+1,NN+1))
	I   	=   np.identity(NN+1) 
	II      =   np.identity(4*(NN+1))
	Ee 	=   block_diag(I,I,I,I)
	DD      =   block_diag(D,D,D,D);
	

	#Cartesian
	#A   =   np.block([[diag(M),diag(Rho),Z,Z],[Z,diag(M),Z,diag(1./Rho)],[Z,Z,diag(M),Z],[Z,I,Z,diag(M)]])
	#B   =   np.block([[Z,Z,diag(Rho),Z],[Z,Z,Z,Z],[Z,Z,Z,diag(1./Rho)],[Z,Z,I,Z]])
	#C   =   np.block([[Z,Z,diag(Rhoy),Z],[Z,Z,diag(My),Z],[Z,Z,Z,Z],[Z,Z,Z,Z]])


	#Cylindrical
	Ae   =   np.block([[Z,diag(Rhob),Z,Z],[Z,Z,Z,diag(1.0/Rhob)],[Z,Z,Z,Z],[Z,I,Z,Z]])
	Be   =   np.block([[diag(Wb),Z,diag(Rhob),Z],[Z,diag(Wb),Z,Z],[Z,Z,diag(Wb),diag(1.0/Rhob)],[Z,Z,I,diag(Wb)]])
	Ce   =   np.block([[Z,diag(Rhob),Z,Z],[Z,Z,Z,Z],[Z,Z,Z,Z],[Z,I,Z,Z]])
	De   =   np.block([[Z,diag(dRhobdr),Z,Z],[Z,Z,Z,Z],[Z,diag(dWbdr),Z,Z],[Z,Z,Z,Z]])
	
	
	for i in range(0,NN+1): 
	
	    for j in range(0,NN+1): 
	
	        Ce[i,j]   = Ce[i,j]/r[j] 
	
	
	A0      =   -np.matmul(Ae,DD)-Ce-De;

	B0      =   np.multiply(1j,Be);
	

	return A0,Ee,B0




