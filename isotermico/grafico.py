#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import 	matplotlib   		as 	mt 
from   	path 			import 	pathfinder
from   	Parameters 		import 	*
#from 	SPF 			import 	*


M	=	np.loadtxt('./%s/taxa.dat'%file1,unpack=True,skiprows=1) 
N11     =	M.shape[1]
N12     =	M.shape[0]


#0 line, frequency
Omega	=	M[0,:]

#1 and  3 line, alphar
X	=	np.concatenate([M[1,:],M[3,:]])

#2 and 4 line, alphai 
Y	=	np.concatenate([M[2,:],M[4,:]])

(x2,y2,n2)= 	pathfinder(X,Y,200,distweight=3.0,stopweight=3.1,isextremity=False) 

X[n2]	= np.nan

(x1,y1,n1)=	pathfinder(X,Y,25,distweight=3.0,stopweight=2.0,isextremity=False) 
                                                


g1       =   open('./%s/mods1.dat'%file1,'w+')


nn	=   x1.shape[0]
nn2	=   M.shape[1]

for j in range (0,nn):
	
	for i in range (0,nn2):

		if(x1[j]==M[1,i] or x1[j]==M[3,i]):

			g1.write("%f\t%f\t%f\n"%(Omega[i],x1[j],y1[j])); 

g2       =   open('./%s/mods2.dat'%file1,'w+')

nn	=   x2.shape[0]
nn2	=   M.shape[1]
print(nn2)

for j in range (0,nn):

	for i in range (0,nn2):

		if(x2[j]==M[1,i] or x2[j]==M[3,i]):

			g2.write("%f\t%f\t%f\n"%(Omega[i],x2[j],y2[j])); 


#Direct input 
#plt.rcParams['text.latex.preamble']=[r"\usepackage{elsarticle}"]
#Options

fig = plt.figure()

params = {
	  
	#'text.usetex' : True,
	#'font.family' : 'lmodern',
	#'font.family' : 'elsarticle',
	'font.family' : 'serif',
	#'font.serif'  : 'Computer Modern Sans serif',
	'font.sans-serif'    : 'Helvetica',#, Avant Garde, Computer Modern Sans serif
	#'font.sans-serif'    : 'Computer Modern Sans serif',#, Avant Garde, Computer Modern Sans serif
	'lines.linewidth':2,
        'font.size' : 15,
        'text.latex.unicode': True,
	#'legend.fontsize': 'x-large',
        'figure.figsize': (20, 10),
	'axes.labelsize': '20',#'x-Large',
        'axes.titlesize': '25',#'x-Large',
        'xtick.labelsize':'15',#'x-Large',
        'ytick.labelsize':'15',#'x-Large'
          }
plt.rcParams.update(params) 


#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 

ax = plt.axes()

plt.xlabel(r'$\alpha_r$')
plt.ylabel(r'-$\alpha_i$')
plt.plot(x1,y1,'-r',label ='Mode I ')
plt.plot(x2,y2,'-.b',label ='Mode II')
plt.savefig('%s.eps'%(file1), format='eps', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 5, 0.01, 0.75])


plt.legend()
#plt.plot(X[10],Y[10],'ko')
#plt.plot(X[11],Y[11],'ko')
#plt.plot(X,Y,'k.')
#SPF()
plt.show()


