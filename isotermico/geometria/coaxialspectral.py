#matplotlib inline
#config InlineBackend.figure_format='svg'
from chebPy   import *      
from mapping  import *
from Baseflow import *
from numpy    import matrix,matmul,dot,diag,real,imag,argsort,zeros,exp,log,linspace,polyval,polyfit,where,tan,tanh,cosh,identity
from scipy.linalg  import eig
from scipy.special import airy
#from matplotlib   import figure,subplot,plot,title,grid,show,axis
from scipy.linalg  import block_diag
import numpy as np 
import matplotlib.pyplot as plt 

# Creates a folder in the current directory called data
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)
# Example
#createFolder('./data/')

file1       =   'joncas' 


f           =   open('taxacoaxial.dat','w+')
g           =   open('autospectroM09.dat','w+') 
#number of collocation nodes=N+1, because the cheb func
N           =   250 


omegai      =   0.3
omegaf      =   5.3
iomega      =   25
domega      =   (omegaf-omegai)/iomega 

omega       =   zeros(iomega)
omega       =   np.arange(0,iomega,1)*domega+omegai

#omega[0]    =   1.9#omegai


D,n1        =   cheb(N); D2=dot(D,D); D2[1:N,1:N]

#maximum r
maxr        =   20   

r           =   mappingtan(N,n1,maxr,D,D2) 

Wb,Tb,Rhob,T_0,T_r,M_r,R1,theta1  =   Baseflow(r)

dWbdr      =   dot(D,Wb)
dRhobdr    =   dot(D,Rhob)


# coordinate selectors
rr  =   np.arange(0,N,1)#0:N+1      
uu  =   rr+N
vv  =   uu+N
pp  =   vv+N


Z   =   zeros((N+1,N+1))
I   =   identity(N+1) 

#Cartesian
#A   =   np.block([[diag(M),diag(Rho),Z,Z],[Z,diag(M),Z,diag(1./Rho)],[Z,Z,diag(M),Z],[Z,I,Z,diag(M)]])
#B   =   np.block([[Z,Z,diag(Rho),Z],[Z,Z,Z,Z],[Z,Z,Z,diag(1./Rho)],[Z,Z,I,Z]])
#C   =   np.block([[Z,Z,diag(Rhoy),Z],[Z,Z,diag(My),Z],[Z,Z,Z,Z],[Z,Z,Z,Z]])
#Cylindrical

Ae   =   np.block([[Z,diag(Rhob),Z,Z],[Z,Z,Z,diag(1.0/Rhob)],[Z,Z,Z,Z],[Z,I,Z,Z]])
Be   =   np.block([[diag(Wb),Z,diag(Rhob),Z],[Z,diag(Wb),Z,Z],[Z,Z,diag(Wb),diag(1.0/Rhob)],[Z,Z,I,diag(Wb)]])
Ce   =   np.block([[Z,diag(Rhob),Z,Z],[Z,Z,Z,Z],[Z,Z,Z,Z],[Z,I,Z,Z]])
De   =   np.block([[Z,diag(dRhobdr),Z,Z],[Z,Z,Z,Z],[Z,diag(dWbdr),Z,Z],[Z,Z,Z,Z]])
Ee   =   block_diag(I,I,I,I)


for i in range(0,N+1): 

    for j in range(0,N+1): 

        Ce[i,j]   = Ce[i,j]/r[j] 

#A=np.array([[1, 1],[2 , 2]])
#B=np.array([[0, 0],[3 , 3]])
##C=np.block([[Z,Mdiag],[I, I]])
#n2=np.shape(C) 
#print('C', n2)
#print(C[1,1]) 

DD      =   block_diag(D,D,D,D);
II      =   identity(4*(N+1))
Ov      =   zeros((1,(4*(N+1))))

#def getKey(item):
#    return item[1]

for iomega in range(0,iomega): 
#for iomega in range(0,1): 

    print(omega[iomega])

    A0      =   np.multiply((1j*omega[iomega]),Ee)-matmul(Ae,DD)-Ce-De;
    B0      =   np.multiply(1j,Be)
    
    
    #[0,N]=N+1 PONTOS
    # rho
    A0[0,:]               = Ov
    B0[0,:]               = Ov
    B0[0,0]               = 1 # A0[N,:]               = Ov 
    B0[N,:]               = Ov 
    B0[N,N]               = 1  
    
    #u
    A0[N+1,:]             = Ov 
    B0[N+1,:]             = Ov 
    B0[N+1,0]             = 1  
    
    A0[2*N,:]             = Ov
    B0[2*N,:]             = Ov
    B0[2*N,2*N]           = 1
    
    #v
    A0[2*N+1,:]           = Ov
    B0[2*N+1,:]           = Ov
    B0[2*N+1,N+1]         = 1
    
    A0[3*N,:]             = Ov
    B0[3*N,:]             = Ov
    B0[3*N,3*N ]          = 1
    
    #p
    A0[3*N+1,:]           = Ov
    B0[3*N+1,:]           = Ov
    B0[3*N+1,3*N+1]       = 1
    
    
    A0[4*N,:]             = Ov
    B0[4*N,:]             = Ov
    B0[4*N,4*N]           = 1
    #
    
    eigvals, eigvecs = eig(A0, B0) #

    #S   =   list(filter(lambda x: np.abs(imag(x))<2.0 and 0.3< real(x) <5.0 , eigvals))
    S   =   list(filter(lambda x: 0.00<imag(x)<5.0 and 0.35< real(x) <10.0 , eigvals))

    print(S[0])
    #Ssort= np.sorted(S,key=getKey)


    Ssort=sorted(S, key=lambda x: x.imag,reverse=True) 
    #Ssort=  np.sort(S,lambda x:x.real) 
    #print(Ssort[0])
    #print(Ssort[1])

    #print(S,'ff')
    #S= eigvals 

    Lamreal =   real(Ssort)
    Lamimag =   imag(Ssort)

    alphai1 =   Lamimag[0]
    alphai2 =   Lamimag[1]


    print(omega[iomega],alphai1,alphai2)

    f.write("%f %f %f\n"%(omega[iomega],alphai1,alphai2)); 


    NN=np.shape(Lamreal)

    print(NN[0],'dd') 

    for kk in range(0,NN[0]): 

        g.write("%f %f\n"%(Lamreal[kk],Lamimag[kk])) 

        #Lreal[i][k]=Lamreal[i]
        #Limag[i][k]=Lamimag[i]

    #plt.figure(2)
    plt.plot(Lamreal,Lamimag,'.r')
    #plt.axis((0.0,5.0,-1.0,1.0))
    plt.grid()
#    plt.show()


f.close
g.close

plt.show()

#     %%%% solve for eigenvalues
#disp('computing eigenvalues')
#tic; [U,S]=eig(B0,A0); toc
#
#S=diag(S);
#
#%plot(real(S),imag(S),'*'); hold on
#%xlimvec=[-3, 3]; ylimvec=[-2,0];
#
#for ind=1:length(S)
#            fprintf(arq1,'%f  %f \n',real(S(ind)),imag(S(ind)));
#






