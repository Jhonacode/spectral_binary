# Program to solve the pressure instability equation 
# Development by :Jhonatan Andres Aguirre Manco
# Date:25/04/2018
# Pseu_spectral Method
# Equation: Pressure equation, File/Repositories/Thesis



#matplotlib inline
#config InlineBackend.figure_format='svg'
from    Parameters import * 
from    chebPy  import *
from    mapping import *
from    Baseflow      import *
from    numpy   import matrix,matmul,dot,diag,real,imag,argsort,zeros,exp,log,linspace,polyval,polyfit,where,tan,tanh,cosh,identity
from scipy.linalg  import eig
from scipy.special import airy
#from matplotlib   import figure,subplot,plot,title,grid,show,axis
from scipy.linalg  import block_diag
import numpy as np 
import matplotlib.pyplot as plt 
from   Boundary      import *


f       =   open('taxapressao.dat','w+')
g       =   open('autospectrop.dat','w+')

#number of collocation nodes=N+1, because the cheb func
N           =   200 


omegai      =   0.2
omegaf      =   1.0
iomega      =   20
domega      =   (omegaf-omegai)/iomega 

omega       =   zeros(iomega)
omega       =   np.arange(0,iomega,1)*domega+omegai



D,n1        =    cheb(N); #D2=dot(D,D);# D2[1:N,1:N]

#maximum r
maxr        =   50   

# Mappint to concentrate the point in 0. Withou using 0
# r= Radial coordinate
r   	    = mappingtan(N,n1,maxr,D)
D2	    = dot(D,D)

Wb,Tb,Rhob  =   Baseflow_geometric(r,Gamma,N)

#azimuthal mode
n   =0

Mc   = 0.50
#*********Thickness of the jet, base flow 
bba  = 0.25
#*********internal radio of the jet, base flow,solution morrismeanflow.f90 
hba  = 0.8871800
#*********Morris, The instability of hight speed jets 
#*********Reservoir temperature
TR   = 0.60
##*********Ambient temperature
T0   = 0.20
#TR   = 1.00
#T0   = 1.0 
#*********Heat capacity relation 
gamma= 1.40
#
a      = gamma 

##dont work with Mach<0.1
#u1b    = 0.40
#u2b    = 0.80
#T1b    = 0.60
#T2b    = 1.00
#delta  = 0.40
#gamma  = 1.40

#a      = T1b


#Wb  =   zeros(N+1)
#Tb  =   zeros(N+1)
#Rhob=   zeros(N+1)
#
#for j in range (0,N+1):
#
#    if(r[j]<hba):
#        Wb[j]   = Mc  
#    else: 
#        Wb[j]   = Mc*exp(-log(2.0)*(r[j]-hba)**2.0/bba**2.0)  
#    
#
#for j in range (0,N+1):
#    #Development Jet
#    #wb[j]    = Mc*exp(-log(2.d0)*n[j]**2.d0/bba**2.d0)
#    Tb[j]    = T0/TR*(1.0+(gamma-1.0)/2.0*Mc**2)*(1.0+(Wb[j])*(TR/T0-1.0))-(gamma-1.0)/2.0*Mc**2.0*(Wb[j]**2.0)
#    Rhob[j]  = 1.0/(Tb[j])


#D2	   =  dot(D,D)

dWbdr      =   dot(D,Wb)
d2Wbdr     =   dot(D,dWbdr)
dRhobdr    =   dot(D,Rhob)


plt.figure(1)
plt.plot(Wb,r,'*b')
plt.plot(dWbdr,r,'*g')
plt.plot(d2Wbdr,r,'*m')
#plt.plot(Rhob,r,'*b')
#plt.plot(dRhobdr,r,'*b')
plt.axis((-5.0,5.0,-0.0,10.5))
plt.grid()
plt.show()
#
# coordinate selectors
rr      =   np.arange(0,N,1)#0:N+1      
uu      =   rr+N
vv      =   uu+N
pp      =   vv+N


Z   =   zeros((N+1,N+1))
I   =   identity(N+1) 

Ov      =   zeros((1,(3*(N+1))))



for iomega in range(0,iomega): 



    R1      =   diag(Wb-Wb**3/a**2)

    Lq2     =   diag(3.0*omega[iomega]*(Wb**2)/a**2-omega[iomega]) 
    
    Lq1     =   -matmul(diag(Wb),D2)-(matmul(diag(Wb/r),D))    \
                +matmul(diag(2.0*dWbdr),D)                   \
                +matmul(diag(Wb/Rhob*dRhobdr),D)-diag(3.0*omega[iomega]**2.0*Wb/a**2) \
                +diag(Wb*n**2.0/(r**2))
    
    Lp      =    np.multiply(omega[iomega]/r,D)+np.multiply(omega[iomega],D2)       \
                -matmul(diag(omega[iomega]*1/Rhob*dRhobdr),D)                       \
                +np.multiply(omega[iomega]**3.0/a**2,I)                             \
                -np.multiply((omega[iomega]*n**2.0/r**2),I) 
    

    print(omega[iomega])

    A0   =   np.block([[-Lq1,-Lq2,-Lp],[I,Z,Z],[Z,I,Z]])
    B0   =   np.block([[Z,R1,Z],[Z,Z,I],[I,Z,Z]]) 
    
    #Boundary Conditions
    boundary_condition(A0,B0,N)	
    #[0,N]=N+1 PONTOS

    eigvals, eigvecs = eig(A0, B0) #

    S= list(filter(lambda x:0.1 <np.abs(imag(x))<5.0 and 0.011< real(x) <5 , eigvals))
    #S= eigvals 

    Lamreal =   real(S)
    Lamimag =   imag(S)

    alphai  =   np.max(Lamimag)

    f.write("%f %f\n"%(omega[iomega],alphai)); 


    NN=np.shape(Lamreal)

    print(NN[0],'dd') 

    for kk in range(1,NN[0]): 

        g.write("%f %f\n"%(Lamreal[kk],Lamimag[kk])); 

        #Lreal[i][k]=Lamreal[i]
        #Limag[i][k]=Lamimag[i]

    #plt.figure(2)
    plt.plot(Lamreal,Lamimag,'.r')
    #plt.axis((0.0,5.0,-1.0,1.0))
    plt.grid()


f.close
g.close

plt.show()

