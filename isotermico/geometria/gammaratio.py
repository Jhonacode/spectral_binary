#################################3#
# Program to generate the 
# instability curves, necesaries 
# reproduce the Joncas and Perreu paper 
#
# Create by: Jhonatan Aguirre 
# Date:08/11/2018
# working: no
###################################

from   chebPy        import *
from   mapping       import *
from   Baseflow      import *
from   Boundary      import *
from   scipy.linalg  import block_diag
import numpy 	         as np 
import matplotlib.pyplot as pl
import os 
import matplotlib  	as 	mpl 
from   plotparameters   import 	*

NN      =   200 
Gamma   =   3.0

#Radii Secondary jet
R2      =   R1*Gamma

# number of collocation nodes=N+1, because the cheb func
D,n1	= cheb(NN)
#D2	= dot(D,D)
#D2[1:N,1:N]

# Mappint to concentrate the point in 0. Withou using 0
# r= Radial coordinate
r   	= mappingtan(NN,n1,maxr,D)
D2	= dot(D,D)
#Base Flow

Gamma_1   =   1.3
Wb_1,Tb,Rhob  =   Baseflow_geometric(r,Gamma_1,NN)
Gamma_2   =   2.0
Wb_2,Tb,Rhob  =   Baseflow_geometric(r,Gamma_2,NN)
Gamma_3  =   3.0
Wb_3,Tb,Rhob  =   Baseflow_geometric(r,Gamma_3,NN)
Gamma_4   =   4.0
Wb_4,Tb,Rhob  =   Baseflow_geometric(r,Gamma_4,NN)

mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()
ax.legend()


p1_1, p1_2 = [0,1], [1, 1]
p2_1, p2_2 = [0,Gamma_1], [Gamma_1,Gamma_1]
plt.plot(p1_1, p1_2, p2_1, p2_2, color='gray',dashes=[1, 1])
plt.text(0.71, 1.0,r'$\mathrm{R_1}$')
plt.text(0.71, Gamma_1,r'$\mathrm{R_2}$')

p2_1, p2_2 = [0,Gamma_2], [Gamma_2,Gamma_2]
plt.plot(p2_1, p2_2, color='gray',dashes=[1, 1])
plt.text(0.71, Gamma_2,r'$\mathrm{R_2}$')
#
p2_1, p2_2 = [0,Gamma_3], [Gamma_3,Gamma_3]
plt.plot(p2_1, p2_2, color='gray',dashes=[1, 1])
plt.text(0.71, Gamma_3,r'$\mathrm{R_2}$')
#
p2_1, p2_2 = [0,Gamma_4], [Gamma_4,Gamma_4]
plt.plot(p2_1, p2_2, color='gray',dashes=[1, 1])
plt.text(0.71, Gamma_4,r'$\mathrm{R_2}$')



ax.xaxis.set_major_locator(plt.MultipleLocator(0.1))
ax.yaxis.set_major_locator(plt.MultipleLocator(1.00))
plt.axis([ 0,0.7,0.01, 6.5,])

plt.plot(Wb_1,r,color='slateblue',dashes=[1, 0],label ='$\mathrm{\Gamma=1.3}$')
plt.plot(Wb_2,r,color='green',dashes=[1, 1],label ='$\mathrm{\Gamma=2.0}$')
plt.plot(Wb_3,r,color='m',dashes=[3, 1],label ='$\mathrm{\Gamma=3.0}$')
plt.plot(Wb_4,r,color='k',dashes=[2, 2],label ='$\mathrm{\Gamma=4.0}$')

plt.legend()
ax.legend(frameon=False)

plt.savefig('geometric.pdf', format='pdf', dpi=1000)

pl.show()
