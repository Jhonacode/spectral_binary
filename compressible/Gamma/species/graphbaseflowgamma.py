#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib 		as 	mpl
from   	plotparameters 		import 	*


M1	=	np.loadtxt('./H2O2G13/taxa1/baseflow.dat',unpack=True,skiprows=1) 

r_1	=	M1[0,:]
Wb_1	=	M1[1,:]
#Tb_1	=	M1[2,:]
Rhob_1	=	M1[3,:]
Y_11	=	M1[5,:]
Y_21	=	M1[6,:]

M2	=	np.loadtxt('./H2O2G2/taxa1/baseflow.dat',unpack=True,skiprows=1) 
r_2	=	M2[0,:]
Wb_2	=	M2[1,:]
#Tb_1	=	M[2,:]
Rhob_2	=	M2[3,:]
Y_12	=	M2[5,:]
Y_22	=	M2[6,:]

M3	=	np.loadtxt('./H2O2G3/taxa1/baseflow.dat',unpack=True,skiprows=1) 
r_3	=	M3[0,:]
Wb_3	=	M3[1,:]
#Tb_1	=	M[2,:]
Rhob_3	=	M3[3,:]
#Y_13	=	M3[5,:]
#Y_23	=	M3[6,:]

M4	=	np.loadtxt('./H2O2G4/taxa1/baseflow.dat',unpack=True,skiprows=1) 
r_4	=	M4[0,:]
Wb_4	=	M4[1,:]
#Tb_1	=	M[2,:]
Rhob_4	=	M4[3,:]



#####################################

#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 

mpl.rcParams.update(params)

fig 	= plt.figure()
ax	= plt.axes()

p1_1, p1_2 = [0,1], [1, 1]
p2_1, p2_2 = [0,2], [2, 2]
p3_1, p3_2 = [0,3], [3, 3]
p4_1, p4_2 = [0,4], [4, 4]

#####################################

plt.xlabel(r'$\mathrm{\overline{w}}$')
plt.ylabel(r'$\mathrm{r}$')

plt.text(1.01, 1.0,r'$\mathrm{R_1}=1$')
plt.text(1.01, 2.0,r'$\mathrm{R_2}=2$')
plt.text(1.01, 3.0,r'$\mathrm{R_2}=3$')
plt.text(1.01, 4.0,r'$\mathrm{R_2}=4$')

plt.plot(p1_1, p1_2, p2_1, p2_2, color='gray',dashes=[1, 1],alpha=0.5)
plt.plot(p3_1, p3_2, p4_1, p4_2, color='gray',dashes=[1, 1],alpha=0.5)

plt.plot(Wb_1,r_1,color='slateblue',dashes=[1, 0],label ='$\mathrm{\Gamma=1.3}$')

plt.plot(Wb_2,r_2,color='green',dashes=[1, 1],label ='$\mathrm{\Gamma=2.0}$')

plt.plot(Wb_3,r_3,color='m',dashes=[3, 1],label = '$\mathrm{\Gamma=3.0}$')
            
plt.plot(Wb_4,r_4,color='k',dashes=[1, 2],label ='$\mathrm{\Gamma=4.0}$')

plt.plot(Y_11,r_1,color='blue',dashes=[2, 1],label ='$\mathrm{\\bar{Y}_1}$')

plt.plot(Y_21,r_1,color='c',dashes=[5, 1],label ='$\mathrm{\\bar{Y}_2}$')

#####################################

ax.xaxis.set_major_locator(plt.MultipleLocator(0.2))
ax.yaxis.set_major_locator(plt.MultipleLocator(1.0))
plt.axis([0.01, 1.01,0.01,6.0])

plt.legend()
ax.legend(frameon=False)
plt.axis([-0.01, 1.01,-0.01,6.0])


plt.savefig('baseflowgammay1.pdf', format='pdf', dpi=1000)

#####################################
plt.show()
