#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib 		as 	mpl
from   	plotparameters 		import 	*


M	=	np.loadtxt('./H2O2G2/mods1.dat',unpack=True) 
omega1_1=	M[0,:]
x1_1	=	M[1,:]
y1_1	=	M[2,:]
phase1_1=	(omega1_1)/(x1_1)


################################################

M	=	np.loadtxt('./H2O2G4/mods1.dat',unpack=True) 
omega2_1=	M[0,:]
x2_1	=	M[1,:]
y2_1	=	M[2,:]
phase2_1=	(omega2_1)/(x2_1)


################################################

M	=	np.loadtxt('./O2H2G13/mods1.dat',unpack=True) 
x3_1	=	M[1,:]
y3_1	=	M[2,:]
omega3_1=	M[0,:]
phase3_1=	(omega3_1)/(x3_1)


################################################

M	=	np.loadtxt('./O2H2G2/mods1.dat',unpack=True) 
x4_1	=	M[1,:]
y4_1	=	M[2,:]
omega4_1=	M[0,:]
phase4_1=	(omega4_1)/(x4_1)

################################################
M	=	np.loadtxt('../homogeneo/G2/taxaJh1/mods1.dat',unpack=True)
x5_1	=	M[1,:]
y5_1	=	M[2,:]
omega5_1=	M[0,:]
phase5_1=	(omega5_1)/(x5_1)


###############################################33
mpl.rcParams.update(params)


fig = plt.figure()
ax  = plt.axes()

plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{k_i}$')

plt.plot(omega1_1,y1_1,color='slateblue',dashes=[1, 0],label ='$\mathrm{\Gamma=2.0, H_2-O_2}$')
################################                                 
plt.plot(omega2_1,y2_1, color='g',dashes=[1, 1],label ='$\mathrm{\Gamma=4.0, H_2-O_2}$')
################################                                 
plt.plot(omega3_1,y3_1,'m',dashes=[3, 1],label ='$\mathrm{\Gamma=1.3, O_2-H_2}$')
################################                                 
plt.plot(omega4_1,y4_1,'k',dashes=[1, 2],label ='$\mathrm{\Gamma=2.0,O_2-H_2}$')
################################                                 
plt.plot(omega5_1,y5_1,'b',dashes=[2, 1],label ='$\mathrm{\Gamma=2.0, \\bar{\\rho}=1}$')
#############

plt.text(0.2, 0.125,r'Mode I')

plt.axis([0.1, 5.5, 0.01, 0.15])
ax.xaxis.set_major_locator(plt.MultipleLocator(0.7))
#ax.yaxis.set_major_locator(plt.MultipleLocator(0.025))

ax.legend()
ax.legend(frameon=False)


plt.savefig('walphaiallI.pdf', format='pdf', dpi=1000)


plt.show()

