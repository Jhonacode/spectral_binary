#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib 		as 	mpl
from   	plotparameters 		import 	*

###############################################33
#####################################

M	=	np.loadtxt('./H2O2G2/mods2.dat',unpack=True) 
omega1_2=	M[0,:]
x1_2	=	M[1,:]
y1_2	=	M[2,:]
phase1_2=	(omega1_2)/(x1_2)

M	=	np.loadtxt('./H2O2G3/mods2.dat',unpack=True) 
omega2_2=	M[0,:]
x2_2	=	M[1,:]
y2_2	=	M[2,:]
phase2_2=	(omega2_2)/(x2_2)
M	=	np.loadtxt('./O2H2G2/mods2.dat',unpack=True) 

x3_2	=	M[1,:]
y3_2	=	M[2,:]
omega3_2=	M[0,:]
phase3_2=	(omega3_2)/(x3_2)

M	=	np.loadtxt('./O2H2G3/mods2.dat',unpack=True) 

x4_2	=	M[1,:]
y4_2	=	M[2,:]
omega4_2=	M[0,:]
phase4_2=	(omega4_2)/(x4_2)

M	=	np.loadtxt('../homogeneo/G2/taxaJh1/mods2.dat',unpack=True) #N11     =	M.shape[1] #N12     =	M.shape[0]
x5_2	=	M[1,:]
y5_2	=	M[2,:]
omega5_2=	M[0,:]
phase5_2=	(omega5_2)/(x5_2)

#######################################################

mpl.rcParams.update(params)

fig 	= plt.figure()
ax 	= plt.axes()

plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{k_i}$')

plt.plot(omega1_2,y1_2,color='slateblue',dashes=[1, 0],label ='$\mathrm{\Gamma=2.0, H_2-O_2}$')                        
################################                                 
plt.plot(omega2_2,y2_2, color='g',dashes=[1, 1],label ='$\mathrm{\Gamma=3.0, H_2-O_2}$')                        
################################                                 
plt.plot(omega3_2,y3_2,'m',dashes=[3, 1],label ='$\mathrm{\Gamma=2.0, O_2-H_2}$')                        
################################                                 
plt.plot(omega4_2,y4_2,'k',dashes=[1, 2],label ='$\mathrm{\Gamma=3.0, O_2-H_2}$')                        
################################                                 
plt.plot(omega5_2,y5_2,'b',dashes=[2, 1],label ='$\mathrm{\Gamma=2.0, \\bar{\\rho}=1}$')
#############

plt.text(0.2, 0.75,r'Mode II')


plt.axis([0.1, 1.5, 0.01, 0.85])
#ax.xaxis.set_major_locator(plt.multiplelocator(0.25))
#ax.yaxis.set_major_locator(plt.multiplelocator(0.1))

ax.legend()
ax.legend(frameon=False)

plt.savefig('walphaiallII.pdf', format='pdf', dpi=1000)
#
plt.show()
