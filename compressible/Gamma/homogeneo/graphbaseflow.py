#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


M1	=	np.loadtxt('./G13/taxa1/baseflow.dat',unpack=True,skiprows=1) 

r_1	=	M1[0,:]
Wb_1	=	M1[1,:]
Tb_1	=	M1[2,:]
Rhob_1	=	M1[3,:]

M2	=	np.loadtxt('./G2/taxaJh1/baseflow.dat',unpack=True,skiprows=1) 

r_2	=	M2[0,:]
Wb_2	=	M2[1,:]
#Tb_1	=	M[2,:]
#Rhob_1	=	M[3,:]

M3	=	np.loadtxt('./G3/taxa1/baseflow.dat',unpack=True,skiprows=1) 
r_3	=	M3[0,:]
Wb_3	=	M3[1,:]
#Tb_1	=	M[2,:]
#Rhob_1	=	M[3,:]

M4	=	np.loadtxt('./G4/taxa1/baseflow.dat',unpack=True,skiprows=1) 
r_4	=	M4[0,:]
Wb_4	=	M4[1,:]


mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()
ax.legend()

#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 


plt.xlabel(r'$\mathrm{\overline{w}}$')
plt.ylabel(r'$\mathrm{r}$')

plt.text(0.71, 1.0,r'$\mathrm{R_1}$')
plt.text(0.71, 2.0,r'$\mathrm{R_2}$')
plt.text(0.71, 3.0,r'$\mathrm{R_2}$')
plt.text(0.71, 4.0,r'$\mathrm{R_2}$')

p1_1, p1_2 = [0,1], [1, 1]
p2_1, p2_2 = [0,2], [2, 2]
p3_1, p3_2 = [0,3], [3, 3]
p4_1, p4_2 = [0,4], [4, 4]

plt.plot(p1_1, p1_2, p2_1, p2_2,p3_1, p3_2,p4_1, p4_2, color='gray',dashes=[1, 1])


plt.plot(Wb_1,r_1,color='slateblue',dashes=[1, 0],label ='$\mathrm{\Gamma=1.3}$')
plt.plot(Wb_2,r_2,color=    'green',dashes=[1, 1],label ='$\mathrm{\Gamma=2.0}$')
plt.plot(Wb_3,r_3,color=        'm',dashes=[3, 1],label ='$\mathrm{\Gamma=3.0}$')
plt.plot(Wb_4,r_4,color=        'k',dashes=[1, 2],label ='$\mathrm{\Gamma=4.0}$')

#plt.plot(Rhob_1,r_1,color='m',dashes=[3, 1],label ='$\mathrm{h=0.9}$')
#plt.plot(Tb_1,r_1,color='m',dashes=[3, 1],label ='$\mathrm{h=0.9}$')


#plt.text(0.0, 2.0,r'$\mathrm{\bar{W}(r)}$')

#########x####################         ########

ax.xaxis.set_major_locator(plt.MultipleLocator(0.1))
ax.yaxis.set_major_locator(plt.MultipleLocator(1.0))
plt.axis([ 0,0.7,0.01, 6.1,])

plt.legend()
ax.legend(frameon=False)
plt.savefig('baseflow.pdf', format='pdf', dpi=1000)

plt.show()
