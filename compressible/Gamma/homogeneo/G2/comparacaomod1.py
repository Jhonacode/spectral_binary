#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


################################################
################################################

########################################################
M	=	np.loadtxt('./joncas/mods1.dat',unpack=True) 
#omega5_1=	M[0,:]
x1	=	M[0,:]
y1	=	M[1,:]

M	=	np.loadtxt('../../jhonatanincom/mods1.dat',unpack=True) 
x1_1	=	M[1,:]
y1_1	=	M[2,:]
omega1_1=	M[0,:]
phase1_1=	(omega1_1)/(x1_1)

M	=	np.loadtxt('../../jhonatanincom/mods2.dat',unpack=True) 
x1_2	=	M[1,:]
y1_2	=	M[2,:]
omega1_2=	M[0,:]
phase1_2=	(omega1_2)/(x1_2)
################################################

M	=	np.loadtxt('./taxaJh01/mods1.dat',unpack=True) 
omega2_1=	M[0,:]
x2_1	=	M[1,:]
y2_1	=	M[2,:]
phase2_1=	(omega2_1)/(x2_1)

M	=	np.loadtxt('./taxaJh01/mods2.dat',unpack=True) 
omega2_2=	M[0,:]
x2_2	=	M[1,:]
y2_2	=	M[2,:]
phase2_2=	(omega2_2)/(x2_2)

################################################
M	=	np.loadtxt('./taxaJh025/mods1.dat',unpack=True) 
omega3_1=	M[0,:]
x3_1	=	M[1,:]
y3_1	=	M[2,:]
phase3_1=	(omega3_1)/(x3_1)

M	=	np.loadtxt('./taxaJh025/mods2.dat',unpack=True) 
omega3_2=	M[0,:]
x3_2	=	M[1,:]
y3_2	=	M[2,:]
phase3_2=	(omega3_2)/(x3_2)

################################################
M	=	np.loadtxt('./taxaJh05/mods1.dat',unpack=True) 
omega4_1=	M[0,:]
x4_1	=	M[1,:]
y4_1	=	M[2,:]
phase4_1=	(omega4_1)/(x4_1)

M	=	np.loadtxt('./taxaJh05/mods2.dat',unpack=True) 
omega4_2=	M[0,:]
x4_2	=	M[1,:]
y4_2	=	M[2,:]
phase4_2=	(omega4_2)/(x4_2)
################################################
M	=	np.loadtxt('./taxaJh1/mods1.dat',unpack=True) 
omega5_1=	M[0,:]
x5_1	=	M[1,:]
y5_1	=	M[2,:]
phase5_1=	(omega5_1)/(x5_1)

M	=	np.loadtxt('./taxaJh1/mods2.dat',unpack=True) 
omega5_2=	M[0,:]
x5_2	=	M[1,:]
y5_2	=	M[2,:]
phase5_2=	(omega5_2)/(x5_2)

################################################
M	=	np.loadtxt('./taxaJh10/mods1.dat',unpack=True) 
omega6_1=	M[0,:]
x6_1	=	M[1,:]
y6_1	=	M[2,:]
phase6_1=	(omega6_1)/(x6_1)

M	=	np.loadtxt('./taxaJh10/mods2.dat',unpack=True) 
omega6_2=	M[0,:]
x6_2	=	M[1,:]
y6_2	=	M[2,:]
phase6_2=	(omega6_2)/(x6_2)

################################################
M	=	np.loadtxt('./taxaJh100/mods1.dat',unpack=True) 
omega7_1=	M[0,:]
x7_1	=	M[1,:]
y7_1	=	M[2,:]
phase7_1=	(omega7_1)/(x7_1)

M       =	np.loadtxt('./taxaJh100/mods2.dat',unpack=True) 
x7_2	=	M[1,:]
y7_2	=	M[2,:]
omega7_2=	M[0,:]
phase7_2=	(omega7_2)/(x7_2)

################################################
mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()

ax.legend()
ax.legend(frameon=False)

#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 


plt.xlabel(r' $\mathrm{k_rR_1}$')
plt.ylabel(r'-$\mathrm{k_iR_1}$')
#plt.xlabel(r'$\alpha_r$')
#plt.ylabel(r'$-\alpha_i$')

#########x####################         ########
#plt.plot(x1,y1,'g',dashes=[1, 0],label = 'Incompressible')
#plt.plot(x1_1,y1_1,'g--o--',markersize=9,dashes=[1, 0],label = 'Incompressible')

plt.plot(x1_1,y1_1,'-g',marker='o', markevery=5, markersize=7,label = 'Incompressible')

#########x####################         ########
plt.plot(x2_1,y2_1,dashes=[1, 1],color='slateblue',label = '$\mathrm{\\bar{P}=0.1}$')

#########x####################         ########
plt.plot(x3_1,y3_1,dashes=[2, 1],color='m',label = '$\mathrm{\\bar{P}=0.25}$')

#########x####################         ########
plt.plot(x4_1,y4_1,dashes=[1, 2],color='k',label = '$\mathrm{\\bar{P}=0.5}$')

#########x####################         ########
plt.plot(x5_1,y5_1,dashes=[1, 3],color='b',label = '$\mathrm{\\bar{P}=1.0}$')

#########x####################         ########
plt.plot(x6_1,y6_1,dashes=[3, 1],color='r',label = '$\mathrm{\\bar{P}=10}$')

#########x####################         ########
plt.plot(x7_1,y7_1,dashes=[4, 1],color='c',label = '$\mathrm{\\bar{P}=100}$')



plt.text(0.5, 0.14,'ModeI') 
#plt.text(0.2, 0.75,'ModeII') 

ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.02))
plt.axis([0.1, 6.0, 0.01, 0.16])

plt.legend()
ax.legend(frameon=False)
plt.savefig('alphari.pdf', format='pdf', dpi=1000)

################################################
################################################
################################################
################################################
#mpl.rcParams.update(params)

fig2 = plt.figure()

ax2 = plt.axes()

ax2.legend()
ax2.legend(frameon=False)

plt.xlabel(r' $\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{k_iR_1}$')
#plt.xlabel(r'$\alpha_r$')
#plt.ylabel(r'$-\alpha_i$')

#########x####################         ########
#plt.plot(x1,y1,'g',dashes=[1, 0],label = 'Incompressible')
#plt.plot(x1_1,y1_1,'g--o--',markersize=9,dashes=[1, 0],label = 'Incompressible')

plt.plot(omega1_1,y1_1,'-g',marker='o', markevery=5, markersize=7,label = 'Incompressible')

#########x####################         ########
plt.plot(omega2_1,y2_1,dashes=[1, 1],color='slateblue',label = '$\mathrm{\\bar{P}=0.1}$')

#########x####################         ########
plt.plot(omega3_1,y3_1,dashes=[2, 1],color='m',label = '$\mathrm{\\bar{P}=0.25}$')

#########x####################         ########
plt.plot(omega4_1,y4_1,dashes=[1, 2],color='k',label = '$\mathrm{\\bar{P}=0.5}$')

#########x####################         ########
plt.plot(omega5_1,y5_1,dashes=[1, 3],color='b',label = '$\mathrm{\\bar{P}=1.0}$')

#########x####################         ########
plt.plot(omega6_1,y6_1,dashes=[3, 1],color='r',label = '$\mathrm{\\bar{P}=10}$')

#########x####################         ########
plt.plot(omega7_1,y7_1,dashes=[4, 1],color='c',label = '$\mathrm{\\bar{P}=100}$')



plt.text(0.5, 0.14,'ModeI') 
#plt.text(0.2, 0.75,'ModeII') 

ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.02))
plt.axis([0.1, 6.0, 0.01, 0.16])

plt.legend()
ax2.legend(frameon=False)
plt.savefig('walphai.pdf', format='pdf', dpi=1000)

##SPF()
plt.show()


