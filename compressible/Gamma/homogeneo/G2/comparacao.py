#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


################################################
################################################

M	=	np.loadtxt('../../jhonatanincom/mods1.dat',unpack=True) 
x1_1	=	M[1,:]
y1_1	=	M[2,:]
omega1_1=	M[0,:]
phase1_1=	(omega1_1)/(x1_1)

M	=	np.loadtxt('../../jhonatanincom/mods2.dat',unpack=True) 
x1_2	=	M[1,:]
y1_2	=	M[2,:]
omega1_2=	M[0,:]
phase1_2=	(omega1_2)/(x1_2)
################################################

M	=	np.loadtxt('./taxaJh01/mods1.dat',unpack=True) 
omega2_1=	M[0,:]
x2_1	=	M[1,:]
y2_1	=	M[2,:]
phase2_1=	(omega2_1)/(x2_1)

M	=	np.loadtxt('./taxaJh01/mods2.dat',unpack=True) 
omega2_2=	M[0,:]
x2_2	=	M[1,:]
y2_2	=	M[2,:]
phase2_2=	(omega2_2)/(x2_2)

################################################
M	=	np.loadtxt('./taxaJh025/mods1.dat',unpack=True) 
omega3_1=	M[0,:]
x3_1	=	M[1,:]
y3_1	=	M[2,:]
phase3_1=	(omega3_1)/(x3_1)

M	=	np.loadtxt('./taxaJh025/mods2.dat',unpack=True) 
omega3_2=	M[0,:]
x3_2	=	M[1,:]
y3_2	=	M[2,:]
phase3_2=	(omega3_2)/(x3_2)

################################################
M	=	np.loadtxt('./taxaJh05/mods1.dat',unpack=True) 
omega4_1=	M[0,:]
x4_1	=	M[1,:]
y4_1	=	M[2,:]
phase4_1=	(omega4_1)/(x4_1)

M	=	np.loadtxt('./taxaJh05/mods2.dat',unpack=True) 
omega4_2=	M[0,:]
x4_2	=	M[1,:]
y4_2	=	M[2,:]
phase4_2=	(omega4_2)/(x4_2)
################################################
M	=	np.loadtxt('./taxaJh1/mods1.dat',unpack=True) 
omega5_1=	M[0,:]
x5_1	=	M[1,:]
y5_1	=	M[2,:]
phase5_1=	(omega5_1)/(x5_1)

M	=	np.loadtxt('./taxaJh1/mods2.dat',unpack=True) 
omega5_2=	M[0,:]
x5_2	=	M[1,:]
y5_2	=	M[2,:]
phase5_2=	(omega5_2)/(x5_2)

################################################
M	=	np.loadtxt('./taxaJh10/mods1.dat',unpack=True) 
omega6_1=	M[0,:]
x6_1	=	M[1,:]
y6_1	=	M[2,:]
phase6_1=	(omega6_1)/(x6_1)

M	=	np.loadtxt('./taxaJh10/mods2.dat',unpack=True) 
omega6_2=	M[0,:]
x6_2	=	M[1,:]
y6_2	=	M[2,:]
phase6_2=	(omega6_2)/(x6_2)

################################################
M	=	np.loadtxt('./taxaJh100/mods1.dat',unpack=True) 
omega7_1=	M[0,:]
x7_1	=	M[1,:]
y7_1	=	M[2,:]
phase7_1=	(omega7_1)/(x7_1)

M       =	np.loadtxt('./taxaJh100/mods2.dat',unpack=True) 
x7_2	=	M[1,:]
y7_2	=	M[2,:]
omega7_2=	M[0,:]
phase7_2=	(omega7_2)/(x7_2)

################################################
mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()

ax.legend()
ax.legend(frameon=False)


#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 


plt.xlabel(r' $\mathrm{k_rR_1}$')
plt.ylabel(r'-$\mathrm{k_iR_1}$')
#plt.xlabel(r'$\alpha_r$')
#plt.ylabel(r'$-\alpha_i$')

plt.plot(x1_2,y1_2,'-g',marker='o',markersize=8,markevery=1,label = 'Incompressible')

#########x####################         ########
plt.plot(x2_2,y2_2,dashes=[1, 1],color='slateblue',label = '$\mathrm{\\bar{p}=0.1}$')

#########x####################         ########
plt.plot(x3_2,y3_2,dashes=[2, 1],color='m',label = '$\mathrm{\\bar{p}=0.25}$')

#########x####################         ########
plt.plot(x4_2,y4_2,dashes=[1, 2],color='k',label = '$\mathrm{\\bar{p}=0.5}$')

#########x####################         ########
plt.plot(x5_2,y5_2,dashes=[1, 3],color='b',label = '$\mathrm{\\bar{p}=1.0}$')

#########x####################         ########
plt.plot(x6_2,y6_2,dashes=[3, 1],color='r',label = '$\mathrm{\\bar{p}=10}$')

#########x####################         ########
plt.plot(x7_2,y7_2,dashes=[4, 1],color='c',label = '$\mathrm{\\bar{p}=100}$')


#########x####################         ########
#plt.plot(x4_1,y4_1,'*',color='slateblue',label = 'Spetral Hot' )
#plt.plot(x4_2,y4_2,'*',color='slateblue',label = '')

#########x####################         ########
#plt.plot(x4_1,y4_1,'m',dashes=[2, 1],label = 'Cold' )

#plt.text(1.3, 0.175,'ModeI') 
plt.text(0.2, 0.75,'Mode II') 

ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 5.0, 0.1, 0.80])

params1 = {
 	  'legend.fontsize': 'medium',
	  'legend.frameon':'False'
}

mpl.rcParams.update(params1)

#ax.legend(fontsize='small',)
#ax.legend(frameon=False)
#ax.legend(fontsize='medium')
#legend = ax.legend(frameon=False,fontsize='x-large')
plt.legend()
plt.savefig('alphari.pdf', format='pdf', dpi=1000)

#plt.show()

############################################################################3
mpl.rcParams.update(params)

fig = plt.figure()

ax2 = plt.axes()

ax2.legend()
ax2.legend(frameon=False)

#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 


plt.xlabel(r' $\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{k_iR_1}$')
#plt.xlabel(r'$\alpha_r$')
#plt.ylabel(r'$-\alpha_i$')

#plt.plot(omega1_2,y1_2,'-g',marker='o',markersize=8,markevery=1,label = 'Incompressible')

#########x####################         ########
plt.plot(omega2_2,y2_2,dashes=[1, 1],color='slateblue',label = '$\mathrm{\\bar{p}=0.1}$')

#########x####################         ########
plt.plot(omega3_2,y3_2,dashes=[2, 1],color='m',label = '$\mathrm{\\bar{p}=0.25}$')

#########x####################         ########
plt.plot(omega4_2,y4_2,dashes=[1, 2],color='k',label = '$\mathrm{\\bar{p}=0.5}$')

#########x####################         ########
plt.plot(omega5_2,y5_2,dashes=[1, 3],color='b',label = '$\mathrm{\\bar{p}=1.0}$')

#########x####################         ########
plt.plot(omega6_2,y6_2,dashes=[3, 1],color='r',label = '$\mathrm{\\bar{p}=10}$')

#########x####################         ########
plt.plot(omega7_2,y7_2,dashes=[4, 1],color='c',label = '$\mathrm{\\bar{p}=100}$')


#########x####################         ########
#plt.plot(x4_1,y4_1,'*',color='slateblue',label = 'Spetral Hot' )
#plt.plot(x4_2,y4_2,'*',color='slateblue',label = '')

#########x####################         ########
#plt.plot(x4_1,y4_1,'m',dashes=[2, 1],label = 'Cold' )

#plt.text(1.3, 0.175,'ModeI') 
plt.text(0.2, 0.75,'Mode II') 

ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 1.4, 0.1, 0.80])

plt.legend()
ax2.legend(frameon=False)
plt.savefig('walphai.pdf', format='pdf', dpi=1000)

##############################################################
mpl.rcParams.update(params)

fig = plt.figure()

ax3 = plt.axes()

ax3.legend()
ax3.legend(frameon=False)

#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 


plt.xlabel(r' $\mathrm{\omega}$')
plt.ylabel(r' $\mathrm{C_p}$')
#plt.xlabel(r'$\alpha_r$')
#plt.ylabel(r'$-\alpha_i$')

#plt.plot(omega1_2,y1_2,'-g',marker='o',markersize=8,markevery=1,label = 'Incompressible')

#########x####################         ########
plt.plot(omega2_2,phase2_2,dashes=[1, 1],color='slateblue',label = '$\mathrm{\\bar{p}=0.1}$')

#########x####################         ########
plt.plot(omega3_2,phase3_2,dashes=[2, 1],color='m',label = '$\mathrm{\\bar{p}=0.25}$')

#########x####################         ########
plt.plot(omega4_2,phase4_2,dashes=[1, 2],color='k',label = '$\mathrm{\\bar{p}=0.5}$')

#########x####################         ########
plt.plot(omega5_2,phase5_2,dashes=[1, 3],color='b',label = '$\mathrm{\\bar{p}=1.0}$')

#########x####################         ########
plt.plot(omega6_2,phase6_2,dashes=[3, 1],color='r',label = '$\mathrm{\\bar{p}=10}$')

#########x####################         ########
plt.plot(omega7_2,phase7_2,dashes=[4, 1],color='c',label = '$\mathrm{\\bar{p}=100}$')


#########x####################         ########
#plt.plot(x4_1,y4_1,'*',color='slateblue',label = 'Spetral Hot' )
#plt.plot(x4_2,y4_2,'*',color='slateblue',label = '')

#########x####################         ########
#plt.plot(x4_1,y4_1,'m',dashes=[2, 1],label = 'Cold' )

#plt.text(1.3, 0.175,'ModeI') 
plt.text(0.20, 0.25,'Mode II') 

ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 1.0, 0.2, 0.50])

plt.legend()
ax3.legend(frameon=False)
plt.savefig('wphase.pdf', format='pdf', dpi=1000)

##SPF()
plt.show()


