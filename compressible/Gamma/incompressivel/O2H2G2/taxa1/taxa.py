#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import numpy as np 
import matplotlib.pyplot as plt 

N0  =  30 

omegai      =   0.1
omegaf      =   3.1
iomega      =   N0
domega      =   (omegaf-omegai)/iomega
omega       =   np.zeros(iomega)
omega       =   np.arange(0,iomega,1)*domega+omegai

file1       =   'incompressible2h05'

alphai  =   np.zeros((10000,10000)) 
alphar  =   np.zeros((10000,10000)) 

g       =   open('./%s/taxa%s.dat'%(file1,file1),'w+')

cont2	=   0

for i in range (0,N0):

    M1       =   np.loadtxt('./%s/autospec250i%d.dat'%(file1,i),unpack=True,skiprows=1) 
    M2       =   np.loadtxt('./%s/autospec300i%d.dat'%(file1,i),unpack=True,skiprows=1) 
    N11      =    M1.shape[1]
    N12      =    M1.shape[0]
    #print(N11,N12)
    N21      =    M1.shape[1]
    N22      =    M1.shape[0]
    alphar1  =    M1[0:N11][0]
    alphai1  =   -M1[0:N11][1]
    alphar2  =    M2[0:N21][0]
    alphai2  =   -M2[0:N21][1]
  	
    #plt.figure(0)
    #plt.plot(alphar1,alphai1,'*b')
    #plt.plot(alphar2,alphai2,'.g')
    #plt.show

    cont     =  0

    for j in range (0,N11):

	if (alphai1[j]>0.01 and alphar1[j]>0.01 ):

            for k in range (0,N21):

                if (  (np.abs(alphai1[j]-alphai2[k])<0.005)):

                	if ( (np.abs(alphar1[j]-alphar2[k])<0.005)):

	        #       if (alphar2[k]> 0.0 and (np.abs(alphar1[j]-alphar2[k])<0.005)):
       		             cont    =   cont+1
                    
                     
       		             alphai[cont2,cont]=alphai2[k]
       		             alphar[cont2,cont]=alphar2[k]

       		             #print(cont)



                    #g.write("%f %f\n"%(omega[i],alphai[i,cont])); 

    if (cont>0):
	
	 cont2    =   cont2+1
	
	 #print(cont2)

    print(i,cont2)
   # plt.plot(alphar1,alphai1,'*b')
   # plt.plot(alphar2,alphai2,'*r')

for j in range (0,cont2):

    #g.write("%f\t%f\t%f\n"%(omega[i],alphai[i,1],alphai[i,2])); 
    g.write("%f\t%f\t%f\t%f\t%f\n"%(omega[j],alphai[j,0],alphar[j,0],alphai[j,1],alphar[j,1])); 

g.close

#plt.figure(1)
#plt.plot(omega[0:cont2],alphai[0:cont2,0],'*-r')
#plt.plot(omega[0:cont2],alphai[0:cont2,1],'-b')
#plt.figure(2)
plt.plot(alphar[0:cont2,0],alphai[0:cont2,0],'*b')
plt.plot(alphar[0:cont2,1],alphai[0:cont2,1],'*b')
plt.plot(alphar[0:cont2,3],alphai[0:cont2,3],'*b')
#plt.plot(alphar,alphai,'*b')
#
plt.grid() 
plt.show()


    




