#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


M	=	np.loadtxt('../homogeneo/G2/taxaJh100/mods1.dat',unpack=True) #N11     =	M.shape[1] #N12     =	M.shape[0]

omega1_1=	M[0,:]
x1_1	=	M[1,:]
y1_1	=	M[2,:]
phase1_1=	(omega1_1)/(x1_1)

M	=	np.loadtxt('../homogeneo/G2/taxaJh100/mods2.dat',unpack=True) #N11     =	M.shape[1] #N12     =	M.shape[0]
x1_2	=	M[1,:]
y1_2	=	M[2,:]
omega1_2=	M[0,:]
phase1_2=	(omega1_2)/(x1_2)
################################################

################################################
################################################

M	=	np.loadtxt('./O2H2G2/mods1.dat',unpack=True) 
x2_1	=	M[1,:]
y2_1	=	M[2,:]
omega2_1=	M[0,:]
phase2_1=	(omega2_1)/(x2_1)

M	=	np.loadtxt('./O2H2G2/mods2.dat',unpack=True) 

x2_2	=	M[1,:]
y2_2	=	M[2,:]
omega2_2=	M[0,:]
phase2_2=	(omega2_2)/(x2_2)
#############################################################
M	=	np.loadtxt('./H2O2G2/mods1.dat',unpack=True) 
omega3_1=	M[0,:]
x3_1	=	M[1,:]
y3_1	=	M[2,:]
phase3_1=	(omega3_1)/(x3_1)

M	=	np.loadtxt('./H2O2G2/mods2.dat',unpack=True) 

omega3_2=	M[0,:]
x3_2	=	M[1,:]
y3_2	=	M[2,:]
phase3_2=	(omega3_2)/(x3_2)

M	=	np.loadtxt('./H2O2G2/mods22.dat',unpack=True) 

x3_22	=	M[1,:]
y3_22	=	M[2,:]
omega3_22=	M[0,:]
phase3_22=	(omega3_22)/(x3_22)

################################################

################################################
################################################
######################################################################
M	=	np.loadtxt('../homogeneo/G2/taxaJh1/mods1.dat',unpack=True) #N11     =	M.shape[1] #N12     =	M.shape[0]

x4_1	=	M[1,:]
y4_1	=	M[2,:]
omega4_1=	M[0,:]
phase4_1=	(omega4_1)/(x4_1)

M	=	np.loadtxt('../homogeneo/G2/taxaJh1/mods2.dat',unpack=True) #N11     =	M.shape[1] #N12     =	M.shape[0]
x4_2	=	M[1,:]
y4_2	=	M[2,:]
omega4_2=	M[0,:]
phase4_2=	(omega4_2)/(x4_2)

#############################################################
M	=	np.loadtxt('../species/O2H2G2/mods1.dat',unpack=True) 
x5_1	=	M[1,:]
y5_1	=	M[2,:]
omega5_1=	M[0,:]
phase5_1=	(omega5_1)/(x5_1)

M	=	np.loadtxt('../species/O2H2G2/mods2.dat',unpack=True) 

x5_2	=	M[1,:]
y5_2	=	M[2,:]
omega5_2=	M[0,:]
phase5_2=	(omega5_2)/(x5_2)
#############################################################
M	=	np.loadtxt('../species/H2O2G2/mods1.dat',unpack=True) 
x6_1	=	M[1,:]
y6_1	=	M[2,:]
omega6_1=	M[0,:]
phase6_1=	(omega6_1)/(x6_1)

M	=	np.loadtxt('../species/H2O2G2/mods2.dat',unpack=True) 

x6_2	=	M[1,:]
y6_2	=	M[2,:]
omega6_2=	M[0,:]
phase6_2=	(omega6_2)/(x6_2)

M	=	np.loadtxt('../species/H2O2G2/mods22.dat',unpack=True) 

x6_22	=	M[1,:]
y6_22	=	M[2,:]
omega6_22=	M[0,:]
phase6_22=	(omega6_22)/(x6_22)

################################################
mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()

ax.legend()
ax.legend(frameon=False)

#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 


plt.xlabel(r' $\mathrm{k_r}$')
plt.ylabel(r'-$\mathrm{k_i}$')
#plt.xlabel(r'$\alpha_r$')
#plt.ylabel(r'$-\alpha_i$')

plt.plot(x1_1,y1_1,color='slateblue',linewidth=6,dashes=[1, 0],alpha=0.5 )
plt.plot(x1_2,y1_2,color='slateblue',linewidth=6,dashes=[1, 0],alpha=0.5)

plt.plot(x4_1,y4_1,color='slateblue',dashes=[1, 0],label = 'Homogeneous')
plt.plot(x4_2,y4_2,color='slateblue',dashes=[1, 0])
#########x####################         ########
plt.plot(x2_1,y2_1,'g',linewidth=6,dashes=[1, 1],alpha=0.5 )
plt.plot(x2_2,y2_2,'g',linewidth=6,dashes=[1, 1],alpha=0.5)

plt.plot(x5_1,y5_1,color='g',dashes=[1, 1],label = '$\mathrm{O_2-H_2}$')
plt.plot(x5_2,y5_2,color='g',dashes=[1, 1])
#########x####################         ########
plt.plot(x3_1,y3_1,'m',linewidth=6,dashes=[3, 1],alpha=0.5 )
plt.plot(x3_2,y3_2,'m',linewidth=6,dashes=[3, 1],alpha=0.5)
plt.plot(x3_22,y3_22,'m',linewidth=6,dashes=[3, 1],alpha=0.5)

plt.plot(x6_1,y6_1,color='m',dashes=[3, 1],label = '$\mathrm{H_2-O_2}$')
plt.plot(x6_2,y6_2,color='m',dashes=[3, 1])
plt.plot(x6_22,y6_22,color='m',dashes=[3, 1])

#########x####################         ########

ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 5.3, 0.01, 0.85])

plt.text(4.0, 0.1,r'Mode I')
plt.text(0.8, 0.80,r'Mode II')

plt.legend()
ax.legend(frameon=False)
plt.savefig('alphariG2.pdf', format='pdf', dpi=1000)


################################                                 

fig11 = plt.figure()
ax11  = plt.axes()

plt.xlabel(r' $\mathrm{k_r}$')
plt.ylabel(r'-$\mathrm{k_i}$')

plt.plot(x1_1,y1_1,color='slateblue',linewidth=6,alpha=0.5,dashes=[1, 0])                        
plt.plot(x4_1,y4_1,color='slateblue',dashes=[1, 0],label = 'Homogeneous')
################################                                 
plt.plot(x2_1,y2_1, color='g',linewidth=6,alpha=0.5,dashes=[1, 1])                        
plt.plot(x5_1,y5_1, color='g',dashes=[1, 1],label ='$\mathrm{O_2-H_2}$')
################################                                 
plt.plot(x3_1,y3_1,'m',dashes=[3, 1],linewidth=6,alpha=0.5,)                        
plt.plot(x6_1,y6_1,'m',dashes=[3, 1],label ='$\mathrm{H_2-O_2}$')                        
ax11.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax11.yaxis.set_major_locator(plt.MultipleLocator(0.05))
plt.axis([0.1, 5.0, 0.01, 0.15])

plt.text(0.5, 0.125,r'Mode I')

plt.legend()
ax11.legend(frameon=False)
plt.savefig('alphariG2MI.pdf', format='pdf', dpi=1000)

fig12 = plt.figure()
ax12  = plt.axes()

plt.xlabel(r' $\mathrm{k_r}$')
plt.ylabel(r'-$\mathrm{k_i}$')

################################                                 
plt.plot(x1_2,y1_2,color='slateblue',linewidth=6,alpha=0.5,dashes=[1, 0])                        
plt.plot(x4_2,y4_2,color='slateblue',dashes=[1, 0],label = 'Homogeneous')
################################                                 
plt.plot(x2_2,y2_2, color='g',linewidth=6,alpha=0.5,dashes=[1, 1])                        
plt.plot(x5_2,y5_2, color='g',dashes=[1, 1],label ='$\mathrm{O_2-H_2}$')
################################                                 
plt.plot(x3_2,y3_2,'m',dashes=[3, 1],linewidth=6,alpha=0.5,)                        
plt.plot(x3_22,y3_22,'m',dashes=[3, 1],linewidth=6,alpha=0.5,)                        
plt.plot(x6_2,y6_2,'m',dashes=[3, 1],label ='$\mathrm{H_2-O_2}$')                        
plt.plot(x6_22,y6_22,'m',dashes=[3, 1])                        

ax12.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax12.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.01, 3.5, 0.01, 0.8])

plt.text(3.0, 0.7,r'Mode II')

plt.legend()
ax12.legend(frameon=False)
plt.savefig('alphariG2MII.pdf', format='pdf', dpi=1000)

###############################################33
mpl.rcParams.update(params)

fig2 = plt.figure()
ax2  = plt.axes()

plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{k_i}$')

plt.plot(omega1_1,y1_1,color='slateblue',linewidth=6,alpha=0.5,dashes=[1, 0])
plt.plot(omega1_2,y1_2,color='slateblue',linewidth=6,alpha=0.5,dashes=[1, 0])                        
plt.plot(omega4_1,y4_1,color='slateblue',dashes=[1, 0],label = 'Homogeneous')
plt.plot(omega4_2,y4_2,color='slateblue',dashes=[1, 0])
###############################                                 
plt.plot(omega2_1,y2_1, color='g',linewidth=6,alpha=0.5,dashes=[1, 1])
plt.plot(omega2_2,y2_2, color='g',linewidth=6,alpha=0.5,dashes=[1, 1])                        
plt.plot(omega5_1,y5_1, color='g',dashes=[1, 1],label ='$\mathrm{O_2-H_2}$')
plt.plot(omega5_2,y5_2, color='g',dashes=[1, 1])                        
################################                                 
plt.plot(omega3_1,y3_1,'m',dashes=[3, 1],linewidth=6,alpha=0.5)
plt.plot(omega3_2,y3_2,'m',dashes=[3, 1],linewidth=6,alpha=0.5)                        
plt.plot(omega3_22,y3_22,'m',dashes=[3, 1],linewidth=6,alpha=0.5)                        

plt.plot(omega6_1,y6_1,'m',dashes=[3, 1],label ='$\mathrm{H_2-O_2}$')
plt.plot(omega6_2,y6_2,'m',dashes=[3, 1])                        
plt.plot(omega6_22,y6_22,'m',dashes=[3, 1])                        
################################                                 
#############
ax2.xaxis.set_major_locator(plt.MultipleLocator(0.3))
ax2.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 3.0, 0.01, 0.8])

plt.text(2.0, 0.15,r'Mode I')
plt.text(0.5, 0.7,r'Mode II')

plt.legend()
ax2.legend(frameon=False)
plt.savefig('walphaiG2.pdf', format='pdf', dpi=1000)

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$4

fig22 = plt.figure()
ax22  = plt.axes()

plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{k_i}$')

################################                                 
plt.plot(omega1_2,y1_2,color='slateblue',linewidth=6,alpha=0.5,dashes=[1, 0])                        
plt.plot(omega4_2,y4_2,color='slateblue',dashes=[1, 0],label = 'Homogeneous')
################################                                 
plt.plot(omega2_2,y2_2, color='g',linewidth=6,alpha=0.5,dashes=[1, 1])                        
plt.plot(omega5_2,y5_2, color='g',dashes=[1, 1],label ='$\mathrm{O_2-H_2}$')
################################                                 
plt.plot(omega3_2,y3_2,'m',dashes=[3, 1],linewidth=6,alpha=0.5,)                        
plt.plot(omega3_22,y3_22,'m',dashes=[3, 1],linewidth=6,alpha=0.5,)                        
plt.plot(omega6_2,y6_2,'m',dashes=[3, 1],label ='$\mathrm{H_2-O_2}$')                        
plt.plot(omega6_22,y6_22,'m',dashes=[3, 1])                        
################################                                 
ax22.xaxis.set_major_locator(plt.MultipleLocator(0.1))
ax22.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 1.0, 0.01, 0.8])

plt.text(0.7, 0.7,r'Mode II')

plt.legend()
ax22.legend(frameon=False)
plt.savefig('walphaiG2MII.pdf', format='pdf', dpi=1000)

###############################################33
mpl.rcParams.update(params)

fig2 = plt.figure()
ax2  = plt.axes()


plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'$\mathrm{C_p}$')


plt.plot(omega1_1,phase1_1,color='slateblue',linewidth=6,alpha=0.5,dashes=[1, 0])
plt.plot(omega1_2,phase1_2,color='slateblue',linewidth=6,alpha=0.5,dashes=[1, 0])                        
plt.plot(omega4_1,phase4_1,color='slateblue',dashes=[1, 0],label = 'Homogeneous')
plt.plot(omega4_2,phase4_2,color='slateblue',dashes=[1, 0])                        
################################                                 
plt.plot(omega2_1,phase2_1, color='g',linewidth=6,alpha=0.5,dashes=[1, 1])
plt.plot(omega2_2,phase2_2, color='g',linewidth=6,alpha=0.5,dashes=[1, 1])                        
plt.plot(omega5_1,phase5_1, color='g',dashes=[1, 1],label ='$\mathrm{O_2-H_2}$')
plt.plot(omega5_2,phase5_2, color='g',dashes=[1, 1])                        
################################                                 
plt.plot(omega3_1,phase3_1,'m',linewidth=6,alpha=0.5,dashes=[3, 1])
plt.plot(omega3_2,phase3_2,'m',linewidth=6,alpha=0.5,dashes=[3, 1])                        
plt.plot(omega3_22,phase3_22,'m',linewidth=6,alpha=0.5,dashes=[3, 1])                        
plt.plot(omega6_1,phase6_1,'m',dashes=[3, 1],label ='$\mathrm{H_2-O_2}$')
plt.plot(omega6_2,phase6_2,'m',dashes=[3, 1])                        
plt.plot(omega6_22,phase6_22,'m',dashes=[3, 1])                        
################################                                 
############
ax2.xaxis.set_major_locator(plt.MultipleLocator(0.3))
ax2.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 3.0, 0.2, 0.70])

plt.text(2.0, 0.5,r'Mode I')
plt.text(0.5, 0.3,r'Mode II')

plt.legend(loc=2)

ax2.legend(frameon=False)
plt.savefig('phaseG2.pdf', format='pdf', dpi=1000)


###############################################33
mpl.rcParams.update(params)

fig3 = plt.figure()
ax3  = plt.axes()


plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'$\mathrm{C_p}$')

plt.plot(omega1_2,phase1_2,color='slateblue',linewidth=6,alpha=0.5,dashes=[1, 0])                        
plt.plot(omega4_2,phase4_2,color='slateblue',dashes=[1, 0],label = 'Homogeneous')                        
################################                                 
plt.plot(omega2_2,phase2_2, color='g',linewidth=6,alpha=0.5,dashes=[1, 1])                        
plt.plot(omega5_2,phase5_2, color='g',dashes=[1, 1],label ='$\mathrm{O_2-H_2}$')                        
################################                                 
plt.plot(omega3_2,phase3_2,'m',linewidth=6,alpha=0.5,dashes=[3, 1])                        
plt.plot(omega3_22,phase3_22,'m',linewidth=6,alpha=0.5,dashes=[3, 1])                        
plt.plot(omega6_2,phase6_2,'m',dashes=[3, 1])                        
plt.plot(omega6_22,phase6_22,'m',dashes=[3, 1],label ='$\mathrm{H_2-O_2}$')                        
################################                                 
############
ax3.xaxis.set_major_locator(plt.MultipleLocator(0.1))
ax3.yaxis.set_major_locator(plt.MultipleLocator(0.05))
plt.axis([0.01, 1.0, 0.2, 0.60])

plt.text(2.0, 0.5,r'Mode I')

plt.legend(loc=2)

ax3.legend(frameon=False)
plt.savefig('phaseG2modI.pdf', format='pdf', dpi=1000)
#
#
#
##plt.plot(X[10],Y[10],'ko')
##plt.plot(X[11],Y[11],'ko')
##plt.plot(X,Y,'k.')
##SPF()
plt.show()


