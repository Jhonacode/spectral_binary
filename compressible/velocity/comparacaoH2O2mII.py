#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


M	=	np.loadtxt('./H2O2h05/mods1.dat',unpack=True) #N11     =	M.shape[1] #N12     =	M.shape[0]

omega1_1=	M[0,:]
x1_1	=	M[1,:]
y1_1	=	M[2,:]
phase1_1=	(omega1_1)/(x1_1)

M	=	np.loadtxt('./H2O2h05/mods2.dat',unpack=True) 
omega1_2=	M[0,:]
x1_2	=	M[1,:]
y1_2	=	M[2,:]

phase1_2=	(omega1_2)/(x1_2)


M	 =	np.loadtxt('./H2O2h05/mods22.dat',unpack=True) 
omega1_22=	M[0,:]
x1_22	 =	M[1,:]
y1_22	 =	M[2,:]

phase1_22=	(omega1_22)/(x1_22)
################################################

M	=	np.loadtxt('../Gamma/species/H2O2G2/mods1.dat',unpack=True) 
omega2_1=	M[0,:]
x2_1	=	M[1,:]
y2_1	=	M[2,:]
phase2_1=	(omega2_1)/(x2_1)

M	=	np.loadtxt('../Gamma/species/H2O2G2/mods2.dat',unpack=True) 

omega2_2=	M[0,:]
x2_2	=	M[1,:]
y2_2	=	M[2,:]
phase2_2=	(omega2_2)/(x2_2)

M	=	np.loadtxt('../Gamma/species/H2O2G2/mods22.dat',unpack=True) 

omega2_22=	M[0,:]
x2_22	 =	M[1,:]
y2_22	 =	M[2,:]
phase2_22=	(omega2_22)/(x2_22)

################################################

M	=	np.loadtxt('./H2O2h09/mods1.dat',unpack=True) 
x3_1	=	M[1,:]
y3_1	=	M[2,:]
omega3_1=	M[0,:]
phase3_1=	(omega3_1)/(x3_1)

M	=	np.loadtxt('./H2O2h09/mods2.dat',unpack=True) 
x3_2	=	M[1,:]
y3_2	=	M[2,:]
omega3_2=	M[0,:]
phase3_2=	(omega3_2)/(x3_2)

M	 =	np.loadtxt('./H2O2h09/mods22.dat',unpack=True) 
x3_22	 =	M[1,:]
y3_22	 =	M[2,:]
omega3_22=	M[0,:]
phase3_22=	(omega3_22)/(x3_22)

################################################

M	=	np.loadtxt('./h05/mods1.dat',unpack=True) 
x4_1	=	M[1,:]
y4_1	=	M[2,:]
omega4_1=	M[0,:]
phase4_1=	(omega4_1)/(x4_1)

M	=	np.loadtxt('./h05/mods2.dat',unpack=True) 

x4_2	=	M[1,:]
y4_2	=	M[2,:]
omega4_2=	M[0,:]
phase4_2=	(omega4_2)/(x4_2)

################################################

M	=	np.loadtxt('./H2O2G4h05/mods1.dat',unpack=True) 
x5_1	=	M[1,:]
y5_1	=	M[2,:]
omega5_1=	M[0,:]
phase5_1=	(omega5_1)/(x5_1)

M	=	np.loadtxt('./H2O2G4h05/mods2.dat',unpack=True) 

x5_2	=	M[1,:]
y5_2	=	M[2,:]
omega5_2=	M[0,:]
phase5_2=	(omega5_2)/(x5_2)

M	=	np.loadtxt('./H2O2G4h05/mods22.dat',unpack=True) 

x5_22	=	M[1,:]
y5_22	=	M[2,:]
omega5_22=	M[0,:]
phase5_22=	(omega5_22)/(x5_22)

################################################

mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()

ax.legend()
ax.legend(frameon=False)

#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 

plt.xlabel(r' $\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{k_i}$')
#plt.xlabel(r'$\alpha_r$')
#plt.ylabel(r'$-\alpha_i$')

#plt.plot(x1_1,y1_1,color='slateblue',dashes=[1, 0],label = '$\mathrm{h=0.5}$' )
plt.plot(omega1_2,y1_2,color='slateblue',dashes=[1, 0],label = '$\mathrm{h=0.5}$')
plt.plot(omega1_22,y1_22,color='slateblue',dashes=[1, 0])
#########x####################         ########
#plt.plot(x2_1,y2_1,'g'  ,dashes=[1, 1],label = '$\mathrm{h=0.7}$' )
plt.plot(omega2_2,y2_2,'g'  ,dashes=[1, 1],label = '$\mathrm{h=0.7}$')
plt.plot(omega2_22,y2_22,'g',dashes=[1, 1])
#########x####################         ########
#plt.plot(x3_1,y3_1,'m'  ,dashes=[3, 1],label = '$\mathrm{h=0.9}$' )
plt.plot(omega3_2,y3_2,'m'  ,dashes=[3, 1],label = '$\mathrm{h=0.9}$')
plt.plot(omega3_22,y3_22,'m',dashes=[3, 1])
#########x####################         ########
#plt.plot(x4_1, y4_1, 'b'  ,dashes=[1, 2],label = '$\mathrm{h=0.5, \\bar{\\rho}=1}$' )
plt.plot(omega4_2, y4_2, 'b'  ,dashes=[1, 2],label = '$\mathrm{h=0.5, \\bar{\\rho}=1}$')
#########x####################         ########
#plt.plot(x5_1, y5_1, 'c'  ,dashes=[1, 2],label = '$\mathrm{h=0.5, \\bar{\\rho}=1}$' )
plt.plot(omega5_22, y5_22, 'c',dashes=[2, 1],label = '$\mathrm{h=0.5, \Gamma=4}$')
#plt.plot(x5_2,y5_2,'c',dashes=[2, 1])

plt.text(0.2, 0.80,'Mode II') 

ax.xaxis.set_major_locator(plt.MultipleLocator(0.250))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.legend(loc=2)
plt.axis([0.05, 1.7, 0.01, 0.85])

#plt.legend(loc='best')
#plt.legend()

ax.legend(loc='upper right',frameon=False)
plt.savefig('walphaiH2O2mII.pdf', format='pdf', dpi=1000)


###############################################33
mpl.rcParams.update(params)

fig2 = plt.figure()
ax2  = plt.axes()


plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{k_i}$')


plt.plot(omega1_1,y1_1,color='slateblue'  ,dashes=[1, 0],label ='$\mathrm{h=0.5}$')
#plt.plot(omega1_2,y1_2,color='slateblue'  ,dashes=[1, 0])                        
#plt.plot(omega1_22,y1_22,color='slateblue',dashes=[1, 0])                        
################################                                 
plt.plot(omega2_1,y2_1, color='g'  ,dashes=[1, 1],label ='$\mathrm{h=0.7}$')
#plt.plot(omega2_2,y2_2, color='g'  ,dashes=[1, 1])                        
#plt.plot(omega2_22,y2_22, color='g',dashes=[1, 1])                        
################################                                 
plt.plot(omega3_1,y3_1,'m'  ,dashes=[3, 1],label ='$\mathrm{h=0.9}$')
#plt.plot(omega3_2,y3_2,'m'  ,dashes=[3, 1])                        
#plt.plot(omega3_22,y3_22,'m',dashes=[3, 1])                        
################################                                 
plt.plot(omega4_1, y4_1, 'b'  ,dashes=[1, 2],label = '$\mathrm{h=0.5, \\bar{\\rho}=1}$' )
#plt.plot(omega4_2, y4_2, 'b'  ,dashes=[1, 2])
################################
plt.plot(omega5_1, y5_1, 'c'  ,dashes=[2, 1],label = '$\mathrm{h=0.5,\\Gamma=4}$' )

plt.text(0.25, 0.25,'Mode I') 
#############
ax2.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax2.yaxis.set_major_locator(plt.MultipleLocator(0.05))
plt.axis([0.01, 4.5, 0.001, 0.28])

plt.legend()
ax2.legend(frameon=False)
plt.savefig('walphaiH2O2mI.pdf', format='pdf', dpi=1000)

###############################################33
mpl.rcParams.update(params)

fig2 = plt.figure()
ax2  = plt.axes()


plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'$\mathrm{C_p}$')


plt.plot(omega1_1,phase1_1,color='slateblue',dashes=[1, 0],label ='$\mathrm{h=0.5}$')
plt.plot(omega1_2,phase1_2,color='slateblue',dashes=[1, 0])                        
plt.plot(omega1_22,phase1_22,color='slateblue',dashes=[1, 0])                        
################################                                 
plt.plot(omega2_1,phase2_1, color='g',dashes=[1, 1],label ='$\mathrm{h=0.7}$')
plt.plot(omega2_2,phase2_2, color='g',dashes=[1, 1])                        
plt.plot(omega2_22,phase2_22, color='g',dashes=[1, 1])                        
################################                                 
plt.plot(omega3_1,phase3_1,'m',dashes=[3, 1],label ='$\mathrm{h=0.9}$')
plt.plot(omega3_2,phase3_2,'m',dashes=[3, 1])                        
plt.plot(omega3_22,phase3_22,'m',dashes=[3, 1])                        
################################                                 
plt.plot(omega4_1, phase4_1, 'b'  ,dashes=[1, 2],label = '$\mathrm{h=0.5, \\bar{\\rho}=1}$' )
plt.plot(omega4_2, phase4_2, 'b'  ,dashes=[1, 2])
################################                                 
plt.plot(omega5_1,phase5_1,'c',dashes=[2, 1],label ='$\mathrm{h=0.5,\\Gamma=4.0}$')
plt.plot(omega5_2,phase5_2,'c',dashes=[2, 1])                        
plt.plot(omega5_22,phase5_22,'c',dashes=[2, 1])                        

plt.text(2.0, 0.45,'Mode I') 
plt.text(0.8, 0.2,'Mode II') 
#plt.plot(omega4_1,phase4_1,'k',dashes=[2, 1],label ='$\mathrm{\Gamma=4.0}$')
#plt.plot(omega4_2,phase4_2,'k',dashes=[2, 1])                        
################################                                 
#############
ax2.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax2.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 2.6, 0.1, 0.65])

#plt.legend(loc=2)
plt.legend(loc='best')

ax2.legend(frameon=False)
plt.savefig('phaseH2O2mIII.pdf', format='pdf', dpi=1000)

plt.show()


