#matplotlib inline
#config InlineBackend.figure_format='svg'
from   chebPy        import *
from   mapping       import *
from   Baseflow      import *
from   Boundary      import *
from   scipy.linalg  import block_diag
from   Parameters    import *
import numpy 	         as np 
import matplotlib.pyplot as pl
import diffusion         as df
import sys 
import os 

def p_spectral_matrices(NN,maxr,iomega):

	# number of collocation nodes=N+1, because the cheb func
	D,n1	= cheb(NN)
	#D2	= dot(D,D)
	#D2[1:N,1:N]
	
	# Mappint to concentrate the point in 0. Withou using 0
	# r= Radial coordinate
	r   	= mappingtan(NN,n1,maxr,D)
	D2	= dot(D,D)
	#Base Flow

        
	Wb,Tb,Rhob,Y1,Y2  =   Baseflow(r,NN)
        #Fortran program diffusion.f90 
        #   This program calculate the diffusion properties of  
        #   the differens species and the mixture of gases. 
        
        #To run the fortran subroutine.
        #f2py -m diffusion -c  global.f90 diffusion.f90 
        
	(mib,kb,cpb,cpb1,cpb2,D1m,Mw,Ru,Rmix,gammab,gamma_ref,gamma_j,gamma_ratio,P_r,a_ref,a_s,a_1,a_2)=\
        df.properties.init_diffusion(Y1,Y2,Tb,T_i,T_0,T_ref,NN)

	#plt.plot(cpb,r)
	#plt.plot(gammab,r)
	#plt.show()

        #Non dimensional gas ideal equation \bar{p}=1/{gamma_ref}
        Rhob  =  1.0/(Tb*Rmix) 

	###########CONVECTIVE MACH NUMBER

	if iomega == 0:

		for j in range(0,NN+1):


        	    if (R1-0.05<r[j]<R1+0.05):

        	        M1      =   Wb[j]
        	        rho1    =   Rhob[j]
        	        T1      =   Tb[j]

        	    if (R1*Gamma-0.05<r[j]<R1*Gamma+0.05):

        	        M2      =   Wb[j]
        	        rho2    =   Rhob[j]
        	        T2      =   Tb[j]

        	Srho= rho2/rho1

		print'Mach1',M1,'Mach2',M2

		Rratio	=   M2/M1

		Uc_1    = (1.0+Rratio*np.sqrt(gamma_ratio))/(1.0+np.sqrt(Srho))

        	Mc1_1  	= (M1-Uc_1/a_1) 

        	Mc1_2  	= (Uc_1/a_2-M2) 

		print'Convective Mach',Mc1_1,'Convective Mach',Mc1_2

		#M1 =M_o
		#M2 =M_0

		Rratio		=   	M2/M1
        	Srho		=	1#rho1/rho1
		gamma_ratio	=	1

		Uc_2    = (1.0+Rratio*np.sqrt(gamma_ratio))/(1.0+np.sqrt(Srho))

        	Mc2_1  	= (M1-Uc_2/a_1) 

        	Mc2_2  	= (Uc_2/a_2-M2) 

		print'Convective Mach 2',Mc2_1,'Convective Mach 2',Mc2_2
		

        g   	=   open('baseflow.dat','w+') 
	


	for j in range (0,NN+1):
	
	            g.write("%f\t%f\t%f\t%f\t%f\t%f\t%f\n"%(r[j],Wb[j],Tb[j],Rhob[j],a_s[j],Y1[j],Y2[j]));

    
        g.close()
        

	
	dWbdr      =   dot(D,Wb)
	dRhobdr    =   dot(D,Rhob)
	#Species
	dRmixbdr    =   dot(D,Rmix)
	
	
	## Especial Matrices
	Ov      =   np.zeros((1,(3*(NN+1))))
	Z   	=   np.zeros((NN+1,NN+1))
	I   	=   np.identity(NN+1) 
	II      =   np.identity(3*(NN+1))
	Ee 	=   block_diag(I,I,I)
	DD      =   block_diag(D,D,D);
	
	#Cartesian
	#A   =   np.block([[diag(M),diag(Rho),Z,Z],[Z,diag(M),Z,diag(1./Rho)],[Z,Z,diag(M),Z],[Z,I,Z,diag(M)]])
	#B   =   np.block([[Z,Z,diag(Rho),Z],[Z,Z,Z,Z],[Z,Z,Z,diag(1./Rho)],[Z,Z,I,Z]])
	#C   =   np.block([[Z,Z,diag(Rhoy),Z],[Z,Z,diag(My),Z],[Z,Z,Z,Z],[Z,Z,Z,Z]])


	#P base flow 
	#joncas incompressible
        #barP   =   gamma*(1.0/(gamma*M0**2.0))
        #barP   =   gamma*(1.0/(gamma*0.001**2.0))
	#jhonatan Parameter Jh
        #barP    =   gamma*(1.0/(gamma))
        barP   =   gamma_ref*gammab*(1.0/(gamma_ref))*Jh

	#Cylindrical
	Ae   =   np.block([
                          [Z         ,Z,          diag(1.0/Rhob)],
                          [Z         ,Z,          Z		],
                          [I*barP    ,Z,          Z		]
                          ])

	Be   =   np.block([
                          [diag(Wb)   ,Z          ,Z		],
                          [Z          ,diag(Wb)   ,diag(1.0/Rhob)],
                          [Z          ,I*barP     ,diag(Wb)] 
			  ])


	Cs      =  diag(barP*(1/r))


	Ce   	=  np.block([
                            [Z          ,Z,Z],
                            [diag(dWbdr),Z,Z],
                            [Cs         ,Z,Z]
                            ])
	
	
	A0      =   -np.matmul(Ae,DD)-Ce;

	B0      =   np.multiply(1j,Be);
	

	return A0,Ee,B0




