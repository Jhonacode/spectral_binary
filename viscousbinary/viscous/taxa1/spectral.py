#matplotlib inline
#config InlineBackend.figure_format='svg'
from   chebPy        import *
from   mapping       import *
from   Baseflow      import *
from   Boundary      import *
from   scipy.linalg  import block_diag
from   Parameters    import *
import numpy 	         as np 
import matplotlib.pyplot as pl
import diffusion         as df
import sys 
import os 

def p_spectral_matrices(NN,maxr,iomega):

	# number of collocation nodes=N+1, because the cheb func
	D,n1	= cheb(NN)
	#D2	= dot(D,D)
	#D2[1:N,1:N]
	
	# Mappint to concentrate the point in 0. Withou using 0
	# r= Radial coordinate
	r   	= mappingtan(NN,n1,maxr,D)
	D2	= dot(D,D)
	#Base Flow

        
	Wb,Tb,Rhob,Y1,Y2  =   Baseflow(r,NN)



        #Fortran program diffusion.f90 
        #   This program calculate the diffusion properties of  
        #   the differens species and the mixture of gases. 
        
        #To run the fortran subroutine.
        #f2py -m diffusion -c  global.f90 diffusion.f90 
        
	(mib,kb,cpb,cpb1,cpb2,D1m,Mw,Ru,Rmix,gammab,gamma_j,gamma_ratio,P_r,a_ref,a_s,a_1,a_2)=\
        df.properties.init_diffusion(Y1,Y2,Tb,T_i,T_0,T_ref,NN)

	

        #M1  =   M_i
        #M2  =   M_o#M_i*h

        #Mc  =   (M1*a_1-M2*a_2)/(a_1+a_2)


	#print'Convective Mach',Mc


        #####
        #Non dimensional gas ideal equation \bar{p}=1/{gamma_ref}
        Rhob  =  1.0/(Tb*Rmix) 

	###########CONVECTIVE MACH NUMBER

	if iomega == 0:

		for j in range(0,NN+1):


        	    if (R1-0.05<r[j]<R1+0.05):

        	        M1      =   Wb[j]
        	        rho1    =   Rhob[j]
        	        T1      =   Tb[j]

        	    if (R1*Gamma-0.05<r[j]<R1*Gamma+0.05):

        	        M2      =   Wb[j]
        	        rho2    =   Rhob[j]
        	        T2      =   Tb[j]

        	Srho= rho2/rho1

		print'Mach1',M1,'Mach2',M2

		Rratio	=   M2/M1

		Uc_1    = (1.0+Rratio*np.sqrt(gamma_ratio))/(1.0+np.sqrt(Srho))

        	Mc1_1  	= (M1-Uc_1/a_1) 

        	Mc1_2  	= (Uc_1/a_2-M2) 

		print'Convective Mach',Mc1_1,'Convective Mach',Mc1_2

		#M1 =M_o
		#M2 =M_0

		Rratio		=   	M2/M1
        	Srho		=	1#rho1/rho1
		gamma_ratio	=	1

		Uc_2    = (1.0+Rratio*np.sqrt(gamma_ratio))/(1.0+np.sqrt(Srho))

        	Mc2_1  	= (M1-Uc_2/a_1) 

        	Mc2_2  	= (Uc_2/a_2-M2) 

		print'Convective Mach 2',Mc2_1,'Convective Mach 2',Mc2_2
		

        g   	=   open('baseflow.dat','w+') 
	

        #Mc_1  = 1.0/(1.0+np.sqrt(Srho))*np.sqrt(gamma_ratio)*M2*(1.0/(M2/M1)-1.0)

        #Mc_2  = M_i*(1.0-h)/(1.0+np.sqrt(gamma_ratio/Srho))

        #print'Convective Mach Jackson',Mc_1,Mc_2,T1,T2

        #sys.exit()

	for j in range (0,NN+1):
	
	            g.write("%f\t%f\t%f\t%f\t%f\t%f\t%f\n"%(r[j],Wb[j],Tb[j],Rhob[j],a_s[j],Y1[j],Y2[j]));

    
    #dWbdr   =   np.gradient(Wb, dr)

    
        g.close()
        

	
	dWbdr      =   dot(D,Wb)
	dRhobdr    =   dot(D,Rhob)
	
        lambdab     =   zeros(N+1)
        lambdab     =   -2.0/3.0*mib
        
        #Universal molecular gas constant  
        #[J/(g*K)]=[KJ/(kg*K)]=m^2/(s^2*K)

        Rg          =   Ru/Mw
        gamma       =   1.0/(1.0-Rg/cp_r)     
        gammab      =   1.0/(1.0-Rg/cpb)     
        
        dmibdr      =   dot(D,lambdab)
        dlambdabdr  =   dot(D,lambdab)
        
        
        #Non Dimensional numbers
        #a_r=    np.sqrt(T_r/(1.0/Rg-1.0/cp_r))     
        #Rg*1000=[J/g*K]*1000=[J/kg*K]
        a_r     =   np.sqrt(gamma*Rg*1e3*T_r)     
        rho_0   =   0.575#(kg/m^3)atT_r=0.5 
        #rho_0   =   1.16 #(kg/m^3)atT_r=1 
        
        W_r=    M_r*a_r
        rho_r=(1.0/1.0)*rho_0#=1/(Tb_r)d
        
        print(rho_r,M_r,W_r,mi_r,theta1)
        
        #Re  =   3000
        #Re  =   1E8
        #Re=(rho_r*W_r*R1)/(mi_r*1e-7)*theta1
        RR  =   Re*(mi_r*1e-7)/(rho_r*W_r)
        print(RR)
        
        Pr  =   1.0
        
        # coordinate selectors
        
        rr  =   np.arange(0,N,1)#0:N+1      
        uu  =   rr+N
        vv  =   uu+N
        pp  =   vv+N
        
        Z   =   zeros((N+1,N+1))
        I   =   identity(N+1) 
        
        ##############################
        #Elemental Matrix
	## Especial Matrices

	Ov      =   np.zeros((1,(8*(NN+1))))
	Z   	=   np.zeros((NN+1,NN+1))
	I   	=   np.identity(NN+1) 
	II      =   np.identity(4*(NN+1))
	Ee 	=   block_diag(I,I,I,I)
	DD      =   block_diag(D,D,D,D);
        DD2     =   block_diag(D2,D2,D2,D2);
	

        
        #Cylindrical
        Ae   =   np.block([[Z,diag(Rhob),Z,Z],[Z,Z,Z,diag(1.0/Rhob)],[Z,Z,Z,Z],[Z,I,Z,Z]])
        Be   =   np.block([[diag(Wb),Z,diag(Rhob),Z],[Z,diag(Wb),Z,Z],[Z,Z,diag(Wb),diag(1.0/Rhob)],[Z,Z,I,diag(Wb)]])
        
        C12     =   diag(Rhob/r) 
        C42     =   diag(1.0/r) 
        Ce      =   np.block([[Z,diag(dRhobdr)+C12,Z,Z],[Z,Z,Z,Z],[Z,diag(dWbdr),Z,Z],[Z,C42,Z,Z]])
        
        D_u     =   diag(lambdab)+diag(2.0*mib)
        De      =   np.block([[Z,Z,Z,Z],[Z,D_u,Z,Z],[Z,Z,diag(mib),Z],[Z,Z,Z,Z]])
        
        E_w     =   diag(lambdab)+diag(2.0*mib)
        Ee      =   np.block([[Z,Z,Z,Z],[Z,diag(mib),Z,Z],[Z,Z,E_w,Z],[Z,Z,Z,Z]])
        
        F_u     =   diag(lambdab)+diag(mib)
        F_w     =   diag(lambdab)+diag(mib)
        Fe      =   np.block([[Z,Z,Z,Z],[Z,Z,F_w,Z],[Z,F_u,Z,Z],[Z,Z,Z,Z]])
        
        Gu_1    =   diag(2.0*lambdab/r)
        Gu_4    =   diag(2.0*mib/r)
        G_u     =   Gu_1+diag(2*dmibdr)+diag(dlambdabdr)+Gu_4 
        Gw_2    =   diag(mib/r)
        #
        #cuidado tem que ver isto aqui se faz diferenza
        #G_w     =   diag(dmibdr)+Gu_4 
        G_w     =   diag(dmibdr)+Gw_2 
        #
        K_e     =   diag(mib*(gammab-1.0)*dWbdr) 
        Ge      =   np.block([[Z,Z,Z,Z],[Z,G_u,Z,Z],[Z,Z,G_w,Z],[Z,Z,K_e,Z]])
        
        Hu_2    =   diag(mib/r)
        Hu_3    =   diag(lambdab/r)
        H_u     =   diag(dmibdr)+Hu_2+Hu_3
        Hw_1    =   diag(lambdab/r) 
        H_w     =   Hu_3+diag(dlambdabdr)
        He      =   np.block([[Z,Z,Z,Z],[Z,Z,H_w,Z],[Z,H_u,Z,Z],[Z,K_e,Z,Z]])
        
        
        Iu_1    =   diag(dlambdabdr*1.0/r)
        I_u     =   Iu_1
        Ie      =   np.block([[Z,Z,Z,Z],[Z,I_u,Z,Z],[Z,Z,Z,Z],[Z,Z,Z,Z]])
        
        
        A2_1    =  diag((1.0/(Re)*1.0/Rhob))
        A2_2    =  np.block([[A2_1,Z,Z,Z],[Z,A2_1,Z,Z],[Z,Z,A2_1,Z],[Z,Z,Z,A2_1]])
        A2      =  matmul(A2_2,Ee)
        
        A1_1    =  np.multiply(1j,Be)
        A1_21   =  diag(-1j*1.0/(Re)*1.0/Rhob)
        A1_22   =  np.block([[A1_21,Z,Z,Z],[Z,A1_21,Z,Z],[Z,Z,A1_21,Z],[Z,Z,Z,A1_21]])
        A1_23   =  matmul(A1_22,Fe)
        A1_2    =  matmul(A1_23,DD)
        A1_3    =  matmul(A1_22,He)
        
        A1      =   A1_1+A1_2+A1_3
        
        
        #In the loop
        #A0_1    =  np.multiply((-1j*omega[iomega]),II)
        A0_2    =  matmul(Ae,DD)
        A0_3    =  Ce 
        A0_41   =  diag(-1.0/(Re)*1.0/Rhob)
        A0_42   =  np.block([[A0_41,Z,Z,Z],[Z,A0_41,Z,Z],[Z,Z,A0_41,Z],[Z,Z,Z,A0_41]])
        A0_43   =  matmul(A0_42,De)
        A0_4    =  matmul(A0_43,DD2)
        A0_51   =  matmul(A0_42,Ge)
        A0_5    =  matmul(A0_51,DD)
        A0_6    =  matmul(A0_42,Ie)
        
        A0_1    =   np.multiply((-1j*omega[iomega]),II)
        A0      =   A0_1+A0_2+A0_3+A0_4+A0_5+A0_6
        
        AA      =   np.block([[ZZ, II],[-A0,-A1]])
        BB      =   np.block([[II, ZZ],[ ZZ, A2]]) 
        
            
	return AA,BB




