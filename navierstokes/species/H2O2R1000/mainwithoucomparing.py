#################################3#
# Program to generate the 
# instability curves, necesaries 
# reproduce the Joncas and Perreu paper 
#
# Create by: Jhonatan Aguirre 
# Date:08/11/2018
# working: no
###################################


import numpy      	as     np
import matplotlib 	as     pl
from   Parameters  	import *
from   scipy.linalg  	import eig
from   scipy.special 	import airy
from   spectral   	import *
from   Boundary   	import *
from   Eigenvalues   	import *
from   path   		import pathfinder
from   subprocess       import call


# Creates a folder in the current directory called data
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)
# Example
#createFolder('./data/')

#file1       =   'incompressible' 
#
#createFolder('./%s/'%file1)

g1       =   open('%s1.dat'%file1,'w+')
g2       =   open('allspec.dat','w+')
#g2       =   open('%s2.dat'%file1,'w+')

#domega	=	(omegaf-omegai)/iomega 
omega	=	np.zeros(iomega)
omega	=	np.arange(0,iomega,1)*domega+omegai


alphai  =   	np.zeros((10000,10000)) 
alphar  =   	np.zeros((10000,10000))  

N1	=	N

A_08,A1,II,ZZ,BB	=	p_spectral_matrices(N1,maxr) 

for iomega in range(0,iomega): 

		print(omega[iomega],'omega')

	        #IS IN FUCTION OF OMEGA

        	A0_1    =   np.multiply((-1j*omega[iomega]),II)

        	A0      =   A0_1+A_08      
        	# Definition of psedo_spectral matices 

        	AA      =   block([[ZZ, II],[-A0,-A1]])  		
        
                alphar1,alphai1	=	eigenvalues_f(AA,BB,N1,min_imag,max_imag,min_real,max_real)
		NN		=	alphai1.shape[0];	
		
		#g1.write("%f\t%f\t%f\n"%(omega[iomega],alphar1[0],alphai1[0],alphar1[1],alphai1[0])); 

		for s in range(0,NN): 
		
			g2.write("%f\t%f\t%f\n"%(omega[iomega],alphar1[s],alphai1[s])); 


		pl.plot(alphar1,alphai1,'r*')

g2.close()
plt.show()

