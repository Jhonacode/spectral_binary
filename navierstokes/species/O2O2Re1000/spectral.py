#matplotlib inline
#config InlineBackend.figure_format='svg'
from   chebPy        import *
from   mapping       import *
from   Baseflow      import *
from   Boundary      import *
from   scipy.linalg  import block_diag
from   Parameters    import *
from   numpy         import matrix,matmul,dot,diag,real,imag,argsort,zeros,exp,log,linspace,polyval,polyfit,where,tan,tanh,cosh,identity,block
import numpy 	         as np 
#my own function 
import Block             as bk 
import matplotlib        as pl
import diffusion         as df
import sys 
import os 

def p_spectral_matrices(NN,maxr):

	# number of collocation nodes=N+1, because the cheb func
	D,n1	= cheb(NN)
	#D2	= dot(D,D)
	#D2[1:N,1:N]
	
	# Mappint to concentrate the point in 0. Withou using 0
	# r= Radial coordinate
	r   	= mappingtan(NN,n1,maxr,D)
	D2	= dot(D,D)
	#Base Flow

        
	Wb,Tb,Rhob,Y1,Y2  =   Baseflow(r,NN)


        #Fortran program diffusion.f90 
        #   This program calculate the diffusion properties of  
        #   the differens species and the mixture of gases. 
        
        #To run the fortran subroutine.
        #f2py -m diffusion -c  global.f90 diffusion.f90 
        
	(mib,kb,cpb,cpb1,cpb2,D1m,Mw,Ru,Rmix,gammab,gamma_j,gamma_ratio,Pbdiff,\
	a_ref,mi_ref,ka_ref,k1_ref,k2_ref,cp_ref,cp1_ref,cp2_ref,gamma_ref,\
	gamma_1,gamma_2,D_ref,Rmix_ref,Rmix_1,Rmix_2,a_s,a_1,a_2)=\
        df.properties.init_diffusion(Y1,Y2,Tb,T_i,T_0,T_ref,P_ref,NN)

        #####
	#Pb	=   1.0/gamma_ref 
	Pb	=   1.0#1.0/gamma_ref 
        #####
        #Non dimensional gas ideal equation \bar{p}=1/{gamma_ref}
        Rhob  =  1.0/(Tb*Rmix) 

        #Rhob(j)  =  gamma_ref*Pb(j)/(Rmixb(j)*Tb(j))

        #Dimensionaless 
        rho_1      =  (P_ref*100.0)/((Rmix_1)*T_i*T_ref)

        rho_2      =  (P_ref*100.0)/((Rmix_2)*T_0*T_ref)

        rho_ref    =  (P_ref*100.0)/((Rmix_ref)*T_0*T_ref)

	print 'rho_1',rho_1,'rho_2',rho_2

	l_ref	=	0.1#[m] 

	#Reynolds real
	global Reynolds
	global Peclet
	global Le1
	global Le2

	Reynolds= Reynolds/M0

	print Reynolds
	
	u_ref  	= 	Reynolds/(rho_ref*l_ref)*(mi_ref*1e-7)
	
	#Re   = (bba*rho_0*a_0)/(mi_0*1e-3)
	#Rmix_ref = Ru*Mw1[J][g*K] 
	
	Pr  	= 	(cp_ref*mi_ref)/ka_ref#*1.d0/Mw_ref

	#[J/gr K][(gr/cm*s)x10-6]/[W/cm*K]x10-6]
	#[J gr/gr K cm s]/[W/cm*K]]
	#[J gr cm*K/W gr K cm s]
	#[J*gr/W*gr*s]
	#[J*gr*s/J*mol*s]
	#[gr/gr]

	#print('********  Pe  =',Peclet,'     ********')
	
	Peclet  	=	Reynolds*Pr
	
	#Diffusion
	#[cm^2/s]

	#Lewis number, inner specie

	#[W/cm*K]x10-6/(kg/m^3*cm^2/s*J/gr K)
	#[W/cm*K]x10-6/(kg*cm^2*J/m^3*s*gr*K)
	#[J/s*cm*K]x10-6/(kg*cm^2*J/m^3*s*gr*K)
	#[J*m^3*s*gr*K/kg*cm^2*J*s*cm*K]x10-6/(/)
	#[m^3*gr/kg*cm^2*cm]x10-6
	#[m^3*gr1kg/1000gr//kg*cm^2*cm]x10-6
	#[m^3/cm^2*cm]x10-3
	#[m^3/(cm*1m/100cm)^3]x10-3
	#[m^3/(cm^3*1m^3/1000000cm^3)]x10-3
	#[1/(1/1000000]x10-3
	#[1/(1/100)^3]x10-3
	#[1/(1/10)^6]x10-3
	#[10)^6]x10-3
	#10^3


 	Le1=k1_ref/(rho_1*D_ref*cp1_ref)*10**-3	

 	Le2=k2_ref/(rho_2*D_ref*cp2_ref)*10**-3	
	
	print('***********************************')
	print('********  U   =',u_ref,'[m/s]********')
	print('********  Re  =',Reynolds,'********')
	print('********  Pe  =',Peclet,'     ********')
	print('********  Pr  =',Pr,'     ********')
	print('********  Lewis inner  =',Le1,'     ********')
	print('********  Lewis outter =',Le2,'     ********')
	print('***********************************')


      #  plt.plot(D1m,r)
      #  plt.plot(mib,r)
      #  plt.plot(kb,r)
      #  plt.plot(cpb,r,"r-")

       #plt.show()

        #pl.plot(r,Rhob)
        #pl.show()

        #
        #Adimensional numbers


	###########CONVECTIVE MACH NUMBER

	#if iomega == 0:

	for j in range(0,NN+1):


            if (R1-0.05<r[j]<R1+0.05):

                M1      =   Wb[j]
                rho1    =   Rhob[j]
                T1      =   Tb[j]

            if (R1*Gamma-0.05<r[j]<R1*Gamma+0.05):

                M2      =   Wb[j]
                rho2    =   Rhob[j]
                T2      =   Tb[j]

        Srho= rho2/rho1

	print'Mach1',M1,'Mach2',M2

	Rratio	=   M2/M1

	Uc_1    = (1.0+Rratio*np.sqrt(gamma_ratio))/(1.0+np.sqrt(Srho))

        Mc1_1  	= (M1-Uc_1/a_1) 

        Mc1_2  	= (Uc_1/a_2-M2) 

	print'Convective Mach',Mc1_1,'Convective Mach',Mc1_2

	#M1 =M_o
	#M2 =M_0

	Rratio		=   	M2/M1
        Srho		=	1#rho1/rho1
	gamma_ratio	=	1

	Uc_2    = (1.0+Rratio*np.sqrt(gamma_ratio))/(1.0+np.sqrt(Srho))

        Mc2_1  	= (M1-Uc_2/a_1) 

        Mc2_2  	= (Uc_2/a_2-M2) 

	print'Convective Mach 2',Mc2_1,'Convective Mach 2',Mc2_2
		

        g   	=   open('baseflow.dat','w+') 
	

	for j in range (0,NN+1):
	
	            g.write("%f\t%f\t%f\t%f\t%f\t%f\t%f\n"%(r[j],Wb[j],Tb[j],Rhob[j],a_s[j],Y1[j],Y2[j]));
        g.close()

        
        #sys.exit()
        
        
        #Differences necessary 

        dWbdr       =   dot(D ,Wb)
        dRhobdr     =   dot(D ,Rhob)
        d2Rhobdr    =   dot(D ,dRhobdr)
        d1Rhobdr    =   dot(D ,(1.0/Rhob))
        d21Rhobdr   =   dot(D ,d1Rhobdr )
        dTbdr       =   dot(D ,Tb)
        d2Tbdr      =   dot(D ,dTbdr)
        d2Wbdr      =   dot(D ,dWbdr)
        dY1bdr      =   dot(D ,Y1)
        dkbdr       =   dot(D ,kb)
        d2Y1bdr     =   dot(D ,dY1bdr)
        d1PRhoRbdr  =   dot(D ,(Pb/(Rhob*Rmix)))
        d2PRhobRdr  =   dot(D ,d1PRhoRbdr )
        dD1mdr      =   dot(D ,D1m )
        

        lambdab     =   zeros(N+1)
        lambdab     =   -2.0/3.0*mib
        
        dmibdr      =   dot(D,mib)
        dlambdabdr  =   dot(D,lambdab)
        

        # coordinate selectors
        
        rr  =   np.arange(0,NN,1)#0:N+1      
        uu  =   rr+N
        vv  =   uu+N
        pp  =   vv+N
        
        Z   =   zeros((NN+1,NN+1))
        I   =   identity(NN+1) 
        
        ##############################
        #Elemental Matrix
        DD      =   block_diag(D,D,D);
        DD2     =   block_diag(D2,D2,D2);
        II      =   identity(3*(NN+1))
 	ZZ      =   block_diag(Z,Z,Z);
        Ov      =   zeros((1,(6*(NN+1))))
        
        #remain to create a class of objects 
        #to passa all the varibles necessary for the matrix#creation funciontion
        #def matrices(mib,kb,cpb,D1m,D2m,D3m,Mw,Ru,Rmix,gammab,P_r):

        
        #Cylindrical
        Ae   =   block([
                           [Z               ,Z,diag(1.0/Rhob)],
                           [Z               ,Z,Z             ],
                           [diag(gammab*Pb) ,Z,Z             ],
                          ])
        
        Be   =   block([
                          [diag(Wb),Z                 ,Z             ],
                          [Z       ,diag(Wb)          ,diag(1.0/Rhob)],
                          [Z       ,diag(gammab*Pb)  ,diag(Wb)       ],
                          ])
        
        C12 =   diag(dRhobdr+Rhob/r) 
        Cu  =   diag(gammab*Pb/r) 
        
        Ce  =   block([
                         [  Z         ,Z,Z],
                         [diag(dWbdr) ,Z,Z],
                         [Cu          ,Z,Z],
                         ])
        
        D_u     =   diag(lambdab)+diag(2.0*mib)
        
        De      =   block([
                             [D_u,Z        ,Z],
                             [Z  ,diag(mib),Z],
                             [Z  ,Z        ,Z],
                             ])
        
        E_w     =   diag(lambdab)+diag(2.0*mib)
        
        Ee      =   block([
                             [diag(mib),Z  ,Z],
                             [Z        ,E_w,Z],
                             [Z        ,Z  ,Z],
                             ])
        
        F_u     =   diag(lambdab)+diag(mib)
        F_w     =   diag(lambdab)+diag(mib)
        
        Fe      =   block([
                             [Z  ,F_w,Z],
                             [F_u,Z  ,Z],
                             [Z  ,Z  ,Z],
                             ])
        
        Gu_1    =   diag(2.0*lambdab/r)
        Gu_4    =   diag(2.0*mib/r)
        G_u     =   Gu_1+diag(2*dmibdr)+diag(dlambdabdr)+Gu_4 
        Gw_2    =   diag(mib/r)
        G_w     =   diag(dmibdr)+Gw_2 
        
        #cuidado com este mas
        #K_e     =   diag(mib*(gammab-1.0)*dWbdr) 
        #tem que ser colocada na equacao de energia 
        
        Ge      =   block([
                             [G_u,Z   ,Z],
                             [Z  ,G_w ,Z],
                             [Z  ,Z   ,Z],
                             ])
        
        Hu_2    =   diag(mib/r)
        Hu_3    =   diag(lambdab/r)
        H_u     =   diag(dmibdr)+Hu_2+Hu_3
        H_w     =   Hu_3+diag(dlambdabdr)
        
        He      =   block([
                             [Z  ,H_w,Z],
                             [H_u,Z  ,Z],
                             [Z  ,Z  ,Z],
                             ])
        
        Iu_1    =   diag(dlambdabdr*1.0/r)
        I_u     =   Iu_1
        Ie      =   block([
                             [I_u,Z,Z],
                             [  Z,Z,Z],
                             [  Z,Z,Z],
                             ])
        
        
        #J      =   np.block([
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    ])
        
        #K      =   np.block([
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    ])
        
        
        Lw     =   diag(mib*dWbdr)
        
        Le     =   block([
                            [Z,  Z,Z],
                            [Z,  Z,Z],
                            [Z, Lw,Z],
                            ])
        
        Mu=Lw
        
        Me      =   block([
                            [Z  ,Z,Z],
                            [Z  ,Z,Z],
                            [Mu ,Z,Z],
                            ])
        
        
        
        Op      =   diag(kb/(Rhob*Rmix)) 
        
        Oe      =   block([
                            [Z,Z,Z ],
                            [Z,Z,Z ],
                            [Z,Z,Op],
                            ])
        
        ########### Matrix  P = O
        Pe      =  Oe 
        
        
        
        #Energy
        
        #Heat conduction
        Qp1     =  diag(1.0/(Rhob*Rmix)*(dkbdr+kb/r))
        
        #Energy, transport of energy
        D2m=D1m
        
        Espe    =  1.0/Le1*D1m*cpb1-1.0/Le2*D2m*cpb2
        
        Qp2     =  diag(Espe*1.0/Rmix*dY1bdr)
        
        Qy1     =  diag(Rhob*d1PRhoRbdr*Espe)
        
        Qe      =   block([
                            [Z,Z,Z      ],
                            [Z,Z,Z      ],
                            [Z,Z,Qp1+Qp2],
                            ])
        
        #1/Rhob=Tb
        #heat condution 
        Sp1     =   diag(kb*d2Tbdr)+diag((dkbdr+kb/r)*dTbdr)
        
        #energy species transport 
        Srho    =   diag(d1PRhoRbdr*Espe*dY1bdr)
        Sp2     =   diag(Rhob*Espe*dY1bdr*d1PRhoRbdr)
        
        Se      =   block([
                            [Z   ,Z,Z],
                            [Z   ,Z,Z],
                            [Srho,Z,Z],
                            ])
        
        Te      =   block([
                             [Z,Z,Z],
                             [Z,Z,Z],
                             [Z,Z,Z],
                             ])
        
        Ue      =  Te 
                    
        
        Vrho     =   diag(D1m/Rhob*dY1bdr)
        
        Vy1      =   diag(D1m/Rhob*dRhobdr+dD1mdr+D1m/r)
        
        Ve      =    block([
                             [Z,Z,Z],
                             [Z,Z,Z],
                             [Z,Z,Z],
                             ])
        
        
        Xrho    =   diag(D1m/Rhob*d2Y1bdr+1.0/Rhob*(dD1mdr+D1m/r)*dY1bdr)
        
        Xe      =   block([
                             [Z,Z,Z],
                             [Z,Z,Z],
                             [Z,Z,Z],
                             ])
        
        #Lembrar nunca mais fazer isto deste jeito, e melhor 
        #colocar todo mundo dentro, vai ficar mais facil 
        
        A2_1    =   diag(1.0/(Reynolds)*1.0/Rhob)
        A2_2    =   bk.Block(A2_1,Z)
        A2e     =   matmul(A2_2,Ee)
        
        A2_3    =   diag(gamma_j*(gammab-1.0)/Peclet)
        A2_4    =   bk.Block(A2_3,Z)
        A2p     =   matmul(A2_4,Pe)
        
        A2u     =   np.multiply(1.0/(Le1*Peclet),Ue)
        
        A2      =   A2e+A2p+A2u
        
        A1_1    =   matmul(Fe,DD)+He
        A1_2    =   diag(1.0/(Reynolds)*1.0/Rhob)
        A1_3    =   bk.Block(A1_2,Z)
        A1_4    =   matmul(A1_3,A1_1)
        
        A1_5    =   diag((gammab-1.0)/Reynolds)
        A1_6    =   bk.Block(A1_5,Z)
        A1_7    =   matmul(A1_6,Me)
        
        A1_8    =   Be-A1_4-A1_7
        A1      =   np.multiply(1j,A1_8)
        
        
        #In the loop
        #A0_1    =  np.multiply((-1j*omega[iomega]),II)
        
        A0_2    =  matmul(Ae,DD)
        
        A0_3    =  Ce 
        
        A0_41   =  matmul(De,DD2)+matmul(Ge,DD)+Ie
        A0_42   =  diag(1.0/(Reynolds)*1.0/Rhob)
        A0_43   =  bk.Block(A0_42,Z)
        A0_4    =  matmul(A0_43,A0_41)
        
        A0_51   =  matmul(Le,DD)
        A0_52   =  diag((gammab-1)/(Reynolds))
        A0_53   =  bk.Block(A0_52,Z)
        A0_5    =  matmul(A0_53,A0_51)
        
        A0_61   =  matmul(Oe,DD2)+matmul(Qe,DD)+Se
        A0_62   =  diag(gamma_j*(gammab-1.0)/Peclet)
        A0_63   =  bk.Block(A0_62,Z)
        A0_6    =  matmul(A0_63,A0_61)
        
        
        A0_71   =  matmul(Te,DD2)+matmul(Ve,DD)+Xe
        A0_7    =  np.multiply(1.0/(Le1*Peclet),A0_71)
        
        A0_8    = A0_2+A0_3-A0_4-A0_5-A0_6-A0_7

        #IS IN FUCTION OF OMEGA, IT in the main program to must not creating this matrics per each time iteration.

#        A0_1    =   np.multiply((-1j*omega),II)
#
#        A0      =   A0_1+A0_8      
#        
#        # Definition of psedo_spectral matices 
#
#        AA      =   block([[ZZ, II],[-A0,-A1]])  		
       
        BB      =   block([[II, ZZ],[ ZZ, A2]]) 
        
	return A0_8,A1,II,ZZ,BB




