#################################3#
# Program to generate the 
# instability curves, necesaries 
# reproduce the Joncas and Perreu paper 
#
# Create by: Jhonatan Aguirre 
# Date:08/11/2018
# working: no
###################################


import numpy      	as     np
import matplotlib 	as     pl
from   Parameters  	import *
from   scipy.linalg  	import eig
from   scipy.special 	import airy
from   spectral   	import *
from   Boundary   	import *
from   Eigenvalues   	import *
from   path   		import pathfinder
from   subprocess       import call


# Creates a folder in the current directory called data
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)
# Example
#createFolder('./data/')

#file1       =   'incompressible' 
#
#createFolder('./%s/'%file1)

g1       =   open('%s1.dat'%file1,'w+')
g2       =   open('allspec.dat','w+')
#g2       =   open('%s2.dat'%file1,'w+')

#domega	=	(omegaf-omegai)/iomega 
omega	=	np.zeros(iomega)
omega	=	np.arange(0,iomega,1)*domega+omegai


alphai  =   	np.zeros((10000,10000)) 
alphar  =   	np.zeros((10000,10000))  

N1	=	N

for iomega in range(0,iomega): 

		print(omega[iomega],'omega')

		A1,B1           = 	p_spectral_matrices(N1,maxr,omega[iomega])
        
                alphar1,alphai1	=	eigenvalues_f(A1,B1,N1,min_imag,max_imag,min_real,max_real)

		NN		=	alphai1.shape[0];	
		
		#g1.write("%f\t%f\t%f\n"%(omega[iomega],alphar1[0],alphai1[0],alphar1[1],alphai1[0])); 

		for s in range(0,NN): 
		
			g2.write("%f\t%f\t%f\n"%(omega[iomega],alphar1[s],alphai1[s])); 


		#pl.plot(alphar1,alphai1,'r*')

g2.close()
plt.show()

