#Function to  created matrix of 
# adecuate size that  have the 
# dimesionaless numbers 

import numpy as np
import math  as mt

def Block(A,Z):

    B    =   np.block([
                      [A,Z,Z,Z,Z],
                      [Z,A,Z,Z,Z],
                      [Z,Z,A,Z,Z],
                      [Z,Z,Z,A,Z],
                      [Z,Z,Z,Z,A]])
    
    return B
