MODULE properties 

!****** Description: 
!****** This model is using to 
!****** calculate the diffusivity properties(mi_k_D)  of the 
!****** different species in function of  
!****** the temperature. 
!****** Author: Jhonatan 
!****** Date: 16/11/2017   
!paper:Transport Coefficients for the NASA Çewis Chemical Equilibrium Program
!author: Roger Svehla 1995

use global

contains

subroutine init_diffusion(mib,kb,cpb,cpb_1,cpb_2,gammab,a_s,Mw,Rmix,Db_1m,&
                         &gamma_ref,gamma_j,gamma_ratio,                  &
                         &mi_ref,k_ref,k_1,k_2,cp_ref,cp_1,cp_2,          &
                         &rho_ref,rho_1,rho_2,a_1,a_2,a_ref,              &
                         &D_ref,Rmix_1,Rmix_2,Rmix_ref,Mw_ref,Ru,         &  
                         &Y1,Y2,Tb,T_i,T_o,T_0,T_ref,P_ref,N)

!IN 
!...rho_ref= Characteristic density [kg/m^3]
!...P_ref  = Characteristic Pressure[bar]
!...T_ref  = Characteristic temperature[k]
!...T_i    = Temperature specie 1 inner jet dimensionaless
!...T_o    = Temperature specie 2 outer jet dimensionaless 
!...T0     = Temperature ambient   dimensionaless 
!...Tb[j]  = Temperature base of the mixing, global.f90
!...Y1[j]  = Mass Fraction specie 1
!...Y2[j]  = Mass Fraction specie 2
!...N      = Number of collocation points,mesh in radial direction 

!OUT
!...mib[j]  = viscosity  base of the mixing, global.f90  
!...kb [j]  = Conductivity base of the mixing, global.f90  
!...cpb[j]  = Specific heat base of the mixing, global.f90  
!...mi_ref  = Characteristic viscosity  specie 1 [gr/cm*sx10-6]
!...k_ref   = Characteristic conductivity specie 1 [x10-6]
!...k1      = Conductivity specie 1 
!...k2      = Conductivity specie 2
!...cp_ref  = Characteristic Specific heat
!...cp1_ref = Specific heat specie 1  
!...cp2_ref = Specific heat specie 2 
!...rho_1   = Density  specie 1 
!...rho_2   = Density  specie 2
!...rho_ref = Characteristic Density
!...D_ref   = Characteristic Diffusivity 
!...Rmix_ref= Characteristic Gas constant  
!...Rmix_1  = Gas constant specie 1 
!...Rmix_2  = Gas constant specie 2 
!...a_s     = Sound Velocity, mixture
!...a_1     = Sound Velocity, specie 1
!...a_2     = Sound Velocity, specie 2
!...a_ref   = Characteristic Sound Velocity

!Internals
!Coeficcient fitting polynome 
!paper:Transport Coefficients for the NASA Lewis Chemical Equilibrium Program
!author: Roger Svehla 1995
!A_MI
!B_MI
!C_MI
!D_MI

implicit none
integer :: i,j

!!!!!!!!!!!!!IN
!f2py intent(in)::N
integer ::N
!f2py intent(in)::T_i,T_o,T_ref,T_0,P_ref
real(kind=ip)::T_i,T_o,T_0,T_ref,P_ref
!f2py intent(in)::Y1,Y2,Tb
real(kind=ip),dimension(0:N)::Y1,Y2,Y3,Tb

!!!!!!!!!!!!!OUT

!f2py intent(out)::mib,kb,cpb,gammab,a_s,Mw,Rmix,Db_1m
real(kind=ip),dimension(0:N)::mib,kb,cpb,gammab,a_s,Mw,Rmix,Db_1m
!f2py intent(out)::gamma_ref,gamma_j,gamma_ratio
real(kind=ip)               :: gamma_ref,gamma_j,gamma_ratio

!f2py intent(out)::mi_ref,k_ref,cp_ref,rho_ref,a_ref
real(kind=ip)::mi_ref,k_ref,cp_ref,rho_ref,a_ref

!f2py intent(out)::D_ref,Rmix_ref,Mw_ref,Ru
real(kind=ip)::D_ref,Rmix_ref,Mw_ref,Ru


!Molecualar Fraction, Xss sum of molecualr fraction 
real(kind=ip),dimension(0:N)::X1,X2,X3,Xss
!Base FLow properties
real(kind=ip),dimension(0:N)::mib_1,mib_2,mib_3
real(kind=ip),dimension(0:N)::kb_1,kb_2,kb_3

!f2py intent(out)::cpb_1,cpb_2,cpb_3
real(kind=ip),dimension(0:N)::cpb_1,cpb_2,cpb_3

real(kind=ip),dimension(0:N)::alphab

!Species propertiees

real(kind=ip)::mi_1, mi_2, mi_3

real(kind=ip)::D_1m,D_2m,D_3m

!f2py intent(out)::k_1, k_2, k_3
real(kind=ip)::k_1, k_2, k_3
!f2py intent(out)::cp_1,cp_2,cp_3
real(kind=ip)::cp_1,cp_2,cp_3

!f2py intent(out)::rho_1,rho_2
real(kind=ip)::rho_1,rho_2

!f2py intent(out)::a_1,a_2
real(kind=ip)::a_1 , a_2 , a_3

real(kind=ip)::gamma_1, gamma_2,gamma_3
!f2py intent(out)::Rmix_1, Rmix_2
real(kind=ip)::Rmix_1, Rmix_2,Rmix_3
real(kind=ip)::Mw1 , Mw2 , Mw3

!Diffusity reference
real(kind=ip)::alpha_ref


!Molar gas constant
Ru      = 8.314510d0![J/g*K]


call proper_2(mi_1, mib_1, k_1, kb_1, cp_1, cpb_1, Tb, T_i, T_ref, Mw1, Ru, N)

call proper_2(mi_2, mib_2, k_2, kb_2, cp_2, cpb_2, Tb, T_o, T_ref, Mw2, Ru, N)

call proper_2(mi_3, mib_3, k_3, kb_3, cp_3, cpb_3, Tb, T_0, T_ref, Mw3, Ru, N)


!!!!!!!!!!!!!!!!(Mix    ,prop1    ,    prop2,     pro3)
call mix_proper(mib,Y1,Y2,Y3,mib_1,mib_2,mib_3,Mw1,Mw2,Mw3,N)

call mix_proper(kb ,Y1,Y2,Y3,kb_1 ,kb_2 ,kb_3 ,Mw1,Mw2,Mw3,N)
!

do j=s_s,e_s

        Mw(j)        =    1.d0/(Y1(j)/Mw1+Y2(j)/Mw2)

        Rmix(j)      =    Ru/Mw(j) 

        !Anderson: Frozem cp
        cpb (j)      =    Y1(j)*cpb_1(j)+Y2(j)*cpb_2(j)

        gammab(j)    =    1.d0/(1.d0-Rmix(j)/cpb(j))     

        a_s(j)       =    sqrt(gammab(j)*Rmix(j)*1000*(Tb(j)*T_ref))

        !mol fraction 
        X1(j)        =   Y1(j)/(Mw1/(Mw(j)))
        X2(j)        =   Y2(j)/(Mw2/(Mw(j)))
        X3(j)        =   Y3(j)/(Mw3/(Mw(j)))

        !Sum of mol fractions 
        Xss(j)        =   X1(j)+X2(j)+X3(j)


end do 

call mix_proper_diffusion(Db_1m,D_1m,D_2m,X1,X2,X3,Mw1,Mw2,Mw3,P_ref,Tb,T_i,T_o,T_0,T_ref,N) 
!Reference specified heat ratio 

gamma_1         =       1.d0/(1.d0-(Ru/Mw1)/cp_1)     
gamma_2         =       1.d0/(1.d0-(Ru/Mw2)/cp_2)     
Rmix_1          =       Ru/Mw1
Rmix_2          =       Ru/Mw2
a_1             =       sqrt(gamma_1*Rmix_1*1000*(T_i*T_ref))
a_2             =       sqrt(gamma_2*Rmix_2*1000*(T_o*T_ref))

!write(*,*)'speed of sound','Ref',a_ref,'a1',a_1,'a2',a_2

!Density of the species[kg/m³] 
rho_1      =  (P_ref*100.0)/((Rmix_1)*T_i*T_ref) 
rho_2      =  (P_ref*100.0)/((Rmix_2)*T_o*T_ref) 

!write(*,*)'rho','Ref','rho_1',rho_1,'rho_2',rho_2

!Reference Properties 

!Reference density
rho_ref         =         rho_2 ![kg/m^3] 
mi_ref          =          mi_2 ![gr/cm*sx10-6]
k_ref           =           k_2 ![W/cm*Kx10-6]
cp_ref          =          cp_2 ![J/gr*K]
Mw_ref          =           Mw2 
a_ref           =           a_2 ![m/s]
gamma_ref       =       gamma_2 

!D_ref           =          D_2m

![W/cm*Kx10-6]/([kg/m^3][J/gr*K])
![J/s*cmx10-6]/([kg/m^3][J/gr])
![1/s*cm]/([1kg/m^3gr])x10-6
![1/s*cm]/([1kg*(1000gr/1kg)/m^3gr])x10-6
![1/s*cm]/(1/[m^3])x10-3*
![[m^2]m*100cm/1m)/s*cm]x10-3
![[m^2]/s]x10-3*100
![(cm^2/)/s]x10-1*100^2
![(cm^2/)/s]x10^3
alpha_ref       =       k_ref/(rho_ref*cp_ref)*(1E-3) ![cm^2/s]
!alpha_ref       =       k_1/(rho_1*cp_1)*(1E-3) ![cm^2/s]


write(*,*)'alpha_ref',alpha_ref,rho_ref,k_ref
stop

D_ref           =       alpha_ref

!Resference respect to thermal diffusivity  
Rmix_ref        =       Ru/Mw_ref

gamma_ratio     =       gamma_2/gamma_1 

gamma_j         =       gamma_ref/(gamma_ref-1.d0)

!dimensionalized the mixture viscosity by the mi_r:
mib     =       mib/mi_ref

!dimensionalized the mixture conductivity by the k_r:
kb      =       kb/k_ref

!dimensionalized the mixture heat capacity by the cp_r:
cpb     =       cpb/cp_ref

!dimensionalized the mixture constant:
Rmix    =       Rmix/Rmix_ref 

!dimensionalized diffusion coeficient:
Db_1m   =       Db_1m/D_ref

end subroutine init_Diffusion

subroutine mix_proper_diffusion(D12,D_i,D_o,X1,X2,X3,Mw1,Mw2,Mw3,P_ref,Tb,T_i,T_o,T_0,T_ref,N)

implicit none                  
integer :: i,j,N
real(kind=ip)::Mw1,Mw2,Mw3
real(kind=ip)::Mw11_df,Mw22_df,Mw33_df
real(kind=ip)::Mw12_df,Mw13_df,Mw23_df
real(kind=ip)::T_ref,T_i,T_o,T_0,D_i,D_o,P_ref
real(kind=ip)::T_st_r,T_st_0,omega_df_r,omega_df_0
real(kind=ip)::epsilonk1_df ,epsilonk2_df ,epsilonk3_df
real(kind=ip)::epsilonk12_df,epsilonk13_df,epsilonk23_df
real(kind=ip)::delta1_df,delta2_df,delta3_df
real(kind=ip)::delta11_df,delta22_df,delta33_df
real(kind=ip)::delta12_df,delta13_df,delta23_df
real(kind=ip)::A_df,B_df,C_df,D_df,E_df,F_df,G_df,H_df 
real(kind=ip),dimension(0:N)::X1,X2,X3
real(kind=ip),dimension(0:N)::omega11_df,omega22_df,omega33_df
real(kind=ip),dimension(0:N)::omega12_df,omega13_df,omega23_df
real(kind=ip),dimension(0:N)::D12,D13,D23
real(kind=ip),dimension(0:N)::Tb,Tb_st,Tb_st1,Tb_st2,Tb_st3
real(kind=ip),dimension(0:N)::Tb_st12,Tb_st13,Tb_st23

!ALL IS MESSURE RESPECT TO T_i


call diffusion_O2(delta1_df,epsilonk1_df)
call diffusion_O2(delta2_df,epsilonk2_df)
call diffusion_O2(delta3_df,epsilonk3_df)

epsilonk12_df    =       (epsilonk1_df*epsilonk2_df)**(0.5d0)

delta12_df       =       (delta1_df+delta2_df)/(2.d0)

Mw12_df          =       2.d0*1.d0/(1.d0/Mw1+1.d0/Mw2)


!Book:
!THE PROPERTIES OF
!GASES AND LIQUIDS
!Bruce E. Poling
!Professor of Chemical Engineering
!University of Toledo
!John M. Prausnitz
!Professor of Chemical Engineering
!University of California at Berkeley
!John P. O’Connell
!Professor of Chemical Engineering
!University of Virginia
!omega_d is tabulated as a function of kT / epsilon for the 12-6 Lennard-Jones potential

!Chapter 11
!pag:640
!table
!pag:780
A_df    =       1.06036d0
B_df    =       0.15610d0
C_df    =       0.19300d0
D_df    =       0.47635d0
E_df    =       1.03587d0
F_df    =       1.52996d0
G_df    =       1.76474d0
H_df    =       3.89411d0

Tb_st   =       Tb*T_ref 

Tb_st12 =       Tb_st/epsilonk12_df 

T_st_r       =      (T_ref*T_i)/epsilonk12_df
T_st_0       =      (T_ref*T_o)/epsilonk12_df



!Colision Integral

omega_df_r  =      A_df/((T_st_r)**(B_df)   )   &
                      &  +    C_df/((dexp(D_df*T_st_r)))   &
                      &  +    E_df/((dexp(F_df*T_st_r)))   &
                      &  +    G_df/((dexp(H_df*T_st_r)))

omega_df_0   =      A_df/((T_st_0)**(B_df)   )   &
                      &  +    C_df/((dexp(D_df*T_st_0)))   &
                      &  +    E_df/((dexp(F_df*T_st_0)))   &
                      &  +    G_df/((dexp(H_df*T_st_0)))

omega12_df      =     A_df/((Tb_st12)**(B_df)  )    &
                & +   C_df/((dexp(D_df*Tb_st12)))   &
                & +   E_df/((dexp(F_df*Tb_st12)))   &
                & +   G_df/((dexp(H_df*Tb_st12)))   



do j=s_s,e_s

        D12(j)    = 0.00266d0*(Tb(j)*T_ref)**(1.5d0)/(P_ref*Mw12_df**(0.5d0)*delta12_df**2.d0*omega12_df(j))

end do 

D_i   = 0.00266d0*(T_i*T_ref)**(1.5d0)/(P_ref*Mw12_df**(0.5d0)*delta12_df**2.d0*omega_df_r)

!1bar= 0.74[cm^2/s]
!0.7bar= 1.4[cm^2/s]

D_o   = 0.00266d0*(T_o*T_ref)**(1.5d0)/(P_ref*Mw12_df**(0.5d0)*delta12_df**2.d0*omega_df_0)


end subroutine mix_proper_diffusion 

subroutine mix_proper(Pmix,X1,X2,X3,Pb1,Pb2,Pb3,Mw1,Mw2,Mw3,N)

implicit none
integer :: j,N
real(kind=ip)::Mw1,Mw2,Mw3
real(kind=ip)::phi11,phi22,phi33
real(kind=ip),dimension(0:N)::phi12,phi13,phi21,phi23,phi31,phi32
real(kind=ip),dimension(0:N)::X1,X2,X3,Pb1,Pb2,Pb3,Pmix,Pb_1,Pb_2,Pb_3

phi11   = 1.d0 
phi22   = 1.d0 

call phi_f(phi12 ,Mw1,Mw2,Pb1,Pb2,N)
call phi_f(phi21 ,Mw2,Mw1,Pb2,Pb1,N)

do j=s_s,e_s

        Pb_1(j) = X1(j)*Pb1(j)/(X1(j)*phi11   +X2(j)*phi12(j))
        Pb_2(j) = X2(j)*Pb2(j)/(X1(j)*phi21(j)+X2(j)*phi22   )

        Pmix(j) = Pb_1(j)+Pb_2(j)
end do 


end subroutine mix_proper 

subroutine phi_f(phi_ij,Mwi,Mwj,Pbi,Pbj,N)

implicit none

integer::j,N
real(kind=ip)::Mwi,Mwj,phi_1
real(kind=ip),dimension(0:N)::phi_ij,Pbi,Pbj

phi_1   = 1.d0/dsqrt(8.d0) 

do j=s_s,e_s

        phi_ij(j)  = phi_1*(1.d0+Mwi/Mwj)**(-0.5d0)*(1.d0+(Pbi(j)/Pbj(j))**(0.5d0)*(Mwj/Mwi)**(0.25d0))**2.d0 

end do 

end subroutine phi_f 

subroutine proper_1(mi_d,mib,k_d,kb,cp_d,cpb,Tb,T_i,T_ref,Mw,Ru,N)

implicit none
integer      ::j
real(kind=ip)::T_ref,T_i,T_d
real(kind=ip)::A_mi,B_mi,C_mi,D_mi
real(kind=ip)::A_k , B_k ,C_k ,D_k 
real(kind=ip)::A_cp ,B_cp ,C_cp ,D_cp,E_cp
integer      ::N
real(kind=ip),dimension(0:N)::Tb
real(kind=ip),dimension(0:N)::mib,kb,cpb
real(kind=ip)::mi_d,k_d,cp_d
real(kind=ip)::Mw,Ru


CALL          visc_H2(A_mi, B_mi , C_mi ,D_mi)
CALL          cond_H2(A_k , B_k  , C_k  ,D_k )
CALL  heatcapacity_H2(A_cp, B_cp , C_cp ,D_cp,E_cp,Mw)

!.....Dimensionaless equation for 
!.....temperature, using T_i T'=T[k]/T_r[k]

!ALL IS MESSURE RESPECT TO T_i
T_d     = T_ref*T_i
!viscosity
!.....g/(cm*s)x10-6=Kg/(m*s)x10-7
mi_d    = dexp(A_mi*dlog(T_d) + B_mi/(T_d) + C_mi/(T_d)/(T_d) + D_mi) 

!Head conductinity
!.....W/(cm*K)x10-6
k_d     = dexp(A_k*dlog(T_d) + B_k/(T_d) + C_k/(T_d)/(T_d) + D_k ) 

!Head capacity 
! Cp is dimensionalees by the Ru, Cp=Cp/Ru
Cp_d    = A_cp + B_cp*T_d + C_cp*T_d**2.d0 + D_cp*T_d**3.d0 + E_cp*T_d**4.d0


s_s=0
e_s=N



!Dimesional variables
do j=s_s,e_s
        
        T_d     = T_ref*Tb(j)
        
        mib(j)  =  dexp(A_mi*dlog(T_d)  + B_mi/(T_d)  + C_mi/(T_d)/(T_d) + D_mi) 
        
        kb(j)   =  dexp( A_k*dlog(T_d)  + B_k /(T_d)  + C_k /(T_d)/(T_d) + D_k ) 
        
        Cpb(j)  =  (A_cp + B_cp*(T_d)   + C_cp*(T_d)**2.d0 + D_cp*(T_d)**3.d0 + E_cp*(T_d)**4.d0) 

!
END DO 


!Real value of cp, because is non dimesionalised by Ru
Cp_d  = Cp_d*Ru/Mw
Cpb   = Cpb *Ru/Mw
!Cp_d  = Cp_d*Ru
!Cpb   = Cpb *Ru
!write(*,*)'Cp_r',Cp_r*Ru/Mw1

END SUBROUTINE proper_1 

subroutine proper_2(mi_d,mib,k_d,kb,cp_d,cpb,Tb,T_i,T_ref,Mw,Ru,N)

implicit none
integer      ::j
real(kind=ip)::T_ref,T_i,T_d
real(kind=ip)::A_mi,B_mi,C_mi,D_mi
real(kind=ip)::A_k , B_k ,C_k ,D_k 
real(kind=ip)::A_cp ,B_cp ,C_cp ,D_cp,E_cp
integer      ::N
real(kind=ip),dimension(0:N)::Tb
real(kind=ip),dimension(0:N)::mib,kb,cpb
real(kind=ip)::mi_d,k_d,cp_d
real(kind=ip)::Mw,Ru


CALL          visc_O2(A_mi,B_mi,C_mi,D_mi)
CALL          cond_O2(A_k ,B_k ,C_k ,D_k )
CALL  heatcapacity_O2(A_cp ,B_cp ,C_cp ,D_cp,E_cp,Mw)

!.....Dimensionaless equation for 
!.....temperature, using T_i T'=T[k]/T_r[k]

!ALL IS MESSURE RESPECT TO T_i
T_d     = T_ref*T_i
!viscosity
!.....g/(cm*s)x10-6=Kg/(m*s)x10-7
mi_d    = dexp(A_mi*dlog(T_d) + B_mi/(T_d) + C_mi/(T_d)/(T_d) + D_mi) 

!Head conductinity
!.....W/(cm*K)x10-6
k_d     = dexp(A_k*dlog(T_d) + B_k/(T_d) + C_k/(T_d)/(T_d) + D_k ) 

!Head capacity 
! Cp is dimensionalees by the Ru, Cp=Cp/Ru
Cp_d    = A_cp + B_cp*T_d + C_cp*T_d**2.d0 + D_cp*T_d**3.d0 + E_cp*T_d**4.d0


s_s=0
e_s=N


!Dimesional variables
do j=s_s,e_s

        T_d     = T_ref*Tb(j)
        
        mib(j)  =  dexp(A_mi*dlog(T_d)  + B_mi/(T_d)  + C_mi/(T_d)/(T_d) + D_mi) 
        
        kb(j)   =  dexp( A_k*dlog(T_d)  + B_k /(T_d)  + C_k /(T_d)/(T_d) + D_k ) 
        
        Cpb(j)  =  (A_cp + B_cp*(T_d)   + C_cp*(T_d)**2.d0 + D_cp*(T_d)**3.d0 + E_cp*(T_d)**4.d0) 

END DO 

!Real value of cp, because is non dimesionalised by Ru
Cp_d  = Cp_d*Ru/Mw
Cpb   = Cpb *Ru/Mw

!write(*,*)'Cp_r',Cp_r*Ru/Mw1

END SUBROUTINE proper_2 

subroutine proper_3(mi_d,mib,k_d,kb,cp_d,cpb,Tb,T_i,T_ref,Mw,Ru,N)

implicit none
integer      ::j
real(kind=ip)::T_ref,T_i,T_d
real(kind=ip)::A_mi,B_mi,C_mi,D_mi
real(kind=ip)::A_k , B_k ,C_k ,D_k 
real(kind=ip)::A_cp ,B_cp ,C_cp ,D_cp,E_cp
integer      ::N
real(kind=ip),dimension(0:N)::Tb
real(kind=ip),dimension(0:N)::mib,kb,cpb
real(kind=ip)::mi_d,k_d,cp_d
real(kind=ip)::Mw,Ru


CALL          visc_N2(A_mi,B_mi,C_mi,D_mi)
CALL          cond_N2(A_k ,B_k ,C_k ,D_k )
CALL  heatcapacity_N2(A_cp ,B_cp ,C_cp ,D_cp,E_cp,Mw)

!.....Dimensionaless equation for 
!.....temperature, using T_i T'=T[k]/T_r[k]

!ALL IS MESSURE RESPECT TO T_i
T_d     = T_ref*T_i
!viscosity
!.....g/(cm*s)x10-6=Kg/(m*s)x10-7
mi_d    = dexp(A_mi*dlog(T_d) + B_mi/(T_d) + C_mi/(T_d)/(T_d) + D_mi) 

!Head conductinity
!.....W/(cm*K)x10-6
k_d     = dexp(A_k*dlog(T_d) + B_k/(T_d) + C_k/(T_d)/(T_d) + D_k ) 

!Head capacity 
! Cp is dimensionalees by the Ru, Cp=Cp/Ru
Cp_d    = A_cp + B_cp*T_d + C_cp*T_d**2.d0 + D_cp*T_d**3.d0 + E_cp*T_d**4.d0


s_s=0
e_s=N


!Dimesional variables
do j=s_s,e_s
        
        T_d     = T_ref*Tb(j)
        
        mib(j)  =  dexp(A_mi*dlog(T_d)  + B_mi/(T_d)  + C_mi/(T_d)/(T_d) + D_mi) 
        
        kb(j)   =  dexp( A_k*dlog(T_d)  + B_k /(T_d)  + C_k /(T_d)/(T_d) + D_k ) 
        
        Cpb(j)  =  (A_cp + B_cp*(T_d)   + C_cp*(T_d)**2.d0 + D_cp*(T_d)**3.d0 + E_cp*(T_d)**4.d0) 

END DO 

!Real value of cp, because is non dimesionalised by Ru
Cp_d  = Cp_d*Ru/Mw
Cpb   = Cpb *Ru/Mw

!write(*,*)'Cp_r',Cp_r*Ru/Mw1

END SUBROUTINE proper_3 


!###### NITROGEN_2 Ar[200-1000 K] ######
SUBROUTINE VISC_N2(A_d,B_d,C_d,D_d)

!IN-OUT 
!Coeficcient fitting polynome 
!paper:Transport Coefficients for the NASA Lewis Chemical Equilibrium Program
!author: Roger Svehla 1995
!A_MI
!B_MI
!C_MI
!D_MI
!Temperature range[200-1000][K]


IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d


A_d=       0.62526577D0 
B_d=       -31.779652D0 
C_d=       -1640.7983D0 
D_d=        1.7454992D0 

END SUBROUTINE VISC_N2

SUBROUTINE Cond_N2(A_d,B_d,C_d,D_d)
!Temperature range[200-1000][K]
IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d

  
A_d=    0.85372829D0 
B_d=     105.18665D0 
C_d=    -12299.753D0 
D_d=    0.48299104D0 

END SUBROUTINE cond_N2

SUBROUTINE heatcapacity_N2(A_d,B_d,C_d,D_d,E_d,Mw)
!Temperature range[1000-3000][K]
IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d,E_d,Mw

!200k<T<6000k

A_d=     3.53100528D0 
B_d=    -1.23660987E-4 
C_d=    -5.02999437E-7 
D_d=     2.43530612E-9 
E_d=    -1.40881235E-12 

Mw =    28.01348D0
!T>1000k

!	A_d= 2.95257626D0 
!	B_d= 1.39690057E-3 
!        C_d=-4.92631691E-7 
!	D_d= 7.86010367E-11 
!	E_d=-4.60755321E-15 

!        Mw = 28.01348D0

END SUBROUTINE heatcapacity_N2

SUBROUTINE diffusion_N2(delta_df,epsilonk_df)

!IN-OUT 
!Coeficcient fitting polynome 
!paper:Viscosities and Thermal Conductivities 
!       of Gases at Hight Temperatures
!author: Roger Svehla 1962
!delta_df
!epsilonk_df

IMPLICIT NONE
real(kind=ip)::delta_df,epsilonk_df

        delta_df        = 3.798d0       
        epsilonk_df     =  71.4d0       

END SUBROUTINE diffusion_N2
!


SUBROUTINE visc_N(A_d,B_d,C_d,D_d)
!Temperature range[1000-3000][K]
IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d

A_d=    0.82926975D0 
B_d=     405.82833D0 
C_d=    -159002.42D0 
D_d=    0.17740763D0 


END SUBROUTINE VISC_N

SUBROUTINE Cond_N(A_d,B_d,C_d,D_d)
!Temperature range[1000-3000][K]
IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d

A_d=    0.82928303D0 
B_d=     405.77643D0 
C_d=    -158950.37D0 
D_d=    0.97751462D0 
END SUBROUTINE cond_N


SUBROUTINE heatcapacity_N(A_d,B_d,C_d,D_d,E_d,Mw)
!Temperature range[1000-3000][K]
IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d,E_d,Mw

!T<1000k

A_d=    2.5D0 
B_d=    0.D0 
C_d=    0.D0 
D_d=    0.D0 
E_d=    0.D0 

Mw =    14.00674D0

END SUBROUTINE heatcapacity_N
!
SUBROUTINE diffusion_N(delta_df,epsilonk_df)

!IN-OUT 
!Coeficcient fitting polynome 
!paper:Viscosities and Thermal Conductivities 
!       of Gases at Hight Temperatures
!author: Roger Svehla 1962
!delta_df
!epsilonk_df

IMPLICIT NONE
real(kind=ip)::delta_df,epsilonk_df

        delta_df        = 3.798d0       
        epsilonk_df     =  71.4d0       

END SUBROUTINE diffusion_N
!
!
SUBROUTINE VISC_He(A_d,B_d,C_d,D_d)
!Temperature range[200-1000][K]
IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d
A_d=    0.75015944D0 
B_d=     35.763243D0 
C_d=    -2212.1291D0 
D_d=    0.92126352D0 
END SUBROUTINE VISC_He

!###### HIDROGEN_2 Ar[200-1000 K] ######
SUBROUTINE VISC_H2(A_d,B_d,C_d,D_d)

!IN-OUT 
!Coeficcient fitting polynome 
!paper:Transport Coefficients for the NASA Lewis Chemical Equilibrium Program
!author: Roger Svehla 1995
!A_MI
!B_MI
!C_MI
!D_MI
!Temperature range[200-1000][K]


IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d


A_d=       0.74553182D0 
B_d=       43.555109D0 
C_d=       -3257.9340D0 
D_d=       0.13556243D0 

END SUBROUTINE VISC_H2

SUBROUTINE Cond_H2(A_d,B_d,C_d,D_d)
!Temperature range[200-1000][K]
IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d

  
A_d=    1.0240124D0 
B_d=    297.09752D0 
C_d=    -31396.363D0 
D_d=    1.0560824D0 

END SUBROUTINE cond_H2

SUBROUTINE heatcapacity_H2(A_d,B_d,C_d,D_d,E_d,Mw)
!Temperature range[1000-3000][K]
IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d,E_d,Mw

!200k<T<6000k

A_d=     2.34433112D0
B_d=     7.98052075E-3
C_d=    -1.94781510E-5
D_d=     2.01572094E-8
E_d=    -7.37611761E-12

Mw =     2.01588D0
!T>1000k

!	A_d= 2.95257626D0 
!	B_d= 1.39690057E-3 
!        C_d=-4.92631691E-7 
!	D_d= 7.86010367E-11 
!	E_d=-4.60755321E-15 

!        Mw = 28.01348D0

END SUBROUTINE heatcapacity_H2
!
SUBROUTINE diffusion_H2(delta_df,epsilonk_df)

!IN-OUT 
!Coeficcient fitting polynome 
!paper:Viscosities and Thermal Conductivities 
!       of Gases at Hight Temperatures
!author: Roger Svehla 1962
!delta_df
!epsilonk_df

IMPLICIT NONE
real(kind=ip)::delta_df,epsilonk_df

        delta_df        = 2.827d0       
        epsilonk_df     =  59.7d0       

END SUBROUTINE diffusion_H2
!
!###### OXIGEN_2 Ar[200-1000 K] ######
SUBROUTINE VISC_O2(A_d,B_d,C_d,D_d)

!IN-OUT 
!Coeficcient fitting polynome 
!paper:Transport Coefficients for the NASA Lewis Chemical Equilibrium Program
!author: Roger Svehla 1995
!A_MI
!B_MI
!C_MI
!D_MI
!Temperature range[200-1000][K]


IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d


A_d     =    0.60916180D0 
B_d     =   -52.244847D0 
C_d     =   -599.74009D0 
D_d     =    2.0410801D0 

END SUBROUTINE VISC_O2

SUBROUTINE COND_O2(A_d,B_d,C_d,D_d)
!Temperature range[200-1000][K]
IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d

  
A_d  =      0.77238828D0 
B_d  =      6.9293259D0 
C_d  =     -5900.8518D0 
D_d  =      1.2202965D0 

END SUBROUTINE COND_O2

SUBROUTINE heatcapacity_O2(A_d,B_d,C_d,D_d,E_d,Mw)
!Temperature range[200-6000][K]
IMPLICIT NONE
real(kind=ip)::A_d,B_d,C_d,D_d,E_d,Mw

!200k<T<6000k

A_d=      3.78246636D0  
B_d=     -2.99673416E-03 
C_d=      9.84730200E-06
D_d=     -9.68129608E-09  
E_d=      3.24372836E-12

Mw =      31.9988D0
!T>1000k

!	A_d= 2.95257626D0 
!	B_d= 1.39690057E-3 
!        C_d=-4.92631691E-7 
!	D_d= 7.86010367E-11 
!	E_d=-4.60755321E-15 

!        Mw = 28.01348D0

END SUBROUTINE heatcapacity_O2

SUBROUTINE diffusion_O2(delta_df,epsilonk_df)

!IN-OUT 
!Coeficcient fitting polynome 
!paper:Viscosities and Thermal Conductivities 
!       of Gases at Hight Temperatures
!author: Roger Svehla 1962
!delta_df
!epsilonk_df

IMPLICIT NONE
real(kind=ip)::delta_df,epsilonk_df

        delta_df        = 3.467d0       
        epsilonk_df     = 106.7d0       

END SUBROUTINE diffusion_O2


END MODULE

