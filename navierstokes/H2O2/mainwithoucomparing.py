#################################3#
# Program to generate the 
# instability curves, necesaries 
# reproduce the Joncas and Perreu paper 
#
# Create by: Jhonatan Aguirre 
# Date:08/11/2018
# working: no
###################################


import numpy      	as     np
import matplotlib 	as     pl
from   Parameters  	import *
from   scipy.linalg  	import eig
from   scipy.special 	import airy
from   spectral   	import *
from   Boundary   	import *
from   Eigenvalues   	import *
from   path   		import pathfinder
from   subprocess       import call


# Creates a folder in the current directory called data
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)
# Example
#createFolder('./data/')

#file1       =   'incompressible' 
#
#createFolder('./%s/'%file1)

g1       =   open('%s_1.dat'%file1,'w+')
g2       =   open('%s_2.dat'%file1,'w+')
g3       =   open('allspec.dat','w+')
#g2       =   open('%s2.dat'%file1,'w+')

#domega	=	(omegaf-omegai)/iomega 
omega	=	np.zeros(iomega)
omega	=	np.arange(0,iomega,1)*domega+omegai


alphai  =   	np.zeros((10000,10000)) 
alphar  =   	np.zeros((10000,10000))  

N1	=	N

A_08,A1,II,ZZ,BB,DD	=	p_spectral_matrices(N1,maxr) 

for iomega in range(0,iomega): 

		print(omega[iomega],'omega')

	        #IS IN FUCTION OF OMEGA

        	A0_1    =   np.multiply((-1j*omega[iomega]),II)

        	A0      =   A0_1+A_08      
        	# Definition of psedo_spectral matices 

        	AA      =   block([[ZZ, II],[-A0,-A1]])  		
        
                alphar1,alphai1	=	eigenvalues_f(AA,BB,DD,N1,min_imag,max_imag,min_real,max_real)

		NN		=	alphai1.shape[0];	

		for k in range(0,NN): 

	        	g3.write("%f\t%f\t%f\n"%(omega[iomega],alphar1[k],alphai1[k])); 



		alphai_1     =   np.min(alphai1)
    		pt           =   np.argmin(alphai1)
    		alphai1[pt]  =   np.inf
		alphar_1     =   alphar1[pt]

		print omega[iomega],(alphar_1,alphai_1)

		g1.write("%f\t%f\t%f\n"%(omega[iomega],alphar_1,alphai_1)); 

		if(alphai_1<-0.01):

    			alphai_2     =   np.min(alphai1)
    			pt           =   np.argmin(alphai1)
    			alphai1[pt]  =   np.inf
			alphar_2     =   alphar1[pt]

			print omega[iomega],(alphar_1,alphai_1),(alphar_2,alphai_2)

			g2.write("%f\t%f\t%f\t%f\t%f\n"%(omega[iomega],alphar_1,alphai_1,alphar_2,alphai_2)); 

		plt.plot(alphar1,alphai1,'r*')
  

		

g1.close()
g2.close()
g3.close()
plt.show()

