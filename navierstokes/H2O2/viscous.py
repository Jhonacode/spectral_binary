#matplotlib inline #config InlineBackend.figure_format='svg'
#f2py -m diffusion -c  global.f90 diffusion.f90 

from chebPy   import *      
from mapping  import *
from Baseflow import *
from numpy    import matrix,matmul,dot,diag,real,imag,argsort,zeros,exp,log,linspace,polyval,polyfit,where,tan,tanh,cosh,identity
from scipy.linalg  import eig
from scipy.special import airy
#from matplotlib   import figure,subplot,plot,title,grid,show,axis
from scipy.linalg  import block_diag
import numpy as np 
import diffusion as df
#from diffusion import visc_n2
import matplotlib.pyplot as plt 
import os

# Creates a folder in the current directory called data
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)
# Example
#createFolder('./data/')





#number of collocation nodes=N+1, because the cheb func
N           =   250 

#g           =   open('autospectroN%d.dat'%N,'w+')

file1       =   'Re10000/test/' 
Re          =      10000.0

createFolder('./%s/'%file1)

omegai      =   0.1
omegaf      =   4.1
iomega      =   20
domega      =   (omegaf-omegai)/iomega 

omega       =   zeros(iomega)
omega       =   np.arange(0,iomega,1)*domega+omegai

#omega[0]    =   omegai


D,n1        =   cheb(N); D2=dot(D,D); D2[1:N,1:N]

#maximum r
maxr        =   20   

r           =   mappingtan(N,n1,maxr,D,D2) 

#Python module Baseflow.py
Wb,Tb,Rhob,T_0,T_r,M_r,R1,theta1  =   Baseflow(r)
                 
dWbdr       =   dot(D,Wb)
dRhobdr     =   dot(D,Rhob)
d2Wbdr      =   dot(D,dWbdr)



#Fortran program diffusion.f90 
#f2py -m diffusion -c  global.f90 diffusion.f90 
#visc_n2(A_mi,B_mi,C_mi,D_mi)
(mib,mi_r,kb,k_r,cpb,cp_r,Mw,Ru)=df.properties.init_diffusion(Tb,T_r,T_0,N)

#f           =   open('propiertiesviscous.dat','w+')
#for j in range(0,N): 
#
#    f.write("%f\t%f\t%f\t%f\t%f\t%f\n"%(r[j],Wb[j],Rhob[j],mib[j],kb[j],cpb[j])); 
#
#f.close


lambdab     =   zeros(N+1)
lambdab     =   -2.0/3.0*mib

#Universal molecular gas constant  
#[J/(g*K)]=[KJ/(kg*K)]=m^2/(s^2*K)
Rg          =   Ru/Mw
gamma       =   1.0/(1.0-Rg/cp_r)     
gammab      =   1.0/(1.0-Rg/cpb)     

dmibdr      =   dot(D,lambdab)
dlambdabdr  =   dot(D,lambdab)


#Non Dimensional numbers
#a_r=    np.sqrt(T_r/(1.0/Rg-1.0/cp_r))     
#Rg*1000=[J/g*K]*1000=[J/kg*K]
a_r     =   np.sqrt(gamma*Rg*1e3*T_r)     
rho_0   =   0.575#(kg/m^3)atT_r=0.5 
#rho_0   =   1.16 #(kg/m^3)atT_r=1 

W_r=    M_r*a_r
rho_r=(1.0/1.0)*rho_0#=1/(Tb_r)d

print(rho_r,M_r,W_r,mi_r,theta1)

#Re  =   3000
#Re  =   1E8
#Re=(rho_r*W_r*R1)/(mi_r*1e-7)*theta1
RR  =   Re*(mi_r*1e-7)/(rho_r*W_r)
print(RR)

Pr  =   1.0

#plt.plot(Wb ,r,'*-r')
#plt.plot(mib,r,'o-b')
#plt.plot(kb ,r,'o-g')
#plt.plot(cpb,r,'o-y')
#plt.show()


#plt.figure(1)
#plt.plot(Wb,r,'*b')
#plt.figure(2)
#plt.plot(dWbdr,r,'*b')
#plt.figure(3)
#plt.plot(d2Wbdr,r,'*-r')
#plt.plot(Rhob,r,'*b')
#plt.plot(dRhobdr,r,'*b')
##plt.axis((0.0,1.0,-0.0,4.0))
#plt.grid()
#plt.show()
#
# coordinate selectors

rr  =   np.arange(0,N,1)#0:N+1      
uu  =   rr+N
vv  =   uu+N
pp  =   vv+N

Z   =   zeros((N+1,N+1))
I   =   identity(N+1) 

##############################
#Elemental Matrix
DD      =   block_diag(D,D,D,D);
DD2     =   block_diag(D2,D2,D2,D2);
II      =   identity(4*(N+1))
ZZ      =   block_diag(Z,Z,Z,Z);
#II      =   np.block([[I,Z,Z,Z],[Z,I,Z,Z],[Z,I,Z,Z],[Z,Z,Z,I]])
Ov      =   zeros((1,(8*(N+1))))


#Cylindrical
Ae   =   np.block([[Z,diag(Rhob),Z,Z],[Z,Z,Z,diag(1.0/Rhob)],[Z,Z,Z,Z],[Z,I,Z,Z]])
Be   =   np.block([[diag(Wb),Z,diag(Rhob),Z],[Z,diag(Wb),Z,Z],[Z,Z,diag(Wb),diag(1.0/Rhob)],[Z,Z,I,diag(Wb)]])

C12     =   diag(Rhob/r) 
C42     =   diag(1.0/r) 
Ce      =   np.block([[Z,diag(dRhobdr)+C12,Z,Z],[Z,Z,Z,Z],[Z,diag(dWbdr),Z,Z],[Z,C42,Z,Z]])

D_u     =   diag(lambdab)+diag(2.0*mib)
De      =   np.block([[Z,Z,Z,Z],[Z,D_u,Z,Z],[Z,Z,diag(mib),Z],[Z,Z,Z,Z]])

E_w     =   diag(lambdab)+diag(2.0*mib)
Ee      =   np.block([[Z,Z,Z,Z],[Z,diag(mib),Z,Z],[Z,Z,E_w,Z],[Z,Z,Z,Z]])

F_u     =   diag(lambdab)+diag(mib)
F_w     =   diag(lambdab)+diag(mib)
Fe      =   np.block([[Z,Z,Z,Z],[Z,Z,F_w,Z],[Z,F_u,Z,Z],[Z,Z,Z,Z]])

Gu_1    =   diag(2.0*lambdab/r)
Gu_4    =   diag(2.0*mib/r)
G_u     =   Gu_1+diag(2*dmibdr)+diag(dlambdabdr)+Gu_4 
Gw_2    =   diag(mib/r)
#
#cuidado tem que ver isto aqui se faz diferenza
#G_w     =   diag(dmibdr)+Gu_4 
G_w     =   diag(dmibdr)+Gw_2 
#
K_e     =   diag(mib*(gammab-1.0)*dWbdr) 
Ge      =   np.block([[Z,Z,Z,Z],[Z,G_u,Z,Z],[Z,Z,G_w,Z],[Z,Z,K_e,Z]])

Hu_2    =   diag(mib/r)
Hu_3    =   diag(lambdab/r)
H_u     =   diag(dmibdr)+Hu_2+Hu_3
Hw_1    =   diag(lambdab/r) 
H_w     =   Hu_3+diag(dlambdabdr)
He      =   np.block([[Z,Z,Z,Z],[Z,Z,H_w,Z],[Z,H_u,Z,Z],[Z,K_e,Z,Z]])


Iu_1    =   diag(dlambdabdr*1.0/r)
I_u     =   Iu_1
Ie      =   np.block([[Z,Z,Z,Z],[Z,I_u,Z,Z],[Z,Z,Z,Z],[Z,Z,Z,Z]])


A2_1    =  diag((1.0/(Re)*1.0/Rhob))
A2_2    =  np.block([[A2_1,Z,Z,Z],[Z,A2_1,Z,Z],[Z,Z,A2_1,Z],[Z,Z,Z,A2_1]])
A2      =  matmul(A2_2,Ee)

A1_1    =  np.multiply(1j,Be)
A1_21   =  diag(-1j*1.0/(Re)*1.0/Rhob)
A1_22   =  np.block([[A1_21,Z,Z,Z],[Z,A1_21,Z,Z],[Z,Z,A1_21,Z],[Z,Z,Z,A1_21]])
A1_23   =  matmul(A1_22,Fe)
A1_2    =  matmul(A1_23,DD)
A1_3    =  matmul(A1_22,He)

A1      =   A1_1+A1_2+A1_3


#In the loop
#A0_1    =  np.multiply((-1j*omega[iomega]),II)
A0_2    =  matmul(Ae,DD)
A0_3    =  Ce 
A0_41   =  diag(-1.0/(Re)*1.0/Rhob)
A0_42   =  np.block([[A0_41,Z,Z,Z],[Z,A0_41,Z,Z],[Z,Z,A0_41,Z],[Z,Z,Z,A0_41]])
A0_43   =  matmul(A0_42,De)
A0_4    =  matmul(A0_43,DD2)
A0_51   =  matmul(A0_42,Ge)
A0_5    =  matmul(A0_51,DD)
A0_6    =  matmul(A0_42,Ie)


for iomega in range(0,iomega): 

    g       =   open('./%s/autospec%di%d.dat'%(file1,N,iomega),'w+')

    g.write("%f \n"%(omega[iomega])); 

    print(omega[iomega])

    A0_1    =   np.multiply((-1j*omega[iomega]),II)
    A0      =   A0_1+A0_2+A0_3+A0_4+A0_5+A0_6

    AA      =   np.block([[ZZ, II],[-A0,-A1]])
    BB      =   np.block([[II, ZZ],[ ZZ, A2]]) 

    #A0      =   np.multiply((1j*omega[iomega]),Ee)-matmul(Ae,DD)-Ce-De;
    #B0      =   np.multiply(1j,Be)
    
    
    #[0,N]=N+1 PONTOS
    # rho
    AA[0,:]               = Ov
    BB[0,:]               = Ov
    BB[0,0]               = 1 # A0[N,:]               = Ov 
    BB[N,:]               = Ov 
    BB[N,N]               = 1  
    
    #u
    AA[N+1,:]             = Ov 
    BB[N+1,:]             = Ov 
    BB[N+1,0]             = 1  
    
    AA[2*N,:]             = Ov
    BB[2*N,:]             = Ov
    BB[2*N,2*N]           = 1
    
    #v
    AA[2*N+1,:]           = Ov
    BB[2*N+1,:]           = Ov
    BB[2*N+1,N+1]         = 1
    
    AA[3*N,:]             = Ov
    BB[3*N,:]             = Ov
    BB[3*N,3*N ]          = 1
    
    #p
    AA[3*N+1,:]           = Ov
    BB[3*N+1,:]           = Ov
    BB[3*N+1,3*N+1]       = 1
    
    
    AA[4*N,:]             = Ov
    BB[4*N,:]             = Ov
    BB[4*N,4*N]           = 1
   # #
    
    eigvals, eigvecs = eig(AA, BB) #

    S= list(filter(lambda x: np.abs(imag(x))<1.2 and 0.1< real(x) <5 , eigvals))
    #S= eigvals 

    Lamreal =   real(S)
    Lamimag =   imag(S)


    #Lr          =   Lamreal
    #Li          =   Lamimag

    #alphai1     =   np.max(Li)
    #pt          =   np.argmax(Li)
    #Li[pt]      =   0
    #alphai2     =   np.max(Li)

    #if alphai2<0.1: 
    #     
    #    alphai2 =alphai1
    #    

    #print(omega[iomega],alphai1,alphai2)

    #f.write("%f %f %f\n"%(omega[iomega],alphai1,alphai2)); 


    NN=np.shape(Lamreal)

    print(NN[0],'dd') 

    for kk in range(1,NN[0]): 

        g.write("%f %f\n"%(Lamreal[kk],Lamimag[kk])); 

        #Lreal[i][k]=Lamreal[i]
        #Limag[i][k]=Lamimag[i]

    #plt.figure(2)
    plt.plot(Lamreal,Lamimag,'.r')
    #plt.axis((0.0,5.0,-1.0,1.0))
    plt.grid()
    #plt.show()


#f.close
g.close

plt.show()

#     %%%% solve for eigenvalues
#disp('computing eigenvalues')
#tic; [U,S]=eig(B0,A0); toc
#
#S=diag(S);
#
#%plot(real(S),imag(S),'*'); hold on
#%xlimvec=[-3, 3]; ylimvec=[-2,0];
#
#for ind=1:length(S)
#            fprintf(arq1,'%f  %f \n',real(S(ind)),imag(S(ind)));
#

#increse virtual memory


#You could try creating the swap file from the terminal to temporarily increase virtual memory manually.
#
#Example:
#
#    sudo swapoff -a
#    sudo fallocate -l 5G /swapfile5g
#    sudo mkswap /swapfile5g
#    sudo chmod 600 /swapfile5g
#    sudo swapon /swapfile5g








