#matplotlib inline
#config InlineBackend.figure_format='svg'
from   chebPy        import *
from   mapping       import *
from   Baseflow      import *
from   Boundary      import *
from   scipy.linalg  import block_diag
from   Parameters    import *
from   numpy         import matrix,matmul,dot,diag,real,imag,argsort,zeros,exp,log,linspace,polyval,polyfit,where,tan,tanh,cosh,identity,block
import numpy 	         as np 
#my own function 
import Block             as bk 
import matplotlib        as pl
import diffusion         as df
import sys 
import os 

def p_spectral_matrices(NN,maxr):

	# number of collocation nodes=N+1, because the cheb func
	D,n1	= cheb(NN)
	#D2	= dot(D,D)
	#D2[1:N,1:N]
	
	# Mappint to concentrate the point in 0. Withou using 0
	# r= Radial coordinate
	r   	= mappingtan(NN,n1,maxr,D)
	D2	= dot(D,D)
	#Base Flow

        
	Wb,Tb,Rhob,Y1,Y2  =   Baseflow(r,NN)

        #Fortran program diffusion.f90 
        #   This program calculate the diffusion properties of  
        #   the differens species and the mixture of gases. 
        
        #To run the fortran subroutine.
        #f2py -m diffusion -c  global.f90 diffusion.f90 

        
	(mib,kb,cpb,cpb1,cpb2,gammab,a_s,Mw,Rmix,Db_1m,	\
	 gamma_ref,gamma_j,gamma_ratio,			\
	 mi_ref,ka_ref,k_1,k_2,cp_ref,cp_1,cp_2,		\
	 rho_ref,rho_1,rho_2,a_1,a_2,a_ref,			\
	 D_ref,Rmix_1,Rmix_2,Rmix_ref,Mw_ref,Ru)=			\
         df.properties.init_diffusion(Y1,Y2,Tb,T_i,T_o,T_0,T_ref,P_ref,NN) 

	#Simetric diffusion 
	Db_2m =	Db_1m


        #####
        #Non dimensional gas ideal equation \bar{p}=1/{gamma_ref}
        #Rhob(j)  =  gamma_ref*Pb(j)/(Rmixb(j)*Tb(j))
        Rhob  	=  1.0/(Tb*Rmix) 

	#Mixing thermal difusivity 
	alphab 	=  kb/(cpb*Rhob)

       	#plt.plot(1.0/Db_1m,r,'b*')
       	#plt.plot(Db_1m,r,'r*')
        #plt.show()

	l_ref	=	0.1#[m] 

	#Reynolds real
	global Reynolds
	global Peclet
	global Le1
	global Le2

	#Reynolds Real
	#Reynolds= Reynolds/M0

	u_ref  	= 	Reynolds/(rho_ref*l_ref)*(mi_ref*1e-7)
	
	#Re   = (bba*rho_0*a_0)/(mi_0*1e-3)
	#Rmix_ref = Ru*Mw1[J][g*K] 
	
	Pr  	= 	(cp_ref*mi_ref)/ka_ref#*1.0/Mw_ref

	#[J/gr K][(gr/cm*s)x10-6]/[W/cm*K]x10-6]
	#[J gr/gr K cm s]/[W/cm*K]]
	#[J gr cm*K/W gr K cm s]
	#[J*gr/W*gr*s]
	#[J*gr*s/J*mol*s]
	#[gr/gr]

	#print('********  Pe  =',Peclet,'     ********')
	
	Peclet  	=	Reynolds*Pr
	
	#Diffusion
	#[cm^2/s]

	#Lewis number, inner specie

	#[W/cm*K]x10-6/(kg/m^3*cm^2/s*J/gr K)
	#[W/cm*K]x10-6/(kg*cm^2*J/m^3*s*gr*K)
	#[J/s*cm*K]x10-6/(kg*cm^2*J/m^3*s*gr*K)
	#[J*m^3*s*gr*K/kg*cm^2*J*s*cm*K]x10-6/(/)
	#[m^3*gr/kg*cm^2*cm]x10-6
	#[m^3*gr1kg/1000gr//kg*cm^2*cm]x10-6
	#[m^3/cm^2*cm]x10-3
	#[m^3/(cm*1m/100cm)^3]x10-3
	#[m^3/(cm^3*1m^3/1000000cm^3)]x10-3
	#[1/(1/1000000]x10-3
	#[1/(1/100)^3]x10-3
	#[1/(1/10)^6]x10-3
	#[10)^6]x10-3
	#10^3

 	Le1=1/Db_1m[100]
 	Le2=1/Db_1m[0]
	
	print('***********************************')
	print('********  U   =',u_ref,'[m/s]********')
	print('********  Re  =',Reynolds,'********')
	print('********  Pe  =',Peclet,'     ********')
	print('********  Pr  =',Pr,'     ********')
	print('********  Lewis inner  =',Le1,'     ********')
	print('********  Lewis outter =',Le2,'     ********')
	print('***********************************')


	###########CONVECTIVE MACH NUMBER

	#if iomega == 0:

	for j in range(0,NN+1):


            if (R1-0.05<r[j]<R1+0.05):

                M1      =   Wb[j]
                rho1    =   Rhob[j]
                T1      =   Tb[j]

            if (R1*Gamma-0.05<r[j]<R1*Gamma+0.05):

                M2      =   Wb[j]
                rho2    =   Rhob[j]
                T2      =   Tb[j]

        Srho= rho2/rho1

	print'Mach1',M1,'Mach2',M2

	Rratio	=   M2/M1

	Uc_1    = (1.0+Rratio*np.sqrt(gamma_ratio))/(1.0+np.sqrt(Srho))

        Mc1_1  	= (M1-Uc_1/a_1) 

        Mc1_2  	= (Uc_1/a_2-M2) 

	print'Convective Mach',Mc1_1,'Convective Mach',Mc1_2

	#M1 =M_o
	#M2 =M_0

	Rratio		=   	M2/M1
        Srho		=	1#rho1/rho1
	gamma_ratio	=	1

	Uc_2    = (1.0+Rratio*np.sqrt(gamma_ratio))/(1.0+np.sqrt(Srho))

        Mc2_1  	= (M1-Uc_2/a_1) 

        Mc2_2  	= (Uc_2/a_2-M2) 

	print'Convective Mach 2',Mc2_1,'Convective Mach 2',Mc2_2
		

        g   	=   open('baseflow.dat','w+') 
	

	for j in range (0,NN+1):
	
	            g.write("%f\t%f\t%f\t%f\t%f\t%f\t%f\n"%(r[j],Wb[j],Tb[j],Rhob[j],a_s[j],Y1[j],Y2[j]));
        g.close()

        #sys.exit()
        
        #Differences necessary 

        dWbdr       =   dot(D ,Wb)
        dRhobdr     =   dot(D ,Rhob)
        d2Rhobdr    =   dot(D ,dRhobdr)
        d1Rhobdr    =   dot(D ,(1.0/Rhob))
        d21Rhobdr   =   dot(D ,d1Rhobdr )
        dTbdr       =   dot(D ,Tb)
        d2Tbdr      =   dot(D ,dTbdr)
        d2Wbdr      =   dot(D ,dWbdr)
        dY1bdr      =   dot(D ,Y1)
        dY2bdr      =   dot(D ,Y2)
        dkbdr       =   dot(D ,kb)
        d2Y1bdr     =   dot(D ,dY1bdr)
        d2Y2bdr     =   dot(D ,dY2bdr)
        #d1PRhoRbdr  =   dot(D ,(P_ref/(Rhob*Rmix)))
        #d2PRhobRdr  =   dot(D ,d1PRhoRbdr )
        dDb_1mdr      =   dot(D ,Db_1m )
        

        lambdab     =   zeros(N+1)
        lambdab     =   (-2.0/3.0)*mib
        
        dmibdr      =   dot(D,mib)
        dlambdabdr  =   dot(D,lambdab)
        

        # coordinate selectors
        
        rr  =   np.arange(0,NN,1)#0:N+1      
        uu  =   rr+N
        vv  =   uu+N
        pp  =   vv+N
        
        Z   =   zeros((NN+1,NN+1))
        I   =   identity(NN+1) 
        
        ##############################
        #Elemental Matrix
        DD      =   block_diag(D,D,D,D,D);
        DD2     =   block_diag(D2,D2,D2,D2,D2);
        II      =   identity(5*(NN+1))
        ZZ      =   block_diag(Z,Z,Z,Z,Z);
        Ov      =   zeros((1,(10*(NN+1))))
        
        #remain to create a class of objects 
        #to passa all the varibles necessary for the matrix#creation funciontion
        #def matrices(mib,kb,cpb,Db_1m,Db_2m,D3m,Mw,Ru,Rmix,gammab,P_r):

	##Base flow pressure.

	Pbar  	=   1.0/gamma_ref
        
        #Cylindrical
        Ae   =   block([
                           [Z ,diag(Rhob)       ,Z,Z             ,Z],
                           [Z ,Z                ,Z,diag(1.0/Rhob),Z],
                           [Z ,Z                ,Z,Z             ,Z],
                           [Z ,diag(gamma_ref*gammab*Pbar)
						,Z,Z             ,Z],
                           [Z ,Z                ,Z,Z             ,Z]
                          ])
        
        Be   =   block([
                          [diag(Wb),Z       ,diag(Rhob)        ,Z             ,Z        ],
                          [Z       ,diag(Wb),Z                 ,Z             ,Z        ],
                          [Z       ,Z       ,diag(Wb)          ,diag(1.0/Rhob),Z        ],
                          [Z       ,Z       ,diag(gamma_ref*gammab*Pbar) 
							       ,diag(Wb)      ,Z        ],
                          [Z       ,Z       ,Z                 ,Z             ,diag(Wb) ]
                          ])
        
        C12 =   diag(dRhobdr+Rhob/r) 
        Cu  =   diag(gamma_ref*gammab*Pbar/r) 
        
        Ce  =   block([
                         [Z,C12         ,Z,Z,Z],
                         [Z,  Z         ,Z,Z,Z],
                         [Z,diag(dWbdr) ,Z,Z,Z],
                         [Z,Cu          ,Z,Z,Z],
                         [Z,diag(dY1bdr),Z,Z,Z]
                         ])
        
        D_u     =   diag(1.0/Rhob*(lambdab+2.0*mib))
        D_w	=   diag(1.0/Rhob*mib)

        De      =   block([
                             [Z,Z  ,Z  ,Z,Z],
                             [Z,D_u,Z  ,Z,Z],
                             [Z,Z  ,D_w,Z,Z],
                             [Z,Z  ,Z  ,Z,Z],
                             [Z,Z  ,Z  ,Z,Z]
                             ])
        
        Ee      =   block([
                             [Z,Z  ,Z  ,Z,Z],
                             [Z,D_w,Z  ,Z,Z],
                             [Z,Z  ,D_u,Z,Z],
                             [Z,Z  ,Z  ,Z,Z],
                             [Z,Z  ,Z  ,Z,Z]
                             ])
        
        F_u     =   diag(1.0/Rhob*(lambdab+mib))
        
        Fe      =   block([
                             [Z,Z  ,Z  ,Z,Z],
                             [Z,Z  ,F_u,Z,Z],
                             [Z,F_u,Z  ,Z,Z],
                             [Z,Z  ,Z  ,Z,Z],
                             [Z,Z  ,Z  ,Z,Z]
                             ])
        
        Gu_1    =   lambdab/r
        Gu_4    =   mib/r
        G_u     =   diag(1.0/Rhob*(2*Gu_1+dlambdabdr+2*Gu_4+2*dmibdr)) 
        G_w     =   diag(1.0/Rhob*(dmibdr+Gu_4))
        
        #cuidado com este mas
        #K_e     =   diag(mib*(gammab-1.0)*dWbdr) 
        #tem que ser colocada na equacao de energia 
        
        Ge      =   block([
                             [Z,Z  ,Z   ,Z,Z],
                             [Z,G_u,Z   ,Z,Z],
                             [Z,Z  ,G_w ,Z,Z],
                             [Z,Z  ,Z   ,Z,Z],
                             [Z,Z  ,Z   ,Z,Z]
                             ])
        
        Hu_2    =   mib/r
        Hu_3    =   lambdab/r
        H_u     =   diag(1.0/Rhob*(dmibdr+Hu_2+Hu_3))
        H_w     =   diag(1.0/Rhob*(Hu_3+dlambdabdr))
        
        He      =   block([
                             [Z,Z  ,Z  ,Z,Z],
                             [Z,Z  ,H_w,Z,Z],
                             [Z,H_u,Z  ,Z,Z],
                             [Z,Z  ,Z  ,Z,Z],
                             [Z,Z  ,Z  ,Z,Z]
                             ])
        
        Iu_1    =   diag(1.0/Rhob*(dlambdabdr*1.0/r))
        I_u     =   Iu_1
        Ie      =   block([
                             [Z,  Z,Z,Z,Z],
                             [Z,I_u,Z,Z,Z],
                             [Z,  Z,Z,Z,Z],
                             [Z,  Z,Z,Z,Z],
                             [Z,  Z,Z,Z,Z]
                             ])
        
        
        #J      =   np.block([
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    ])
        
        #K      =   np.block([
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    [Z,Z,Z,Z,Z],
        #                    ])
        
        #Energy
        
        Lw     =   diag(mib*dWbdr)
        
        Le     =   block([
                            [Z,Z,  Z,Z,Z],
                            [Z,Z,  Z,Z,Z],
                            [Z,Z,  Z,Z,Z],
                            [Z,Z, Lw,Z,Z],
                            [Z,Z,  Z,Z,Z]
                            ])
        
        #Mu=Lw
        
        Me      =   block([
                            [Z,Z  ,Z,Z,Z],
                            [Z,Z  ,Z,Z,Z],
                            [Z,Z  ,Z,Z,Z],
                            [Z,Lw ,Z,Z,Z],
                            [Z,Z  ,Z,Z,Z]
                            ])
        
        T1r     =   (dkbdr+kb/r) 
        T2r     =   Rhob*(Db_1m*cpb1*dY1bdr+Db_2m*cpb2*dY2bdr)

        #T3r     =   diag(Rhob*(Db_1m*cpb1*dY1bdr+Db_2m*cpb2*dY2bdr))
        
        Qy1     =   Rhob*(Db_1m*cpb1+Db_2m*cpb2)*dTbdr

        Srho    =   Rhob*(Db_1m*cpb1*dY1bdr+Db_2m*cpb2)*dTbdr

        Qp      =   gamma_ref/(Rhob*Rmix)
        Qrho    =   Tb/(Rhob)
        QR      =   Tb/(Rmix)

        #Firts Derivative
        Sp      =   dot(D ,Qp  )
        S2rho   =   dot(D ,Qrho)
        SR      =   dot(D ,QR  )

        #Second Derivative
        S2p     =   dot(D ,Sp   )
        S3rho   =   dot(D ,S2rho)
        S2R     =   dot(D ,SR   )

        

        Op      =   diag(kb*Qp)
        Orho    =   diag(kb*Qrho)
        OR      =   diag(kb*QR)
        OY1     =   diag(kb*Rmix_1*QR)
        OY2     =   diag(kb*Rmix_2*QR)
        
        Oe      =   block([
                            [Z   ,Z,Z,Z ,Z  ],
                            [Z   ,Z,Z,Z ,Z  ],
                            [Z   ,Z,Z,Z ,Z  ],
                            [Orho,Z,Z,Op,OY1],
                            [Z   ,Z,Z,Z ,Z  ]
                            ])
        
        ########### Matrix  P = O
        Pe      =  Oe 

        Re      =  Oe 

        Q1      =  diag(kb*2.0*S2rho+Qrho*(T1r+T2r))

        Q2      =  diag(kb*2.0*Sp+Qp*(T1r+T2r))

        Q3      =  diag(kb*2.0*SR+QR*(T1r+T2r))

        Qe      =   block([
                            [Z ,Z,Z,Z  ,Z        ],
                            [Z ,Z,Z,Z  ,Z        ],
                            [Z ,Z,Z,Z  ,Z        ],
                            [Q1,Z,Z,Q2 ,Rmix_1*Q3 ],
                            [Z ,Z,Z,Z  ,Z        ]
                            ])

        S1      =  diag(kb*S3rho+S2rho*(T1r+T2r)+Srho)

        S2      =  diag(kb*S2p+Sp*(T1r+T2r))

        S3      =  diag(kb*S2R+SR*(T1r+T2r))

        Se      =   block([
                            [Z   ,Z,Z,Z ,Z        ],
                            [Z   ,Z,Z,Z ,Z        ],
                            [Z   ,Z,Z,Z ,Z        ],
                            [S1  ,Z,Z,S2,Rmix_1*S3 ],
                            [Z   ,Z,Z,Z ,Z        ] 
                            ])
        
        
        Te      =   block([
                             [Z,Z,Z,Z,         Z],
                             [Z,Z,Z,Z,         Z],
                             [Z,Z,Z,Z,         Z],
                             [Z,Z,Z,Z,         Z],
                             [Z,Z,Z,Z, diag(Db_1m)] 
                             ])
        
        Ue      =  Te 
                    
        
        Vrho     =   diag(Db_1m/Rhob*dY1bdr)
        
        Vy1      =   diag(Db_1m/Rhob*dRhobdr+dDb_1mdr+Db_1m/Rhob*1.0/r)
        
        Ve      =    block([
                             [Z   ,Z,Z,Z,  Z],
                             [Z   ,Z,Z,Z,  Z],
                             [Z   ,Z,Z,Z,  Z],
                             [Z   ,Z,Z,Z,  Z],
                             [Vrho,Z,Z,Z,Vy1]
                             ])
        
        
        Xrho    =   diag(Db_1m/Rhob*d2Y1bdr+1.0/Rhob*(dDb_1mdr+Db_1m/r)*dY1bdr)
        
        Xe      =   block([
                             [Z   ,Z,Z,Z,Z],
                             [Z   ,Z,Z,Z,Z],
                             [Z   ,Z,Z,Z,Z],
                             [Z   ,Z,Z,Z,Z],
                             [Xrho,Z,Z,Z,Z] 
                             ])
        
        #A2_1    =   diag(1.0/(Reynolds))
        #A2_2    =   bk.Block(A2_1,Z)
        #A2e     =   matmul(A2_2,Ee)

        A2e     =   np.multiply(1.0/(Reynolds),Ee)

	AgP_1  	=   diag(gamma_j*(gamma_ref*gammab-1.0)/(Peclet))
	AgP     =   bk.Block(AgP_1,Z)
        A2p     =   matmul(AgP,Pe)

        A2u     =   np.multiply(1.0/(Peclet),Ue)
        
        #A2      =   A2e+A2p+A2u

        A2      =   A2e#+A2p+A2u
        
#########################################################
        A1_21   =   matmul(Fe,DD)+He
        A1_2    =   np.multiply(1.0/(Reynolds),A1_21)

	AgR1  	=   diag((gamma_ref*gammab-1.0)/(Reynolds))
	AgR  	=   bk.Block(AgR1,Z)
        A1_3    =   matmul(AgR,Me)

        A1_4    =   matmul(AgP,Re)

        #A1_5    =   Be-A1_2-A1_3-A1_4        

        A1_5    =   Be-A1_2-A1_3#-A1_4        

        A1      =   np.multiply(1j,A1_5)

        
########################################################## 
        #In the loop
        #A0_1    =  np.multiply((-1j*omega[iomega]),II)

        A0_2    =  matmul(Ae,DD)
        
        A0_41   =  matmul(De,DD2)+matmul(Ge,DD)+Ie
        A0_4    =  np.multiply(1.0/(Reynolds),A0_41)
        
        A0_51   =  matmul(Le,DD)
        A0_5    =  matmul(AgR,A0_51)
        
        A0_61   =  matmul(Oe,DD2)+matmul(Qe,DD)+Se
        A0_6    =  matmul(AgP,A0_61)
        
        A0_71   =  matmul(Te,DD2)+matmul(Ve,DD)+Xe
        A0_7    =  np.multiply(1.0/Peclet,A0_71)
        
        #A0_8    = A0_2+Ce-A0_4-A0_5-A0_6-A0_7

        A0_8    = A0_2+Ce-A0_4-A0_5#-A0_6-A0_7

        #IS IN FUCTION OF OMEGA, IT in the main program to must not creating this matrics per each time iteration.

#        A0_1    =   np.multiply((-1j*omega),II)
#
#        A0      =   A0_1+A0_8      
#        
#        # Definition of psedo_spectral matices 
#
#        AA      =   block([[ZZ, II],[-A0,-A1]])  		
       

        BB      =   block([[II, ZZ],[ ZZ, A2]]) 
        #BB      =   block([[II, ZZ],[ ZZ, ZZ]]) 
        
	return A0_8,A1,II,ZZ,BB,DD




