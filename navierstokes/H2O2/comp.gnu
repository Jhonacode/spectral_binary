plot\
	'taxaRe250_2.dat'       u 1:(-$3) w lp,\
	'taxaRe250_2.dat'       u 1:(-$5) w lp,\
	'taxaRe500_2.dat'       u 1:(-$3) w lp,\
	'taxaRe500_2.dat'       u 1:(-$5) w lp,\
	'taxaRe1000_2.dat'      u 1:(-$3) w lp,\
	'taxaRe1000_2.dat'      u 1:(-$5) w lp,\
	'taxaRe2000_2.dat'      u 1:(-$3) w lp,\
	'taxaRe2000_2.dat'      u 1:(-$5) w lp,\
	'taxaRe5000_2.dat'      u 1:(-$3) w lp,\
	'taxaRe5000_2.dat'      u 1:(-$5) w lp,\
	'taxaRe10000_2.dat'     u 1:(-$3) w lp,\
	'taxaRe10000_2.dat'     u 1:(-$5) w lp,\
	'taxaRe20000_2.dat'     u 1:(-$3) w lp,\
	'taxaRe20000_2.dat'     u 1:(-$5) w lp,\
	'taxaRe30000_2.dat'     u 1:(-$3) w lp,\
	'taxaRe30000_2.dat'     u 1:(-$5) w lp,\
	'taxaRe10000N250_2.dat' u 1:(-$3) w lp,\
	'taxaRe10000N250_2.dat' u 1:(-$5) w lp,\
	'../../compressible/Gamma/homogeneo/G2/taxaJh1/mods1.dat' u 1:3,\
	'../../compressible/Gamma/homogeneo/G2/taxaJh1/mods2.dat' u 1:3

pause mouse keypress "Type a letter from A-F in the active window" 
