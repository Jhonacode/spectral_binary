#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib              as      mpl
from   	path 			import 	pathfinder
from   	plotparameters 		import 	*


M	=	np.loadtxt('./S1h07HO/mods1.dat',unpack=True) 
#N11     =	M.shape[1]
#N12     =	M.shape[0]

omega1_1=	M[0,:]
x1_1	=	M[1,:]
y1_1	=	M[2,:]

M	=	np.loadtxt('./S1h07HO/mods2.dat',unpack=True) 
omega1_2=	M[0,:]
x1_2	=	M[1,:]
y1_2	=	M[2,:]
################################################

M	=	np.loadtxt('./S1h07OO/mods1.dat',unpack=True) 
omega2_1=	M[0,:]
x2_1	=	M[1,:]
y2_1	=	M[2,:]

M	=	np.loadtxt('./S1h07OO/mods2.dat',unpack=True) 

omega2_2=	M[0,:]
x2_2	=	M[1,:]
y2_2	=	M[2,:]

M	=	np.loadtxt('../isotermico/h07/mods1.dat',unpack=True) 
omega22_1=	M[0,:]
x22_1	=	M[1,:]
y22_1	=	M[2,:]

M	=	np.loadtxt('../isotermico/h07/mods2.dat',unpack=True) 

omega22_2=	M[0,:]
x22_2	=	M[1,:]
y22_2	=	M[2,:]
#################################################

M	=	np.loadtxt('./S1h07OH/mods1.dat',unpack=True,skiprows=1) 
omega3_1=	M[0,:]
x3_1	=	M[1,:]
y3_1	=	M[2,:]

M	=	np.loadtxt('./S1h07OH/mods2.dat',unpack=True,skiprows=1) 
omega3_2=	M[0,:]
x3_2	=	M[1,:]
y3_2	=	M[2,:]

#Direct input 
#plt.rcParams['text.latex.preamble']=[r"\usepackage{elsarticle}"]
#Options

mpl.rcParams.update(params)


fig1 = plt.figure()
ax1  = plt.axes()

#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 

plt.xlabel(r' $\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{\alpha_i}$')

plt.plot(omega1_1,y1_1,color='slateblue',dashes=[1, 0],label = '$\mathrm{H_2-O_2}$' )
plt.plot(omega1_2,y1_2,color='slateblue',dashes=[1, 0])
#########omega####################         ########

plt.plot(omega2_1,y2_1,'g',dashes=[1, 1],label ='$\mathrm{O_2-O_2}$' )
plt.plot(omega2_2,y2_2,'g',dashes=[1, 1])

#########omega####################         ########
plt.plot(omega3_1,y3_1,'m',dashes=[3, 1],label ='$\mathrm{O_2-H_2}$' )
plt.plot(omega3_2,y3_2,'m',dashes=[3, 1])


#########################
plt.plot(omega22_1,y22_1,'*g',dashes=[1, 5],label ='$\mathrm{Without}$' )
plt.plot(omega22_2,y22_2,'*g',dashes=[1, 5])

ax1.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax1.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 2.6, 0.01, 0.70])
plt.legend()

plt.savefig('walphai.pdf', format='pdf', dpi=1000)

####################################################################3
fig2 = plt.figure()
ax2  = plt.axes()

plt.xlabel(r' $\mathrm{\alpha_r}$')
plt.ylabel(r'-$\mathrm{\alpha_i}$')

plt.plot(x1_1,y1_1,color='slateblue',dashes=[1, 0],label = '$\mathrm{H-O_2}$' )
plt.plot(x1_2,y1_2,color='slateblue',dashes=[1, 0])
#############################         ########

plt.plot(x2_1,y2_1,color='g',dashes=[1, 1],label ='$\mathrm{O_2-O_2}$' )
plt.plot(x2_2,y2_2,color='g',dashes=[1, 1])

#############################         ########

plt.plot(x3_1,y3_1,color='m',dashes=[3, 1],label ='$\mathrm{O_2-H_2}$' )
plt.plot(x3_2,y3_2,color='m',dashes=[3, 1])

##########################33

plt.plot(x22_1,y22_1,'*',color='g',dashes=[1, 5],label ='$\mathrm{Without}$' )
plt.plot(x22_2,y22_2,'*',color='g',dashes=[1, 5])

plt.legend()
ax2.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax2.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 5.0, 0.01, 0.70])

plt.savefig('alphari.pdf', format='pdf', dpi=1000)

plt.show()


