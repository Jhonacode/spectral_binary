#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import 	matplotlib   		as 	mt 
from   	path 			import 	pathfinder
#from   	Parameters 		import 	*
#from 	SPF 			import 	*

file1	=   	'taxa1'
file2	=   	'taxa2'
file3	=   	'S1h07OO'


M1	=	np.loadtxt('./%s/taxa.dat'%file1,unpack=True) 
M2	=	np.loadtxt('./%s/taxa.dat'%file2,unpack=True) 

M11     =	M1.shape[1]
M12     =	M1.shape[0]
M21     =	M2.shape[1]
M22     =	M2.shape[0]


#0 line, frequency
Omega	=	np.concatenate([M1[0,:]])

#1 and  3 line, alphar
X	=	np.concatenate([M1[1,:],M1[3,:]])
X1	=	np.concatenate([M1[1,:],M1[3,:]])

#2 and 4 line, alphai 
Y	=	np.concatenate([M1[2,:],M1[4,:]])

g       =   open('dados.dat','w+')

nn	=   X.shape[0]

for j in range (0,nn):
	
	g.write("%f\t%f\n"%(X[j],Y[j])); 


(x1,y1,n1)	=pathfinder(X,Y,5,distweight=6.5,stopweight=6.0,isextremity=False) 

X[n1]		=np.nan

(x2,y2,n2)	=pathfinder(X,Y,7,distweight=6.5,stopweight=6.5,isextremity=False) 




nn	=   x1.shape[0]
nn2	=   X1.shape[0]

g1       =   open('./mods1.dat','w+')

for j in range (0,nn):
	
	for i in range (0,nn2):

		if(x1[j]==X1[i] ):

			g1.write("%f\t%f\t%f\n"%(Omega[j],x1[j],y1[j])); 

nn	=   x2.shape[0]
nn2	=   X1.shape[0]

g2       =   open('./mods2.dat','w+')

print(nn2)

for j in range (0,nn):

	for i in range (0,nn2):

		if(x2[j]==X1[i] ):

			g2.write("%f\t%f\t%f\n"%(Omega[j],x2[j],y2[j])); 


#Direct input 
#plt.rcParams['text.latex.preamble']=[r"\usepackage{elsarticle}"]
#Options

fig = plt.figure()

params = {
	  
	#'text.usetex' : True,
	#'font.family' : 'lmodern',
	#'font.family' : 'elsarticle',
	'font.family' : 'serif',
	#'font.serif'  : 'Computer Modern Sans serif',
	'font.sans-serif'    : 'Helvetica',#, Avant Garde, Computer Modern Sans serif
	#'font.sans-serif'    : 'Computer Modern Sans serif',#, Avant Garde, Computer Modern Sans serif
	'lines.linewidth':2,
        'font.size' : 15,
        'text.latex.unicode': True,
	#'legend.fontsize': 'x-large',
        'figure.figsize': (20, 10),
	'axes.labelsize': '20',#'x-Large',
        'axes.titlesize': '25',#'x-Large',
        'xtick.labelsize':'15',#'x-Large',
        'ytick.labelsize':'15',#'x-Large'
          }
plt.rcParams.update(params) 


#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 

ax = plt.axes()

plt.xlabel(r'$\alpha_r$')
plt.ylabel(r'-$\alpha_i$')
plt.plot(x1,y1,'-r',label ='Mode I ')
plt.plot(x2,y2,'-.b',label ='Mode II')
plt.savefig('%s.eps'%(file3), format='eps', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 5, 0.01, 0.75])


plt.legend()
#plt.plot(X[10],Y[10],'ko')
#plt.plot(X[11],Y[11],'ko')
#plt.plot(X,Y,'k.')
#SPF()
plt.show()


