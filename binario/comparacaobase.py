#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import 	matplotlib   		as 	mt 
from   	path 			import 	pathfinder
#from   	Parameters 		import 	*
#from 	SPF 			import 	*

#r,w,t,rho,y1,y2

M	=	np.loadtxt('./S1h07HO/taxa2/baseflow.dat',unpack=True) 
#N11     =	M.shape[1]
#N12     =	M.shape[0]

r_1	=	M[0,:]
Wb_1	=	M[1,:]
Tb_1	=	M[2,:]
Rhob_1	=	M[3,:]
Y1_1	=	M[4,:]
Y2_1	=	M[5,:]

################################################ 
M	=	np.loadtxt('./S1h07OO/taxa2/baseflow.dat',unpack=True) 
#N11     =	M.shape[1]
#N12     =	M.shape[0]

r_2	=	M[0,:]
Wb_2	=	M[1,:]
Tb_2	=	M[2,:]
Rhob_2	=	M[3,:]
Y1_2	=	M[4,:]
Y2_2	=	M[5,:]
################################################

M	=	np.loadtxt('./S1h07OH/taxa2/baseflow.dat',unpack=True) 
#N11     =	M.shape[1]
#N12     =	M.shape[0]

r_3	=	M[0,:]
Wb_3	=	M[1,:]
Tb_3	=	M[2,:]
Rhob_3	=	M[3,:]
Y1_3	=	M[4,:]
Y2_3	=	M[5,:]
#Direct input 
#plt.rcParams['text.latex.preamble']=[r"\usepackage{elsarticle}"]
#Options

fig = plt.figure(1)

params = {
	  
	#'text.usetex' : True,
	#'font.family' : 'lmodern',
	#'font.family' : 'elsarticle',
	'font.family' : 'serif',
	#'font.serif'  : 'Computer Modern Sans serif',
	'font.sans-serif'    : 'Helvetica',#, Avant Garde, Computer Modern Sans serif
	#'font.sans-serif'    : 'Computer Modern Sans serif',#, Avant Garde, Computer Modern Sans serif
	'lines.linewidth':2,
        'font.size' : 15,
        'text.latex.unicode': True,
	#'legend.fontsize': 'x-large',
        'figure.figsize': (20, 10),
	'axes.labelsize': '20',#'x-Large',
        'axes.titlesize': '25',#'x-Large',
        'xtick.labelsize':'15',#'x-Large',
        'ytick.labelsize':'15',#'x-Large'
          }
plt.rcParams.update(params) 


#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 

ax = plt.axes()

plt.ylabel(r'$r$')
plt.xlabel(r'$Y_1$')

plt.plot(Y1_1,r_1,'g',dashes=[1, 0],label = 'H-O')
plt.plot(Y1_2,r_2,'b',dashes=[2, 0],label = 'O-O')
plt.plot(Y1_3,r_3,'k',dashes=[3, 0],label = 'O-H')
##############################         ########
############
plt.savefig('%especies.eps', format='eps', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.5))
plt.axis([-0.01, 1, 0.00, 4.00])

plt.legend()

fig = plt.figure(2)

plt.ylabel(r'$r$')
plt.xlabel(r'$\bar{\rho}$')

plt.plot(Rhob_1,r_1,'g',dashes=[1, 0],label = 'H-O')
plt.plot(Rhob_2,r_2,'b',dashes=[2, 0],label = 'O-O')
plt.plot(Rhob_3,r_3,'k',dashes=[3, 0],label = 'O-H')
##############################         ########
############
plt.savefig('%especies.eps', format='eps', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.5))
plt.axis([0.00, 16, 0.00, 4.00])

plt.legend()

plt.show()


