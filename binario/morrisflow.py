
Mc   = 0.50
#*********Thickness of the jet, base flow 
bba  = 0.25
#*********internal radio of the jet, base flow,solution morrismeanflow.f90 
hba  = 0.8071800
#*********Morris, The instability of hight speed jets 
#*********Reservoir temperature
TR   = 0.60
#*********Ambient temperature
T0   = 0.60
#*********Heat capacity relation 
gamma= 1.40

Wb  =   zeros(N+1)
Tb  =   zeros(N+1)
Rhob=   zeros(N+1)

for j in range (0,N+1):

    if(r[j]<hba):
        Wb[j]   = Mc  
    else: 
        Wb[j]   = Mc*exp(-log(2.0)*(r[j]-hba)**2.0/bba**2.0)  
    

for j in range (0,N+1):
    #Development Jet
    #wb[j]    = Mc*exp(-log(2.d0)*n[j]**2.d0/bba**2.d0)
    Tb[j]    = T0/TR*(1.0+(gamma-1.0)/2.0*Mc**2)*(1.0+(Wb[j])*(TR/T0-1.0))-(gamma-1.0)/2.0*Mc**2.0*(Wb[j]**2.0)
    Rhob[j]  = 1.0/(Tb[j])
