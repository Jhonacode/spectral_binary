#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import 	matplotlib   		as 	mt 
from   	path 			import 	pathfinder
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


M	=	np.loadtxt('./mods1.dat',unpack=True) 
#N11     =	M.shape[1]
#N12     =	M.shape[0]

omega1_1=	M[0,:]
x1_1	=	M[1,:]
y1_1	=	M[2,:]

phase_v1=	(omega1_1)/(x1_1)

M	=	np.loadtxt('./mods2.dat',unpack=True) 
omega1_2=	M[0,:]
x1_2	=	M[1,:]
y1_2	=	M[2,:]

phase_v2=	(omega1_2)/(x1_2)
################################################

M	=	np.loadtxt('./taxa1/baseflow.dat',unpack=True,skiprows=1) 

r_1	=	M[0,:]
Wb_1	=	M[1,:]
Tb_1	=	M[2,:]
Rhob_1	=	M[3,:]



#Direct input 
#plt.rcParams['text.latex.preamble']=[r"\usepackage{elsarticle}"]
#Options

fig = plt.figure(0)

params = {
	  
	#'text.usetex' : True,
	#'font.family' : 'lmodern',
	#'font.family' : 'elsarticle',
	'font.family' : 'serif',
	#'font.serif'  : 'Computer Modern Sans serif',
	'font.sans-serif'    : 'Helvetica',#, Avant Garde, Computer Modern Sans serif
	#'font.sans-serif'    : 'Computer Modern Sans serif',#, Avant Garde, Computer Modern Sans serif
	'lines.linewidth':2,
        'font.size' : 15,
        'text.latex.unicode': True,
	#'legend.fontsize': 'x-large',
        'figure.figsize': (20, 10),
	'axes.labelsize': '20',#'x-Large',
        'axes.titlesize': '25',#'x-Large',
        'xtick.labelsize':'15',#'x-Large',
        'ytick.labelsize':'15',#'x-Large'
          }
plt.rcParams.update(params) 


#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 

ax = plt.axes()

plt.xlabel(r'$\alpha_r$')
plt.ylabel(r'-$\alpha_i$')

plt.plot(x1_1,y1_1,'-.r',label = 'h=0.7 ')
plt.plot(x1_2,y1_2,'-.r')
################################
############
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.01, 5.0, 0.01, 0.70])

plt.savefig('%geometria.eps', format='eps', dpi=1000)

plt.legend()


fig = plt.figure(1)

plt.xlabel(r'$\omega$')
plt.ylabel(r'-$\alpha_i$')

plt.plot(omega1_1,y1_1,'-.b',label = 'h=0.7 ')
plt.plot(omega1_2,y1_2,'-.b')
############
plt.savefig('%omega.eps', format='eps', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.01, 3.0, 0.01, 0.75])

plt.legend()
plt.show()

fig = plt.figure(2)

plt.xlabel(r'$\omega$')
plt.ylabel(r'Phase Velocity = $\frac{\omega}{\alpha_r}$')

plt.plot(x1_1,phase_v1,'-.y',label = 'h=0.7 ')
plt.plot(x1_2,phase_v2,'-.m')
###########
plt.savefig('%omega.eps', format='eps', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 3.6, 0.01, 0.7])

plt.legend()


fig = plt.figure(3)

plt.xlabel(r'$\bar{W}$')
plt.ylabel(r'$r$')

plt.plot(Wb_1,r_1,'-.y',label = 'Base Flow ')
###########
plt.savefig('%omega.eps', format='eps', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.2))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.5))
plt.axis([0.0, 0.7, 0.0, 4.50])

plt.legend()

#plt.plot(X[10],Y[10],'ko')
#plt.plot(X[11],Y[11],'ko')
#plt.plot(X,Y,'k.')
#SPF()
plt.show()


