#################################3#
# Program to generate the 
# instability curves, necesaries 
# reproduce the Joncas and Perreu paper 
#
# Create by: Jhonatan Aguirre 
# Date:08/11/2018
# working: no
###################################


from   chebPy           import *
from   mapping          import *
from   Baseflow         import *
from   Parameters  	import *
import numpy      	as     np
import matplotlib 	as     pl

D,n1	= cheb(N)

# Mappint to concentrate the point in 0. Withou using 0
# r= Radial coordinate
r   	= mappingtan(N,n1,maxr,D)
D2	= dot(D,D)
#Base Flow

        
Wb_dns,Tb_dns,Rhob_dns,Y1_dns,Y2_dns  =   Baseflow_dns(r,N)
Wb_lst,Tb_lst,Rhob_lst,Y1_lst,Y2_lst  =   Baseflow_lst(r,N)

#(mib,kb,cpb,cpb1,cpb2,D1m,Mw,Ru,Rmix,gammab,gamma_j,gamma_ratio,P_r,a_ref,a_s,a_1,a_2)=\#        df.properties.init_diffusion(Y1,Y2,Tb,T_i,T_0,T_ref,N)

#Rhob  =  1.0/(Tb*Rmix) 


g1   	=   open('baseflow_dns.dat','w+') 
g2   	=   open('baseflow_lst.dat','w+') 

for j in range (0,N+1):

            g1.write("%f\t%f\t%f\t%f\t%f\t%f\n"%(r[j],Wb_dns[j],Tb_dns[j],Rhob_dns[j],Y1_dns[j],Y2_dns[j]));
            g1.write("%f\t%f\t%f\t%f\t%f\t%f\n"%(r[j],Wb_lst[j],Tb_lst[j],Rhob_lst[j],Y1_lst[j],Y2_lst[j]));


g1.close()
g2.close()

plt.plot(Wb_dns,r)
plt.plot(Wb_lst,r)

#ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
#ax.yaxis.set_major_locator(plt.MultipleLocator(0.05))
plt.axis([0.0, 0.8, 0.00, 4.00])

plt.legend()

plt.show()


