#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*


M	=	np.loadtxt('./G12h07M201/mods1.dat',unpack=True) 

omega1_1=	M[0,:]
y1_1	=	M[2,:]

################################################

M	=	np.loadtxt('./perturbation/G12M201/zmalha/naouniformetaxa.dat',unpack=True) 
omega2_1=	M[0,:]
y2_1	=	M[1,:]

M	=	np.loadtxt('./perturbation/G12M201/zmalha2/naouniformetaxa.dat',unpack=True) 
omega3_1=	M[0,:]
y3_1	=	M[1,:]

M	=	np.loadtxt('./perturbation/G12M201/zmalha3/naouniformetaxa2.dat',unpack=True) 
omega4_1=	M[0,:]
y4_1	=	M[1,:]

################################################

################################################
mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()

ax.legend()
ax.legend(frameon=False)


plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{k_i}$')
markers_on = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2]

plt.plot(omega1_1,y1_1,color='slateblue',dashes=[1, 0],label ='$\mathrm{LST}$')
################################                                 
#plt.plot(omega2_1,y2_1,'m*',markersize=10,dashes=[1, 1],label ='$\mathrm{\Delta_z=0.02,\Delta_r=0.01}$')
#plt.plot(omega4_1,y4_1,'bX',markersize=10,dashes=[2, 1],label ='$\mathrm{\Delta_z=0.01,\Delta_r=0.01}$')
#plt.plot(omega3_1,y3_1,'gP',markersize=10,dashes=[1, 2],label ='$\mathrm{\Delta_z=0.01,\Delta_r=0.005}$')
plt.plot(omega2_1,y2_1,'m*',markersize=13,label ='$\mathrm{\Delta_z=0.02,\Delta_r=0.01}$')
plt.plot(omega4_1,y4_1,'bX',markersize=13,label ='$\mathrm{\Delta_z=0.01,\Delta_r=0.01}$')
plt.plot(omega3_1,y3_1,'gP',markersize=13,label ='$\mathrm{\Delta_z=0.01,\Delta_r=0.005}$')
################################                                 
ax.xaxis.set_major_locator(plt.MultipleLocator(0.2))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 1.7, 0.01, 0.50])
################################                                 
ax.legend(loc='upper right',fontsize=26,frameon=False)
plt.savefig('comparacaodnsperturbation.pdf', format='pdf', dpi=1000)
#
plt.show()


