#! /usr/bin/python 
#FFT of DNS code to find the 
#spatial eigenvalues 
# Create by: Jhonatan
# date: 06-03-2018
#Module for symbolic albegra 
import numpy as np
import math  as mt
import read  as rd
#from math import exp, expm1
import matplotlib.pyplot as plt
#from sci to make fft
import scipy.fftpack
from scipy.io import FortranFile

f = open('naouniformetaxa.dat','w+')
################################
# Letura de aquivos para obter as transformadas


#Fortran program diffusion.f90 
#To run the fortran subroutine.
#f2py -m read -c   read.f90 

#time,ecx,m =rd.fortran.read_energyfile(nfile)

#number of frequency files 
N       = 7
#name of frequency files
file1   ='ekx3_'
# frequency  
wa      = range(0,N,1)

pi      = mt.pi
################################
#step of point in time 
dt      = 0.005
################################
#step of point in x direction 
dx      = 0.04
################################
x_ini   = 14.0
################################

#mean of the alpha using fortwar differences
alpham1  =   np.zeros(N)

alpham1t  =   np.zeros(N)
#mean of the alpha using central differences
alpham3  =   np.zeros(N)
#mean of the alpha using four orde scheme 
alpham4  =   np.zeros(N)

for i in range(2,N):

    nfile='%s%d.dat'%(file1,i)

    #Program to read fortran files
    time,M1,wa[i],m =rd.fortran.read_energyfile(nfile)


    #Number of columns of the Matrix,Matrix[t ekx1 ekx1+dx ekx1+2dx ekx1+3dx...........]
    #################################

    #Number of rows 
    N1  =   m 
    #Number of columns 
    N2  =   M1.shape[0]
    #print N1,N2

    x1  =   time[0:N1]
    f1  =   M1[0][0:N1]

    #Initialization of variables
    #FFT, Matrix
    H        =   np.zeros((N2,N1))
    #alpha 2 pontos, Matrix
    alpha1   =   np.zeros((N2))
    #alpha 3 pontos, Matrix  
    alpha3   =   np.zeros((N2))
    #alpha 4 pontos, Matrix  
    alpha4   =   np.zeros((N2))

    ######################################
    #fft of the temporal Marix M
    for l in range(0,N2):
        H[l,:]   =   scipy.fftpack.fft(M1[l][0:N1])
        H        =   H.real

    N1  =   H.shape[1]
    N2  =   H.shape[0]

    #plt.plot(x1,H[0,:],'r')
    #plt.plot(x1,H[1,:],'b')
    #plt.plot(x1,H[2,:],'m')
    #plt.show()

    #####################################
    #frequency step
    dw   =    pi/(dt*N1)

    #Created the frequency vetor 
    k    =    range(0,N1,1)
    kf   =    np.asfarray(k,dtype='float')
    kf   =    kf*dw

    gaint      =    range(0,N1,1)
    alpha1t    =    range(0,N1,1)
    #plt.plot(kf,H[3,:],'*-b')
    #plt.plot(kf,H[4,:],'-r')
    #plt.grid() 
    #plt.show()

    
    ######################################

    #for l in range(1,N2-1):

    for ii in range(0,N1):

            gaint[ii]  = (M1[1][ii])/(M1[0][ii])
            alpha1t[ii]  = -1.0/2.0*(mt.log(float(gaint[ii])))*1.0/dx

    alpham1t[i] =np.mean(alpha1t[0:N1],axis=0)

   # print 'alpha',alpham1t

    ll=5
    for l in range(1,N2-1):
    #for l in range(5,6): 

            gain         = (H[l+1][0])/(H[l][0])
            #print gain

            #gain         = (H[l+1][0])/(H[l][0])
            alpha1[l]   = -1.0/2.0*(mt.log(float(gain)))*1.0/dx

            gain3            = (H[l+1][0])/(H[l-1][0])
            #gain3           = (H[l+1][0])/(H[l-1][0])
            alpha3[l] = -1.0/2.0*(mt.log(float(gain3)))*1.0/(2.0*dx)

            #print wa[i],alpha1[l][i],alpha3[l][i]
            gain4        = H[ll+3][0]**8*H[ll][0]/(H[ll+4][0]*H[ll+1][0]**8)
            ##gain4        = H[l+3][0]**8*H[l][0]/(H[l+4][0]*H[l+1][0]**8)
            alpha4 = -1.0/2.0*(mt.log(float(gain4)))*1.0/(12.0*dx)

    #plt.plot(kf,gaint[0:N1],'*b',alpha1t[0:N1],'-r')
    #plt.grid() 
    #plt.show()

    alpham1[i]=np.mean(alpha1[1:N2-2],axis=0)
    alpham3[i]=np.mean(alpha3[1:N2-2],axis=0)
    alpham4[i]=alpha4

    print wa[i],alpham1[i],alpham1t[i],alpham3[i],alpham4[i]

    #alpham1[i]=alpha1[l][i]
    #alpham3[i]=alpha3[l][i]
    #alpham4[i]=alpha4[l][i]



plt.plot(wa,-alpham1 ,'*b',label='alpha1, Forwart')
plt.plot(wa,-alpham1t,'*r',label='alpha1, completo')
plt.plot(wa,-alpham3 ,'*y',label='alpha3, central')
plt.plot(wa,-alpham4 ,'*m',label='alpha4, Four Order')
plt.grid()
plt.legend()
plt.show()

for i in range(0,N):

    f.write("%f\t%f\t%f\t%f\t%f\n"%(wa[i],-alpham1[i],-alpham1t[i],-alpham3[i],-alpham4[i]))

f.close



