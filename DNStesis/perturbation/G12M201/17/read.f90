MODULE fortran 

contains 

SUBROUTINE read_energyfile(time,ecx,omega,m,nfile)


implicit none
INTEGER, PARAMETER :: n = 10000 ! Number of points in the domain of the velocity field
!f2py intent(in)::nfile
!f2py intent(out)::time,ecx,omega,m
INTEGER :: i, m, IO, nn ! Index
integer, parameter :: ip = selected_real_kind(15, 307)
real(kind=ip)::omega
real(kind=ip),dimension(1:n)::time
real(kind=ip),dimension(1:10,1:n)::ecx
character(40)::nfile


! File reading

40      FORMAT(1x,d20.14,2x,d20.14,2x,d20.14,2x,d20.14, &
        & 2x,d20.14,2x,d20.14,2x,d20.14,2x,d20.14, &
        & 2x,d20.14,2x,d20.14,2x,d20.14)

! Reads the first file:

!open(unit=icount+201,file=contornofile1,form="formatted",status='unknown')
!write (nfile,'("ekx1_"I0".dat")')nn
OPEN(UNIT=102,FILE=nfile,ACTION='READ') ! <-- mudar o nome dor arquivo aqui



m = 0
DO ! Loop over file lines

        IF (m==0) THEN 
                READ(102,40,IOSTAT=IO) omega 
        ELSE ! If the line is not empty,

                READ(102,40,IOSTAT=IO) time(m),(ecx(i,m),i=1,10) 
        ENDIF

        IF (IO.LT.0) THEN 

                EXIT ! Stop if end of file is reached

        ELSE ! If the line is not empty,
                m = m + 1
        ENDIF
ENDDO

m=m-1

CLOSE(102)

END SUBROUTINE

END MODULE
