#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


M	=	np.loadtxt('./G2h07M201/taxa1/baseflow.dat',unpack=True) #N11     =	M.shape[1]
#N12     =	M.shape[0]

x1_1	=	M[0,:]
y1_1	=	M[1,:]

################################################

M	=	np.loadtxt('../compressible/Gamma/homogeneo/G2/taxaJh1/baseflow.dat',unpack=True) 
x2_1	=	M[0,:]
y2_1	=	M[1,:]

################################################

################################################
mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()

ax.legend()
ax.legend(frameon=False)


plt.xlabel(r'$\mathrm{\overline{w}}$')
plt.ylabel(r'r')


plt.plot(y1_1,x1_1,color='slateblue',dashes=[1, 0],label ='Coflow')
################################                                 
plt.plot(y2_1,x2_1,'m',label ='LST')

################################                                 
ax.xaxis.set_major_locator(plt.MultipleLocator(0.1))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.5))
plt.axis([-0.01, 0.7, 0.01, 4])
################################                                 
ax.legend(frameon=False)
plt.savefig('baseflowdns.pdf', format='pdf', dpi=1000)
#
plt.show() 
