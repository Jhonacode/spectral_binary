#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


M	=	np.loadtxt('./G13h07M201/mods1.dat',unpack=True) #N11     =	M.shape[1]
#N12     =	M.shape[0]

omega1_1=	M[0,:]
y1_1	=	M[2,:]

################################################

M	=	np.loadtxt('./w015_075/naouniformetaxa.dat',unpack=True) 
x2_1	=	M[1,:]
y2_1	=	M[2,:]
omega2_1=	M[0,:]
phase2_1=	(omega2_1)/(x2_1)

################################################

################################################
mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()

ax.legend()
ax.legend(frameon=False)


plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{k_i}$')


plt.plot(omega1_1,y1_1,color='slateblue',dashes=[1, 0],label ='$\mathrm{LST}$')
################################                                 
plt.plot(omega2_1,y2_1,'m*',markersize=10,label ='$\mathrm{DNS}$')

################################                                 
ax.xaxis.set_major_locator(plt.MultipleLocator(0.1))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.05))
plt.axis([0.1, 0.75, 0.01, 0.35])
################################                                 
ax.legend(frameon=False)
plt.savefig('comparacaodns.pdf', format='pdf', dpi=1000)
#
plt.show()


