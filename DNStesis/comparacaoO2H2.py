#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*


M	=	np.loadtxt('./O2H2M202G2/mods2.dat',unpack=True) 

omega1_1=	M[0,:]
y1_1	=	M[2,:]

################################################

M	=	np.loadtxt('./eulerwo2h2/naouniformetaxa.dat',unpack=True) 
omega2_1=	M[0,:]
y2_1	=	M[1,:]

M	=	np.loadtxt('./eulerwo2h2/pontomeio/naouniformetaxa.dat',unpack=True) 
omega2_2=	M[0,:]
y2_2	=	M[1,:]

################################################

################################################
mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()

ax.legend()
ax.legend(frameon=False)


plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{k_i}$')


plt.plot(omega1_1,y1_1,color='slateblue',dashes=[1, 0],label ='$\mathrm{LST}$')
################################                                 
plt.plot(omega2_1,y2_1,'m*',markersize=10,label ='$\mathrm{DNS}$')
plt.plot(omega2_2,y2_2,'m*',markersize=10                        )

################################                                 
ax.xaxis.set_major_locator(plt.MultipleLocator(0.1))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.05))
plt.axis([0.1, 1.1, 0.01, 0.20])
################################                                 
ax.legend(frameon=False)
plt.savefig('comparacaodnsO2H2.pdf', format='pdf', dpi=1000)
#
plt.show()


