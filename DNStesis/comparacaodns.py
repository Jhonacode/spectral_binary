#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


M	=	np.loadtxt('./G2h07M201/mods1.dat',unpack=True) #N11     =	M.shape[1]

x1_1	=	M[0,:]
y1_1	=	M[2,:]

M	=	np.loadtxt('./G2h07M201/mods2.dat',unpack=True) #N11     =	M.shape[1]
x1_2	=	M[0,:]
y1_2	=	M[2,:]

################################################

M	=	np.loadtxt('../compressible/Gamma/homogeneo/G2/taxaJh1/mods1.dat',unpack=True) 
x2_1	=	M[0,:]
y2_1	=	M[2,:]

M	=	np.loadtxt('../compressible/Gamma/homogeneo/G2/taxaJh1/mods2.dat',unpack=True) 
x2_2	=	M[0,:]
y2_2	=	M[2,:]
################################################

################################################
mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()

ax.legend()
ax.legend(frameon=False)


plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{k_i}$')

plt.text(1.4, 0.20,r'ModeI')
plt.text(0.6, 0.65,r'ModeII')

plt.plot(x1_1,y1_1,color='slateblue',dashes=[1, 0],label ='LST $\mathrm{M_\infty=0.1}$')
plt.plot(x1_2,y1_2,color='slateblue',dashes=[1, 0])
################################                                 
plt.plot(x2_1,y2_1,'m',dashes=[1, 1],label ='LST $\mathrm{M_\infty=0.0}$')
plt.plot(x2_2,y2_2,'m',dashes=[1, 1])

################################                                 
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([ 0.01, 3,0.01, 0.7,])
################################                                 
ax.legend(frameon=False)
plt.savefig('G2h07dnslst.pdf', format='pdf', dpi=1000)
#
plt.show() 
