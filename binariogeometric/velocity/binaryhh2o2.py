#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*
#from   	Parameters 		import 	*
#from 	SPF 			import 	*

###############################################

M	=	np.loadtxt('./H2O2G2h05/mods1.dat',unpack=True) 
omega1_1=	M[0,:]
x1_1	=	M[1,:]
y1_1	=	M[2,:]
phase1_1=	(omega1_1)/(x1_1)

M	=	np.loadtxt('./H2O2G2h05/mods2.dat',unpack=True) 

omega1_2=	M[0,:]
x1_2	=	M[1,:]
y1_2	=	M[2,:]
phase1_2=	(omega1_2)/(x1_2)

M	=	np.loadtxt('./H2O2G2h05/mods22.dat',unpack=True) 

omega1_22=	M[0,:]
x1_22	=	M[1,:]
y1_22	=	M[2,:]
phase1_22=	(omega1_22)/(x1_22)

###############################################

M	=	np.loadtxt('./H2O2G2/mods1.dat',unpack=True) 
omega2_1=	M[0,:]
x2_1	=	M[1,:]
y2_1	=	M[2,:]
phase2_1=	(omega2_1)/(x2_1)

M	=	np.loadtxt('./H2O2G2/mods2.dat',unpack=True) 

omega2_2=	M[0,:]
x2_2	=	M[1,:]
y2_2	=	M[2,:]
phase2_2=	(omega2_2)/(x2_2)


M	=	np.loadtxt('./H2O2G2/mods22.dat',unpack=True) 

omega2_22=	M[0,:]
x2_22	=	M[1,:]
y2_22	=	M[2,:]
phase2_22=	(omega2_22)/(x2_22)

################################################
M	=	np.loadtxt('./H2O2G2h09/mods1.dat',unpack=True) 

omega3_1=	M[0,:]
x3_1	=	M[1,:]
y3_1	=	M[2,:]
phase3_1=	(omega3_1)/(x3_1)

M	=	np.loadtxt('./H2O2G2h09/mods2.dat',unpack=True) 

omega3_2=	M[0,:]
x3_2	=	M[1,:]
y3_2	=	M[2,:]
phase3_2=	(omega3_2)/(x3_2)


################################################
mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()

ax.legend()
ax.legend(frameon=False)

#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 


plt.xlabel(r' $\mathrm{\alpha_r}$')
plt.ylabel(r'-$\mathrm{\alpha_i}$')
#plt.xlabel(r'$\alpha_r$')
#plt.ylabel(r'$-\alpha_i$')

#########x####################         ########
plt.plot(x1_1,y1_1,color='slateblue',dashes=[1, 0],label = '$\mathrm{h=0.5}$' )
plt.plot(x1_2,y1_2,color='slateblue',dashes=[1, 0])
plt.plot(x1_22,y1_22,color='slateblue',dashes=[1, 0])
#########x####################         ########
plt.plot(x2_1,y2_1,'g',dashes=[1, 1],label = '$\mathrm{h=0.7}$' )
plt.plot(x2_2,y2_2,'g',dashes=[1, 1])
plt.plot(x2_22,y2_22,'g',dashes=[1, 1])
#########x####################         ########
plt.plot(x3_1,y3_1,'m',dashes=[3, 1],label = '$\mathrm{h=0.9}$' )
plt.plot(x3_2,y3_2,'m',dashes=[3, 1])

ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([-0.0, 3.8, 0.01, 1.00])

plt.legend()
ax.legend(frameon=False)
plt.savefig('alpharih2o2.pdf', format='pdf', dpi=1000)


###############################################33
mpl.rcParams.update(params)

fig2 = plt.figure()
ax2  = plt.axes()


plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{\alpha_i}$')


plt.plot(omega1_1,y1_1,color='slateblue',dashes=[1, 0],label = '$\mathrm{h=0.5}$' )
plt.plot(omega1_2,y1_2,color='slateblue',dashes=[1, 0])
plt.plot(omega1_22,y1_22,color='slateblue',dashes=[1, 0])
#########omega####################         ########
plt.plot(omega2_1,y2_1,'g',dashes=[1, 1],label = '$\mathrm{h=0.7}$' )
plt.plot(omega2_2,y2_2,'g',dashes=[1, 1])
plt.plot(omega2_22,y2_22,'g',dashes=[1, 1])
#########omega####################         ########
plt.plot(omega3_1,y3_1,'m',dashes=[3, 1],label = '$\mathrm{h=0.9}$' )
plt.plot(omega3_2,y3_2,'m',dashes=[3, 1])

################################                                 
#############
ax2.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax2.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([-0.0, 1.7, 0.01, 1.00])

plt.legend()
ax2.legend(frameon=False)
plt.savefig('walpharh2o2.pdf', format='pdf', dpi=1000)

###############################################33
mpl.rcParams.update(params)

fig2 = plt.figure()
ax2  = plt.axes()


plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'$\mathrm{C_p}$')


plt.plot(omega1_1,phase1_1,color='slateblue',dashes=[1, 0],label = '$\mathrm{h=0.5}$' )
plt.plot(omega1_2,phase1_2,color='slateblue',dashes=[1, 0])
plt.plot(omega1_22,phase1_22,color='slateblue',dashes=[1, 0])
#########omega####phase###############         ########
plt.plot(omega2_1,phase2_1,'g',dashes=[1, 1],label = '$\mathrm{h=0.7}$' )
plt.plot(omega2_2,phase2_2,'g',dashes=[1, 1])
plt.plot(omega2_22,phase2_22,'g',dashes=[1, 1])
#########omega####phase###############         ########
plt.plot(omega3_1,phase3_1,'m',dashes=[3, 1],label = '$\mathrm{h=0.9}$' )
plt.plot(omega3_2,phase3_2,'m',dashes=[3, 1])
#########omega####phase###############         ########
################################                                 
#############
ax2.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax2.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.0, 1.7, 0.19, 1.0])

plt.legend()
ax2.legend(frameon=False)
plt.savefig('phaseh2o2.pdf', format='pdf', dpi=1000)
#
#
#
##plt.plot(X[10],Y[10],'ko')
##plt.plot(X[11],Y[11],'ko')
##plt.plot(X,Y,'k.')
##SPF()
plt.show()


