#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import 	matplotlib   		as 	mt 
from   	path 			import 	pathfinder
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


M	=	np.loadtxt('./h05/mods1.dat',unpack=True,skiprows=1) 
#N11     =	M.shape[1]
#N12     =	M.shape[0]

omega1_1=	M[0,:]
x1_1	=	M[1,:]
y1_1	=	M[2,:]

M	=	np.loadtxt('./h05/mods2.dat',unpack=True,skiprows=1) 
omega1_2=	M[0,:]
x1_2	=	M[1,:]
y1_2	=	M[2,:]
################################################

M	=	np.loadtxt('./h07/mods1.dat',unpack=True,skiprows=1) 
omega2_1=	M[0,:]
x2_1	=	M[1,:]
y2_1	=	M[2,:]

M	=	np.loadtxt('./h07/mods2.dat',unpack=True,skiprows=1) 

omega2_2=	M[0,:]
x2_2	=	M[1,:]
y2_2	=	M[2,:]
################################################

M	=	np.loadtxt('./h09/mods1.dat',unpack=True,skiprows=1) 
omega3_1=	M[0,:]
x3_1	=	M[1,:]
y3_1	=	M[2,:]

M	=	np.loadtxt('./h09/mods2.dat',unpack=True,skiprows=1) 
omega3_2=	M[0,:]
x3_2	=	M[1,:]
y3_2	=	M[2,:]


#Direct input 
#plt.rcParams['text.latex.preamble']=[r"\usepackage{elsarticle}"]
#Options

fig = plt.figure(1)

params = {
	  
	#'text.usetex' : True,
	#'font.family' : 'lmodern',
	#'font.family' : 'elsarticle',
	'font.family' : 'serif',
	#'font.serif'  : 'Computer Modern Sans serif',
	'font.sans-serif'    : 'Helvetica',#, Avant Garde, Computer Modern Sans serif
	#'font.sans-serif'    : 'Computer Modern Sans serif',#, Avant Garde, Computer Modern Sans serif
	'lines.linewidth':2,
        'font.size' : 15,
        'text.latex.unicode': True,
	#'legend.fontsize': 'x-large',
        'figure.figsize': (20, 10),
	'axes.labelsize': '20',#'x-Large',
        'axes.titlesize': '25',#'x-Large',
        'xtick.labelsize':'15',#'x-Large',
        'ytick.labelsize':'15',#'x-Large'
          }
plt.rcParams.update(params) 


#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 

ax = plt.axes()

plt.xlabel(r'$\alpha_r$')
plt.ylabel(r'-$\alpha_i$')

plt.plot(x1_1,y1_1,'g',dashes=[1, 0],label = 'h=0.5 ')
plt.plot(x1_2,y1_2,'g',dashes=[1, 0])
##############################         ########
plt.plot(x2_1,y2_1,'b',dashes=[2, 2],label ='h=0.7 ')
plt.plot(x2_2,y2_2,'b',dashes=[2, 2])
##############################         ########
plt.plot(x3_1,y3_1,'m',dashes=[3, 1],label ='h=0.9 ')
plt.plot(x3_2,y3_2,'m',dashes=[3, 1])
############
plt.savefig('%geometria.eps', format='eps', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.05))
plt.axis([0.1, 5, 0.01, 0.70])

plt.legend()

plt.figure(2)
plt.xlabel(r'$\omega$')
plt.ylabel(r'-$\alpha_i$')

plt.plot(omega1_1,y1_1,'g',dashes=[1, 0],label = 'h=0.5 ')
plt.plot(omega1_2,y1_2,'g',dashes=[1, 0])
################################
plt.plot(omega2_1,y2_1,'b',dashes=[2, 2],label ='h=0.7 ')
plt.plot(omega2_2,y2_2,'b',dashes=[2, 2])
################################
plt.plot(omega3_1,y3_1,'m',dashes=[3, 1],label ='h=0.9 ')
plt.plot(omega3_2,y3_2,'m',dashes=[3, 1])
############
plt.savefig('%omega.eps', format='eps', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.05))
plt.axis([0.1, 3, 0.01, 0.70])

plt.legend()



#plt.plot(X[10],Y[10],'ko')
#plt.plot(X[11],Y[11],'ko')
#plt.plot(X,Y,'k.')
#SPF()
plt.show()


