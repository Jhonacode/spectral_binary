#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib as mpl
from   	plotparameters 		import 	*
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


################################################

M	=	np.loadtxt('./allspec.dat',unpack=True) 

omega1_1=	M[0,:]
x1_1	=	M[1,:]
y1_1	=	M[2,:]
phase1_1=	(omega1_1)/(x1_1)

################################################
mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()

ax.legend()
ax.legend(frameon=False)

#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 

nn	=   x1_1.shape[0]

for j in range (0,nn):
 		
	if x1_1[j]<0.001:

		x1_1[j]=np.nan
		y1_1[j]=np.nan
		omega1_1[j]=np.nan 
		phase1_1[j]=np.nan 

plt.xlabel(r' $\mathrm{\alpha_r}$')
plt.ylabel(r'-$\mathrm{\alpha_i}$')
#plt.xlabel(r'$\alpha_r$')
#plt.ylabel(r'$-\alpha_i$')

plt.plot(x1_1,y1_1,'*',color='g',label = '$\mathrm{Same\quad}$' )

#
ax.xaxis.set_major_locator(plt.MultipleLocator(0.1))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([-0.01, 1.0, 0.001, 0.80])

plt.legend()
ax.legend(frameon=False)
plt.savefig('alphari.pdf', format='pdf', dpi=1000)


###############################################33
mpl.rcParams.update(params)

fig2 = plt.figure()
ax2  = plt.axes()


plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'-$\mathrm{\alpha_i}$')


plt.plot(omega1_1,y1_1,'.',color='g',label ='$\mathrm{Same\quad}$')
################################                                 
#############
ax2.xaxis.set_major_locator(plt.MultipleLocator(0.1))
ax2.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([-0.01, 1, 0.001, 0.80])

plt.legend()
ax2.legend(frameon=False)
plt.savefig('alphar.pdf', format='pdf', dpi=1000)

###############################################33
mpl.rcParams.update(params)

fig2 = plt.figure()
ax2  = plt.axes()


plt.xlabel(r'$\mathrm{\omega}$')
plt.ylabel(r'$\mathrm{C_p}$')


plt.plot(omega1_1,phase1_1,'.',color='g',label ='$\mathrm{Same\quad}$')
################################                                 
#############
ax2.xaxis.set_major_locator(plt.MultipleLocator(0.1))
ax2.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.01, 1, 0.01, 1.00])

plt.legend()
ax2.legend(frameon=False)
plt.savefig('phase.pdf', format='pdf', dpi=1000)
#
#
#
##plt.plot(X[10],Y[10],'ko')
##plt.plot(X[11],Y[11],'ko')
##plt.plot(X,Y,'k.')
##SPF()
plt.show()


