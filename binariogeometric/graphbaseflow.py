#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import  matplotlib 		as 	mpl
#from   	path 			import 	pathfinder
from   	plotparameters 		import 	*
#from   	Parameters 		import 	*
#from 	SPF 			import 	*


M1	=	np.loadtxt('./H2O2G2/taxa1/baseflow.dat',unpack=True,skiprows=1) 
r_1	=	M1[0,:]
Wb_1	=	M1[1,:]
#Tb_1	=	M1[2,:]
Rhob_1	=	M1[3,:]

M2	=	np.loadtxt('./O2H2G2/taxa1/baseflow.dat',unpack=True,skiprows=1) 

r_2	=	M2[0,:]
Wb_2	=	M2[1,:]
#Tb_1	=	M[2,:]
Rhob_2	=	M2[3,:]

M3	=	np.loadtxt('./O2O2/baseflow.dat',unpack=True,skiprows=1) 
r_3	=	M3[0,:]
Wb_3	=	M3[1,:]
#Tb_1	=	M[2,:]
Rhob_3	=	M3[3,:]


mpl.rcParams.update(params)

fig = plt.figure()

ax = plt.axes()
ax.legend()

#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 


plt.xlabel(r'$\mathrm{\bar{\rho}}$')
plt.ylabel(r'$r$')


p1_1, p1_2 = [0,1], [1, 1]
p2_1, p2_2 = [0,2], [2, 2]

plt.plot(p1_1, p1_2, p2_1, p2_2, color='gray',dashes=[1, 1])
plt.plot(Rhob_1,r_1,color='slateblue',dashes=[1, 0],label ='$\mathrm{H_2O_2}$')

plt.text(0.71, 1.0,r'$\mathrm{R_1}$')
plt.text(0.71, 2.0,r'$\mathrm{R_2}$')

#plt.plot(Rhob_1,r_1,color='m',dashes=[3, 1],label ='$\mathrm{h=0.9}$')
#plt.plot(Tb_1,r_1,color='m',dashes=[3, 1],label ='$\mathrm{h=0.9}$')

plt.plot(Rhob_2,r_2,color='green',dashes=[1, 1],label = '$\mathrm{O_2H_2}$')

plt.plot(Rhob_3,r_3,color='m',dashes=[3, 1],label ='$\mathrm{Same}$')

#plt.text(0.0, 2.0,r'$\mathrm{\bar{W}(r)}$')

#########x####################         ########

ax.xaxis.set_major_locator(plt.MultipleLocator(0.1))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.5))
plt.axis([ 0,1.1,0.01, 3.5,])

plt.legend()
ax.legend(frameon=False)
plt.savefig('baseflow.pdf', format='pdf', dpi=1000)

plt.show()
