#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import 	numpy 			as 	np 
import 	matplotlib.pyplot 	as 	plt 
import 	matplotlib   		as 	mt 
from   	path 			import 	pathfinder
#from   	Parameters 		import 	*
#from 	SPF 			import 	*

file1	=   	'taxa1'
file2	=   	'taxa2'
file3	=   	'taxa3'
file4	=   	'taxa4'
file5	=   	'G2'


M1_1	=	np.loadtxt('allspec.dat',unpack=True) 
#M1_2	=	np.loadtxt('taxa2.dat',unpack=True) 
#m1_1     =	M1_1.shape[1]
#n1_2     =	M1_2.shape[0]



#0 line, frequency
#Omega	=	np.concatenate([M1_1[0,:],M1_2[0,:],M1_2[0,:]])
Omega	=	np.concatenate([M1_1[0,:]
					  ])
Omega1	=	np.concatenate([M1_1[0,:]
					  ])

#1 and  3 line, alphar
X	=	np.concatenate([M1_1[1,:]
					  ])
X1	=	np.concatenate([M1_1[1,:]
								])


#2 and 4 line, alphai 
#Y	=	np.concatenate([M1[2,:],M2[2,:],M2[4,:],M3[2,:]])

Y	=	np.concatenate([M1_1[2,:]
								])


#nn	=   X.shape[0]

#for j in range (0,nn):
	
	#g.write("%f\t%f\n"%(X[j],Y[j])); 
	#print(j,X[j],Y[j]); 


(omega1,y1,n1)	=pathfinder(Omega,Y,30,distweight=10.0,stopweight=10.0,isextremity=False) 

Omega1[n1]	=np.nan

(omega2,y2,n2)	=pathfinder(Omega1,Y,60,distweight=1.0,stopweight=40.0,isextremity=False) 


nn	=   omega1.shape[0]

g       =   open('dados.dat','w+')

#for j in range (0,nn):
#	
#	#if X[j]!=np.nan:
#
#		g.write("%f\t%f\t%f\n"%(Omega[j],X[j],Y[j])); 
#
#		print(j,X[j],Y[j]); 
#g.close()


nn	=   y1.shape[0]
nn2	=   Y.shape[0]

g1       =   open('./mods1.dat','w+')

x1=omega1*0.0

for j in range (0,nn):
	
	for i in range (0,nn2):

		if(y1[j]==Y[i]):

                        x1[j]=X[i]
			g1.write("%f\t%f\t%f\n"%(omega1[j],X[i],y1[j])) 
g1.close()

nn	=   y2.shape[0]
nn2	=   Y.shape[0]

g2       =   open('./mods2.dat','w+')

print(nn2)

x2=omega2*0.0

for j in range (0,nn):

	for i in range (0,nn2):

		if(y2[j]==Y[i] ):

                        x2[j]=X[i]
			g2.write("%f\t%f\t%f\n"%(omega2[j],X[i],y2[j])); 


g2.close()
#Direct input 
#plt.rcParams['text.latex.preamble']=[r"\usepackage{elsarticle}"]
#Options

fig = plt.figure()

params = {
	  
	#'text.usetex' : True,
	#'font.family' : 'lmodern',
	#'font.family' : 'elsarticle',
	'font.family' : 'serif',
	#'font.serif'  : 'Computer Modern Sans serif',
	'font.sans-serif'    : 'Helvetica',#, Avant Garde, Computer Modern Sans serif
	#'font.sans-serif'    : 'Computer Modern Sans serif',#, Avant Garde, Computer Modern Sans serif
	'lines.linewidth':2,
        'font.size' : 15,
        'text.latex.unicode': True,
	#'legend.fontsize': 'x-large',
        'figure.figsize': (20, 10),
	'axes.labelsize': '20',#'x-Large',
        'axes.titlesize': '25',#'x-Large',
        'xtick.labelsize':'15',#'x-Large',
        'ytick.labelsize':'15',#'x-Large'
          }
plt.rcParams.update(params) 


#You must select the correct size of the plot in advance
#fig.set_size_inches(3.54,3.54) 

ax = plt.axes()

plt.xlabel(r'$\alpha_r$')
plt.ylabel(r'-$\alpha_i$')
plt.plot(omega1,y1,'-r',label ='Mode I ')
plt.plot(omega2,y2,'-.b',label ='Mode II')
plt.plot(x1,y1,'-m',label ='Mode I ')
plt.plot(x2,y2,'-.k',label ='Mode II')
plt.savefig('%s.eps'%(file5), format='eps', dpi=1000)
ax.xaxis.set_major_locator(plt.MultipleLocator(0.5))
ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
plt.axis([0.1, 5, 0.01, 0.75])


plt.legend()
#plt.plot(X[10],Y[10],'ko')
#plt.plot(X[11],Y[11],'ko')
#plt.plot(X,Y,'k.')
#SPF()
plt.show()


