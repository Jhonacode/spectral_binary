#matplotlib inline
#config InlineBackend.figure_format='svg'
from   chebPy        import *
from   mapping       import *
from   Baseflow      import *
from   Boundary      import *
from   scipy.linalg  import block_diag
from   Parameters    import *
import numpy 	         as np 
import matplotlib.pyplot as pl
import diffusion         as df
import sys 
import os 

def p_spectral_matrices(NN,maxr,iomega):

	# number of collocation nodes=N+1, because the cheb func
	D,n1	= cheb(NN)
	#D2	= dot(D,D)
	#D2[1:N,1:N]
	
	# Mappint to concentrate the point in 0. Withou using 0
	# r= Radial coordinate
	r   	= mappingtan(NN,n1,maxr,D)
	D2	= dot(D,D)
	#Base Flow

        
	Wb,Tb,Rhob,Y1,Y2  =   Baseflow(r,NN)



        #Fortran program diffusion.f90 
        #   This program calculate the diffusion properties of  
        #   the differens species and the mixture of gases. 
        
        #To run the fortran subroutine.
        #f2py -m diffusion -c  global.f90 diffusion.f90 
        
	(mib,kb,cpb,cpb1,cpb2,D1m,Mw,Ru,Rmix,gammab,gamma_j,gamma_ratio,P_r,a_ref,a_s,a_1,a_2)=\
        df.properties.init_diffusion(Y1,Y2,Tb,T_i,T_0,T_ref,NN)

	

        #M1  =   M_i
        #M2  =   M_o#M_i*h

        #Mc  =   (M1*a_1-M2*a_2)/(a_1+a_2)


	print'Sound velocity Ref',a_1,a_2


        #####
        #Non dimensional gas ideal equation \bar{p}=1/{gamma_ref}
        Rhob  =  1.0/(Tb*Rmix) 

	###########CONVECTIVE MACH NUMBER

	if iomega == 0:

		for j in range(0,NN+1):


        	    if (R1-0.05<r[j]<R1+0.05):

        	        M1      =   Wb[j]
        	        rho1    =   Rhob[j]
        	        T1      =   Tb[j]

        	    if (R1*Gamma-0.05<r[j]<R1*Gamma+0.05):

        	        M2      =   Wb[j]
        	        rho2    =   Rhob[j]
        	        T2      =   Tb[j]

        	Srho= rho2/rho1

		print'Mach1',M1,'Mach2',M2

		Rratio	=   M2/M1

		Uc_1    = (1.0+Rratio*np.sqrt(gamma_ratio))/(1.0+np.sqrt(Srho))

        	Mc1_1  	= (M1-Uc_1/a_1) 

        	Mc1_2  	= (Uc_1/a_2-M2) 

		print'Convective Mach',Mc1_1,'Convective Mach',Mc1_2

		#M1 =M_o
		#M2 =M_0

		Rratio		=   	M2/M1
        	Srho		=	1#rho1/rho1
		gamma_ratio	=	1

		Uc_2    = (1.0+Rratio*np.sqrt(gamma_ratio))/(1.0+np.sqrt(Srho))

        	Mc2_1  	= (M1-Uc_2/a_1) 

        	Mc2_2  	= (Uc_2/a_2-M2) 

		print'Convective Mach 2',Mc2_1,'Convective Mach 2',Mc2_2
		

        g   	=   open('baseflow.dat','w+') 
	

#Mc_1  = 1.0/(1.0+np.sqrt(Srho))*np.sqrt(gamma_ratio)*M2*(1.0/(M2/M1)-1.0)

        #Mc_2  = M_i*(1.0-h)/(1.0+np.sqrt(gamma_ratio/Srho))

        #print'Convective Mach Jackson',Mc_1,Mc_2,T1,T2

        #sys.exit()

	for j in range (0,NN+1):
	
	            g.write("%f\t%f\t%f\t%f\t%f\t%f\t%f\n"%(r[j],Wb[j],Tb[j],Rhob[j],a_s[j],Y1[j],Y2[j]));

    
    #dWbdr   =   np.gradient(Wb, dr)

    
        g.close()
        

	
	dWbdr      =   dot(D,Wb)
	dRhobdr    =   dot(D,Rhob)
	
	# coordinate selectors
	#rr      =   np.arange(0,N,1)#0:N+1      
	#uu      =   rr+N
	#vv      =   uu+N
	#pp      =   vv+N
	
	
	## Especial Matrices

	Ov      =   np.zeros((1,(4*(NN+1))))
	Z   	=   np.zeros((NN+1,NN+1))
	I   	=   np.identity(NN+1) 
	II      =   np.identity(4*(NN+1))
	Ee 	=   block_diag(I,I,I,I)
	DD      =   block_diag(D,D,D,D);
	

	#Cartesian
	#A   =   np.block([[diag(M),diag(Rho),Z,Z],[Z,diag(M),Z,diag(1./Rho)],[Z,Z,diag(M),Z],[Z,I,Z,diag(M)]])
	#B   =   np.block([[Z,Z,diag(Rho),Z],[Z,Z,Z,Z],[Z,Z,Z,diag(1./Rho)],[Z,Z,I,Z]])
	#C   =   np.block([[Z,Z,diag(Rhoy),Z],[Z,Z,diag(My),Z],[Z,Z,Z,Z],[Z,Z,Z,Z]])


	#Cylindrical
	Ae   =   np.block([[Z,diag(Rhob),Z,Z],[Z,Z,Z,diag(1.0/Rhob)],[Z,Z,Z,Z],[Z,I,Z,Z]])
	Be   =   np.block([[diag(Wb),Z,diag(Rhob),Z],[Z,diag(Wb),Z,Z],[Z,Z,diag(Wb),diag(1.0/Rhob)],[Z,Z,I,diag(Wb)]])
	Ce   =   np.block([[Z,diag(Rhob),Z,Z],[Z,Z,Z,Z],[Z,Z,Z,Z],[Z,I,Z,Z]])
	De   =   np.block([[Z,diag(dRhobdr),Z,Z],[Z,Z,Z,Z],[Z,diag(dWbdr),Z,Z],[Z,Z,Z,Z]])
	
	
	for i in range(0,NN+1): 
	
	    for j in range(0,NN+1): 
	
	        Ce[i,j]   = Ce[i,j]/r[j] 
	
	
	A0      =   -np.matmul(Ae,DD)-Ce-De;

	B0      =   np.multiply(1j,Be);
	

	return A0,Ee,B0




