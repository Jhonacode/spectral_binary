#! /usr/bin/python 
# Base flow to spectral methods 
# create by: jhonatan
# date: 11-05-2018
# Jets Parameters
# Paper: D Perrault-Joncas and S.A Maslowe,
#       "Linear Stability of compressible coaxial jet 
#        with continuos velocity and temperature profile "
#module for symbolic albegra 
import numpy as np
import math  as mt
#from math import exp, expm1
import matplotlib.pyplot as plt
#from sci to make fft
import scipy.fftpack
from  Parameters import * 
################################
# letura de aquivos para obter as transformadas
# number of frequency files 

def Baseflow(r,NN):

    #number of spectral points
    #N1      =   np.shape(r)

    #N       =   N1[0]
    #print(N)

    dr      =   r[1]-r[0]
    
    Wb      =   np.zeros(NN+1)

    Tb      =   np.zeros(NN+1)

    Rhob    =   np.zeros(NN+1)

    M1      =   np.zeros(NN+1)

    M2      =   np.zeros(NN+1)

    M3      =   np.zeros(NN+1)

    T1      =   np.zeros(NN+1)

    T2      =   np.zeros(NN+1)

    T3      =   np.zeros(NN+1)


    #Mass fraction profile
    Yb      =   np.zeros(NN+1)

    Y1      =   np.zeros(NN+1)

    Y2      =   np.zeros(NN+1)

    #Diameter Primary 
    D1      =   2.0*R1 
    #Momemtum thickness Primary 
    theta1  =   3.0/100.0*(D1+2.0/3.0*D1) 

    #Radii Secondary jet
    R2      =   R1*Gamma
    #Diameter Primary 
    D2      =   2.0*R2
    ##Momemtum thickness Secondary

    theta2  =   3.0/100.0*(D1+2.0/3.0*D2) 
    
    #Parameters bases flow

    b1      =   R1/(4.0*theta1)
    b2      =   R2/(4.0*theta2)
    
    
    for j in range (0,NN+1):
    
        #Primary Stream
        M1[j]   =   0.5*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1))) 
        T1[j]   =   0.5*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1))) 
        #Secondary Stream
        M2[j]   =   0.5*(1.0+np.tanh(b2*(R2/r[j]-r[j]/R2))) 
        T2[j]   =   0.5*(1.0+np.tanh(b2*(R2/r[j]-r[j]/R2))) 
        #Ambient
        #M3[j]   =   M_0*0.5*(1.0+np.tanh(b2*(R2/(-r[j])-(-r[j])/R2))) 
        #T3[j]   =   T_0*0.5*(1.0+np.tanh(b2*(R2/(-r[j])-(-r[j])/R2))) 

        Wb[j]    = ((1.0-h)*M1[j]+h*M2[j])*M0
	#Wb[j]	=  (M_i-M_o)*M1[j]+M_o*M2[j]+M3[j]

	#Tb[j]	=  (T_i-T_o)*T1[j]+T_o*T2[j]+T3[j]
        Tb[j]    =  0.5*(1-S_T)*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1)))+S_T

        Rhob[j]  = 1.0/(Tb[j]) 

        #Primary Stream
        Y1[j]   =   1.0*0.5*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1))) 
        #Secondary Stream
        Y2[j]   =   1-(Y1[j])

    
    #dWbdr   =   np.gradient(Wb, dr)
    
    
    return Wb,Tb,Rhob,Y1,Y2
