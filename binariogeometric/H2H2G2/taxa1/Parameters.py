#Defining the parameters using 
#in the execution of the progra

import numpy as np 
#def dadosin():

file1       =   'taxa' 
file2       =   'baseflow' 

N	    =   200

omegai      =   0.1
omegaf      =   3.0
domega	    = 	0.02
iomega	    =	np.int(round((omegaf-omegai)/domega))
print(iomega)
#iomega      =   128
#maximum size r
maxr    =   50
#Jets Parameters
#h      = U_secondary/U_primary
h       =   0.70
#M0=M1, sound velocity adimensionalization, Joncas is with the U1
#As only exist one specie, its necesary to keep the mach equal 
#each for O2O2 AND H2H2, this is the only form to hold the 
#same parameters, if the be change the mach number we will have
#compresisbility effecsts  
#Hydrogem at M=0.164 will be behavior as Oxigen at Mach=0.6 
#the mach number is respet to specie and must be the same.
#Mach number is adimensional number.
M0      =   0.6558#0.8

#this is diferent that the case whete we have two species 
#is necesary to change the mach number if we change the 
#referencecondition, this will be chanege the mach number 
#definition, see H2O2ref2 H2O2ref1 

#M_i     =   0.6#0.8
#M_o     =   0.4#0.8
#M_0     =   0.2#0.8
#M2     =   h*M1#0.4 
#h      = U_infty/U_primary,r=0
S_T     =   1.00
#T0=T_ref, sound velocity adimensionalization, Joncas is with the U1
T_ref   =   300#0.8
T_o     =   1.0 
T_0     =   1.0
T_i     =   1.0/S_T
#Gamma  = R2/R1
Gamma   =   2.0 
#Radii Primary jet
R1      =   1.0
###
###
#numero de modos a encontrar if  >nm breakk 
nm	=   0
# Tolerancia para procura dos autovalores
tol	=   0.0005
#Max_imaginary part 
min_imag=   0.01
max_imag=   0.7
min_real=   0.01
max_real=   5.0
#
#tol	=   0.005
