module rhspml

use global
use derivs
use eqeuler
use eqpml

contains   

subroutine rhs(dUdt,U,q1,q2)

   implicit none
     
   integer::k,i,j
   real(kind=ip),dimension(4,im,jm)::dUdt,U,q1,q2
   real(kind=ip),dimension(4,im,jm)::dE,dpml

!........................ derivadas das variaveis dependentes ..........
   call deriv(U,q1,q2)

!........................ RHS da eq de Euler ...........................
   call nonlinear(U,dE)

!........................ RHS da eq com PML ............................
   call pml(U,q1,q2,dpml)

!.......................................................................
  do i=1,imax
    do j=1,jmax
      if(((i>imaxpml).and.(i<imax-D+1)).and.((j>jmaxpml).and.(j<jmax-D+1)))then
             dUdt(1,i,j)  = - dE(1,i,j)  
             dUdt(2,i,j)  = - dE(2,i,j)  
             dUdt(3,i,j)  = - dE(3,i,j)  
             dUdt(4,i,j)  = - dE(4,i,j) !+ s(i,j) 
       else 
             dUdt(1,i,j)  = - dpml(1,i,j)          
             dUdt(2,i,j)  = - dpml(2,i,j)          
             dUdt(3,i,j)  = - dpml(3,i,j)          
             dUdt(4,i,j)  = - dpml(4,i,j) !+ s(i,j)  
       end if    
    end do
  end do 

end subroutine

end module
