module initialize
use global
use globalq
use diff
!use global2
contains

subroutine init(U)
implicit none

integer::i,j,k,l

real(kind=ip),dimension(4,im,jm)::U
real(kind=ip):: u1b,u2b,T1,T2,gama,alpha,lamda 
real(kind=ip):: Dx,Dy 
real(kind=ip):: Aa,alpham 
real(kind=ip)::sigmastr,xmax,xp,deltastr,mt,dxmax,dxp
real(kind=ip)::ymax,yp,nt,dymax,dyp
real(kind=ip),dimension(im)::x
real(kind=ip),dimension(jm)::y

!................................ input data ..........................
namelist /grid/ imax, jmax, maxit,dt,alphafx,alphafy
namelist /freeadm/ Mx,My,mmin, mmax, nmin, nmax,px,py
namelist /pmlgrid/ imaxpml,jmaxpml,D,sigmamx,sigmamy,sigmamxc,sigmamyc,alpha,c0,delta

!................................ read input data ......................
open(unit=1,file='dados.in',status='old')
open(unit=2,file='resultpml.dat',status='unknown')

!................................ grid .................................
read (1,nml=grid)
write(2,nml=grid) 

!................................ free stream conditions ...............
read (1,nml=freeadm)
write(2,nml=freeadm)

!.................................PML GRIDe..............................
read (1,nml=pmlgrid)
!write(*,nml=pmlgrid)
close(unit=1)
!.......................................................................
 dm       =    (mmax-mmin)/real(imax-1)
 dn       =    (nmax-nmin)/real(jmax-1)

 write(*,*)'dx=',dm
 write(*,*)'dy=',dn
 write(2,*)'dx=',dm
 write(2,*)'dy=',dn

! Definição do sigma 
 Dx       =    dm*D
 Dy       =    dn*D

! Começo da Pml ezquerda,direita,inferior,superior
 xli= mmin+Dx
 xld= mmax-Dx
 yli= nmin+Dy
 yls= nmax-Dy

!***********************************************
do i = 1,imax
   m(i)  = mmin + real(i-1)*dm
end do

do j = 1,jmax
   n(j) = nmin + real(j-1)*dn
end do

!Grid compress

!***********x
Aa      = 2.d0
alpham  = 2.d0
meshx   = 1.d0

do i = imaxpml+1,1,-1
    meshx(i) =1.d0+Aa*(abs((m(i)-(xli))/(Dx)))**alpham
end do

do i = imax-D,imax
    meshx(i) =1.d0+Aa*(abs((m(i)-xld)/(Dx)))**alpham
end do

!*********y
meshy   = 1.d0
do j =jmaxpml+1,1,-1
    meshy(j) =1.d0+Aa*(abs((n(j)-(yli))/(Dy)))**alpham
end do

do j = jmax-D,jmax
    meshy(j) =1.d0+Aa*(abs((n(j)-yls)/(Dy)))**alpham
end do

!*********************************************************************************************************************************yy

!x=m
!
!dxp   = 1.0001d0
!dxmax = 5.d0
!sigmastr = dxp/dm-1.d0
!
!
!xp    = -mmax+D
!xmax  = -mmax!10.d0
!mt    = (-mmax*(1.d0+(dxmax)/dm)-xmax)/(dxmax/dm)
!deltastr = log(dxmax/(sigmastr*dm))/(mt-xp)
!
!do i = imaxpml+1,1,-1
!      x(i)     = m(i) + dxmax/(deltastr*dm)*log(exp(deltastr*(m(i)-mt))+1.d0)
!      meshx(i) = 1.d0 + dxmax/(deltastr*dm)*1.d0/(log(exp(deltastr*(m(i)-mt))+1.d0))*exp(deltastr*(m(i)-mt))*deltastr
!end do
!
!xp    = mmax-D
!xmax  = mmax!10.d0
!mt    = (mmax*(1.d0+(dxmax)/dm)- xmax)/(dxmax/dm)
!deltastr = log(dxmax/(sigmastr*dm))/(mt-xp)
!
!do i = imax-D,imax
!      x(i)     = m(i)+dxmax/(deltastr*dm)*log(exp(deltastr*(m(i)-mt))+1.d0)
!      meshx(i) = 1.d0 + dxmax/(deltastr*dm)*1.d0/(log(exp(deltastr*(m(i)-mt))+1.d0))*exp(deltastr*(m(i)-mt))*deltastr
!end do
! 
!y=n
!
!dyp   = 1.0001d0
!dymax = 5.d0
!sigmastr = dyp/dn-1.d0
!
!
!yp    = -nmax+D
!ymax  = -nmax!10.d0
!nt    = (-nmax*(1.d0+(dymax)/dn)-ymax)/(dymax/dn)
!deltastr = log(dymax/(sigmastr*dn))/(nt-yp)
!
!do j = jmaxpml+1,1,-1
!      y(j)     = n(j) + dymax/(deltastr*dn)*log(exp(deltastr*(n(j)-nt))+1.d0)
!      meshy(j) = 1.d0 + dymax/(deltastr*dn)*1.d0/(log(exp(deltastr*(n(j)-nt))+1.d0))*exp(deltastr*(n(j)-nt))*deltastr
!end do
!
!yp    = nmax-D
!ymax  = nmax!10.d0
!nt    = (nmax*(1.d0+(dymax)/dn)- ymax)/(dymax/dn)
!deltastr = log(dymax/(sigmastr*dn))/(nt-yp)
!
!do j = jmax-D,jmax
!      y(j)     = n(j)+dymax/(deltastr*dn)*log(exp(deltastr*(n(j)-nt))+1.d0)
!      meshy(j) = 1.d0 + dymax/(deltastr*dn)*1.d0/(log(exp(deltastr*(n(j)-nt))+1.d0))*exp(deltastr*(n(j)-nt))*deltastr
!end do
!
!
!
!do j=1,jmax
!do i=1,imax
!write(1,*)m(i),n(j),x(i),y(j)
!end do
!end do
!
!stop

!*********************************************************************************************************************************yy

!SIGMAS
do i = imaxpml,1,-1
   sigmax(i) =sigmamx*(abs((m(i)-(xli))/(Dx)))**alpha
end do

do i = imax-D+1,imax
   sigmax(i) =sigmamx*(abs((m(i)-xld)/(Dx)))**alpha
end do

do i =imaxpml,1,-1
   sigmaxc(i) =sigmamxc*(abs((m(i)-(xli))/Dx))**alpha
end do

do i = imax-D+1,imax
   sigmaxc(i) =sigmamxc*(abs((m(i)-xld)/(Dx)))**alpha
end do


do j =jmaxpml,1,-1
   sigmay(j) =sigmamy*(abs((n(j)-(yli))/(Dy)))**alpha
end do

do j = jmax-D+1,jmax
   sigmay(j) =sigmamy*(abs((n(j)-yls)/(Dy)))**alpha
end do

do j = jmaxpml,1,-1
   sigmayc(j) =sigmamyc*(abs((n(j)-(yli))/(Dy)))**alpha
end do

do j = jmax-D+1,jmax
   sigmayc(j) =sigmamyc*(abs((n(j)-yls)/(Dy)))**alpha
end do

!beta=-1.d0/c0
!
!!u1b  = 0.8d0
!!u2b  = 0.2d0
!!T1   = 1.0d0
!!T2   = 0.8d0
!!gama = 0.4d0
!!lamda= 1.4d0
!!
!!do j=1,jmax
!!   uba(j)   = 0.5d0*(u1b+u2b+(u1b-u2b)*tanh(2.d0*n(j)/gama))
!!   Tb(j)   = T1*(uba(j)-u2b)/(u1b-u2b)+T2*(u1b-uba(j))/(u1b-u2b)+(lamda-1.d0)/2.d0*(u1b-Uba(j))*(uba(j)-u2b)
!!   rhob(j) = 1.d0/(Tb(j))
!!end do

!............................zerando as variaveis......................

!U  = 0.d0
q1 = 0.d0
q2 = 0.d0

!........................... inicialização ............................
do i=1,imax
  do j=1,jmax

   U(1,i,j)   = 0.d0!rhob(j) 
   U(2,i,j)   = 0.d0!uba(j)
   U(3,i,j)   = 0.d0!0.d0
   U(4,i,j)   =  exp(-log(2.d0)*((m(i))**2+n(j)*n(j))/16.d0)!0.d0!1.d0/lamda 

  end do 
end do

!do i=1,imax
!  do j=1,jmax
!
!   Ub(1,i,j)   = rhob(j) 
!   Ub(2,i,j)   = uba(j)
!   Ub(3,i,j)   = 0.d0
!   Ub(4,i,j)   = 0.d0 
!
!  end do 
!end do

!call dernb(dubadn,uba)
!call dernb(drhobdn,rhob)
!
!do i=1,imax
!  do j=1,jmax
!
!   dUbdn(1,i,j)   = drhobdn(j) 
!   dUbdn(2,i,j)   = dubadn(j)
!   dUbdn(3,i,j)   = 0.d0
!   dUbdn(4,i,j)   = 0.d0 
!
!  end do 
!end do


A(1,1,:,:)  = Mx
A(1,2,:,:)  = 1
A(1,3,:,:)  = 0.0d0
A(1,4,:,:)  = 0.0d0

A(2,1,:,:)  = 0.d0 
A(2,2,:,:)  = Mx
A(2,3,:,:)  = 0.d0
A(2,4,:,:)  = 1.d0

A(3,1,:,:)  = 0.d0 
A(3,2,:,:)  = 0.d0
A(3,3,:,:)  = Mx 
A(3,4,:,:)  = 0.d0

A(4,1,:,:)  = 0.d0 
A(4,2,:,:)  = 1.d0
A(4,3,:,:)  = 0.d0
A(4,4,:,:)  = Mx 


B(1,1,:,:)  = My 
B(1,2,:,:)  = 0.d0
B(1,3,:,:)  = 1.d0 
B(1,4,:,:)  = 0.d0

B(2,1,:,:)  = 0.d0 
B(2,2,:,:)  = My 
B(2,3,:,:)  = 0.d0
B(2,4,:,:)  = 0.d0

B(3,1,:,:)  = 0.d0 
B(3,2,:,:)  = 0.d0
B(3,3,:,:)  = My 
B(3,4,:,:)  = 1.d0

B(4,1,:,:)  = 0.d0 
B(4,2,:,:)  = 0.d0
B(4,3,:,:)  = 1.d0
B(4,4,:,:)  = My 

do k=1,4
 do l=1,4
    do i=1,imax
      do j=1,jmax

       A0(k,l,i,j) = A(k,l,i,j)-Mx
       B0(k,l,i,j) = B(k,l,i,j)-My

      end do
    end do 
 end do 
end do
end subroutine

end module
