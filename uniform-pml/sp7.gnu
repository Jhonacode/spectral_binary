#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 3.7 patchlevel 2
#    	last modified Sat Jan 19 15:23:37 GMT 2002
#    	System: Linux 2.4.18-14
#    
#    	Copyright(C) 1986 - 1993, 1998 - 2002
#    	Thomas Williams, Colin Kelley and many others
#    
#    	Type `help` to access the on-line reference manual
#    	The gnuplot FAQ is available from
#    	http://www.gnuplot.info/gnuplot-faq.html
#    
#    	Send comments and requests for help to <info-gnuplot@dartmouth.edu>
#    	Send bugs, suggestions and mods to <bug-gnuplot@dartmouth.edu>
#    
# set terminal x11 
# set output
#set noclip points
#set clip one
#set noclip two
set bar 1.000000
set border 31 lt -1 lw 1.000
set xdata
set ydata
set zdata
set x2data
set y2data
set boxwidth
set dummy u,v
set format x "%g"
set format y "%g"
set format x2 "% g"
set format y2 "% g"
set format z "%g"
set angles radians
set nogrid
set key title ""
set key right top Right noreverse box linetype -2 linewidth 1.000 samplen 4 spacing 1 width 0
unset label
unset arrow
#unset linestyle
unset logscale
set offsets 0, 0, 0, 0
set pointsize 1
set encoding default
set nopolar
set parametric
set view 0, 0, 1, 1
set samples 100, 100
set isosamples 10, 10
set nosurface
set contour base
set clabel '%8.3g'
set mapping cartesian
set nohidden3d
set cntrparam order 4
set cntrparam linear
set cntrparam levels auto 9
set cntrparam points .1
set size ratio 0 1,1
set origin 0,0
set style data lines
# set function style lines
set xzeroaxis lt 0 lw 1.000
set x2zeroaxis lt -2 lw 1.000
set yzeroaxis lt 0 lw 1.000
set y2zeroaxis lt -2 lw 1.000
set tics in
#set ticslevel 0.5
#set ticscale 1 0.5
set mxtics default
set mytics default
set mx2tics default
set my2tics default
set xtics border mirror norotate autofreq 
set ytics border mirror norotate autofreq 
set ztics border nomirror norotate autofreq 
set nox2tics
set noy2tics
#set timestamp "" bottom norotate 0.000000,0.000000  ""
set rrange [ * : * ] noreverse nowriteback  # (currently [-0.00000:10.0000] )
set trange [ * : * ] noreverse nowriteback  # (currently [-5.00000:5.00000] )
set urange [ * : * ] noreverse nowriteback  # (currently [-5.00000:5.00000] )
set vrange [ * : * ] noreverse nowriteback  # (currently [-5.00000:5.00000] )
set timefmt "%d/%m/%y\n%H:%M"
set xrange [ * : * ] noreverse nowriteback  # (currently [-4.00000:4.00000] )
set x2range [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set yrange [ -5 : .1 ] noreverse nowriteback  # (currently [-2.00000:2.00000] )
set y2range [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set zrange [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set zero 1e-02
set lmargin -1
set bmargin -1
set rmargin -1
set tmargin -1
set autoscale
#set locale "C"
#set pm3d
splot 'fort.10'
pause 0.05
splot 'fort.11'
pause 0.05
splot 'fort.12'
pause 0.05
splot 'fort.13'
pause 0.05
splot 'fort.14'
pause 0.05
splot 'fort.15'
pause 0.05
splot 'fort.16'
pause 0.05
splot 'fort.17'
pause 0.05
splot 'fort.18'
pause 0.05
splot 'fort.19'
pause 0.05
splot 'fort.20'
pause 0.05
splot 'fort.21'
pause 0.05
splot 'fort.22'
pause 0.05
splot 'fort.23'
pause 0.05
splot 'fort.24'
pause 0.05
splot 'fort.25'
pause 0.05
splot 'fort.26'
pause 0.05
splot 'fort.27'
pause 0.05
splot 'fort.28'
pause 0.05
splot 'fort.29'
pause 0.05
splot 'fort.30'
pause 0.05
splot 'fort.31'
pause 0.05
splot 'fort.32'
pause 0.05
splot 'fort.33'
pause 0.05
splot 'fort.34'
pause 0.05
splot 'fort.35'
pause 0.05
splot 'fort.36'
pause 0.05
splot 'fort.37'
pause 0.05
splot 'fort.38'
pause 0.05
splot 'fort.39'
pause 0.05
splot 'fort.40'
pause 0.05
splot 'fort.41'
pause 0.05
splot 'fort.42'
pause 0.05
splot 'fort.43'
pause 0.05
splot 'fort.44'
pause 0.05
splot 'fort.45'
pause 0.05
splot 'fort.46'
pause 0.05
splot 'fort.47'
pause 0.05
splot 'fort.48'
pause 0.05
splot 'fort.49'
pause 0.05
splot 'fort.50'
pause 0.05
splot 'fort.51'
pause 0.05
splot 'fort.52'
pause 0.05
splot 'fort.53'
pause 0.05
splot 'fort.54'
pause 0.05
splot 'fort.55'
pause 0.05
splot 'fort.56'
pause 0.05
splot 'fort.57'
pause 0.05
splot 'fort.58'
pause 0.05
splot 'fort.59'
pause 0.05
splot 'fort.60'
pause 0.05
splot 'fort.61'
pause 0.05
splot 'fort.62'
pause 0.05
splot 'fort.63'
pause 0.05
splot 'fort.64'
pause 0.05
splot 'fort.65'
pause 0.05
splot 'fort.66'
pause 0.05
splot 'fort.67'
pause 0.05
splot 'fort.68'
pause 0.05
splot 'fort.69'
pause 0.05
splot 'fort.70'
pause 0.1
splot 'fort.71'
pause 0.1
splot 'fort.72'
pause 0.1
splot 'fort.73'
pause 0.1
splot 'fort.74'
pause 0.1
splot 'fort.75'
pause 0.1
splot 'fort.76'
pause 0.1
splot 'fort.77'
pause 0.1
splot 'fort.78'
pause 0.1
splot 'fort.79'
pause 0.1
splot 'fort.80'
pause 0.1
splot 'fort.81'
pause 0.1
splot 'fort.82'
pause 0.1
splot 'fort.83'
pause 0.1
splot 'fort.84'
pause 0.1
splot 'fort.85'
pause 0.1
splot 'fort.86'
pause 0.1
splot 'fort.87'
pause 0.1
splot 'fort.88'
pause 0.1
splot 'fort.89'
pause 0.1
splot 'fort.90'
pause 0.1
splot 'fort.91'
pause 0.1
splot 'fort.92'
pause 0.1
splot 'fort.93'
pause 0.1
splot 'fort.94'
pause 0.1
splot 'fort.95'
pause 0.1
splot 'fort.96'
pause 0.1
splot 'fort.97'
pause 0.1
splot 'fort.98'
pause 0.1
splot 'fort.99'
pause 0.1
splot 'fort.100'
pause 0.1
splot 'fort.101'
pause 0.1
splot 'fort.102'
pause 0.1
splot 'fort.103'
pause 0.1
splot 'fort.104'
pause 0.1
splot 'fort.105'
pause 0.1
splot 'fort.106'
pause 0.1
splot 'fort.107'
pause 0.1
splot 'fort.108'
pause 0.1
splot 'fort.109'
pause 0.1
splot 'fort.110'
pause 0.1
splot 'fort.111'
pause 0.1
splot 'fort.112'
pause 0.1
splot 'fort.113'
pause 0.1
splot 'fort.114'
pause 0.1
splot 'fort.115'
pause 0.1
splot 'fort.116'
pause 0.1
splot 'fort.117'
pause 0.1
splot 'fort.118'
pause 0.1
splot 'fort.119'
pause 0.1
splot 'fort.120'
pause 0.1
splot 'fort.121'
pause 0.1
splot 'fort.122'
pause 0.1
splot 'fort.123'
pause 0.1
splot 'fort.124'
pause 0.1
splot 'fort.125'
pause 0.1
splot 'fort.126'
pause 0.1
splot 'fort.127'
pause 0.1
splot 'fort.128'
pause 0.1
splot 'fort.129'
pause 0.1
splot 'fort.130'
pause 0.1
splot 'fort.131'
pause 0.1
splot 'fort.132'
pause 0.1
splot 'fort.133'
pause 0.1
splot 'fort.134'
pause 0.1
splot 'fort.135'
pause 0.1
splot 'fort.136'
pause 0.1
splot 'fort.137'
pause 0.1
splot 'fort.138'
pause 0.1
splot 'fort.139'
pause 0.1
splot 'fort.140'
pause 0.1
splot 'fort.141'
pause 0.1
splot 'fort.142'
pause 0.1
splot 'fort.143'
pause 0.1
splot 'fort.144'
pause 0.1
splot 'fort.145'
pause 0.1
splot 'fort.146'
pause 0.1
splot 'fort.147'
pause 0.1
splot 'fort.148'
pause 0.1
splot 'fort.149'
pause 0.1
splot 'fort.150'
pause 0.1
splot 'fort.151'
pause 0.1
splot 'fort.152'
pause 0.1
splot 'fort.153'
pause 0.1
splot 'fort.154'
pause 0.1
splot 'fort.155'
pause 0.1
splot 'fort.156'
pause 0.1
splot 'fort.157'
pause 0.1
splot 'fort.158'
pause 0.1
splot 'fort.159'

