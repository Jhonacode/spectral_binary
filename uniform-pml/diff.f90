module diff
use global, only : im,jm,ip,imax,jmax,dm,dn 
contains
subroutine dermcompact(dudm,u,ii,fi,ij,fj)


      implicit none
      integer::k,i,j,ii,fi,ij,fj
      
      real(kind=ip)::alpha,a1,b1,c1
      real(kind=ip)::alphab,ab,bb,cb,db
      real(kind=ip),dimension(4,im,jm):: u, dudm
      real(kind=ip),dimension(im):: a,b,c,g

      alpha  = 3.d0/8.d0
      a1     = 1.d0/6.d0*(alpha+9.d0)
      b1     = 1.d0/15.d0*(32.d0*alpha-9.d0)
      c1     = 1.d0/10.d0*(-3.d0*alpha+1.d0)
      
   !   alpha  = 1.d0/3.d0
   !   a1     = 14.d0/9.d0!*(alpha+9.d0)
   !   b1     = 1.d0/9.d0!*(32.d0*alpha-9.d0)
   !   c1     = 0.d0!*(-3.d0*alpha+1.d0)

      alphab   = 2.d0
      ab       = -(11.d0+2.d0*alphab)/6.d0!-17.d0/6.d0!*(alpha+9.d0)
      bb       = (6.d0-alphab)/2.d0!3.d0/2.d0!*(32.d0*alpha-9.d0)
      cb       = (2.d0*alphab-3.d0)/2.d0!3.d0/2.d0!*(-3.d0*alpha+1.d0)
      db       = (2.d0-alphab)/6.d0!-1.d0/6.d0!*(-3.d0*alpha+1.d0)


      a  = alpha
      b  = 1.d0
      c  = alpha

      a(ii)    = 0.d0
      b(ii)    = 1.d0
      c(ii)    = alphab

      a(ii+1)  = 0.d0
      b(ii+1)  = 1.d0
      c(ii+1)  = alphab
      
      a(ii+2)  = 0.d0
      b(ii+2)  = 1.d0
      c(ii+2)  = alphab
      
     
      a(fi)    = alphab
      b(fi)    = 1.d0
      c(fi)    = 0.d0
      
      a(fi-1)  = alphab
      b(fi-1)  = 1.d0
      c(fi-1)  = 0.d0 
      
      a(fi-2)  = alphab
      b(fi-2)  = 1.d0
      c(fi-2)  = 0.d0

do k=1,4 
      do j=ij,fj
  
         g(ii)      =  1.d0/dm*(ab*u(k,ii,j)   + bb*u(k,ii+1,j)   + cb*u(k,ii+2,j)   + db*u(k,ii+3,j)) 
         g(ii+1)    =  1.d0/dm*(ab*u(k,ii+1,j) + bb*u(k,ii+1+1,j) + cb*u(k,ii+1+2,j) + db*u(k,ii+1+3,j))
         g(ii+2)    =  1.d0/dm*(ab*u(k,ii+2,j) + bb*u(k,ii+2+1,j) + cb*u(k,ii+2+2,j) + db*u(k,ii+2+3,j))
                                                                                                     
         g(fi)      = -1.d0/dm*(ab*u(k,fi,j)   + bb*u(k,fi-1,j)   + cb*u(k,fi-2,j)   + db*u(k,fi-3,j))
         g(fi-1)    = -1.d0/dm*(ab*u(k,fi-1,j) + bb*u(k,fi-1-1,j) + cb*u(k,fi-1-2,j) + db*u(k,fi-1-3,j))
         g(fi-2)    = -1.d0/dm*(ab*u(k,fi-2,j) + bb*u(k,fi-2-1,j) + cb*u(k,fi-2-2,j) + db*u(k,fi-2-3,j))
         

         do i=ii+3,fi-3
         
           g(i)  = c1*(u(k,i+3,j)-u(k,i-3,j))/(6.d0*dm)+b1*(u(k,i+2,j)-u(k,i-2,j))/(4.d0*dm)+a1*(u(k,i+1,j)-u(k,i-1,j))/(2.d0*dm)
       
         end do

         call thomas(a,b,c,g,dudm(k,ii:fi,j),im)!fi-ii)


         ! if (k==4)then
         ! do i=ii,fi
         ! write(*,*) a(i),b(I),c(i),g(i),dudm(k,i,j)
         ! end do
         ! pause
         ! end if
      end do
end do

end subroutine

subroutine derncompact(dudn,u,ii,fi,ij,fj)


      implicit none
      integer::k,i,j,ii,fi,ij,fj

      
      real(kind=ip)::alpha,a1,b1,c1
      real(kind=ip)::alphab,ab,bb,cb,db
      real(kind=ip),dimension(4,im,jm):: u, dudn
      real(kind=ip),dimension(jm):: a,b,c,g
      real(kind=ip),dimension(jm):: x

         
          
      alpha  = 3.d0/8.d0
      a1     = 1.d0/6.d0*(alpha+9.d0)
      b1     = 1.d0/15.d0*(32.d0*alpha-9.d0)
      c1     = 1.d0/10.d0*(-3.d0*alpha+1.d0)

!      alpha  = 1.d0/3.d0
!      a1     = 14.d0/9.d0!*(alpha+9.d0)
!      b1     = 1.d0/9.d0!*(32.d0*alpha-9.d0)
!      c1     = 0.d0!*(-3.d0*alpha+1.d0)
      
      alphab   = 2.d0
      ab       = -(11.d0+2.d0*alphab)/6.d0!-17.d0/6.d0!*(alpha+9.d0)
      bb       = (6.d0-alphab)/2.d0!3.d0/2.d0!*(32.d0*alpha-9.d0)
      cb       = (2.d0*alphab-3.d0)/2.d0!3.d0/2.d0!*(-3.d0*alpha+1.d0)
      db       = (2.d0-alphab)/6.d0!-1.d0/6.d0!*(-3.d0*alpha+1.d0)

    !  alphab  = 3.d0
    !  ab      =- 17.d0/6.d0!*(alpha+9.d0)
    !  bb      = 3.d0/2.d0!*(32.d0*alpha-9.d0)
    !  cb      = 3.d0/2.d0!*(-3.d0*alpha+1.d0)
    !  db      = -1.d0/6.d0!*(-3.d0*alpha+1.d0)
      
      a  = alpha
      b  = 1.d0
      c  = alpha
      
      a(ij)      = 0.d0
      b(ij)      = 1.d0
      c(ij)      = alphab
      
      a(ij+1)    = 0.d0
      b(ij+1)    = 1.d0
      c(ij+1)    = alphab
      
      a(ij+2)    = 0.d0
      b(ij+2)    = 1.d0
      c(ij+2)    = alphab
      
     
      a(fj)      = alphab
      b(fj)      = 1.d0
      c(fj)      = 0.d0
      
      a(fj-1)    = alphab
      b(fj-1)    = 1.d0
      c(fj-1)    = 0.d0 
      
      a(fj-2)    = alphab
      b(fj-2)    = 1.d0
      c(fj-2)    = 0.d0
      
do k=1,4 
      do i=ii,fi
  
         g(ij)      =  1.d0/dn*(ab*u(k,i,ij)   + bb*u(k,i,ij+1)   + cb*u(k,i,ij+2)   + db*u(k,i,ij+3)) !dUdn(k,i,ij)
         g(ij+1)    =  1.d0/dn*(ab*u(k,i,ij+1) + bb*u(k,i,ij+1+1) + cb*u(k,i,ij+1+2) + db*u(k,i,ij+1+3))!dUdn(k,i,ij+1)
         g(ij+2)    =  1.d0/dn*(ab*u(k,i,ij+2) + bb*u(k,i,ij+2+1) + cb*u(k,i,ij+2+2) + db*u(k,i,ij+2+3))!dUdn(k,i,ij+2)
         
         g(fj)      = -1.d0/dn*(ab*u(k,i,fj)   + bb*u(k,i,fj-1)   + cb*u(k,i,fj-2)   + db*u(k,i,fj-3))!dUdn(k,i,fj)
         g(fj-1)    = -1.d0/dn*(ab*u(k,i,fj-1) + bb*u(k,i,fj-1-1) + cb*u(k,i,fj-1-2) + db*u(k,i,fj-1-3))!dUdn(k,i,fj-1)
         g(fj-2)    = -1.d0/dn*(ab*u(k,i,fj-2) + bb*u(k,i,fj-2-1) + cb*u(k,i,fj-2-2) + db*u(k,i,fj-2-3))!dUdn(k,i,fj-2)

 
         do j=ij+3,fj-3
         
          g(j)  = c1*(u(k,i,j+3)-u(k,i,j-3))/(6.d0*dn)+b1*(u(k,i,j+2)-u(k,i,j-2))/(4.d0*dn)+a1*(u(k,i,j+1)-u(k,i,j-1))/(2.d0*dn)
       
           write(*,*)ij,fj
         end do
     
       call thomas(a,b,c,g,dudn(k,i,ij:fj),jmax)
     
      end do

end do

end subroutine

real(kind=ip) function dv6th(a1,a2,a3,a4,a5,d)
    
      implicit none
      real(kind=ip)::a1,a2,a3,a4,a5,d
      
      
      dv6th= (35.d0*a1-56.d0*a2+28.d0*a3-8.d0*a4+a5)/(20.d0*d)
      

end function

subroutine thomas(a,b,c,d,x,n)
implicit none
integer::i,n
real(kind=ip),dimension(n)::a,b,c,d
real(kind=ip),dimension(n)::cp,dp
real(kind=ip),dimension(n)::x
real(kind=ip)::m

cp(1)=c(1)/b(1)
dp(1)=d(1)/b(1)

do i=2,n

        m      = b(i)-cp(i-1)*a(i)        
        cp(i)  = c(i)/m
        dp(i)  = (d(i)-dp(i-1)*a(i))/m

end do
   
x(n)=dp(n)

do i=n-1,1,-1

     x(i)=dp(i)-cp(i)*x(i+1)
  
end do

end subroutine

subroutine dernb(dudn,u)

!.......................................................................
!     subrotina para calculo da derivada primeira de uma funcao u
!     utilizando diferencas centradas de quarta ordem nos pontos
!     internos do dominio, diferencas centradas de segunda ordem
!     nos pontos vizinhos 'a fronteira e diferencas unilateral de
!     segunda ordem nos pontos de fronteira.
!.......................................................................
      implicit none
      
      integer:: i, j
      
      real(kind=ip):: dv4th, dv2nd, onesp, onesm
      real(kind=ip):: ap2, ap1, am1, am2, del, a 
      real(kind=ip),dimension(jm):: u, dudn
     
    
      
!......................................................function statement
      dv4th(ap2,ap1,am1,am2,del) =                                      &
     &          (-ap2 + 8.d0*ap1 - 8.d0*am1 + am2)     / 12.d0 / del
      dv2nd(ap1,am1,del)   = ( ap1 - am1 )             * 0.5d0 / del
      onesp(a,ap1,ap2,del) = (-3.d0*a + 4.d0*ap1 - ap2) * 0.5d0 / del
      onesm(a,am1,am2,del) = ( 3.d0*a - 4.d0*am1 + am2) * 0.5d0 / del


!.................. Dentro do Dominio ..................................
      do j=3,jmax-2
         dudn(j) = dv4th(u(j+2),u(j+1),u(j-1),u(j-2),dn)
      enddo

!..........................Linha Especiais .............................
      j=2
         dudn(j) = dv2nd(u(j+1),u(j-1),dn)

!.......................................................................
      j=jmax-1
         dudn(j) = dv2nd(u(j+1),u(j-1),dn)

!.......................................................................
      j=jmax
         dudn(j) = onesm(u(j),u(j-1),u(j-2),dn)

!.......................................................................
      j=1
         dudn(j) = onesp(u(j),u(j+1),u(j+2),dn)
      
end subroutine

!.................................................................
subroutine dermb(dudm,u)

      implicit none
      
      integer:: i, j
      
      real(kind=ip):: dv4th, dv2nd, onesp, onesm
      real(kind=ip):: ap2, ap1, am1, am2, del, a 
      real(kind=ip),dimension(jm):: u, dudm
     
    
      
!......................................................function statement
      dv4th(ap2,ap1,am1,am2,del) =                                      &
     &          (-ap2 + 8.d0*ap1 - 8.d0*am1 + am2)     / 12.d0 / del
      dv2nd(ap1,am1,del)   = ( ap1 - am1 )             * 0.5d0 / del
      onesp(a,ap1,ap2,del) = (-3.d0*a + 4.d0*ap1 - ap2) * 0.5d0 / del
      onesm(a,am1,am2,del) = ( 3.d0*a - 4.d0*am1 + am2) * 0.5d0 / del


!.................. Dentro do Dominio ..................................
      do i=3,imax-2
         dudm(i) = dv4th(u(i+2),u(i+1),u(i-1),u(i-2),dm)
      enddo

!..........................Linha Especiais .............................
      i=2
         dudm(i) = dv2nd(u(i+1),u(i-1),dm)

!.......................................................................
      i=imax-1
         dudm(i) = dv2nd(u(i+1),u(i-1),dm)

!.......................................................................
      i=imax
         dudm(i) = onesm(u(i),u(i-1),u(i-2),dm)

!.......................................................................
      i=1
         dudm(i) = onesp(u(i),u(i+1),u(i+2),dm)
      
end subroutine



subroutine derm(dU,U,ii,fi,ij,fj)

!.......................................................................
!     subrotina para calculo da derivada primeira de uma funcao u
!     utilizando diferencas centradas de quarta ordem nos pontos
!     internos do dominio, diferencas centradas de segunda ordem
!     nos pontos vizinhos 'a fronteira e diferencas unilateral de
!     segunda ordem nos pontos de fronteira.
!.......................................................................
      implicit none
      
      integer:: k, i, j, ii,fi,ij,fj
      
      real(kind=ip):: dv4th, dv2nd, onesp, onesm
      real(kind=ip):: ap2, ap1, am1, am2, del, a 
      real(kind=ip),dimension(4,im,jm):: U,dU
    
     
      
!......................................................function statement
      dv4th(ap2,ap1,am1,am2,del) =                                      &
     &          (-ap2 + 8.d0*ap1 - 8.d0*am1 + am2)     / 12.d0 / del
      dv2nd(ap1,am1,del)   = ( ap1 - am1 )             * 0.5d0 / del
      onesp(a,ap1,ap2,del) = (-3.d0*a + 4.d0*ap1 - ap2) * 0.5d0 / del
      onesm(a,am1,am2,del) = ( 3.d0*a - 4.d0*am1 + am2) * 0.5d0 / del

do k=1,4
!.................. Dentro do Dominio ..................................
      do j=ij,fj
      do i=ii+2,fi-2      
         du(k,i,j)  = dv4th(u(k,i+2,j),u(k,i+1,j),u(k,i-1,j),u(k,i-2,j),dm)
      enddo
      enddo
!.......................................................................
      i=ii
      do j=ij,fj
         du(k,i,j) = onesp(u(k,i,j),u(k,i+1,j),u(k,i+2,j),dm)
      enddo
!......................................................................      
      
      i=ii+1
      do j=ij,fj
         du(k,i,j) = dv2nd(u(k,i+1,j),u(k,i-1,j),dm)
      enddo
!.......................................................................     
      
      i=fi
      do j=ij,fj
         du(k,i,j) = onesm(u(k,i,j),u(k,i-1,j),u(k,i-2,j),dm)
      enddo

!.......................................................................
      i=fi-1
      do j=ij,fj
         du(k,i,j) = dv2nd(u(k,i+1,j),u(k,i-1,j),dm)
      enddo

!.......................................................................
end do      
end subroutine

subroutine dern(dU,U,ii,fi,ij,fj)

!.......................................................................
!     subrotina para calculo da derivada primeira de uma funcao u
!     utilizando diferencas centradas de quarta ordem nos pontos
!     internos do dominio, diferencas centradas de segunda ordem
!     nos pontos vizinhos 'a fronteira e diferencas unilateral de
!     segunda ordem nos pontos de fronteira.
!.......................................................................
      implicit none
      
      integer:: ii,fi,ij,fj,k, i,j
      
      real(kind=ip):: dv4th, dv2nd, onesp, onesm
      real(kind=ip):: ap2, ap1, am1, am2, del, a 
      real(kind=ip),dimension(4,im,jm):: U,dU
     
     
      
!......................................................function statement
      dv4th(ap2,ap1,am1,am2,del) =                                      &
     &          (-ap2 + 8.d0*ap1 - 8.d0*am1 + am2)     / 12.d0 / del
      dv2nd(ap1,am1,del)   = ( ap1 - am1 )             * 0.5d0 / del
      onesp(a,ap1,ap2,del) = (-3.d0*a + 4.d0*ap1 - ap2) * 0.5d0 / del
      onesm(a,am1,am2,del) = ( 3.d0*a - 4.d0*am1 + am2) * 0.5d0 / del

do  k=1,4
!.................. Dentro do Dominio ..................................
      do j=ij+2,fj-2
      do i=ii,fi
         du(k,i,j) = dv4th(u(k,i,j+2),u(k,i,j+1),u(k,i,j-1),u(k,i,j-2),dn)
      enddo
      enddo

!..........................Linha Especiais .............................
      j=ij+1
      do i=ii,fi
         du(k,i,j) = dv2nd(u(k,i,j+1),u(k,i,j-1),dn)
      enddo

!.......................................................................
      j=fj-1
      do i=ii,fi
         du(k,i,j) = dv2nd(u(k,i,j+1),u(k,i,j-1),dn)
      enddo

!.......................................................................
      j=fj
      do i=ii,fi
         du(k,i,j) = onesm(u(k,i,j),u(k,i,j-1),u(k,i,j-2),dn)
      enddo

!.......................................................................
      j=ij
      do i=ii,fi
         du(k,i,j) = onesp(u(k,i,j),u(k,i,j+1),u(k,i,j+2),dn)
      enddo
!.......................................................................
end do      
end subroutine

subroutine deropm(dU,U,ii,fi,ij,fj)

!.......................................................................
!     subrotina para calculo da derivada primeira de uma funcao u
!     utilizando diferencas centradas de quarta ordem nos pontos
!     internos do dominio, diferencas centradas de segunda ordem
!     nos pontos vizinhos 'a fronteira e diferencas unilateral de
!     segunda ordem nos pontos de fronteira.
!.......................................................................
      implicit none
      
      integer:: k, i, j, ii, fi, ij, fj
      
      real(kind=ip),dimension(4,im,jm):: U,dU

      do k=1,4
!....   ...................................................................
         do j=ij,fj
!....   ...................................................................
           do i=ii+3,fi-3      
               du(k,i,j)  = dudx(u,k,i,j)
           end do
           i=ii+2
           du(k,i,j) = dx4(u,k,i,j,1,7,1)
           i=fi-2
           du(k,i,j) = dx4(u,k,i,j,7,1,-1)
!....     ...................................................................
           i=ii+1
           du(k,i,j) = dx5(u,k,i,j,1,7,1)
           i=fi-1
           du(k,i,j) = dx5(u,k,i,j,7,1,-1)
!....     ...................................................................
           i=ii
           du(k,i,j) = dx6(u,k,i,j,1,7,1)
           i=fi
           du(k,i,j) = dx6(u,k,i,j,7,1,-1)
!....   ...................................................................
         end do 
!....   ...................................................................
     end do 
end subroutine

real function dudx(u,k,i,j)

    integer::k,i,j,z
    real(kind=ip),dimension(4,im,jm):: U
    real(kind=ip),dimension(3)::a

    !a(1)=!0.766855408972008323 ! 0.770882380518d0    
    !a(2)=!-0.163484327177606692!-0.166705904415d0   
    !a(3)=!0.02003774846106832  ! 0.0208431427703d0   

    a(1)= 0.770882380518d0   
    a(2)=-0.166705904415d0   
    a(3)= 0.0208431427703d0 

    dudx=0.d0

    do z=1,3
         dudx= dudx + (a(z)*(u(k,i+z,j)-u(k,i-z,j)))/dm
    end do

end function dudx

real function dx4(u,k,i,j,iz,fz,p)

    integer::k,i,j,z,iz,fz,p
    real(kind=ip),dimension(4,im,jm):: U
    real(kind=ip),dimension(7)::a

    if(p==1)then 

       a(1)= 0.049041958000d0   
       a(2)=-0.46884035700d0  
       a(3)=-0.47476091400d0   
       a(4)= 1.27327473700d0  
       a(5)=-0.51848452600d0
       a(6)= 0.16613853300d0
       a(7)=-0.026369431000d0  

       dx4=0.d0
        
       do z=iz,fz,p
          dx4= dx4+a(z)*u(k,i+z-3,j)/dm
       end do 

    else

       a(7)=-0.049041958000d0 
       a(6)= 0.46884035700d0  
       a(5)= 0.47476091400d0  
       a(4)=-1.27327473700d0  
       a(3)= 0.51848452600d0
       a(2)=-0.16613853300d0
       a(1)= 0.026369431000d0
        
       dx4=0.d0
   
       do z=iz,fz,p
          dx4= dx4+a(z)*u(k,i+z-5,j)/dm
       end do 

    end if

end function dx4

real function dx5(u,k,i,j,iz,fz,p)

    integer::k,i,j,z,iz,fz,p
    real(kind=ip),dimension(4,im,jm):: U
    real(kind=ip),dimension(7)::a

    if(p==1)then 

       a(1)=-0.20933762200d0    
       a(2)=-1.08487567600d0   
       a(3)= 2.14777605000d0   
       a(4)=-1.38892832200d0   
       a(5)= 0.76894976600d0 
       a(6)=-0.28181465000d0 
       a(7)= 0.048230454000d0 

       dx5=0.d0
        
       do z=iz,fz,p
          dx5= dx5+a(z)*u(k,i+z-2,j)/dm
       end do 

    else

       a(7)= 0.20933762200d0    
       a(6)= 1.08487567600d0    
       a(5)=-2.14777605000d0    
       a(4)= 1.38892832200d0    
       a(3)=-0.76894976600d0  
       a(2)= 0.28181465000d0  
       a(1)=-0.048230454000d0 
        
       dx5=0.d0
   
       do z=iz,fz,p
          dx5= dx5+a(z)*u(k,i+z-6,j)/dm
       end do 

    end if

end function dx5

real function dx6(u,k,i,j,iz,fz,p)

    integer::k,i,j,z,iz,fz,p
    real(kind=ip),dimension(4,im,jm):: U
    real(kind=ip),dimension(7)::a

    if(p==1)then 

       a(1)=-2.19228033900d0    
       a(2)= 4.74861140100d0   
       a(3)=-5.10885191500d0   
       a(4)= 4.46156710400d0   
       a(5)=-2.83349874100d0 
       a(6)= 1.12832886100d0 
       a(7)=-0.20387637100d0 

       dx6=0.d0
        
       do z=iz,fz,p
          dx6= dx6+a(z)*u(k,i+z-1,j)/dm
       end do 

    else

       a(7)=  2.19228033900d0    
       a(6)= -4.74861140100d0   
       a(5)=  5.10885191500d0   
       a(4)= -4.46156710400d0   
       a(3)=  2.83349874100d0 
       a(2)= -1.12832886100d0 
       a(1)=  0.20387637100d0 
        
       dx6=0.d0
   
       do z=iz,fz,p
          dx6= dx6+a(z)*u(k,i+z-7,j)/dm
       end do 

    end if

end function dx6

subroutine deropn(dU,U,ii,fi,ij,fj)

!.......................................................................
!
!.......................................................................
      implicit none
      
      integer:: k, i, j, ii, fi, ij, fj
      
      real(kind=ip),dimension(4,im,jm):: U,dU

      do k=1,4
!....   ...................................................................
         do i=ii,fi
!....   ...................................................................
           do j=ij+3,fj-3      
               du(k,i,j)  = dudy(u,k,i,j)
           end do
           j=ij+2
           du(k,i,j) = dy4(u,k,i,j,1,7,1)
           j=fj-2
           du(k,i,j) = dy4(u,k,i,j,7,1,-1)
!....     ...................................................................
           j=ij+1
           du(k,i,j) = dy5(u,k,i,j,1,7,1)
           j=fj-1
           du(k,i,j) = dy5(u,k,i,j,7,1,-1)
!....     ...................................................................
           j=ij
           du(k,i,j) = dy6(u,k,i,j,1,7,1)
           j=fj
           du(k,i,j) = dy6(u,k,i,j,7,1,-1)
!....   ...................................................................
         end do 
!....   ...................................................................
     end do 

end subroutine

real function dudy(u,k,i,j)

    integer::k,i,j,z
    real(kind=ip),dimension(4,im,jm):: U
    real(kind=ip),dimension(3)::a

    !a(1)=!0.766855408972008323 ! 0.770882380518d0    
    !a(2)=!-0.163484327177606692!-0.166705904415d0   
    !a(3)=!0.02003774846106832  ! 0.0208431427703d0   

    a(1)= 0.770882380518d0   
    a(2)=-0.166705904415d0   
    a(3)= 0.0208431427703d0 

    dudy=0.d0

    do z=1,3
         dudy= dudy + (a(z)*(u(k,i,j+z)-u(k,i,j-z)))/dn
    end do

end function dudy

real function dy4(u,k,i,j,iz,fz,p)

    integer::k,i,j,z,iz,fz,p
    real(kind=ip),dimension(4,im,jm):: U
    real(kind=ip),dimension(7)::a

    if(p==1)then 

       a(1)= 0.049041958000d0   
       a(2)=-0.46884035700d0  
       a(3)=-0.47476091400d0   
       a(4)= 1.27327473700d0  
       a(5)=-0.51848452600d0
       a(6)= 0.16613853300d0
       a(7)=-0.026369431000d0  

       dy4=0.d0
        
       do z=iz,fz,p
          dy4= dy4+a(z)*u(k,i,j+z-3)/dn
       end do 

    else

       a(7)=-0.049041958000d0 
       a(6)= 0.46884035700d0  
       a(5)= 0.47476091400d0  
       a(4)=-1.27327473700d0  
       a(3)= 0.51848452600d0
       a(2)=-0.16613853300d0
       a(1)= 0.026369431000d0
        
       dy4=0.d0
   
       do z=iz,fz,p
          dy4= dy4+a(z)*u(k,i,j+z-5)/dn
       end do 

    end if

end function dy4

real function dy5(u,k,i,j,iz,fz,p)

    integer::k,i,j,z,iz,fz,p
    real(kind=ip),dimension(4,im,jm):: U
    real(kind=ip),dimension(7)::a

    if(p==1)then 

       a(1)=-0.20933762200d0    
       a(2)=-1.08487567600d0   
       a(3)= 2.14777605000d0   
       a(4)=-1.38892832200d0   
       a(5)= 0.76894976600d0 
       a(6)=-0.28181465000d0 
       a(7)= 0.048230454000d0 

       dy5=0.d0
        
       do z=iz,fz,p
          dy5= dy5+a(z)*u(k,i,j+z-2)/dn
       end do 

    else

       a(7)= 0.20933762200d0    
       a(6)= 1.08487567600d0    
       a(5)=-2.14777605000d0    
       a(4)= 1.38892832200d0    
       a(3)=-0.76894976600d0  
       a(2)= 0.28181465000d0  
       a(1)=-0.048230454000d0 
        
       dy5=0.d0
   
       do z=iz,fz,p
          dy5= dy5+a(z)*u(k,i,j+z-6)/dn
       end do 

    end if

end function dy5

real function dy6(u,k,i,j,iz,fz,p)

    integer::k,i,j,z,iz,fz,p
    real(kind=ip),dimension(4,im,jm):: U
    real(kind=ip),dimension(7)::a

    if(p==1)then 

       a(1)=-2.19228033900d0    
       a(2)= 4.74861140100d0   
       a(3)=-5.10885191500d0   
       a(4)= 4.46156710400d0   
       a(5)=-2.83349874100d0 
       a(6)= 1.12832886100d0 
       a(7)=-0.20387637100d0 

       dy6=0.d0
        
       do z=iz,fz,p
          dy6= dy6+a(z)*u(k,i,j+z-1)/dn
       end do 

    else

       a(7)=  2.19228033900d0    
       a(6)= -4.74861140100d0   
       a(5)=  5.10885191500d0   
       a(4)= -4.46156710400d0   
       a(3)=  2.83349874100d0 
       a(2)= -1.12832886100d0 
       a(1)=  0.20387637100d0 
        
       dy6=0.d0
   
       do z=iz,fz,p
          dy6= dy6+a(z)*u(k,i,j+z-7)/dn
       end do 

    end if

end function dy6


end module
