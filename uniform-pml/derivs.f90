module derivs

use global
use global2
use diff

contains

subroutine deriv(U,q1,q2)

  real(kind=ip),dimension(4,im,jm):: U,dudx,dudy 
  real(kind=ip),dimension(4,im,jm):: q1,q2

  call deropm(dUdm,U,1,imax,1,jmax) 
  call deropn(dUdn,U,1,imax,1,jmax) 

 ! call derm(dq1dm,q1,1,imaxpml,1,jmax)               
 ! call derm(dq1dm,q1,imax-D+1,imax,1,jmax)           
 ! call derm(dq1dm,q1,imaxpml+1,imax-D,1,jmaxpml)     
 ! call derm(dq1dm,q1,imaxpml+1,imax-D,jmax-D+1,jmax) 

!  call dermcompact(dUdm,U,1,imax,1,jmax) 
!  call derncompact(dUdn,U,1,imax,1,jmax) 
!do k=1,4
!do i=1,imax
! do j=1,jmax
!  write(100,*)k,i,j,dudx(k,i,j),dudm(k,i,j),dudy(k,i,j),dudn(k,i,j)
! end do
!end do
!end do
!stop

!  call dermcompact(dq1dm,q1,1,imaxpml,1,jmax)               
!  call dermcompact(dq1dm,q1,imax-D+1,imax,1,jmax)           
!  call dermcompact(dq1dm,q1,imaxpml+1,imax-D,1,jmaxpml)     
!  call dermcompact(dq1dm,q1,imaxpml+1,imax-D,jmax-D+1,jmax) 
!!
!subroutine deropn(dU,U,ii,fi,ij,fj)
  call deropn(dq1dn,q1,1,imaxpml,1,jmax)               
  call deropn(dq1dn,q1,imax-D+1,imax,1,jmax)           
  call deropn(dq1dn,q1,imaxpml+1,imax-D,1,jmaxpml)     
  call deropn(dq1dn,q1,imaxpml+1,imax-D,jmax-D+1,jmax) 

  call deropm(dq2dm,q2,1,imaxpml,1,jmax)               
  call deropm(dq2dm,q2,imax-D+1,imax,1,jmax)           
  call deropm(dq2dm,q2,imaxpml+1,imax-D,1,jmaxpml)     
  call deropm(dq2dm,q2,imaxpml+1,imax-D,jmax-D+1,jmax) 
!
!  call dern(dq2dn,q2,1,imaxpml,1,jmax)               
!  call dern(dq2dn,q2,imax-D+1,imax,1,jmax)           
!  call dern(dq2dn,q2,imaxpml+1,imax-D,1,jmaxpml)     
!  call dern(dq2dn,q2,imaxpml+1,imax-D,jmax-D+1,jmax) 

end subroutine

end module 
