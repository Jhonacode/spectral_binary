reset
#set yrange [-1:1]
set title 'Contorno de Pressão '
set isosamples 100
set hidden3d
set pm3d 
set view map
unset surface
set key outside
set term pdf
#set contour both 
#set palette gray negative
set output "pml1.pdf"
splot 'fort.10' u 1:2:6  with pm3d t ''    
set output "pml2.pdf"
splot 'fort.50' u 1:2:6  with pm3d  t '' 
set output "pml3.pdf"
splot 'fort.100' u 1:2:6  with pm3d  t ''
set output "pml4.pdf"
splot 'fort.150' u 1:2:6  with pm3d  t ''
set output "pml5.pdf"
splot 'fort.200' u 1:2:6  with pm3d  t ''
set output "pml6.pdf"
splot 'fort.250' u 1:2:6  with pm3d t ''

