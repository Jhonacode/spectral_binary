!***********************************************************************
!*                                                                     *
!*                   NONLINEAR EULER EQUATION-AEROACUSTIC              *
!*                                                                     *
!* UPDATE            : 20-08/2013                                      *
!* MODIFICATION      : IMPLEMENTAÇÃO DA PML NAS  EQUAÇÕES NAO LINEARES *
!*                     DE EULER EM VARIAVEIS PRIMITIVAS.               *
!                                                                      *
!* DATE              : 16-08/2013                                      *
!* MODIFY  BY        : JHONATAN                                        *
!* BASED ON          : PML ABC FOR NONLINEAR EULER EQUATIONS IN        *
!*                     PRIMITIVE VARIABLES,AIAA 2009-6 - HU, LIN, LI *
!* GRID              : GENERAL GRID(M,N)                               *
!* ALGORITHMS        : EXPLICIT METHOD- COMPLETE EULER EQUATION        *
!*                     2D-STRECHING NA PML EN X E Y, FILTRO 10 ORDEM   *
!* RESOLT METHOD     : RUNGE-KUTA 4 ORDER                              *
!* BOUNDARY CONDITION:                                                 * 
!* FORTRAN 90        : MODULE , THOMA'S ALGORITHM                      *
!*                                                                     *
!***********************************************************************

program EULER

use global
use globalq
use initialize
use outt
use filtering
use rk
use derivs

!........................................................................
      implicit none

      integer:: iter,icount,cf,cg,i,j
      
      real ::timei,timef
      real(kind=ip)::t
      real(kind=ip),dimension(4,im,jm):: U
      real(kind=ip),dimension(jm):: ec, decdm
      
      call CPU_TIME(timei)
      
      pi=acos(-1.d0)  

!................Inicialização das variaveis..............................
      call init(U)
      write(*,*)m(px),n(py)
      write(2,*)'ponto',m(px),n(py)
!.........................................................................
!Contador Graficas
      icount = 0
!Contador filtro 
      cf=0
      cg=0
!.........................................................................

      do iter = 0,maxit

         t=iter*dt
       
         !call rkpml(U,t)
         call rk_euler(U,t,iter)

         if(cf==10)then
             write(*,*)iter,dt*iter,U(4,px,py)
             call filtercompactx(U,1,imax,1,jmax)
             call filtercompacty(U,1,imax,1,jmax)
!             call output(U,icount)
!             icount = icount+1
             cf=0
         end if

         !write(2,*)dt*icount,U(4,px,py),U(3,px,py),U(2,px,py),U(1,px,py)

         cf=cf+1

        if(cg==1)then
          call output(U,icount)
          icount = icount+2
          cg = 0
        end if

        cg=cg+1
!      write(*,*)iter

      end do 

!      do i=imaxpml,imax-imaxpml
!         ec(i) = 0.d0
!         do j=jmaxpml+1,jmax-jmaxpml-1
!            ec(i) = ec(i) + (.5*(U(2,i,j)**2   + U(3,i,j)**2)+&
!      &                      .5*(U(2,i,j+1)**2 + U(3,i,j+1)**2))*dn
!         end do
!      end do

!      call dermb(decdm,ec)

!      do i=imaxpml,imax-imaxpml
!        write(2,*) m(i), ec(i), decdm(i)/ec(i)
!      end do
     

      call CPU_TIME(timef) 
      write(2,*)'Tempo :',timef-timei,'segundos'

end program EULER

