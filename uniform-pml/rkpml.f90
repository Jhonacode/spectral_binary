module rk

use global
use globalq
use rhspml
use rhsq
use cc
use filtering
contains


subroutine rkpml(U,t)
     
      implicit none         
      integer:: k, i, j

      real(kind=ip)::t,dt2, dt6
      real(kind=ip),dimension(4,im,jm):: U
      real(kind=ip),dimension(4,im,jm):: Up1,Up2,Up3,Up4
      real(kind=ip),dimension(4,im,jm):: dUdt,dUdtp1,dUdtp2,dUdtp3
      real(kind=ip),dimension(4,im,jm):: q1p1,q1p2,q1p3
      real(kind=ip),dimension(4,im,jm):: q2p1,q2p2,q2p3
   
!......................................................................
      dt2 = dt * 0.5d0
      dt6 = dt / 6.d0

!...................Parametros Fonte...................................
      w  = 1.0d0
      r0 = 0.05d0  

!....................fonte.............................................
   do i=1,imax
     do j=1,jmax
       s(i,j)= sin(w*(t+dt2))*exp(-log(2.d0)*((m(i))**2+(n(j))**2)/(r0*r0))
     end do
   end do
!......................................................................
      call rhs(dUdt,U,q1,q2)
      call rhsq1(u,q1,q1dtp0)
      call rhsq2(u,q2,q2dtp0)

!.................. First Step .....................................
      do k=1,4 
        do j = 2,jmax-1
            do i = 2,imax-1
                Up1(k,i,j) = U(k,i,j) + dt2*dUdt(k,i,j)
            end do
        end do
      end do 

      call ccpml(Up1)

  !    call filtercompactx(Up1)
  !    call filtercompacty(Up1)
  !    call q1fun(q1p1,q1dtp0,1,imaxpml,1,jmax,dt2)                     
  !    call q1fun(q1p1,q1dtp0,imax-D+1,imax,1,jmax,dt2)                   
  !    call q1fun(q1p1,q1dtp0,imaxpml+1,imax-D,1,jmaxpml,dt2)             
  !    call q1fun(q1p1,q1dtp0,imaxpml+1,imax-D,jmax-D+1,jmax,dt2)           

      ! call q2fun(q2p1,q2dtp0,1,imaxpml,1,jmax,dt2)                     
      ! call q2fun(q2p1,q2dtp0,imax-D+1,imax,1,jmax,dt2)                   
      ! call q2fun(q2p1,q2dtp0,imaxpml+1,imax-D,1,jmaxpml,dt2)             
      ! call q2fun(q2p1,q2dtp0,imaxpml+1,imax-D,jmax-D+1,jmax,dt2)           

!....................Fonte...................................................
    do i=1,imax
      do j=1,jmax
        s(i,j)= sin(w*(t+dt2))*exp(-log(2.d0)*((m(i))**2+(n(j))**2)/(r0*r0))
      end do
    end do
!.......................................................................

      call rhs(dUdtp1,Up1,q1p1,q2p1)
      call rhsq1(up1,q1p1,q1dtp1)
      call rhsq2(up1,q2p1,q2dtp1)

!..................  Second Step.....................................

      do k=1,4 
        do j = 2,jmax-1
            do i = 2,imax-1
                Up2(k,i,j) = U(k,i,j) + dt2*dUdtp1(k,i,j)
            end do
        end do
      end do 

      call ccpml(Up2)
  !    call filtercompactx(Up2)
  !    call filtercompacty(Up2)

 !     call q1fun(q1p2,q1dtp1,1,imaxpml,1,jmax,dt2)                     
 !     call q1fun(q1p2,q1dtp1,imax-D+1,imax,1,jmax,dt2)                   
 !     call q1fun(q1p2,q1dtp1,imaxpml+1,imax-D,1,jmaxpml,dt2)             
 !     call q1fun(q1p2,q1dtp1,imaxpml+1,imax-D,jmax-D+1,jmax,dt2)           

      ! call q2fun(q2p2,q2dtp1,1,imaxpml,1,jmax,dt2)                     
      ! call q2fun(q2p2,q2dtp1,imax-D+1,imax,1,jmax,dt2)                   
      ! call q2fun(q2p2,q2dtp1,imaxpml+1,imax-D,1,jmaxpml,dt2)             
      ! call q2fun(q2p2,q2dtp1,imaxpml+1,imax-D,jmax-D+1,jmax,dt2)           

!....................fonte...................................................
   do i=1,imax
     do j=1,jmax
       s(i,j)= sin(w*(t+dt))*exp(-log(2.d0)*((m(i))**2+(n(j))**2)/(r0*r0))
     end do
   end do
!.......................................................................

      call rhs(dUdtp2,Up2,q1p2,q2p2)
      call rhsq1(up2,q1p2,q1dtp2)
      call rhsq2(up2,q2p2,q2dtp2)

!..................  Third Step.....................................

      do k=1,4 
        do j = 2,jmax-1
            do i = 2,imax-1
                Up3(k,i,j) = U(k,i,j) + dt*dUdtp2(k,i,j)
            end do
        end do
      end do 

      call ccpml(Up3)
 !     call filtercompactx(Up3)
 !     call filtercompacty(Up3)

 !     call q1fun(q1p3,q1dtp2,1,imaxpml,1,jmax,dt)                     
 !     call q1fun(q1p3,q1dtp2,imax-D+1,imax,1,jmax,dt)                   
 !     call q1fun(q1p3,q1dtp2,imaxpml+1,imax-D,1,jmaxpml,dt)             
 !     call q1fun(q1p3,q1dtp2,imaxpml+1,imax-D,jmax-D+1,jmax,dt)           

      ! call q2fun(q2p3,q2dtp2,1,imaxpml,1,jmax,dt)                     
      ! call q2fun(q2p3,q2dtp2,imax-D+1,imax,1,jmax,dt)                   
      ! call q2fun(q2p3,q2dtp2,imaxpml+1,imax-D,1,jmaxpml,dt)             
      ! call q2fun(q2p3,q2dtp2,imaxpml+1,imax-D,jmax-D+1,jmax,dt)           

!....................fonte...................................................
   do i=1,imax
     do j=1,jmax
       s(i,j)= sin(w*(t+dt6))*exp(-log(2.d0)*((m(i))**2+(n(j))**2)/(r0*r0))
     end do
   end do
!.......................................................................

      call rhs(dUdtp3,Up3,q1p3,q2p3)
      call rhsq1(up3,q1p3,q1dtp3)
      call rhsq2(up3,q2p3,q2dtp3)

!.......................Fourt Step ..................................
!!
      call q1f(1,imaxpml,1,jmax,dt6)                    
      call q1f(imax-D+1,imax,1,jmax,dt6)                
      call q1f(imaxpml+1,imax-D,1,jmaxpml,dt6)          
      call q1f(imaxpml+1,imax-D,jmax-D+1,jmax,dt6)      
                                                 
      call q2f(1,imaxpml,1,jmax,dt6)                    
      call q2f(imax-D+1,imax,1,jmax,dt6)                
      call q2f(imaxpml+1,imax-D,1,jmaxpml,dt6)          
      call q2f(imaxpml+1,imax-D,jmax-D+1,jmax,dt6)      


      do k=1,4 
        do j = 2,jmax-1
          do i = 2,imax-1
             U(k,i,j) = U(k,i,j) + dt6 * (dUdt(k,i,j) +   &     
       &       2.d0*dUdtp1(k,i,j) + 2.d0*dUdtp2(k,i,j) + dUdtp3(k,i,j))
          end do
        end do
       end do

       call ccpml(U)
    

!      call filtercompactx(U)
!      call filtercompacty(U)
end subroutine

subroutine q1fun(qp,qt,ii,fi,ij,fj,dt)   

      implicit none
      integer::ii,fi,ij,fj,k,i,j
      real(kind=ip),dimension(4,im,jm):: qp
      real(kind=ip),dimension(4,im,jm):: qt
      real(kind=ip):: dt

      do k=1,4
        do j = ij,fj
          do i = ii,fi
              qp(k,i,j) = q1(k,i,j) + dt*qt(k,i,j)
          end do
        end do
     end do

end subroutine

subroutine q2fun(qp,qt,ii,fi,ij,fj,dt)   

      implicit none
      integer::ii,fi,ij,fj,k,i,j
      real(kind=ip),dimension(4,im,jm):: qp
      real(kind=ip),dimension(4,im,jm):: qt
      real(kind=ip):: dt

      do k=1,4
        do j = ij,fj
          do i = ii,fi
              qp(k,i,j) = q2(k,i,j) + dt*qt(k,i,j)
          end do
        end do
     end do

end subroutine

subroutine q1f(ii,fi,ij,fj,dt)

      implicit none
      integer::ii,fi,ij,fj,k,i,j
      real(kind=ip):: dt


      do k=1,4
        do i = ii,fi
           do j=ij,fj
               q1(k,i,j) = q1(k,i,j) + dt * (q1dtp0(k,i,j) +          &
     &            2.d0*q1dtp1(k,i,j) + 2.d0*q1dtp2(k,i,j)  + q1dtp3(k,i,j))
          end do
        end do
      end do

end subroutine

subroutine q2f(ii,fi,ij,fj,dt)

      implicit none
      integer::ii,fi,ij,fj,k,i,j
      real(kind=ip):: dt


      do k=1,4
        do i = ii,fi
           do j=ij,fj
               q2(k,i,j) = q2(k,i,j) + dt * (q2dtp0(k,i,j) +          &
     &            2.d0*q2dtp1(k,i,j) + 2.d0*q2dtp2(k,i,j)  + q2dtp3(k,i,j))
          end do
        end do
      end do

end subroutine

subroutine rk_euler(U,t,iter)
     
      implicit none         
      integer:: k, i, j,iter,irk
      real(kind=ip)::t,dt2, dt6
      real(kind=ip),dimension(6)::alphat,betat,ct
      real(kind=ip),dimension(4,im,jm):: U,dudt,q1,q2,w,wq1,wq2
      real(kind=ip),dimension(4,im,jm):: q1dt,q2dt
      real(kind=ip),dimension(4,im,jm):: q1p1,q1p2,q1p3,q1p4,q1p5,q1p6
      real(kind=ip),dimension(4,im,jm):: q2p1,q2p2,q2p3,q2p4,q2p5,q2p6

   
!......................................................................
     !inicializacao das variaveis do avanco temporal segundo Berland
            w   = 0
            wq1 = 0
            wq2 = 0
!......................................................................
     !coefiecientes do metodo lddrk do Berland de 6 passo e 4 ordem

      alphat(1) =  0.d0
      alphat(2) = -0.737101392796d0
      alphat(3) = -1.634740794341d0
      alphat(4) = -0.744739003780d0
      alphat(5) = -1.469897351522d0
      alphat(6) = -2.813971388035d0
      
      betat(1)  =  0.032918605146d0
      betat(2)  =  0.823256998200d0
      betat(3)  =  0.381530948900d0
      betat(4)  =  0.200092213184d0
      betat(5)  =  1.718581042715d0
      betat(6)  =  0.27d0
     
          ct(1) =  0.0d0
          ct(2) =  0.032918605146
          ct(3) =  0.249351723343
          ct(4) =  0.466911705055
          ct(5) =  0.582030414044
          ct(6) =  0.847252983783

!...........................................................................
!                        Parametros Fonte

       wf  = 1.0d0!*pi
       r0  = 0.05d0  


 !......................................................................

!.................. First Step .....................................
      do irk=1,6

!...........................................................................
!                        Acoustic font, location: here
         call  acousticfont(ct(irk),iter)
!............................................................................
!                       Temporal derivs, dudt=right hand side, location: rhs.f90
         call rhs(dUdt,U,q1,q2)
         call rhsq1(u,q1,q1dt)
         call rhsq2(u,q2,q2dt)

         !if (irk==6)then

         !       call q1f(1,imaxpml,1,jmax,ct(irk))                    
         !       call q1f(imax-D+1,imax,1,jmax,ct(irk))                
         !       call q1f(imaxpml+1,imax-D,1,jmaxpml,ct(irk))          
         !       call q1f(imaxpml+1,imax-D,jmax-D+1,jmax,ct(irk))      
         !                                                  
         !       call q2f(1,imaxpml,1,jmax,ct(irk))                    
         !       call q2f(imax-D+1,imax,1,jmax,ct(irk))                
         !       call q2f(imaxpml+1,imax-D,1,jmaxpml,ct(irk))          
         !       call q2f(imaxpml+1,imax-D,jmax-D+1,jmax,ct(irk))      

         !end if
!............................................................................
!                        Steps-Runge-Kutta, location: here
         call  rk_steep(alphat(irk),betat(irk),ct(irk),U,dUdt,w)

!Filter is applied at each estage of the rk to ETA boundary condition 

         !call exchange6_band_x(U)
         !call exchange6_band_y(U)
!        ! 
         !call lddfilterx(U)
         !call lddfiltery(U)
!!.......!.....................................................................
!        !                !Boundary conditons
!        !                !Boundary conditons
         call ccpml(U)

    end do
    
end subroutine

subroutine rk_steep(alphat,betat,ct,U,dudt,w) 
     implicit none
     integer::k,i,j
     real(kind=ip)::alphat,betat,ct 
     real(kind=ip),dimension(4,1:imax,1:jmax):: U,dudt,w

      do k=1,4 

              do i =1,imax
                do j =1,jmax
                    w(k,i,j) = alphat*w(k,i,j)+dt*dudt(k,i,j)
                    U(k,i,j) = U(k,i,j) + betat*w(k,i,j)
                end do  
              end do  

      end do 

end subroutine

subroutine acousticfont(ct,iter)
!...........................................................................
!...........................................................................
!       Fonte acustica para gerar o pulso de pressão que instabiliza o escoamento  
!...........................................................................
!     allocate (s_acou(i_s-g_p:d_s+g_p,s_s:e_s))

     implicit none
     integer::i,j,iter
     real(kind=ip)::ct 

!                        Parametros Fonte
!********Raio da fonte: Defined at dados.in, r0
  
!*******A FONTE ESTA EN X,Y(0.0)
     do i =1,imax 
        do  j=1,jmax 
           s(i,j)= sin(wf*((iter+ct)*dt))*exp(-log(2.d0)*((m(i)-0.0d0)**2+(n(j)-0.0d0)**2)/(r0*r0))
        end do
     end do

end subroutine




end module

