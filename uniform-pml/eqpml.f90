module eqpml

use global
use global2

contains

subroutine pml(U,q1,q2,dpml)

implicit none 
integer:: i, j
real(kind=ip),dimension(4,im,jm):: U,q1,q2,dpml


call ylayer(U,q1,q2,dpml, imaxpml+1, imax-D, 1       , jmaxpml) 
call ylayer(U,q1,q2,dpml, imaxpml+1, imax-D, jmax-D+1, jmax) 

call xlayer(U,q1,q2,dpml, 1       , imaxpml, jmaxpml+1, jmax-D) 
call xlayer(U,q1,q2,dpml, imax-D+1, imax   , jmaxpml+1, jmax-D) 

call corner(U,q1,q2,dpml, 1       , imaxpml, 1       , jmaxpml) 
call corner(U,q1,q2,dpml, imax-D+1, imax   , 1       , jmaxpml) 
call corner(U,q1,q2,dpml, 1       , imaxpml, jmax-D+1, jmax) 
call corner(U,q1,q2,dpml, imax-D+1, imax   , jmax-D+1, jmax) 

end subroutine              


!***********************************************************************
subroutine xlayer(U,q1,q2,dpml,ii,fi,ij,fj)
implicit none                     
integer::k,l,i,j,ii,fi,ij,fj
real(kind=ip)::Betax
real(kind=ip),dimension(4,im,jm)::U,q1,q2,dpml
real(kind=ip),dimension(im,jm)::temp,temp2

temp2(ii:fi,ij:fj)= 0.d0

Betax=Mx/(1.d0-Mx**2)

do k=1,4
 do l=1,4
    do i=ii,fi
      do j=ij,fj
  
         temp(i,j) =  A(k,l,i,j)*(dUdm(l,i,j))/meshx(i)       &
                &  +  B(k,l,i,j)*(dUdn(l,i,j))                &
                &  +  sigmax(i)*B0(k,l,i,j)*dq1dn(l,i,j)      &
                &  +  sigmax(i)*Betax*A(k,l,i,j)*(u(l,i,j))
      end do
    end do
    temp2(ii:fi,ij:fj) = temp(ii:fi,ij:fj)+temp2(ii:fi,ij:fj) 
  end do
  dpml(k,ii:fi,ij:fj) = temp2(ii:fi,ij:fj)
  temp2(ii:fi,ij:fj) = 0.d0
end do

do k=1,4
    do i=ii,fi
      do j=ij,fj

        dpml(k,i,j) = dpml(k,i,j) + (sigmax(i))*U(k,i,j)

      end do
    end do
end do

end subroutine 


!***********************************************************************
subroutine ylayer(U,q1,q2,dpml,ii,fi,ij,fj)
implicit none                     
integer::k,l,i,j,ii,fi,ij,fj
real(kind=ip)::Betay
real(kind=ip),dimension(4,im,jm)::U,q1,q2,dpml
real(kind=ip),dimension(im,jm)::temp,temp2

temp2(ii:fi,ij:fj)= 0.d0

Betay=My/(1.d0-My**2)

do k=1,4
 do l=1,4
    do i=ii,fi
      do j=ij,fj
  
         temp(i,j) =  A(k,l,i,j)*(dUdm(l,i,j))                        &
                &  +  B(k,l,i,j)*(dUdn(l,i,j))/meshy(j)               &
                &  +  sigmay(j)*A0(k,l,i,j)*dq2dm(l,i,j)              &
                &  +  sigmay(j)*Betay*B(k,l,i,j)*(u(l,i,j))
      end do
    end do
    temp2(ii:fi,ij:fj) = temp(ii:fi,ij:fj)+temp2(ii:fi,ij:fj) 
  end do
  dpml(k,ii:fi,ij:fj) = temp2(ii:fi,ij:fj)
  temp2(ii:fi,ij:fj) = 0.d0
end do

do k=1,4
    do i=ii,fi
      do j=ij,fj

        dpml(k,i,j) = dpml(k,i,j) + (sigmay(j))*U(k,i,j)

      end do
    end do
end do

end subroutine 


!***********************************************************************
subroutine corner(U,q1,q2,dpml,ii,fi,ij,fj)
implicit none                     
integer::k,l,i,j,ii,fi,ij,fj
real(kind=ip)::Betax
real(kind=ip)::Betay
real(kind=ip),dimension(4,im,jm)::U,q1,q2,dpml
real(kind=ip),dimension(im,jm)::temp,temp2


temp2(ii:fi,ij:fj)= 0.d0

Betax=Mx/(1.d0-Mx**2)
Betay=My/(1.d0-My**2)

do k=1,4
 do l=1,4
    do i=ii,fi
      do j=ij,fj
  
         temp(i,j) =  A(k,l,i,j)*(dUdm(l,i,j))/meshx(i)              &
                &  +  B(k,l,i,j)*(dUdn(l,i,j))/meshy(j)              & 
                &  +  sigmay(j)*A0(k,l,i,j)*dq2dm(l,i,j)/meshx(i)    &
                &  +  sigmax(i)*B0(k,l,i,j)*dq1dn(l,i,j)/meshy(j)    &
                &  +  sigmax(i)*Betax*A(k,l,i,j)*(u(l,i,j))          &
                &  +  sigmay(j)*Betay*B(k,l,i,j)*(u(l,i,j))
      end do
    end do
    temp2(ii:fi,ij:fj) = temp(ii:fi,ij:fj)+temp2(ii:fi,ij:fj) 
  end do
  dpml(k,ii:fi,ij:fj) = temp2(ii:fi,ij:fj)
  temp2(ii:fi,ij:fj) = 0.d0
end do

do k=1,4
  do i=ii,fi
    do j=ij,fj
      dpml(k,i,j) = dpml(k,i,j) &
   &              + (sigmax(i)+sigmay(j))*U(k,i,j) 
    end do
  end do
end do

end subroutine 

end module

