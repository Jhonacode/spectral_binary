#matplotlib inline
#config InlineBackend.figure_format='svg'
#f2py -m diffusion -c  global.f90 diffusion.f90 

from chebPy   import *      
from mapping  import *
from Baseflow import *
from numpy    import matrix,matmul,dot,diag,real,imag,argsort,zeros,exp,log,linspace,polyval,polyfit,where,tan,tanh,cosh,identity
from scipy.linalg  import eig
from scipy.special import airy
#from matplotlib   import figure,subplot,plot,title,grid,show,axis
from scipy.linalg  import block_diag
import numpy as np 
import diffusion as df
#my own function 
import Block as bk 
#from diffusion import visc_n2
import matplotlib.pyplot as plt 
import os
#exit fuction 
import sys

# Creates a folder in the current directory called data
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)
# Example
#createFolder('./data/')


#number of collocation nodes=N+1, because the cheb func
N           =   200 

#g           =   open('autospectroN%d.dat'%N,'w+')

file1       =      'Re10000' 
Reynolds    =      10000.0
Peclet      =      10000.0
Le1         =      1.0
Le2         =      1.0

createFolder('./%s/'%file1)

omegai      =   0.1
omegaf      =   3.1
iomega      =   30
domega      =   (omegaf-omegai)/iomega 

omega       =   zeros(iomega)
omega       =   np.arange(0,iomega,1)*domega+omegai

#omega[0]    =   omegai


D,n1        =   cheb(N); D2=dot(D,D); D2[1:N,1:N]

#maximum r
maxr        =   20   

r           =   mappingtan(N,n1,maxr,D,D2) 

#Python module Baseflow.py
#T_i=inner jet
#T_o=center jet
#T_0=ambientjet

Wb,Tb,Rhob,Y1,Y2,Y3,T_r,T_i,T_o,T_0,M_r,R1,theta1  =   Baseflow(r)
		 


#Fortran program diffusion.f90 
#   This program calculate the diffusion properties of  
#   the differens species and the mixture of gases. 

#To run the fortran subroutine.
#f2py -m diffusion -c  global.f90 diffusion.f90 

(mib,kb,cpb,cpb1,cpb2,D1m,Mw,Ru,Rmix,gammab,gamma_j,P_r)=\
df.properties.init_diffusion(Y1,Y2,Y3,Tb,T_r,T_i,T_o,T_0,N) 


#####
#Non dimensional gas ideal equation \bar{p}=1/{gamma_f}
Rhob  =  1.0/(Tb*Rmix) 

f           =   open('./%s/proper.dat'%file1,'w+')


for j in range(0,N):

	f.write("%f %f %f %f %f %f %f %f %f \n"%(r[j],Rhob[j],Wb[j],P_r[j],Tb[j],Y1[j],gammab[j],D1m[j],Rmix[j])) 


#Differences need 
dWbdr       =   dot(D ,Wb)
dRhobdr     =   dot(D ,Rhob)
d2Rhobdr    =   dot(D ,dRhobdr)
d1Rhobdr    =   dot(D ,(1.0/Rhob))
d21Rhobdr   =   dot(D ,d1Rhobdr )
dTbdr       =   dot(D ,Tb)
d2Tbdr      =   dot(D ,dTbdr)
d2Wbdr      =   dot(D ,dWbdr)
dY1bdr      =   dot(D ,Y1)
dkbdr       =   dot(D ,kb)
d2Y1bdr     =   dot(D ,dY1bdr)
d1PRhoRbdr   =   dot(D ,(P_r/(Rhob*Rmix)))
d2PRhobRdr   =   dot(D ,d1PRhoRbdr )
dD1mdr      =   dot(D ,D1m )

#
plt.figure(0)
plt.plot(Y1,r,'o-r') 
plt.ylim((0,4))
plt.show()

lambdab     =   zeros(N+1)
lambdab     =   -2.0/3.0*mib

dmibdr      =   dot(D,mib)
dlambdabdr  =   dot(D,lambdab)

a_r     =   np.sqrt(gammab*Rmix*1e3*T_r)     

#rho_0   =   0.575#(kg/m^3)atT_r=0.5 
rho_0   =   1.16 #(kg/m^3)atT_r=1 

W_r     =    M_r*a_r

rho_r   =   (1.0/T_r)*rho_0#=1/(Tb_r)d

# coordinate selectors

rr  =   np.arange(0,N,1)#0:N+1      
uu  =   rr+N
vv  =   uu+N
pp  =   vv+N

Z   =   zeros((N+1,N+1))
I   =   identity(N+1) 

##############################
#Elemental Matrix
DD      =   block_diag(D,D,D,D,D);
DD2     =   block_diag(D2,D2,D2,D2,D2);
II      =   identity(5*(N+1))
ZZ      =   block_diag(Z,Z,Z,Z,Z);
Ov      =   zeros((1,(10*(N+1))))

#remain to create a class of objects 
#to passa all the varibles necessary for the matrix#creation funciontion
#def matrices(mib,kb,cpb,D1m,D2m,D3m,Mw,Ru,Rmix,gammab,P_r):

#Cylindrical
Ae   =   np.block([
                   [Z ,diag(Rhob)      ,Z,Z             ,Z],
                   [Z ,Z               ,Z,diag(1.0/Rhob),Z],
                   [Z ,Z               ,Z,Z             ,Z],
                   [Z ,diag(gammab*P_r),Z,Z             ,Z],
                   [Z ,Z               ,Z,Z             ,Z]
                  ])

Be   =   np.block([
                  [diag(Wb),Z       ,diag(Rhob)        ,Z             ,Z        ],
                  [Z       ,diag(Wb),Z                 ,Z             ,Z        ],
                  [Z       ,Z       ,diag(Wb)          ,diag(1.0/Rhob),Z        ],
                  [Z       ,Z       ,diag(gammab*P_r)  ,diag(Wb)      ,Z        ],
                  [Z       ,Z       ,Z                 ,Z             ,diag(Wb) ]
                  ])

C12 =   diag(dRhobdr+Rhob/r) 
Cu  =   diag(gammab*P_r/r) 

Ce  =   np.block([
                 [Z,C12         ,Z,Z,Z],
                 [Z,  Z         ,Z,Z,Z],
                 [Z,diag(dWbdr) ,Z,Z,Z],
                 [Z,Cu          ,Z,Z,Z],
                 [Z,diag(dY1bdr),Z,Z,Z]
                 ])

D_u     =   diag(lambdab)+diag(2.0*mib)

De      =   np.block([
                     [Z,Z  ,Z        ,Z,Z],
                     [Z,D_u,Z        ,Z,Z],
                     [Z,Z  ,diag(mib),Z,Z],
                     [Z,Z  ,Z        ,Z,Z],
                     [Z,Z  ,Z        ,Z,Z]
                     ])

E_w     =   diag(lambdab)+diag(2.0*mib)

Ee      =   np.block([
                     [Z,Z        ,Z  ,Z,Z],
                     [Z,diag(mib),Z  ,Z,Z],
                     [Z,Z        ,E_w,Z,Z],
                     [Z,Z        ,Z  ,Z,Z],
                     [Z,Z        ,Z  ,Z,Z]
                     ])

F_u     =   diag(lambdab)+diag(mib)
F_w     =   diag(lambdab)+diag(mib)

Fe      =   np.block([
                     [Z,Z  ,Z  ,Z,Z],
                     [Z,Z  ,F_w,Z,Z],
                     [Z,F_u,Z  ,Z,Z],
                     [Z,Z  ,Z  ,Z,Z],
                     [Z,Z  ,Z  ,Z,Z]
                     ])

Gu_1    =   diag(2.0*lambdab/r)
Gu_4    =   diag(2.0*mib/r)
G_u     =   Gu_1+diag(2*dmibdr)+diag(dlambdabdr)+Gu_4 
Gw_2    =   diag(mib/r)
G_w     =   diag(dmibdr)+Gw_2 

#cuidado com este mas
#K_e     =   diag(mib*(gammab-1.0)*dWbdr) 
#tem que ser colocada na equacao de energia 

Ge      =   np.block([
                     [Z,Z  ,Z   ,Z,Z],
                     [Z,G_u,Z   ,Z,Z],
                     [Z,Z  ,G_w ,Z,Z],
                     [Z,Z  ,Z   ,Z,Z],
                     [Z,Z  ,Z   ,Z,Z]
                     ])

Hu_2    =   diag(mib/r)
Hu_3    =   diag(lambdab/r)
H_u     =   diag(dmibdr)+Hu_2+Hu_3
H_w     =   Hu_3+diag(dlambdabdr)

He      =   np.block([
                     [Z,Z  ,Z  ,Z,Z],
                     [Z,Z  ,H_w,Z,Z],
                     [Z,H_u,Z  ,Z,Z],
                     [Z,Z  ,Z  ,Z,Z],
                     [Z,Z  ,Z  ,Z,Z]
                     ])

Iu_1    =   diag(dlambdabdr*1.0/r)
I_u     =   Iu_1
Ie      =   np.block([
                     [Z,  Z,Z,Z,Z],
                     [Z,I_u,Z,Z,Z],
                     [Z,  Z,Z,Z,Z],
                     [Z,  Z,Z,Z,Z],
                     [Z,  Z,Z,Z,Z]
                     ])


#J      =   np.block([
#                    [Z,Z,Z,Z,Z],
#                    [Z,Z,Z,Z,Z],
#                    [Z,Z,Z,Z,Z],
#                    [Z,Z,Z,Z,Z],
#                    [Z,Z,Z,Z,Z],
#                    ])

#K      =   np.block([
#                    [Z,Z,Z,Z,Z],
#                    [Z,Z,Z,Z,Z],
#                    [Z,Z,Z,Z,Z],
#                    [Z,Z,Z,Z,Z],
#                    [Z,Z,Z,Z,Z],
#                    ])


Lw     =   diag(mib*dWbdr)

Le     =   np.block([
                    [Z,Z,  Z,Z,Z],
                    [Z,Z,  Z,Z,Z],
                    [Z,Z,  Z,Z,Z],
                    [Z,Z, Lw,Z,Z],
                    [Z,Z,  Z,Z,Z]
                    ])

Mu=Lw

Me      =   np.block([
                    [Z,Z  ,Z,Z,Z],
                    [Z,Z  ,Z,Z,Z],
                    [Z,Z  ,Z,Z,Z],
                    [Z,Mu ,Z,Z,Z],
                    [Z,Z  ,Z,Z,Z]
                    ])



Op      =   diag(kb/(Rhob*Rmix)) 

Oe      =   np.block([
                    [Z,Z,Z,Z ,Z],
                    [Z,Z,Z,Z ,Z],
                    [Z,Z,Z,Z ,Z],
                    [Z,Z,Z,Op,Z],
                    [Z,Z,Z,Z ,Z]
                    ])

########### Matrix  P = O
Pe      =  Oe 



#Energy

#Heat conduction
Qp1     =  diag(1.0/(Rhob*Rmix)*(dkbdr+kb/r))

#Energy, transport of energy
D2m=D1m

Espe    =  1.0/Le1*D1m*cpb1-1.0/Le2*D2m*cpb2

Qp2     =  diag(Espe*1.0/Rmix*dY1bdr)

Qy1     =  diag(Rhob*d1PRhoRbdr*Espe)

Qe      =   np.block([
                    [Z,Z,Z,Z      ,  Z],
                    [Z,Z,Z,Z      ,  Z],
                    [Z,Z,Z,Z      ,  Z],
                    [Z,Z,Z,Qp1+Qp2,Qy1],
                    [Z,Z,Z,Z      ,  Z]
                    ])

#1/Rhob=Tb
#heat condution 
Sp1     =   diag(kb*d2Tbdr)+diag((dkbdr+kb/r)*dTbdr)

#energy species transport 
Srho    =   diag(d1PRhoRbdr*Espe*dY1bdr)
Sp2     =   diag(Rhob*Espe*dY1bdr*d1PRhoRbdr)

Se      =   np.block([
                    [Z   ,Z,Z,Z      ,Z],
                    [Z   ,Z,Z,Z      ,Z],
                    [Z   ,Z,Z,Z      ,Z],
                    [Srho,Z,Z,Sp1+Sp2,Z],
                    [Z   ,Z,Z,      Z,Z] 
                    ])

Te      =   np.block([
                     [Z,Z,Z,Z,         Z],
                     [Z,Z,Z,Z,         Z],
                     [Z,Z,Z,Z,         Z],
                     [Z,Z,Z,Z,         Z],
                     [Z,Z,Z,Z, diag(D1m)] 
                     ])

Ue      =  Te 
            

Vrho     =   diag(D1m/Rhob*dY1bdr)

Vy1      =   diag(D1m/Rhob*dRhobdr+dD1mdr+D1m/r)

Ve      =   np.block([
                     [Z   ,Z,Z,Z,  Z],
                     [Z   ,Z,Z,Z,  Z],
                     [Z   ,Z,Z,Z,  Z],
                     [Z   ,Z,Z,Z,  Z],
                     [Vrho,Z,Z,Z,Vy1]
                     ])


Xrho    =   diag(D1m/Rhob*d2Y1bdr+1.0/Rhob*(dD1mdr+D1m/r)*dY1bdr)

Xe      =   np.block([
                     [Z   ,Z,Z,Z,Z],
                     [Z   ,Z,Z,Z,Z],
                     [Z   ,Z,Z,Z,Z],
                     [Z   ,Z,Z,Z,Z],
                     [Xrho,Z,Z,Z,Z] 
                     ])

#Lembrar nunca mais fazer isto deste jeito, e melhor 
#colocar todo mundo dentro, vai ficar mais facil 

A2_1    =   diag(1.0/(Reynolds)*1.0/Rhob)
A2_2    =   bk.Block(A2_1,Z)
A2e     =   matmul(A2_2,Ee)

A2_3    =   diag(gamma_j*(gammab-1.0)/Peclet)
A2_4    =   bk.Block(A2_3,Z)
A2p     =   matmul(A2_4,Pe)

A2u     =   np.multiply(1.0/(Le1*Peclet),Ue)

A2      =   A2e+A2p+A2u

A1_1    =   matmul(Fe,DD)+He
A1_2    =   diag(1.0/(Reynolds)*1.0/Rhob)
A1_3    =   bk.Block(A1_2,Z)
A1_4    =   matmul(A1_3,A1_1)

A1_5    =   diag((gammab-1.0)/Reynolds)
A1_6    =   bk.Block(A1_5,Z)
A1_7    =   matmul(A1_6,Me)

A1_8    =   Be-A1_4-A1_7
A1      =   np.multiply(1j,A1_8)


#In the loop
#A0_1    =  np.multiply((-1j*omega[iomega]),II)

A0_2    =  matmul(Ae,DD)

A0_3    =  Ce 

A0_41   =  matmul(De,DD2)+matmul(Ge,DD)+Ie
A0_42   =  diag(1.0/(Reynolds)*1.0/Rhob)
A0_43   =  bk.Block(A0_42,Z)
A0_4    =  matmul(A0_43,A0_41)

A0_51   =  matmul(Le,DD)
A0_52   =  diag((gammab-1)/(Reynolds))
A0_53   =  bk.Block(A0_52,Z)
A0_5    =  matmul(A0_53,A0_51)

A0_61   =  matmul(Oe,DD2)+matmul(Qe,DD)+Se
A0_62   =  diag(gamma_j*(gammab-1.0)/Peclet)
A0_63   =  bk.Block(A0_62,Z)
A0_6    =  matmul(A0_63,A0_61)


A0_71   =  matmul(Te,DD2)+matmul(Ve,DD)+Xe
A0_7    =  np.multiply(1.0/(Le1*Peclet),A0_71)

A0_8    = A0_2+A0_3-A0_4-A0_5-A0_6-A0_7


for iomega in range(0,iomega): 

    print(omega[iomega])

    g       =   open('./%s/autospec%di%d.dat'%(file1,N,iomega),'w+')

    g.write("%f \n"%(omega[iomega])); 


    A0_1    =   np.multiply((-1j*omega[iomega]),II)
    A0      =   A0_1+A0_8


    AA      =   np.block([[ZZ, II],[-A0,-A1]])
    BB      =   np.block([[II, ZZ],[ ZZ, A2]]) 

    #A0      =   np.multiply((1j*omega[iomega]),Ee)-matmul(Ae,DD)-Ce-De;
    #B0      =   np.multiply(1j,Be)
    
    
    #[0,N]=N+1 PONTOS
    # rho
    AA[0,:]               = Ov
    BB[0,:]               = Ov
    BB[0,0]               = 1 # A0[N,:]               = Ov 
    BB[N,:]               = Ov 
    BB[N,N]               = 1  
    
    #u
    AA[N+1,:]             = Ov 
    BB[N+1,:]             = Ov 
    BB[N+1,0]             = 1  
    
    AA[2*N,:]             = Ov
    BB[2*N,:]             = Ov
    BB[2*N,2*N]           = 1
    
    #v
    AA[2*N+1,:]           = Ov
    BB[2*N+1,:]           = Ov
    BB[2*N+1,N+1]         = 1
    
    AA[3*N,:]             = Ov
    BB[3*N,:]             = Ov
    BB[3*N,3*N ]          = 1
    
    #p
    AA[3*N+1,:]           = Ov
    BB[3*N+1,:]           = Ov
    BB[3*N+1,3*N+1]       = 1
    
    
    AA[4*N,:]             = Ov
    BB[4*N,:]             = Ov
    BB[4*N,4*N]           = 1

    #y1
    AA[4*N+1,:]           = Ov
    BB[4*N+1,:]           = Ov
    BB[4*N+1,4*N+1]       = 1
    
    
    AA[5*N,:]             = Ov
    BB[5*N,:]             = Ov
    BB[5*N,5*N]           = 1
   # #
    
    eigvals, eigvecs = eig(AA, BB) #

    S= list(filter(lambda x: np.abs(imag(x))<1.2 and 0.1< real(x) <5 , eigvals))
    #S= eigvals 

    Lamreal =   real(S)
    Lamimag =   imag(S)


    #Lr          =   Lamreal
    #Li          =   Lamimag

    #alphai1     =   np.max(Li)
    #pt          =   np.argmax(Li)
    #Li[pt]      =   0
    #alphai2     =   np.max(Li)

    #if alphai2<0.1: 
    #     
    #    alphai2 =alphai1
    #    

    #print(omega[iomega],alphai1,alphai2)

    #f.write("%f %f %f\n"%(omega[iomega],alphai1,alphai2)); 


    NN=np.shape(Lamreal)

    print(NN[0],'dd') 

    for kk in range(1,NN[0]): 

        g.write("%f %f\n"%(Lamreal[kk],Lamimag[kk])); 

        #Lreal[i][k]=Lamreal[i]
        #Limag[i][k]=Lamimag[i]

    #plt.figure(2)
    plt.plot(Lamreal,Lamimag,'.r')
    #plt.axis((0.0,5.0,-1.0,1.0))
    plt.grid()
    #plt.show()


#f.close
g.close

plt.show()

#     %%%% solve for eigenvalues
#disp('computing eigenvalues')
#tic; [U,S]=eig(B0,A0); toc
#
#S=diag(S);
#
#%plot(real(S),imag(S),'*'); hold on
#%xlimvec=[-3, 3]; ylimvec=[-2,0];
#
#for ind=1:length(S)
#            fprintf(arq1,'%f  %f \n',real(S(ind)),imag(S(ind)));
#

#increse virtual memory


#You could try creating the swap file from the terminal to temporarily increase virtual memory manually.
#
#Example:
#
#    sudo swapoff -a
#    sudo fallocate -l 5G /swapfile5g
#    sudo mkswap /swapfile5g
#    sudo chmod 600 /swapfile5g
#    sudo swapon /swapfile5g








