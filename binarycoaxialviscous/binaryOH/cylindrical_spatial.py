#matplotlib inline
#config InlineBackend.figure_format='svg'
from chebPy  import *
from mapping import *
from numpy   import matrix,matmul,dot,diag,real,imag,argsort,zeros,exp,log,linspace,polyval,polyfit,where,tan,tanh,cosh,identity
from scipy.linalg  import eig
from scipy.special import airy
#from matplotlib   import figure,subplot,plot,title,grid,show,axis
from scipy.linalg  import block_diag
import numpy as np 
import matplotlib.pyplot as plt 


f       =   open('trt01.dat','w+')

#number of collocation nodes=N+1, because the cheb func
N           =   300 


omegai      =   0.0
omegaf      =   2.1
iomega      =   30
domega      =   (omegaf-omegai)/iomega 

omega       =   zeros(iomega)
omega       =   np.arange(0,iomega,1)*domega

omega[0]     = omega[0]+0.01


D,n1        =    cheb(N); D2=dot(D,D); D2[1:N,1:N]

#Maximum r 
maxr        =   10
r           =    mappingtan(N,n1,maxr,D,D2)

#nn      =   np.arange(0,N+1,1)#0:N+1      

#plt.figure(1)
#plt.plot(nn,r,'.r')
#plt.axis((0.0,1.0,-2.0,2.0))
#plt.grid()
#plt.show()

Mc   = 0.50
#*********Thickness of the jet, base flow 
bba  = 0.25
#*********internal radio of the jet, base flow,solution morrismeanflow.f90 
hba  = 0.8071800
#*********Morris, The instability of hight speed jets 
#*********Reservoir temperature
TR   = 0.60
#*********Ambient temperature
T0   = 0.60
#*********Heat capacity relation 
gamma= 1.40

Wb  =   zeros(N+1)
Tb  =   zeros(N+1)
Rhob=   zeros(N+1)

for j in range (0,N+1):

    if(r[j]<hba):
        Wb[j]   = Mc  
    else: 
        Wb[j]   = Mc*exp(-log(2.0)*(r[j]-hba)**2.0/bba**2.0)  
    

for j in range (0,N+1):
    #Development Jet
    #wb[j]    = Mc*exp(-log(2.d0)*n[j]**2.d0/bba**2.d0)
    Tb[j]    = T0/TR*(1.0+(gamma-1.0)/2.0*Mc**2)*(1.0+(Wb[j])*(TR/T0-1.0))-(gamma-1.0)/2.0*Mc**2.0*(Wb[j]**2.0)
    Rhob[j]  = 1.0/(Tb[j])


dWbdr      =   dot(D,Wb)
dRhobdr    =   dot(D,Rhob)


#plt.figure(1)
#plt.plot(Wb,r,'*b')
#plt.plot(Rhob,r,'*b')
##plt.plot(dRhobdr,r,'*b')
##plt.axis((0.0,1.0,-0.0,4.0))
#plt.grid()
#plt.show()

# coordinate selectors
rr      =   np.arange(0,N,1)#0:N+1      
uu      =   rr+N
vv      =   uu+N
pp      =   vv+N


Z   =   zeros((N+1,N+1))
I   =   identity(N+1) 
#Cartesian
#A   =   np.block([[diag(M),diag(Rho),Z,Z],[Z,diag(M),Z,diag(1./Rho)],[Z,Z,diag(M),Z],[Z,I,Z,diag(M)]])
#B   =   np.block([[Z,Z,diag(Rho),Z],[Z,Z,Z,Z],[Z,Z,Z,diag(1./Rho)],[Z,Z,I,Z]])
#C   =   np.block([[Z,Z,diag(Rhoy),Z],[Z,Z,diag(My),Z],[Z,Z,Z,Z],[Z,Z,Z,Z]])
#Cylindrical
Ae   =   np.block([[Z,diag(Rhob),Z,Z],[Z,Z,Z,diag(1.0/Rhob)],[Z,Z,Z,Z],[Z,I,Z,Z]])
Be   =   np.block([[diag(Wb),Z,diag(Rhob),Z],[Z,diag(Wb),Z,Z],[Z,Z,diag(Wb),diag(1.0/Rhob)],[Z,Z,I,diag(Wb)]])
Ce   =   np.block([[Z,diag(Rhob),Z,Z],[Z,Z,Z,Z],[Z,Z,Z,Z],[Z,I,Z,Z]])
De   =   np.block([[Z,diag(dRhobdr),Z,Z],[Z,Z,Z,Z],[Z,diag(dWbdr),Z,Z],[Z,Z,Z,Z]])
Ee   =   block_diag(I,I,I,I)


for i in range(0,N+1): 

    for j in range(0,N+1): 

        Ce[i,j]   = Ce[i,j]/r[j] 

#A=np.array([[1, 1],[2 , 2]])
#B=np.array([[0, 0],[3 , 3]])
##C=np.block([[Z,Mdiag],[I, I]])
#n2=np.shape(C) 
#print('C', n2)
#print(C[1,1]) 

DD      =   block_diag(D,D,D,D);
II      =   identity(4*(N+1))
Ov      =   zeros((1,(4*(N+1))))


for iomega in range(0,iomega): 

    print(omega[iomega])

    A0      =   np.multiply((1j*omega[iomega]),Ee)-matmul(Ae,DD)-Ce-De;
    B0      =   np.multiply(1j,Be)
    
    
    #[0,N]=N+1 PONTOS
    # rho
    A0[0,:]               = Ov
    B0[0,:]               = Ov
    B0[0,0]               = 1 # A0[N,:]               = Ov 
    B0[N,:]               = Ov 
    B0[N,N]               = 1  
    
    #u
    A0[N+1,:]             = Ov 
    B0[N+1,:]             = Ov 
    B0[N+1,0]             = 1  
    
    A0[2*N,:]             = Ov
    B0[2*N,:]             = Ov
    B0[2*N,2*N]           = 1
    
    #v
    A0[2*N+1,:]           = Ov
    B0[2*N+1,:]           = Ov
    B0[2*N+1,N+1]         = 1
    
    A0[3*N,:]             = Ov
    B0[3*N,:]             = Ov
    B0[3*N,3*N ]          = 1
    
    #p
    A0[3*N+1,:]           = Ov
    B0[3*N+1,:]           = Ov
    B0[3*N+1,3*N+1]       = 1
    
    
    A0[4*N,:]             = Ov
    B0[4*N,:]             = Ov
    B0[4*N,4*N]           = 1
    #
    
    eigvals, eigvecs = eig(A0, B0) #

    S= list(filter(lambda x: np.abs(imag(x))<2 and 0.3< real(x) <5 , eigvals))
    #S= eigvals 

    Lamreal =   real(S)
    Lamimag =   imag(S)

    alphai  =   np.max(Lamimag)

    f.write("%f %f\n"%(omega[iomega],alphai)); 

    #Lamreal= list(filter(lambda x: 0.01<x<10, number_list))


    #plt.figure(2)
    plt.plot(Lamreal,Lamimag,'.r')
    #plt.axis((0.0,5.0,-1.0,1.0))
    plt.grid()


f.close

#plt.show()

#     %%%% solve for eigenvalues
#disp('computing eigenvalues')
#tic; [U,S]=eig(B0,A0); toc
#
#S=diag(S);
#
#%plot(real(S),imag(S),'*'); hold on
#%xlimvec=[-3, 3]; ylimvec=[-2,0];
#
#for ind=1:length(S)
#            fprintf(arq1,'%f  %f \n',real(S(ind)),imag(S(ind)));
#






