reset
#set term x11 0
#set term jpeg size 1000,800
set term postscript enhanced color #size 1000,800
set output 'taxasall.ps' 

set xrange[0:3.5] 
set yrange[0:0.5] 
set xlabel "{/Symbol w}"
set ylabel "{/Symbol a}"

plot    './Re500/taxaRe500.dat'       u 1:2 w p pt 1 lc rgb "blue" t 'Re=500 '  , \
        './Re500/taxaRe500.dat'       u 1:3 w p pt 1 lc rgb "blue" t ''         , \
        './Re1000/taxaRe1000.dat'     u 1:2 w p pt 2 lc rgb "red" t 'Re=1000' , \
        './Re1000/taxaRe1000.dat'     u 1:3 w p pt 2 lc rgb "red" t '' 	    , \
        './Re3000/taxaRe3000.dat'     u 1:2 w p pt 3 lc rgb "black" t 'Re=3000' , \
        './Re3000/taxaRe3000.dat'     u 1:3 w p pt 3 lc rgb "black" t ''        , \
        './Re10000/taxaRe10000.dat'   u 1:2 w p pt 4 lc rgb "green" t 'Re=10000', \
        './Re10000/taxaRe10000.dat'   u 1:3 w p pt 4 lc rgb "green" t ''        , \
        '../coaxial/taxacoaxial.dat'  u 1:2 w p t 'Euler'

#pause mouse keypress "Type a letter from A-F in the active window"
