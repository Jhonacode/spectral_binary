reset
#set term x11 0
#set term jpeg size 1000,800
set term postscript enhanced color #size 1000,800
set output 'taxa.ps' 

set xrange[0:3.5] 
set yrange[0:0.5] 
set xlabel "{/Symbol w}"
set ylabel "{/Symbol a}"

plot    './Re500/taxaRe500.dat'       u 1:2 w p pt 1 t 'Re= 500' , \
	'./Re1000/taxaRe1000.dat'     u 1:2 w p pt 2 t 'Re=1000' , \
        './Re3000/taxaRe3000.dat'     u 1:2 w p pt 3 t 'Re=3000' , \
        './Re10000/taxaRe10000.dat'   u 1:2 w p pt 4 t 'Re=10000', \
        '../coaxial/taxacoaxial.dat'  u 1:2 w p t 'Euler'

#pause mouse keypress "Type a letter from A-F in the active window"
