#! /usr/bin/python 
# Base flow to spectral methods 
# create by: jhonatan
# date: 11-05-2018
# Jets Parameters
# Paper: D Perrault-Joncas and S.A Maslowe,
#       "Linear Stability of compressible coaxial jet 
#        with continuos velocity and temperature profile "
#module for symbolic albegra 
import numpy as np
import math  as mt
#from math import exp, expm1
import matplotlib.pyplot as plt
#from sci to make fft
import scipy.fftpack
################################
# letura de aquivos para obter as transformadas
# number of frequency files 

def Baseflow(r):

    #number of spectral points
    N1      =   np.shape(r)

    N       =   N1[0]
    #radial step 
    dr      =   r[1]-r[0]
    
    #Inicialization of the base variables

    #Axial velocity profile
    Wb      =   np.zeros(N)

    #Temperature profile
    Tb      =   np.zeros(N)

    #Density profile
    Rhob    =   np.zeros(N)

    #Mass fraction profile
    Yb      =   np.zeros(N)

    M1b     =   np.zeros(N)

    M2b     =   np.zeros(N)

    M3b     =   np.zeros(N)

    T1b     =   np.zeros(N)

    T2b     =   np.zeros(N)

    T3b     =   np.zeros(N)

    Y1      =   np.zeros(N)

    Y2      =   np.zeros(N)

    Y3      =   np.zeros(N)

    #Jets Parameters
    #h      = U_secondary/U_primary
    h       =   0.7
    #Gamma  = R2/R1
    Gamma   =   2.0 
    
    #Radii Primary jet
    R1      =   1.0
    #Diameter Primary 
    D1      =   2.0*R1 
    #Momemtum thickness Primary 
    theta1  =   3.0/100.0*(D1+2.0/3.0*D1) 

    #Radii Secondary jet
    R2      =   R1*Gamma
    #Diameter Primary 
    D2      =   2.0*R2
    ##Momemtum thickness Secondary
    theta2  =   3.0/100.0*(D1+2.0/3.0*D2) 
    

    #Parameters bases flow
    b1      =   R1/(0.3)#(4.0*theta1)
    b2      =   R2/(0.3)#(4.0*theta2)

    #b1      =   R1/(4.0*theta1)
    #b2      =   R2/(4.0*theta2)
    
    M1      =   0.5#0.8

    M2      =   0.4#0.4 

    M3      =   0.3#0.2 

    T1      =   1.0#0.8

    T2      =   1.0 

    T3      =   1.0#0.2 

    #inner jet velocity
    M_r     =   (M1+M2)
    
    
    for j in range (0,N):
    
        #Primary Stream
        M1b[j]   =   0.5*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1))) 

        #Secondary  Stream
        M2b[j]   =   0.5*(1.0+np.tanh(b2*(R2/(r[j])-(r[j])/R2))) 

        #Coflow
        M3b[j]   =   M3*0.5*(1.0+np.tanh(b2*(R2/(-r[j])-(-r[j])/R2))) 

        T1b[j]   =   0.5*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1)))

        T2b[j]   =   0.5*(1.0+np.tanh(b2*(R2/r[j]-r[j]/R2)))

        T3b[j]   =   T3*0.5*(1.0+np.tanh(b2*(R2/(-r[j])-(-r[j])/R2)))
        
        #Primary Stream
        Y1[j]   =   1.0#*0.5*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1))) 
        #Secondary Stream
        Y2[j]   =   1-(Y1[j])


    #Dimensional R=0 temperature[K]
    T_r = 293.15

    
    for j in range (0,N):

        Wb[j]    =  (M1-M2)*M1b[j]+M2*M2b[j]+M3b[j] 
        Tb[j]    =  (T1-T2)*T1b[j]+T2*T2b[j]+T3b[j] 

	#nao pode ser caluclado aqui, preciso o R da mixtura para usar a equacao de gas ideal 
        #Rhob[j]  =  1.0/(Tb[j]) 

    #dWbdr   =   np.gradient(Wb, dr)

    #plt.figure()
    #plt.plot(Wb,r,'*-c')
    #plt.plot(Tb,r,'*-b')
    #plt.plot(Y1,r,'o-m')
    #plt.plot(Y2,r,'o-r')
    #plt.plot(Y3,r,'o-k')
    #plt.ylim((0,4))
    #plt.show()
    ##exit

    
    return Wb,Tb,Rhob,Y1,Y2,Y3,T_r,T1,T2,T3,M_r,R1,theta1 
