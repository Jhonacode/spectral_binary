#Program to read the files 
#of the eig program, compared 
#the eignevalues and make a graph 
#alphai omega

import numpy as np 
import matplotlib.pyplot as plt 

N0  =  25 

omegai      =   0.1
omegaf      =   3.1
iomega      =   30
domega      =   (omegaf-omegai)/iomega
omega       =   np.zeros(iomega)
omega       =   np.arange(0,iomega,1)*domega+omegai

file1       =   'Re10000'

alphai  =   np.zeros((100,100)) 

g       =   open('./%s/taxa%s.dat'%(file1,file1),'w+')

for i in range (0,N0):

    M1       =   np.loadtxt('./%s/autospec200i%d.dat'%(file1,i),unpack=True,skiprows=1) 
    M2       =   np.loadtxt('./%s/autospec250i%d.dat'%(file1,i),unpack=True,skiprows=1) 
    N11      =    M1.shape[1]
    N12      =    M1.shape[0]
    print(N11,N12)
    N21      =    M1.shape[1]
    N22      =    M1.shape[0]
    alphar1  =    M1[0:N11][0]
    alphai1  =   -M1[0:N11][1]
    alphar2  =    M2[0:N21][0]
    alphai2  =   -M2[0:N21][1]

    cont     =  0

    for j in range (0,N11):

        if (alphai1[j]>0.010):

            for k in range (0,N21):

                if (alphai2[k]>0.01 and (np.abs(alphai1[j]-alphai2[k])<0.005)):
                    
                     
                    alphai[i,cont]=alphai2[k]

                    cont    =   cont+1

                    #print(alphai2[k],alphai1[k])

                    #g.write("%f %f\n"%(omega[i],alphai[i,cont])); 

   # plt.plot(alphar1,alphai1,'*b')
   # plt.plot(alphar2,alphai2,'*r')

for j in range (0,N0):

    #g.write("%f\t%f\t%f\n"%(omega[i],alphai[i,1],alphai[i,2])); 
    g.write("%f\t%f\t%f\n"%(omega[j],alphai[j,0],alphai[j,1])); 

g.close

plt.plot(omega[0:N0],alphai[0:N0,0],'*-r')
plt.plot(omega[0:N0],alphai[0:N0,1],'-b')
plt.grid() 
plt.show()


    




