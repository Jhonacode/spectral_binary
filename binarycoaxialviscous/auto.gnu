reset

set yrange[-0.8:0]

pl './Re10000/autospec200i0.dat' u 1:2 w p pt 1,  \
   './Re10000/autospec200i1.dat' u 1:2 w p pt 1,  \
   './Re10000/autospec200i2.dat' u 1:2 w p pt 1,  \
   './Re10000/autospec200i3.dat' u 1:2 w p pt 1,  \
   './Re10000/autospec200i4.dat' u 1:2 w p pt 1,  \
   './Re10000/autospec200i5.dat' u 1:2 w p pt 1,  \
   './Re10000/autospec200i6.dat' u 1:2 w p
