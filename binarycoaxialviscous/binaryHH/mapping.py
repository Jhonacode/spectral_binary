from numpy import pi,tan,arange,ones,tile,dot,eye,diag,linspace,sqrt

def mappingtan(N,n,m,D1,D2):

    #Variable change 
    #Beta    = 0.5
    #concentrte more points in the temperature profile between 
    #[0.6 and 1,6], if continued decresing beta you will concentrare the point more to zero.IF Beta>3 desconcnetred the point for 
    #the important region.
    
    #r       = Beta*tan(pi/2.0*n)+0.00001      
    #                                  
    #deno    = 1.0 +(r/Beta)*(r/Beta)                      
    #nr      = 2.0/(Beta*pi)*(1./deno) 
    #nrr     = -4.0/(Beta**3.0*pi)*r*(1./(deno)**2) 
    
    #D1      = zeros(N,N)  
    #D2      = zeros(N,N)   


    #Cylindrical Coordinates
    #Paremeters of mapping
    c = 0.9
    #Max R
    #m = 20

    #radial coordinate
    r   = c * (1-n)/(1-n**2 + 2*c/m) + 1.e-6
#
    nr  = -1*(-c**2/(2*r**3) + c/r**2)/(2 *sqrt(1 + (2 *c)/m + c**2/(4 *r**2) - c/r)) - c/(2*r**2)
    nrr = (-c**2/(2 *r**3) + c/r**2)**2/(4 *(1 + (2 *c)/m + c**2/(4 *r**2) - c/r)**(3/2)) - ((3*c**2)/(2*r**4) - (2*c)/r**3)/(2 *sqrt(1 + (2 *c)/m + c**2/(4 *r**2) - c/r)) + c/r**3
#        
#
    
    for ind in range(1,N):

        D1[ind,:]  = D1[ind,:]*nr[ind]
        D2[ind,:]  = D2[ind,:]*  (nr[ind]**2.0)+D1[ind,:]*nrr[ind] 


    return r 

