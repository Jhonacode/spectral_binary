#! /usr/bin/python 
#fft of dns code to find the 
#spatial eigenvalues 
# create by: jhonatan
# date: 06-03-2018
#module for symbolic albegra 
import numpy as np
import math  as mt
#from math import exp, expm1
import matplotlib.pyplot as plt
#from sci to make fft
import scipy.fftpack
################################
# letura de aquivos para obter as transformadas
# number of frequency files 

N       =   220
nmax    =   4.2
n       =   np.linspace(0,nmax,N)
n[0]    =   0.0001
r       =   n
n_c     =   np.linspace(0,nmax,N)
n_p     =   np.linspace(0,nmax,N)
dr      =   r[1]-r[0]


Wb      =   np.zeros(N)
mini    =   np.zeros(N-1)
Tb      =   np.zeros(N)
Rhob    =   np.zeros(N)
u1      =   np.zeros(N)
u2      =   np.zeros(N)
u3      =   np.zeros(N)

#Jets Parameters
#Paper: D Perrault-Joncas and S.A Maslowe,
#       "Linear Stability of compressible coaxial jet 
#        with continuos velocity and temperature profile "

#h      =    U_secondary/U_primary
h       =   0.7 
#Radii Primary jet
R1      =   1.0
#Gamma  = R2/R1
Gamma   =   2.0 

#Momemtum thickness  
D1      =   2.0*R1 
#Momemtum thickness Primary 
theta1  =   3.0/100.0*(D1+2.0/3.0*D1) 
R2      =   R1*Gamma
D2      =   2.0*R2
##Momemtum thickness Secondary
theta2  =   3.0/100.0*(D1+2.0/3.0*D2) 

print(R1,theta1,R2,theta2)

b1      =   R1/0.3#(4.0*theta1)
b2      =   R2/0.3#(4.0*theta2)

M1      =   0.6
M2      =   0.3 
#h       =   M2/M1
M3      =   0.3 
#print('M2',M2)


for j in range (0,N):

    u1[j]   =   M1*0.5*(1.0+np.tanh(b1*(R1/r[j]-r[j]/R1))) 
    u2[j]   =   M2*0.5*(1.0+np.tanh(b2*(R2/r[j]-r[j]/R2))) 
    u3[j]   =   0.5*(M1+M3+(M1-M3)*np.tanh(b1*(R1/r[j]-r[j]/R1)))

    Wb[j]   = u3[j]+u2[j]


du1dr   =   np.gradient(u1, dr)
###return the  the min value
v1      =   np.amin(du1dr)
##return the indeces of the min value
pt1     =   np.argmin(du1dr)

print(r[pt1],u1[pt1])

du2dr   =   np.gradient(u2, dr)
v2      =   np.amin(du2dr)
##return the indeces of the min value
pt2     =   np.argmin(du2dr)
print(r[pt2],u2[pt2])


###return the  the min value
#v=np.amin(dWbdr[150:300])
###return the indeces of the min value
#pt=np.argmin(dWbdr)


#plt.figure()
#plt.plot(u1,r,'o-r')
#plt.plot((1.0-h)*u1+h*u2,r,'-*y')
#plt.plot(du1dr,r,'o-r')
#plt.plot(du2dr,r,'*b')

#uma solucao
#plt.plot(M2*u2,r,'*b')
#plt.plot(u3,r,'*m') 
#plt.plot(u3+(M2*u2),r,'*k') 
#plt.show()
#


#for j in range (0,N):
#    Wb[j]    = u3[j]+u2[j]
    #Tb[j]    = T1b*(Wb[j]-u2b)/(u1b-u2b)+T2b*(u1b-Wb[j])/(u1b-u2b)+(gamma-1.0)/2.0*(u1b-Wb[j])*(Wb[j]-u2b)
    #Rhob[j]  = 1.0/(Tb[j]) 

plt.plot(Wb,r,'*r')
plt.show()
##
#
#dWbdr   =   np.gradient(Wb, dr)
#plt.plot(dWbdr,r,'o-r')


##return the  the min value
#3for j in range(1:N):
#3    if(r[j]<1.5):
#3        v   =   np.amin(dWbdr[0:150])
#3        pt  =   np.argmin(dWbdr)
#3        print('Ponto de inflexao 1',n[pt],v)
#3    else
#3    ##return the indeces of the min value


###return the  the min value
#v=np.amin(dWbdr[150:300])
###return the indeces of the min value
#pt=np.argmin(dWbdr)


#plt.figure()
#plt.plot(Wb_c,n_c,'o-r')
#plt.plot(Wb,n,'*b')
#plt.plot(dWbdn,n_c,'*b')
#plt.show()


#plt.figure()
#plt.plot(wb,n,'.r')
#plt.axis((-0.2,0.7, 0, 3))
#plt.grid()
#plt.figure()
#plt.plot(dwdn,n,'-b')
#plt.axis((-2, 1, 0, 4))
#plt.show()
